//--------------------------------------------------------------------
//
//                              GET
//
//--------------------------------------------------------------------

curl 'https://indramahkotapecahan.firebaseio.com/pengguna.json'

curl 'https://indramahkotapecahan.firebaseio.com/nilai.json'

curl 'https://indramahkotapecahan.firebaseio.com/gayabelajar.json'

//--------------------------------------------------------------------
//
//                              POST
//
//--------------------------------------------------------------------

curl -X POST -d '{
    "auditorial": "kosong",
    "avatarPengguna": "avatar_1s",
    "dominan": "kosong",
    "indeksAvatar": "0",
    "katasandi": "indra",
    "kinestetik": "kosong",
    "namaLengkap": "Indra Mahkota",
    "namaPengguna": "indramahkota",
    "nilai": "kosong",
    "status": "aktif",
    "visual": "kosong",
    "waktu": "2018-09-14 01:27:31"
}' \
'https://indramahkotapecahan.firebaseio.com/pengguna.json'

curl -X POST -d '{
    "namaPengguna": "indramahkota",
    "auditorial": "kosong",
    "dominan": "kosong",
    "kinestetik": "kosong",
    "visual": "kosong",
    "waktu": "2018-09-14 01:27:31"
}' \
'https://indramahkotapecahan.firebaseio.com/gayabelajar.json'

curl -X POST -d '{
    "namaPengguna": "indramahkota",
    "nilai": "kosong",
    "waktu": "2018-09-14 01:27:31"
}' \
'https://indramahkotapecahan.firebaseio.com/nilai.json'

{
  "pendahuluantut1": "sudah",
  "menuutamatut0": "sudah",
  "menuutamatut1": "sudah",
  "menuutamatut2": "sudah",
  "menuutamatut3": "sudah",
  "menuutamatut4": "sudah",
  "menuutamatut5": "sudah",
  "menumateritut1": "sudah",
  "menumateritut2": "sudah",
  "menumateritut3": "sudah",
  "menumateritut0": "sudah",
  "menuutamatut6": "sudah"
}

curl -X POST -d '{
    "pendahuluantut1": "sudah",
    "menuutamatut0": "sudah",
    "menuutamatut1": "sudah",
    "menuutamatut2": "sudah",
    "menuutamatut3": "sudah",
    "menuutamatut4": "sudah",
    "menuutamatut5": "sudah",
    "menumateritut1": "sudah",
    "menumateritut2": "sudah",
    "menumateritut3": "sudah",
    "menumateritut0": "sudah",
    "menuutamatut6": "sudah"
}' \
'https://indramahkotapecahan.firebaseio.com/pengguna/-LTpFJnWUk2UnpC5p-wf.json'

//--------------------------------------------------------------------
//
//                              PATCH
//
//--------------------------------------------------------------------

curl -X PATCH -d '{
    "namaPengguna": "indramahkota",
    "nilai": "100",
    "waktu": "2018-09-14 01:27:31"
}' \
'https://indramahkotapecahan.firebaseio.com/nilai/-LTotAcqq-o3jhQkuGhP.json'


curl -X PATCH -d '{
    "auditorial": "kosong",
    "avatarPengguna": "avatar_2s",
    "dominan": "kosong",
    "indeksAvatar": "1",
    "kataSandi": "indra",
    "kinestetik": "kosong",
    "namaLengkap": "Indra Mahkota",
    "namaPengguna": "indramahkota",
    "nilai": "100",
    "status": "aktif",
    "visual": "kosong",
    "waktu": "Sun Dec 16 09:42:26 GMT+0700 2018"
}' \
'https://indramahkotapecahan.firebaseio.com/pengguna/-LTotAcqq-o3jhQkuGhP.json'

curl -X PATCH -d '{
    "auditorial": "kosong",
    "dominan": "kosong",
    "kinestetik": "kosong",
    "visual": "kosong"
}' \
'https://indramahkotapecahan.firebaseio.com/pengguna/-LTotAcqq-o3jhQkuGhP.json'

curl -X PATCH -d '{
  "sharedObject": {
    "menumateritut0": "sudah",
    "menumateritut1": "sudah",
    "menumateritut2": "sudah",
    "menumateritut3": "sudah",
    "menuutamatut0": "sudah",
    "menuutamatut1": "sudah",
    "menuutamatut2": "sudah",
    "menuutamatut3": "sudah",
    "menuutamatut4": "sudah",
    "menuutamatut5": "sudah",
    "menuutamatut6": "sudah",
    "pendahuluantut1": "sudah"
  },
  "auditorial": "50.00",
  "avatarPengguna": "avatar_8s",
  "dominan": "visual",
  "indeksAvatar": "7",
  "kataSandi": "alice",
  "kinestetik": "33.33",
  "namaLengkap": "Alice Kawaii",
  "namaPengguna": "alice",
  "nilai": "33,33",
  "status": "aktif",
  "visual": "71.43",
  "waktu": "Sun Dec 16 11:23:33 GMT+0700 2018"
}' \
'https://indramahkotapecahan.firebaseio.com/pengguna/-LTqgd3_vSslmlj-XHs3.json'


//--------------------------------------------------------------------
//
//                              DELETE
//
//--------------------------------------------------------------------

curl -X DELETE \
'https://indramahkotapecahan.firebaseio.com/nilai/-LTlWh-skTm3wRQbRcrF/nilai.json'
