package renderers
{
	import constants.EventType;

	import feathers.controls.Button;
	import feathers.controls.ButtonState;
	import feathers.controls.Label;
	import feathers.controls.LayoutGroup;
	import feathers.controls.Radio;
	import feathers.controls.renderers.LayoutGroupListItemRenderer;
	import feathers.controls.text.TextBlockTextRenderer;
	import feathers.core.ITextRenderer;
	import feathers.core.ToggleGroup;
	import feathers.layout.AnchorLayout;
	import feathers.layout.AnchorLayoutData;
	import feathers.layout.HorizontalAlign;
	import feathers.layout.VerticalLayout;
	import feathers.layout.VerticalLayoutData;
	import feathers.skins.ImageSkin;

	import flash.geom.Rectangle;
	import flash.text.engine.ElementFormat;
	import flash.text.engine.FontDescription;
	import flash.text.engine.FontLookup;
	import flash.text.engine.FontPosture;
	import flash.text.engine.FontWeight;

	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.filters.DropShadowFilter;
	import starling.textures.RenderTexture;
	import starling.textures.Texture;
	import starling.utils.Color;

	import utils.RoundedQuad;
	
	/**
	 * ...
	 * @author Indra Mahkota
	 */
	public class TesRenderer extends LayoutGroupListItemRenderer
	{
		protected var innerContent:LayoutGroup;
		
		protected var label:Label;
		protected var radio_satu:Radio;
		protected var radio_dua:Radio;
		protected var radio_tiga:Radio;
		protected var radio_empat:Radio;
		protected var group:ToggleGroup;
		
		protected var upTextureBtn:Texture;
		protected var downTextureBtn:Texture;
		protected var radioUpTexture:Texture;
		protected var radioDownTexture:Texture;
		protected var radioSelectedUpTexture:Texture;
		protected var radioSelectedDownTexture:Texture;

		protected var button:Button;
		protected static var fixedHeight:Number;
		
		public function TesRenderer()
		{
			super();
		}
		
		override protected function initialize():void
		{
			this.layout = new VerticalLayout();
			this.layoutData = new VerticalLayoutData(100, NaN);
			
			fixedHeight = stage.stageHeight - 75;
			this.height = fixedHeight;

			radioUpTexture = createRadioUpTexture();
			radioDownTexture = createRadioDownTexture()
			radioSelectedUpTexture = createRadioSelectedUpTexture();
			radioSelectedDownTexture = createRadioSelectedDownTexture();

			var icon1:ImageSkin = new ImageSkin(radioUpTexture);
			icon1.selectedTexture = radioSelectedUpTexture;
			icon1.setTextureForState(ButtonState.DOWN, radioDownTexture);
			icon1.setTextureForState(ButtonState.DOWN_AND_SELECTED, radioSelectedDownTexture);
			icon1.pixelSnapping = true;

			var icon2:ImageSkin = new ImageSkin(radioUpTexture);
			icon2.selectedTexture = radioSelectedUpTexture;
			icon2.setTextureForState(ButtonState.DOWN, radioDownTexture);
			icon2.setTextureForState(ButtonState.DOWN_AND_SELECTED, radioSelectedDownTexture);
			icon2.pixelSnapping = true;

			var icon3:ImageSkin = new ImageSkin(radioUpTexture);
			icon3.selectedTexture = radioSelectedUpTexture;
			icon3.setTextureForState(ButtonState.DOWN, radioDownTexture);
			icon3.setTextureForState(ButtonState.DOWN_AND_SELECTED, radioSelectedDownTexture);
			icon3.pixelSnapping = true;

			var icon4:ImageSkin = new ImageSkin(radioUpTexture);
			icon4.selectedTexture = radioSelectedUpTexture;
			icon4.setTextureForState(ButtonState.DOWN, radioDownTexture);
			icon4.setTextureForState(ButtonState.DOWN_AND_SELECTED, radioSelectedDownTexture);
			icon4.pixelSnapping = true;

			var layoutTesRenderer:VerticalLayout = new VerticalLayout();
			layoutTesRenderer.paddingLeft = layoutTesRenderer.paddingRight = layoutTesRenderer.paddingTop = 30;
			layoutTesRenderer.paddingBottom = 40;
			layoutTesRenderer.gap = 10;
			
			var image:Image = new Image(Main.roundQuadBackground);
			image.pixelSnapping = true;
			image.scale9Grid = new Rectangle(20, 20, 80, 80);
			
			innerContent = new LayoutGroup();
			innerContent.backgroundSkin = image;
			innerContent.layout = layoutTesRenderer;
			innerContent.layoutData = new VerticalLayoutData(100, NaN);
			this.addChild(innerContent);
			
			label = new Label();
			label.paddingBottom = 20;
			label.styleNameList.add("title-label");
			label.layoutData = new VerticalLayoutData(100, NaN);
			label.textRendererProperties.wordWrap = true;
			innerContent.addChild(label);
			
			group = new ToggleGroup();
			group.selectedIndex = -1;
			
			radio_satu = new Radio();
			radio_satu.defaultIcon = icon1;
			radio_satu.label = "Sangat Sering";
			radio_satu.layoutData = new VerticalLayoutData(100, NaN);
			radio_satu.toggleGroup = group;
			radio_satu.gap = 5;
			radio_satu.horizontalAlign = HorizontalAlign.LEFT;
			
			radio_satu.labelFactory = function():ITextRenderer
			{
				var renderer:TextBlockTextRenderer = new TextBlockTextRenderer();
				
				var fontDesc:FontDescription = new FontDescription("SourceSansPro", FontWeight.NORMAL, FontPosture.NORMAL);
				fontDesc.fontLookup = FontLookup.EMBEDDED_CFF;
			
				renderer.elementFormat = new ElementFormat(fontDesc, 18, Color.BLACK);
				return renderer;
			};
			radio_satu.addEventListener(Event.TRIGGERED, function():void
			{
				Main.klik();
			});
			innerContent.addChild(radio_satu);
			
			radio_dua = new Radio();
			radio_dua.defaultIcon = icon2;
			radio_dua.label = "Sering";
			radio_dua.layoutData = new VerticalLayoutData(100, NaN);
			radio_dua.toggleGroup = group;
			radio_dua.gap = 5;
			radio_dua.horizontalAlign = HorizontalAlign.LEFT;
			
			radio_dua.labelFactory = function():ITextRenderer
			{
				var renderer:TextBlockTextRenderer = new TextBlockTextRenderer();
				
				var fontDesc:FontDescription = new FontDescription("SourceSansPro", FontWeight.NORMAL, FontPosture.NORMAL);
				fontDesc.fontLookup = FontLookup.EMBEDDED_CFF;
			
				renderer.elementFormat = new ElementFormat(fontDesc, 18, Color.BLACK);
				return renderer;
			};
			radio_dua.addEventListener(Event.TRIGGERED, function():void
			{
				Main.klik();
			});
			innerContent.addChild(radio_dua);
			
			radio_tiga = new Radio();
			radio_tiga.defaultIcon = icon3;
			radio_tiga.label = "Jarang";
			radio_tiga.layoutData = new VerticalLayoutData(100, NaN);
			radio_tiga.toggleGroup = group;
			radio_tiga.gap = 5;
			radio_tiga.horizontalAlign = HorizontalAlign.LEFT;
			
			radio_tiga.labelFactory = function():ITextRenderer
			{
				var renderer:TextBlockTextRenderer = new TextBlockTextRenderer();
				
				var fontDesc:FontDescription = new FontDescription("SourceSansPro", FontWeight.NORMAL, FontPosture.NORMAL);
				fontDesc.fontLookup = FontLookup.EMBEDDED_CFF;
			
				renderer.elementFormat = new ElementFormat(fontDesc, 18, Color.BLACK);
				return renderer;
			};
			radio_tiga.addEventListener(Event.TRIGGERED, function():void
			{
				Main.klik();
			});
			innerContent.addChild(radio_tiga);
			
			radio_empat = new Radio();
			radio_empat.styleProvider = null;
			radio_empat.defaultIcon = icon4;
			radio_empat.label = "Tidak Pernah";
			radio_empat.layoutData = new VerticalLayoutData(100, NaN);
			radio_empat.toggleGroup = group;
			radio_empat.gap = 5;
			radio_empat.horizontalAlign = HorizontalAlign.LEFT;
			
			radio_empat.labelFactory = function():ITextRenderer
			{
				var renderer:TextBlockTextRenderer = new TextBlockTextRenderer();
				
				var fontDesc:FontDescription = new FontDescription("SourceSansPro", FontWeight.NORMAL, FontPosture.NORMAL);
				fontDesc.fontLookup = FontLookup.EMBEDDED_CFF;
			
				renderer.elementFormat = new ElementFormat(fontDesc, 18, Color.BLACK);
				return renderer;
			};
			radio_empat.addEventListener(Event.TRIGGERED, function():void
			{
				Main.klik();
			});
			innerContent.addChild(radio_empat);
			
			var layoutTombol:LayoutGroup = new LayoutGroup();
			layoutTombol.layout = new AnchorLayout();
			layoutTombol.layoutData = new VerticalLayoutData(100, NaN);
			innerContent.addChild(layoutTombol);

			upTextureBtn = createlightupBackground();
			downTextureBtn = createlightdownBackground();

			var lightSkin:ImageSkin = new ImageSkin(upTextureBtn);
			lightSkin.setTextureForState(ButtonState.DOWN, downTextureBtn);
			lightSkin.scale9Grid = new Rectangle(20, 20, 80, 80);
			
			button = new Button();
			button.label = "Selanjutnya";
			button.height = 65;
			button.paddingLeft = 30;
			button.paddingRight = 30;
			button.defaultSkin = lightSkin;
			button.addEventListener(Event.TRIGGERED, Button_triggeredHandler);
			button.layoutData = new AnchorLayoutData(20, -10, 0, NaN, NaN, NaN);
			button.styleNameList.add("light-button");
			layoutTombol.addChild(button);
			
			super.initialize();
		}
		
		override protected function commitData():void
		{
			if(this.data && this.owner)
			{
				this.alpha = 1;
				label.text = this.data.urutan + ". " + this.data.soal;
				group.selectedIndex = -1;
			}
			else
			{
				label.text = "";
				group.selectedIndex = -1;
			}
		}
		
		override protected function postLayout():void
		{
			button.isEnabled = true;
		}
		
		override public function dispose():void
		{
			if(group)
			{
				group.removeAllItems();
				group = null;
			}

			upTextureBtn.dispose();
			downTextureBtn.dispose();
			radioUpTexture.dispose();
			radioDownTexture.dispose();
			radioSelectedUpTexture.dispose();
			radioSelectedDownTexture.dispose();

			upTextureBtn = null;
			downTextureBtn = null;
			radioUpTexture = null;
			radioDownTexture = null;
			radioSelectedUpTexture = null;
			radioSelectedDownTexture = null;
			
			super.dispose();
		}

		private function createRadioUpTexture():RenderTexture
		{
			var spr:Sprite = new Sprite();
			var rq1:RoundedQuad = new RoundedQuad(5, 25, 25, 0x211b1f);
			spr.addChild(rq1);

			var bnd:Rectangle = spr.bounds;
			var rendertxture:RenderTexture = new RenderTexture(bnd.width, bnd.height);
			rendertxture.draw(spr);
			return rendertxture;
		}

		private function createRadioDownTexture():RenderTexture
		{
			var spr:Sprite = new Sprite();
			var rq1:RoundedQuad = new RoundedQuad(5, 25, 25, 0x211b1f);
			var rq2:RoundedQuad = new RoundedQuad(5, 21, 21, 0xd34836);
			rq2.x = rq2.y = 2;
			spr.addChild(rq1);
			spr.addChild(rq2);

			var bnd:Rectangle = spr.bounds;
			var rendertxture:RenderTexture = new RenderTexture(bnd.width, bnd.height);
			rendertxture.draw(spr);
			return rendertxture;
		}

		private function createRadioSelectedUpTexture():RenderTexture
		{
			var spr:Sprite = new Sprite();
			var rq1:RoundedQuad = new RoundedQuad(5, 25, 25, 0x211b1f);
			var rq3:RoundedQuad = new RoundedQuad(7, 14, 14, 0xd34836);
			rq3.x = rq3.y = ((25 - 14) / 2);
			spr.addChild(rq1);
			spr.addChild(rq3);
			
			var bnd:Rectangle = spr.bounds;
			var rendertxture:RenderTexture = new RenderTexture(bnd.width, bnd.height);
			rendertxture.draw(spr);
			return rendertxture;
		}

		private function createRadioSelectedDownTexture():RenderTexture
		{
			var spr:Sprite = new Sprite();
			var rq1:RoundedQuad = new RoundedQuad(5, 25, 25, 0x211b1f);
			var rq2:RoundedQuad = new RoundedQuad(5, 21, 21, 0xd34836);
			rq2.x = rq2.y = 2;
			var rq3:RoundedQuad = new RoundedQuad(7, 14, 14, 0x211b1f);
			rq3.x = rq3.y = ((25 - 14) / 2);
			spr.addChild(rq1);
			spr.addChild(rq2);
			spr.addChild(rq3);
			
			var bnd:Rectangle = spr.bounds;
			var rendertxture:RenderTexture = new RenderTexture(bnd.width, bnd.height);
			rendertxture.draw(spr);
			return rendertxture;
		}
		
		private function Button_triggeredHandler(event:Event):void
		{
			Main.klik();
			
			if(group.selectedIndex != -1)
			{
				button.isEnabled = false;
				var obj:Object = new Object();
				obj.no = data.no;
				obj.urutan = data.urutan;
				obj.pilihan = group.selectedIndex;
				obj.jenis = data.jenis;
				obj.tipe = data.tipe;
				
				var bubblingEvent:Event = new Event(EventType.PENYIMPANAN, true, obj);
				dispatchEvent(bubblingEvent);
				disposeRenderer();
			}
		}
		
		protected function disposeRenderer():void
		{
			var tween:Tween = new Tween(this, 0.3);
			tween.fadeTo(0);
			tween.onComplete = function():void
			{
				if(owner.dataProvider.length <= 1)
				{
					var bubblingEvent:Event = new Event(EventType.SELESAI, true);
					dispatchEvent(bubblingEvent);
					bubblingEvent.stopImmediatePropagation();
				}
				else
				{
					owner.dataProvider.removeItemAt(owner.dataProvider.getItemIndex(data));
				}
				Starling.juggler.remove(tween);
			};
			Starling.juggler.add(tween);
		}

		private function createlightdownBackground():Texture
		{
			var downdBg:RoundedQuad = new RoundedQuad(5, 100, 100, 0xd34836);
			downdBg.filter = new DropShadowFilter(4, Math.PI / 2, 0, 0.25, 2, 1);
			downdBg.x = downdBg.y = 10;

			var downuBg:RoundedQuad = new RoundedQuad(5, 96, 96, 0x211b1f);
			downuBg.x = downuBg.y = 12;

			var spr:Sprite = new Sprite();
			spr.addChild(downdBg);
			spr.addChild(downuBg);
			
			var bnd:Rectangle = spr.bounds;
			var rendertxture:RenderTexture = new RenderTexture(bnd.width + 20, bnd.height + 20);
			rendertxture.draw(spr);
			return rendertxture;
		}

		private function createlightupBackground():Texture
		{
			var downdBg:RoundedQuad = new RoundedQuad(5, 100, 100, 0xffffff);
			downdBg.x = downdBg.y = 10;

			var downuBg:RoundedQuad = new RoundedQuad(5, 96, 96, 0x211b1f);
			downuBg.x = downuBg.y = 12;

			var spr:Sprite = new Sprite();
			spr.addChild(downdBg);
			spr.addChild(downuBg);
			
			var bnd:Rectangle = spr.bounds;
			var rendertxture:RenderTexture = new RenderTexture(bnd.width + 20, bnd.height + 20);
			rendertxture.draw(spr);
			return rendertxture;
		}
	}
}