package renderers
{
	import constants.EventType;

	import feathers.controls.Button;
	import feathers.controls.ButtonGroup;
	import feathers.controls.ButtonState;
	import feathers.controls.ImageLoader;
	import feathers.controls.LayoutGroup;
	import feathers.controls.renderers.LayoutGroupListItemRenderer;
	import feathers.data.ListCollection;
	import feathers.layout.AnchorLayout;
	import feathers.layout.Direction;
	import feathers.layout.VerticalLayout;
	import feathers.layout.VerticalLayoutData;
	import feathers.skins.ImageSkin;

	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.setTimeout;

	import starling.animation.Transitions;
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.filters.DropShadowFilter;
	import starling.textures.RenderTexture;
	import starling.textures.SubTexture;
	import starling.textures.Texture;

	import utils.Animations;
	import utils.CreateImage;
	import utils.RoundedQuad;
	
	/**
	 * ...
	 * @author Indra Mahkota
	 */
	public class EvaluasiRenderer extends LayoutGroupListItemRenderer
	{
		private var soal:Image;
		private var jawabanA:Image;
		private var jawabanB:Image;
		private var jawabanC:Image;
		private var jawabanD:Image;
		
		private var buttonGroup:ButtonGroup;
		private var innerContent:LayoutGroup;
		private var infoLayoutGroup:LayoutGroup;
		
		private var benar:int = 0;
		private var salah:int = 0;
		
		private var benarimg:ImageLoader;
		private var salahimg:ImageLoader;

		private var upTextureBtn:Texture;
		private var downTextureBtn:Texture;

		private var botomRoundedRect:SubTexture;
		private var bottomRect:Rectangle = new Rectangle(0, 60, 120, 60);
		
		protected static var fixedHeight:Number;
		
		public function EvaluasiRenderer()
		{
			super();
		}
		
		override protected function initialize():void
		{
			this.layout = new AnchorLayout();
			this.layoutData = new VerticalLayoutData(100, NaN);
			
			fixedHeight = stage.stageHeight - 75;
			this.height = fixedHeight;

			botomRoundedRect = new SubTexture(Main.roundQuadBackground, bottomRect);
			
			super.initialize();
		}
		
		override protected function commitData():void
		{
			if(this.data && this.owner)
			{
				if(this.data.id == undefined || this.data.id == null)
				{
					return;
				}
				
				this.alpha = 1;
				
				benarimg = new ImageLoader();
				salahimg = new ImageLoader();
				
				var layoutTesRenderer:VerticalLayout = new VerticalLayout();
				layoutTesRenderer.paddingLeft = 30;
				layoutTesRenderer.paddingRight = 30;
				layoutTesRenderer.paddingTop = 5;
				layoutTesRenderer.paddingBottom = 40;
				layoutTesRenderer.gap = 10;
				layoutTesRenderer.lastGap = 20;
				
				var image:Image = new Image( botomRoundedRect );
				image.pixelSnapping = true;
				image.scale9Grid = new Rectangle(20, 10, 80, 30);
				
				innerContent = new LayoutGroup();
				innerContent.backgroundSkin = image;
				innerContent.layout = layoutTesRenderer;
				innerContent.y = 61;
				innerContent.width = stage.stageWidth;
				this.addChild(innerContent);
				
				infoLayoutGroup = new LayoutGroup();
				infoLayoutGroup.layout = new AnchorLayout();
				infoLayoutGroup.layoutData = new VerticalLayoutData(100, NaN);
				
				innerContent.addChild(infoLayoutGroup);
				
				soal = new CreateImage(this.data.soal, stage.stageWidth - 64);
				innerContent.addChild(soal);
				
				jawabanA = new CreateImage("A. " + this.data.a, stage.stageWidth - 64);
				innerContent.addChild(jawabanA);
				
				jawabanB = new CreateImage("B. " + this.data.b, stage.stageWidth - 64);
				innerContent.addChild(jawabanB);
				
				jawabanC = new CreateImage("C. " + this.data.c, stage.stageWidth - 64);
				innerContent.addChild(jawabanC);
				
				jawabanD = new CreateImage("D. " + this.data.d, stage.stageWidth - 64);
				innerContent.addChild(jawabanD);

				upTextureBtn = createlightupBackground();
				downTextureBtn = createlightdownBackground();
				
				buttonGroup = new ButtonGroup();
				buttonGroup.height = 70;
				buttonGroup.direction = Direction.HORIZONTAL;
				buttonGroup.buttonFactory = function():Button
				{
					var lightSkin:ImageSkin = new ImageSkin(upTextureBtn);
					lightSkin.setTextureForState(ButtonState.DOWN, downTextureBtn);
					lightSkin.scale9Grid = new Rectangle(20, 20, 80, 80);

					var button:Button = new Button();
					button.defaultSkin = lightSkin;
					button.styleNameList.add("light-button");
					return button;
				};

				buttonGroup.layoutData = new VerticalLayoutData(100, NaN);
				
				buttonGroup.dataProvider = new ListCollection([{label: "A", triggered: Button_triggeredHandler}, {label: "B", triggered: Button_triggeredHandler}, {label: "C", triggered: Button_triggeredHandler}, {label: "D", triggered: Button_triggeredHandler}]);
				
				innerContent.addChild(buttonGroup);
			}
			else
			{
				trace("[Renderer Data] : null");
			}
		}
		
		override protected function postLayout():void
		{
			if(buttonGroup)
			{
				buttonGroup.isEnabled = true;
			}
		}
		
		override public function dispose():void
		{
			if(soal != null)
			{
				soal.texture.dispose();
				soal.dispose();
				soal = null;
			}

			if(jawabanA != null)
			{
				jawabanA.texture.dispose();
				jawabanA.dispose();
				jawabanA = null;
			}

			if(jawabanB != null)
			{
				jawabanB.texture.dispose();
				jawabanB.dispose();
				jawabanB = null;
			}

			if(jawabanC != null)
			{
				jawabanC.texture.dispose();
				jawabanC.dispose();
				jawabanC = null;
			}

			if(jawabanD != null)
			{
				jawabanD.texture.dispose();
				jawabanD.dispose();
				jawabanD = null;
			}

			if(downTextureBtn != null)
			{
				downTextureBtn.dispose();
				downTextureBtn = null;
			}

			if(upTextureBtn != null)
			{
				upTextureBtn.dispose();
				upTextureBtn = null;
			}

			super.dispose();
		}
		
		private function Button_triggeredHandler(event:Event):void
		{
			Main.klik();
			
			buttonGroup.isEnabled = false;
			buttonGroup.touchable = false;
			
			var button:Button = Button(event.currentTarget);
			button.isEnabled = false;
			button.touchable = false;
			
			var posisi:Point = new Point();
			posisi = tentukanposisi(button.label);
			trace("[Position] x: " + posisi.x + " y: " + posisi.y);
			
			var obj:Object = new Object();
			obj.urutan = data.urutan;
			obj.pilihan = button.label;
			obj.jawaban = data.jawaban;
			
			var str:String;
			
			if(button.label == data.jawaban)
			{
				obj.nilai = "1";
				obj.icon = Main.getImage("betul");
				str = "benar";
				
				benarimg.x = posisi.x;
				benarimg.y = posisi.y - 75;
				benarimg.source = Main.getImage("betul24dp");
				benarimg.setSize(36, 36);
				this.addChild(benarimg);
			}
			else
			{
				obj.nilai = "0";
				obj.icon = Main.getImage("salah");
				str = "salah";
				
				salahimg.x = posisi.x - 5;
				salahimg.y = posisi.y - 75;
				salahimg.source = Main.getImage("salah24dp");
				salahimg.setSize(36, 36);
				this.addChild(salahimg);
				
				if(Transitions.getTransition(Animations.SHAKE_3X) == null)
				{
					Animations.registerTransitions();
				}
				
				var getar:Tween = new Tween(this, 0.3);
				getar.transition = Animations.SHAKE_3X;
				getar.animate("x", this.x + 5);
				getar.onComplete = function():void
				{
					Starling.juggler.remove(getar);
				}
				Starling.juggler.add(getar);
			}
			
			obj.keterangan = str;
			
			var bubblingEvent:Event = new Event(EventType.PENYIMPANAN, true, obj);
			dispatchEvent(bubblingEvent);
			
			setTimeout(disposeRenderer, 500);
		}
		
		private function tentukanposisi(str:String):Point
		{
			var posisi:Point = new Point();
			
			switch (str)
			{
				case "A":
					posisi = jawabanA.localToGlobal(new Point(0, 0));
					break;
				case "B":
					posisi = jawabanB.localToGlobal(new Point(0, 0));
					break;
				case "C":
					posisi = jawabanC.localToGlobal(new Point(0, 0));
					break;
				case "D":
					posisi = jawabanD.localToGlobal(new Point(0, 0));
					break;
				default:
					break;
			}
			
			return posisi;
		}
		
		protected function disposeRenderer():void
		{
			var tween:Tween = new Tween(this, 0.3);
			tween.fadeTo(0);
			tween.onComplete = function():void
			{
				soal.removeFromParent(true);
				jawabanA.removeFromParent(true);
				jawabanB.removeFromParent(true);
				jawabanC.removeFromParent(true);
				jawabanD.removeFromParent(true);
				benarimg.removeFromParent(true);
				salahimg.removeFromParent(true);
				buttonGroup.removeFromParent(true);
				infoLayoutGroup.removeFromParent(true);
				innerContent.removeFromParent(true);
				
				if(owner.dataProvider.length <= 1)
				{
					var bubblingEvent:Event = new Event(EventType.SELESAI, true);
					dispatchEvent(bubblingEvent);
					bubblingEvent.stopImmediatePropagation();
				}
				else
				{
					owner.dataProvider.removeItemAt(owner.dataProvider.getItemIndex(data));
				}
				Starling.juggler.remove(tween);
			};
			Starling.juggler.add(tween);
		}

		private function createlightdownBackground():Texture
		{
			var downdBg:RoundedQuad = new RoundedQuad(5, 100, 100, 0xd34836);
			downdBg.filter = new DropShadowFilter(4, Math.PI / 2, 0, 0.25, 2, 1);
			downdBg.x = downdBg.y = 10;

			var downuBg:RoundedQuad = new RoundedQuad(5, 96, 96, 0);
			downuBg.x = downuBg.y = 12;

			var spr:Sprite = new Sprite();
			spr.addChild(downdBg);
			spr.addChild(downuBg);
			
			var bnd:Rectangle = spr.bounds;
			var rendertxture:RenderTexture = new RenderTexture(bnd.width + 20, bnd.height + 20);
			rendertxture.draw(spr);
			return rendertxture;
		}

		private function createlightupBackground():Texture
		{
			var downdBg:RoundedQuad = new RoundedQuad(5, 100, 100, 0xffffff);
			downdBg.x = downdBg.y = 10;

			var downuBg:RoundedQuad = new RoundedQuad(5, 96, 96, 0);
			downuBg.x = downuBg.y = 12;

			var spr:Sprite = new Sprite();
			spr.addChild(downdBg);
			spr.addChild(downuBg);
			
			var bnd:Rectangle = spr.bounds;
			var rendertxture:RenderTexture = new RenderTexture(bnd.width + 20, bnd.height + 20);
			rendertxture.draw(spr);
			return rendertxture;
		}
	}
}