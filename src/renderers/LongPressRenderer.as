package renderers
{
	import feathers.controls.renderers.DefaultListItemRenderer;
	import feathers.utils.touch.LongPress;
	
	/**
	 * ...
	 * @author Indra Mahkota
	 */
	public class LongPressRenderer extends DefaultListItemRenderer
	{
		private var _longPress:LongPress;
		
		public function LongPressRenderer()
		{
			super();
			_longPress = new LongPress(this);
		}
	}
}