package renderers
{
	import constants.EventType;

	import feathers.controls.ImageLoader;
	import feathers.controls.renderers.LayoutGroupListItemRenderer;
	import feathers.layout.VerticalLayout;
	import feathers.layout.VerticalLayoutData;

	import flash.geom.Rectangle;

	import starling.display.Image;
	import starling.events.Event;

	import utils.CustomButton;
	
	/**
	 * ...
	 * @author Indra Mahkota
	 */
	public class MateriRenderer extends LayoutGroupListItemRenderer
	{
		protected var button:CustomButton;
		protected var image:ImageLoader;
		
		public function MateriRenderer()
		{
			super();
		}
		
		override protected function initialize():void
		{
			var vl:VerticalLayout = new VerticalLayout();
			vl.paddingLeft = vl.paddingRight = 10;
			vl.paddingTop = 10;
			vl.paddingBottom = 60;

			this.layout = vl;
			this.layoutData = new VerticalLayoutData(100, NaN);
			
			var bgimage:Image = new Image(Main.roundQuadBackground);
			bgimage.pixelSnapping = true;
			bgimage.scale9Grid = new Rectangle(20, 20, 80, 80);
			
			this.backgroundSkin = bgimage;
			
			button = new CustomButton();
			button.styleNameList.add("headertest-button");
			button.layoutData = new VerticalLayoutData(100, NaN);
			button.addEventListener(Event.TRIGGERED, Button_triggeredHandler);
			this.addChild(button);
			
			image = new ImageLoader();
			image.pixelSnapping = true;
			image.paddingLeft = image.paddingRight = 50;
			image.layoutData = new VerticalLayoutData(100, NaN);
			this.addChild(image);

			super.initialize();
		}
		
		override protected function commitData():void
		{
			if(this._data && this._owner)
			{
				button.label = this._data.judul;
				image.source = this._data.icon;
			}
		}
		
		override public function dispose():void
		{
			super.dispose();
		}
		
		private function Button_triggeredHandler(event:Event):void
		{
			var obj:Object = new Object();
			obj.str = this._data.tujuan;;
			obj.materi = this._data.materi;
			obj.urlKonten = this._data.urlKonten;
			
			var bubblingEvent:Event = new Event(EventType.NEXTSTATE, true, obj);
			dispatchEvent(bubblingEvent);
		}
	}
}