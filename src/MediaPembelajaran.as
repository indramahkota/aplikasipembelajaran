package
{
	import com.distriqt.extension.core.Core;

	import feathers.utils.ScreenDensityScaleFactorManager;

	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.display3D.Context3DProfile;
	import flash.display3D.Context3DRenderMode;
	import flash.events.Event;

	import starling.core.Starling;
	
	[SWF(width = "320", height = "480", frameRate = "60", backgroundColor = "#FFFFFF")]
	
	/**
	 * ...
	 * @author Indra Mahkota
	 */
	public class MediaPembelajaran extends Sprite
	{
		private var starling:Starling;
		private var scaler:ScreenDensityScaleFactorManager;
		
		public function MediaPembelajaran()
		{
			if(this.stage)
			{
				this.stage.addEventListener(Event.DEACTIVATE, deactivateHandler, false, 0, true);
				this.stage.scaleMode = StageScaleMode.NO_SCALE;
				this.stage.align = StageAlign.TOP_LEFT;
			}

			Core.init();
			this.mouseEnabled = this.mouseChildren = false;

			starling = new Starling(Main, this.stage, null, null, Context3DRenderMode.AUTO, Context3DProfile.BASELINE);
			starling.skipUnchangedFrames = true;
			starling.antiAliasing = 4;

			scaler = new ScreenDensityScaleFactorManager(starling);
			starling.start();
		}

		private function deactivateHandler(event:Event):void
		{
			starling.stop(true);
			this.stage.addEventListener(Event.ACTIVATE, activateHandler, false, 0, true);
		}
		
		private function activateHandler(event:Event):void
		{
			this.stage.removeEventListener(Event.ACTIVATE, activateHandler);
			starling.start();
		}
	}
}
