package constants
{
	/**
	 * ...
	 * @author Indra Mahkota
	 */
	public class Url
	{
		public static const BASE_URL:String = "https://indramahkotapecahan.000webhostapp.com/";

		//Customize with your Firebase project values
		public static const FIREBASE_API_KEY:String = "AIzaSyDKGBEM8ggZ_rRvq2qO0vWOEUQUlQfAMfQ";
		private static const PROJECT_ID:String = "indramahkotapecahan";

		//Auth and Storage URLs
		public static const FIREBASE_STORAGE_URL:String = "https://firebasestorage.googleapis.com/v0/b/" + PROJECT_ID + ".appspot.com/o/";
		public static const FIREBASE_REDIRECT_URL:String = "https://" + PROJECT_ID + ".firebaseapp.com/__/auth/handler";

		//Database URLs (CREATE, READ, DELETE)
		public static const FIREBASE_DATA_PENGGUNA_URL:String = 'https://' + PROJECT_ID + '.firebaseio.com/pengguna.json';
		public static const FIREBASE_DATA_NILAI_URL:String = 'https://' + PROJECT_ID + '.firebaseio.com/nilai.json';
		public static const FIREBASE_DATA_GAYABELAJAR_URL:String = 'https://' + PROJECT_ID + '.firebaseio.com/gayabelajar.json';

		//Database URLs (UPDATE)
		public static const FIREBASE_PATCH_DATA_PENGGUNA_URL:String = 'https://' + PROJECT_ID + '.firebaseio.com/pengguna/';

		//These URLs are used by the Auth service when using Federated login providers
		public static const FIREBASE_CREATE_AUTH_URL:String = "https://www.googleapis.com/identitytoolkit/v3/relyingparty/createAuthUri?key=" + FIREBASE_API_KEY;
		public static const FIREBASE_VERIFY_ASSERTION_URL:String = "https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyAssertion?key=" + FIREBASE_API_KEY;
		public static const FIREBASE_ACCOUNT_SETINFO_URL:String = "https://www.googleapis.com/identitytoolkit/v3/relyingparty/setAccountInfo?key=" + FIREBASE_API_KEY;
		
		//This URL generates an access_token that is used to sign Auth and Storage requests
		public static const FIREBASE_AUTH_TOKEN_URL:String = "https://securetoken.googleapis.com/v1/token?key=" + FIREBASE_API_KEY;
	}
}