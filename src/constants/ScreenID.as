﻿package constants
{
	/**
	 * ...
	 * @author Indra Mahkota
	 */
	public class ScreenID
	{
		public static const DAFTARSCREEN:String = "login";
		public static const MASUKSCREEN:String = "masuk";
		public static const MENU_UTAMA:String = "menu_utama";
		public static const PENGATURAN:String = "pengaturan";
		public static const TENTANG:String = "tentang";
		public static const PENDAHULUAN:String = "pendahuluan";
		public static const MATERI:String = "materi";
		public static const SIMULASI:String = "simulasi";
		public static const EVALUASI:String = "evaluasi";
		public static const PENGGUNA:String = "pengguna";
		public static const PETUNJUK:String = "petunjuk";
		public static const VIDEOVIEWER:String = "videoviewer";
		public static const VIDEOVIEWERFS:String = "videoviewerfs";
		public static const TESGAYABELAJAR:String = "tes_gaya_belajar";
	}
}