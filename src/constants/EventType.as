package constants
{
	/**
	 * ...
	 * @author Indra Mahkota
	 */
	public class EventType
	{
		public static const SHOW_DAFTAR_SCREEN:String = "showDaftar";
		public static const SHOW_MASUK_SCREEN:String = "showMasuk";
		public static const SHOW_MENU_UTAMA:String = "showMenuUtama";
		public static const SHOW_PENDAHULUAN:String = "showPendahuluan";
		public static const SHOW_TES_GAYA_BELAJAR:String = "showTesGayaBelajar";
		public static const SHOW_MATERI:String = "showMateri";
		public static const SHOW_SIMULASI:String = "showSimulasi";
		public static const SHOW_EVALUASI:String = "showEvaluasi";
		public static const SHOW_PENGGUNA:String = "showPengguna";
		public static const SHOW_PETUNJUK:String = "showPetunjuk";
		public static const SHOW_VIDEO_VIEWER:String = "showVideoViewer";
		public static const SHOW_VIDEO_VIEWER_FS:String = "showVideoViewerFs";
		public static const TOGGlE_LEFTDRAWER:String = "toggleLeftDrawer";
		public static const PENYIMPANAN:String = "penyimpanan";
		public static const NEXTSTATE:String = "nextstate";
		public static const SELESAI:String = "selesai";
		public static const UBAH_DRAWERS:String = "ubahDrawers";
	}
}