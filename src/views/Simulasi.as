package views
{
	import constants.EventType;

	import feathers.controls.Button;
	import feathers.controls.Header;
	import feathers.controls.Label;
	import feathers.controls.LayoutGroup;
	import feathers.controls.PageIndicator;
	import feathers.controls.PanelScreen;
	import feathers.controls.ScrollBarDisplayMode;
	import feathers.controls.ScrollContainer;
	import feathers.events.FeathersEventType;
	import feathers.layout.AnchorLayout;
	import feathers.layout.AnchorLayoutData;
	import feathers.layout.Direction;
	import feathers.layout.HorizontalLayout;
	import feathers.layout.VerticalLayout;
	import feathers.layout.VerticalLayoutData;

	import flash.geom.Rectangle;
	import flash.system.System;
	import flash.utils.setTimeout;

	import simulasi.SimArtiPecahan;
	import simulasi.SimOpHitPecBiasa;
	import simulasi.SimOpHitPecCamp;
	import simulasi.SimPecBiaskeCamp;
	import simulasi.SimPecCampkeBias;
	import simulasi.SimPecSederhana;
	import simulasi.SimPecahanSenilai;

	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.events.Event;
	import starling.text.TextFormat;
	
	/**
	 * ...
	 * @author Indra Mahkota
	 */
	public class Simulasi extends PanelScreen
	{
		protected var _data:NavigatorData;
		
		private var pages:PageIndicator;
		private var scrollerContainer:ScrollContainer;
		
		private static var fixed_height:Number;
		
		private var textFormat:TextFormat = new TextFormat("Calibri", 16);
		
		public function Simulasi()
		{
			super();
		}
		
		public function get data():NavigatorData  { return _data; }
		
		public function set data(value:NavigatorData):void  { _data = value; }
		
		override protected function initialize():void
		{
			this.title = "Simulasi Materi Pecahan";
			this.layout = new AnchorLayout();
			this.backButtonHandler = goBack;
			this.headerFactory = customHeaderFactory;
			
			fixed_height = stage.stageHeight - 77;

			scrollerContainer = new ScrollContainer();
			scrollerContainer.snapToPages = true;
			scrollerContainer.hasElasticEdges = true;
			scrollerContainer.width = this.owner.width;
			scrollerContainer.pageWidth = this.owner.width;
			scrollerContainer.layout = new HorizontalLayout();
			scrollerContainer.scrollBarDisplayMode = ScrollBarDisplayMode.NONE;
			scrollerContainer.addEventListener(FeathersEventType.BEGIN_INTERACTION, startDrag);
			scrollerContainer.addEventListener(FeathersEventType.END_INTERACTION, stopDrag);
			this.addChild(scrollerContainer);
			
			pages = new PageIndicator();
			pages.pageCount = 7;
			pages.gap = pages.padding = 4;
			pages.direction = Direction.HORIZONTAL;
			pages.layoutData = new AnchorLayoutData(NaN, NaN, 50, NaN, 0, NaN);
			pages.normalSymbolFactory = function():DisplayObject
			{
				var image:Image = new Image(Main.getImage("page_disabled"));
				image.scale = 0.5;
				
				return image;
			};
			pages.selectedSymbolFactory = function():DisplayObject
			{
				var image:Image = new Image(Main.getImage("page_selected"));
				image.scale = 0.5;
				
				return image;
			};
			pages.addEventListener(Event.CHANGE, pageIndicator_changeHandler);
			this.addChild(pages);

			var sim0:SimArtiPecahan = new SimArtiPecahan();
            scrollerContainer.addChild(sim0);	
			
			var sim1:SimPecahanSenilai = new SimPecahanSenilai();
            scrollerContainer.addChild(sim1);

			var sim2:SimPecSederhana = new SimPecSederhana();
            scrollerContainer.addChild(sim2);

			var sim3:SimPecBiaskeCamp = new SimPecBiaskeCamp();
            scrollerContainer.addChild(sim3);

			var sim4:SimPecCampkeBias = new SimPecCampkeBias();
            scrollerContainer.addChild(sim4);

			var sim5:SimOpHitPecBiasa = new SimOpHitPecBiasa();
            scrollerContainer.addChild(sim5);

			var sim6:SimOpHitPecCamp = new SimOpHitPecCamp();
            scrollerContainer.addChild(sim6);
			
			if(_data.saveStateSimulasiPecahan != null)
			{
				pages.selectedIndex = _data.saveStateSimulasiPecahan;
				scrollerContainer.horizontalPageIndex = _data.saveStateSimulasiPecahan;
			}
			
			super.initialize();
		}
		
		override public function dispose():void
		{
			System.pauseForGCIfCollectionImminent(0);
			System.gc();
			
			super.dispose();
		}
		
		private function startDrag(event:Event):void { }
		
		private function stopDrag(event:Event):void
		{
			setTimeout(function():void
			{
				pages.selectedIndex = scrollerContainer.horizontalPageIndex;
			}, 150);
		}
		
		private function pageIndicator_changeHandler(event:Event):void
		{
			var pages:PageIndicator = PageIndicator(event.currentTarget);
			scrollerContainer.scrollToPageIndex(pages.selectedIndex, 0, 0.5);
		}
		
		private function createLayoutGroup():LayoutGroup
		{
			var layout:VerticalLayout = new VerticalLayout();
			layout.paddingLeft = 10;
			layout.paddingRight = 10;
			layout.paddingBottom = 20;
			layout.paddingTop = 10;
			
			var layoutGroup:LayoutGroup = new LayoutGroup();
			layoutGroup.width = this.owner.width;
			layoutGroup.height = fixed_height;
			layoutGroup.layout = layout;
			return layoutGroup;
		}
		
		private function createLayoutGroupWithBackground():LayoutGroup
		{
			var layout:VerticalLayout = new VerticalLayout();
			layout.paddingTop = 20;
			layout.gap = 10;
			
			var image:Image = new Image(Main.getImage("roundsquare"));
			image.pixelSnapping = true;
			image.scale9Grid = new Rectangle(8, 8, 10, 8);
			
			var lg:LayoutGroup = new LayoutGroup();
			lg.backgroundSkin = image;
			lg.layout = layout;
			lg.minHeight = stage.stageHeight - 107;
			lg.layoutData = new VerticalLayoutData(100, NaN);
			return lg;
		}
		
		private function createLayoutGroupWithAnchorLayout():LayoutGroup
		{
			var lgs:LayoutGroup = new LayoutGroup();
			lgs.layout = new AnchorLayout();
			lgs.layoutData = new VerticalLayoutData(100, NaN);
			return lgs;
		}
		
		private function createLabel(text:String):Label
		{
			var label:Label = new Label();
			label.text = text;
			
			label.paddingLeft = label.paddingRight = 20;
			label.textRendererProperties.wordWrap = true;
			label.layoutData = new VerticalLayoutData(100, NaN);
			return label;
		}
		
		private function goBack():void
		{
			_data.saveStateSimulasiPecahan = null;
			
			this.dispatchEventWith(Event.COMPLETE);
		}
		
		private function customHeaderFactory():Header
		{
			var header:Header = new Header();
			
			var leftButton:Button = new Button();
			leftButton.styleNameList.add("back-button");
			leftButton.addEventListener(starling.events.Event.TRIGGERED, leftButton_triggeredHandler);
			
			header.leftItems = new <DisplayObject>[leftButton];
			
			var rightButton:Button = new Button();
			rightButton.styleNameList.add("help-button");
			rightButton.addEventListener(starling.events.Event.TRIGGERED, rightButton_triggeredHandler);
			
			header.rightItems = new <DisplayObject>[rightButton];
			
			return header;
		}
		
		private function leftButton_triggeredHandler(event:Event):void
		{
			Main.klik();
			_data.saveStateSimulasiPecahan = null;
			
			this.dispatchEventWith(Event.COMPLETE);
		}
		
		private function rightButton_triggeredHandler(event:Event):void
		{
			Main.klik();
			
			_data.petunjuk = "petunjuksimulasi";
			_data.saveStateSimulasiPecahan = scrollerContainer.horizontalPageIndex;
			
			this.dispatchEventWith(EventType.SHOW_PETUNJUK);
		}
	}
}