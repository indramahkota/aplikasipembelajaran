package views
{
	import com.indramahkota.ane.toast.DurationEnum;
	import com.indramahkota.ane.toast.ToastExtension;

	import constants.EventType;

	import feathers.controls.Button;
	import feathers.controls.Header;
	import feathers.controls.ImageLoader;
	import feathers.controls.Label;
	import feathers.controls.LayoutGroup;
	import feathers.controls.List;
	import feathers.controls.PanelScreen;
	import feathers.controls.ScrollContainer;
	import feathers.controls.ScrollPolicy;
	import feathers.controls.SpinnerList;
	import feathers.controls.TextInput;
	import feathers.controls.ToggleSwitch;
	import feathers.controls.renderers.DefaultListItemRenderer;
	import feathers.controls.renderers.IListItemRenderer;
	import feathers.data.ArrayCollection;
	import feathers.layout.AnchorLayout;
	import feathers.layout.AnchorLayoutData;
	import feathers.layout.HorizontalSpinnerLayout;
	import feathers.layout.VerticalLayout;
	import feathers.layout.VerticalLayoutData;

	import flash.desktop.NativeApplication;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.geom.Rectangle;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.system.System;

	import starling.animation.DelayedCall;
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.events.Event;

	import utils.CustomButton;
	import utils.CustomListItemRenderer;
	import constants.Url;
	
	/**
	 * ...
	 * @author Indra Mahkota
	 */
	public class Pengaturan extends PanelScreen
	{
		private var _list:List;
		private var _listItem:Object;
		private var _toggSwitch:ToggleSwitch;
		private var image:Image;
		private var inputNamaLengkap:TextInput;
		private var inputNamaPengguna:TextInput;
		private var inputPassword:TextInput;
		private var spinnerList:SpinnerList;
		
		private var loading:Label;
		private var scrollerContainer:ScrollContainer;
		
		protected var _data:NavigatorData;
		private var exitHandlerBoolean:Boolean = false;
		private var exitoastHandlerBoolean:Boolean = false;
		private var rightButton:Button = new Button();
		
		public function Pengaturan()
		{
			super();
		}
		
		public function get data():NavigatorData  { return _data; }
		
		public function set data(value:NavigatorData):void  { _data = value; }
		
		override protected function initialize():void
		{
			this.title = "Pengaturan";
			this.layout = new AnchorLayout();
			this.backButtonHandler = exitHandler;
			this.menuButtonHandler = menuHandler;
			this.headerFactory = customHeaderFactory;
			
			var layout:VerticalLayout = new VerticalLayout();
			layout.paddingBottom = 10;
			
			scrollerContainer = new ScrollContainer();
			scrollerContainer.width = stage.stageWidth;
			scrollerContainer.height = stage.stageHeight - 77;
			scrollerContainer.layout = layout;
			scrollerContainer.horizontalScrollPolicy = ScrollPolicy.OFF;
			this.addChild(scrollerContainer);
			
			image = new Image(Main.roundQuadBackground);
			image.pixelSnapping = true;
			image.scale9Grid = new Rectangle(20, 20, 80, 80);
			image.height = 50;
			
			_list = new List();
			_listItem = {text: "Suara Tombol"};
			_list.itemRendererFactory = customItemRendererFactory;
			_list.dataProvider = new ArrayCollection([_listItem]);
			_list.layoutData = new VerticalLayoutData(100, NaN);
			_list.isSelectable = false;
			_list.clipContent = false;
			scrollerContainer.addChild(_list);
			
			_toggSwitch = new ToggleSwitch();
			_listItem.accessory = _toggSwitch;
			
			if(Main.mySo.data.SuaraTombol == "on" || Main.mySo.data.SuaraTombol == undefined)
				_toggSwitch.isSelected = true;
			else _toggSwitch.isSelected = false;
			
			_toggSwitch.addEventListener(starling.events.Event.CHANGE, hasAccessoryToggle_changeHandler);
			
			if(Main.mySo.data.namaPengguna != undefined)
			{
				var layoutspinnerList:LayoutGroup = createLayotGroup();
				var buttonspinnerList:CustomButton = createButton("Avatar");
				layoutspinnerList.addChild(buttonspinnerList);
				
				spinnerList = new SpinnerList();
				spinnerList.paddingTop = spinnerList.paddingBottom = 20;
				spinnerList.layout = new HorizontalSpinnerLayout();
				spinnerList.layoutData = new VerticalLayoutData(100, NaN);
				layoutspinnerList.addChild(spinnerList);
				
				scrollerContainer.addChild(layoutspinnerList);
				
				spinnerList.dataProvider = new ArrayCollection([
					{thumbnail: Main.getImage("avatar_1s")},
					{thumbnail: Main.getImage("avatar_2s")},
					{thumbnail: Main.getImage("avatar_3s")},
					{thumbnail: Main.getImage("avatar_4s")},
					{thumbnail: Main.getImage("avatar_5s")},
					{thumbnail: Main.getImage("avatar_6s")},
					{thumbnail: Main.getImage("avatar_7s")},
					{thumbnail: Main.getImage("avatar_8s")},
					{thumbnail: Main.getImage("avatar_9s")}
				]);
				
				spinnerList.itemRendererFactory = function():IListItemRenderer
				{
					var itemRenderer:CustomListItemRenderer = new CustomListItemRenderer();
					itemRenderer.iconSourceField = "thumbnail";
					itemRenderer.hasLabelTextRenderer = false;
					
					itemRenderer.iconLoaderFactory = function():ImageLoader
					{
						var loader:ImageLoader = new ImageLoader();
						loader.pixelSnapping = true;
						loader.scale = 0.5;
						return loader;
					}
					
					return itemRenderer;
				};
				
				var layoutTINamaLengkap:LayoutGroup = createLayotGroup();
				var buttonTINamaLengkap:CustomButton = createButton("Nama Lengkap");
				layoutTINamaLengkap.addChild(buttonTINamaLengkap);
				
				inputNamaLengkap = new TextInput();
				inputNamaLengkap.maxChars = 25;
				inputNamaLengkap.padding = 20;
				inputNamaLengkap.restrict = "a-zA-Z ";
				inputNamaLengkap.layoutData = new VerticalLayoutData(100, NaN);
				layoutTINamaLengkap.addChild(inputNamaLengkap);
				scrollerContainer.addChild(layoutTINamaLengkap);
				inputNamaLengkap.addEventListener(starling.events.Event.CHANGE, UpdateNamaLengkap);
				
				var layoutTINamaPengguna:LayoutGroup = createLayotGroup();
				var buttonTINamaPengguna:CustomButton = createButton("Nama Pengguna");
				layoutTINamaPengguna.addChild(buttonTINamaPengguna);
				
				inputNamaPengguna = new TextInput();
				inputNamaPengguna.maxChars = 25;
				inputNamaPengguna.padding = 20;
				inputNamaPengguna.restrict = "a-zA-Z0-9 ";
				inputNamaPengguna.layoutData = new VerticalLayoutData(100, NaN);
				layoutTINamaPengguna.addChild(inputNamaPengguna);
				scrollerContainer.addChild(layoutTINamaPengguna);
				
				var layoutTIPassword:LayoutGroup = createLayotGroup();
				var buttonTIPassword:CustomButton = createButton("Kata Sandi");
				layoutTIPassword.addChild(buttonTIPassword);
				
				inputPassword = new TextInput();
				inputPassword.maxChars = 25;
				inputPassword.padding = 20;
				inputPassword.restrict = "a-zA-Z0-9 ";
				inputPassword.layoutData = new VerticalLayoutData(100, NaN);
				layoutTIPassword.addChild(inputPassword);
				scrollerContainer.addChild(layoutTIPassword);
				
				//--------------------------------------
				// Konfigurasi
				//--------------------------------------
				
				inputNamaLengkap.prompt = Main.mySo.data.namaLengkap;
				inputNamaPengguna.prompt = Main.mySo.data.namaPengguna;
				inputPassword.prompt = Main.mySo.data.kataSandi;
				spinnerList.selectedIndex = int(Main.mySo.data.indexAvatar);
			}
			
			super.initialize();
		}
		
		override public function dispose():void
		{
			if(_listItem != null) _listItem = null;

			System.pauseForGCIfCollectionImminent(0);
			System.gc();
			
			super.dispose();
		}
		
		private function UpdateNamaLengkap():void
		{
			var nama:String = inputNamaLengkap.text;
			
			if(nama.charAt(0) == " ")
			{
				inputNamaLengkap.text = "";
				return;
			}
			
			var onlyonespace:RegExp = / {1,}/gi;			
			nama = nama.replace(onlyonespace, " ");
			
			inputNamaLengkap.text = nama;
		}
		
		private function createLayotGroup():LayoutGroup
		{
			var image:Image = new Image(Main.roundQuadBackground);
			image.pixelSnapping = true;
			image.height = 50;
			image.scale9Grid = new Rectangle(20, 20, 80, 80);

			var vl:VerticalLayout = new VerticalLayout();
			vl.paddingLeft = vl.paddingRight = 10;
			vl.paddingTop = 10;
			vl.paddingBottom = 5;
			
			var lg:LayoutGroup = new LayoutGroup();
			lg.backgroundSkin = image;
			lg.layout = vl;
			lg.layoutData = new VerticalLayoutData(100, NaN);
			return lg;
		}
		
		private function createButton(label:String):CustomButton
		{
			var button:CustomButton = new CustomButton();
			button.label = label;
			button.styleNameList.add("headertest-button");
			button.layoutData = new VerticalLayoutData(100, NaN);
			return button;
		}
		
		private function createLabel(text:String):Label
		{
			var label:Label = new Label();
			label.text = text;
			label.paddingLeft = label.paddingRight = label.paddingBottom = 20;
			label.textRendererProperties.wordWrap = true;
			label.layoutData = new VerticalLayoutData(100, NaN);
			return label;
		}
		
		private function createImageLoader():ImageLoader
		{
			var image:ImageLoader = new ImageLoader();
			image.source = Main.getImage("line_x");
			image.scale9Grid = new Rectangle(1, 1, 48, 48);
			image.paddingLeft = image.paddingRight = 20;
			image.pixelSnapping = true;
			image.layoutData = new VerticalLayoutData(100, NaN);
			return image;
		}
		
		private function hasAccessoryToggle_changeHandler(event:starling.events.Event):void
		{
			if(_toggSwitch.isSelected)
				Main.mySo.data.SuaraTombol = "on";
			else Main.mySo.data.SuaraTombol = "off";
		}
		
		private function customItemRendererFactory():IListItemRenderer
		{
			var itemRenderer:DefaultListItemRenderer = new DefaultListItemRenderer();
			itemRenderer.labelField = "text";
			itemRenderer.defaultSkin = image;
			itemRenderer.padding = 20;

			itemRenderer.accessoryField = "accessory";
			return itemRenderer;
		}
		
		private function customHeaderFactory():Header
		{
			var header:Header = new Header();
			
			var leftButton:Button = new Button();
			leftButton.styleNameList.add("menu-button");
			leftButton.addEventListener(starling.events.Event.TRIGGERED, leftButton_triggeredHandler);
			
			header.leftItems = new <DisplayObject>[leftButton];
			
			if(Main.mySo.data.namaPengguna != undefined)
			{
				rightButton.styleNameList.add("check-button");
				rightButton.addEventListener(starling.events.Event.TRIGGERED, rightButton_triggeredHandler);
			}
			else
			{
				rightButton.styleNameList.add("help-button");
				rightButton.addEventListener(starling.events.Event.TRIGGERED, showPetunjuk);
			}
			
			header.rightItems = new <DisplayObject>[rightButton];
			
			return header;
		}
		
		private function showPetunjuk():void
		{
			Main.klik();
			
			exitoastHandlerBoolean = true;
			_data.petunjuk = "petunjukpengaturan";
			dispatchEventWith(EventType.SHOW_PETUNJUK);
		}
		
		private function exitHandler():void
		{
			exitHandlerBoolean = !exitHandlerBoolean;
			
			if(exitHandlerBoolean || exitoastHandlerBoolean)
			{
				if(!exitoastHandlerBoolean)
				{
					if(ToastExtension.isSupported)
					{
						var toast:ToastExtension = new ToastExtension();
						toast.setText("Tekan sekali lagi untuk keluar");
						toast.setDuration(DurationEnum.LENGTH_LONG);
						toast.show();
					}
					
					var delayedCall:DelayedCall = new DelayedCall(function():void
					{
						Starling.juggler.remove(delayedCall);
						exitHandlerBoolean = !exitHandlerBoolean;
					}, 3);
					Starling.juggler.add(delayedCall);
				}
				else
				{
					exitHandlerBoolean = !exitHandlerBoolean;
				}
			}
			else
			{
				NativeApplication.nativeApplication.exit();
			}
		}
		
		private function menuHandler():void
		{
			dispatchEventWith(EventType.TOGGlE_LEFTDRAWER);
		}
		
		private function rightButton_triggeredHandler(event:starling.events.Event):void
		{
			Main.klik();

			if(inputNamaPengguna.text.indexOf(" ") >= 0 && inputPassword.text.indexOf(" ") >= 0)
			{
				inputNamaPengguna.errorString = "";
				inputPassword.errorString = "";
				createLoadingLabel("Karakter 'spasi' tidak diperbolehkan.");
				return;
			}

			if(inputNamaPengguna.text.indexOf(" ") >= 0)
			{
				inputNamaPengguna.errorString = "";
				createLoadingLabel("Karakter 'spasi' tidak diperbolehkan.");
				return;
			}

			if(inputPassword.text.indexOf(" ") >= 0)
			{
				inputPassword.errorString = "";
				createLoadingLabel("Karakter 'spasi' tidak diperbolehkan.");
				return;
			}

			if(inputNamaPengguna.errorString != null)
			{
				inputNamaPengguna.errorString = null;
			}

			if(inputPassword.errorString != null)
			{
				inputPassword.errorString = null;
			}

			var namalengkap:String, namapengguna:String;
			var katasandi:String, avatarpengguna:String;
			var indeksavatar:String;

			if((inputNamaLengkap.text.length <= 0 || inputNamaLengkap.text.length > 2) &&
				(inputNamaPengguna.text.length <= 0 || inputNamaPengguna.text.length > 2) &&
				(inputPassword.text.length <= 0 || inputPassword.text.length > 2))
			{
				if(inputNamaLengkap.text.length > 2)
				{
					//triming (menghapus spasi sebelum dan sesudah kata/kalimat).
					namalengkap = inputNamaLengkap.text.replace(/^\s+|\s+$/g, "");
				}
				else
				{
					namalengkap = Main.mySo.data.namaLengkap;
				}
				
				if(inputNamaPengguna.text.length > 2)
				{
					namapengguna = inputNamaPengguna.text;
				}
				else
				{
					namapengguna = Main.mySo.data.namaPengguna;
				}
				
				if(inputPassword.text.length > 2)
				{
					katasandi = inputPassword.text;
				}
				else
				{
					katasandi = Main.mySo.data.kataSandi;
				}
				
				avatarpengguna = ubahNilai(spinnerList.selectedIndex);
				indeksavatar = String(spinnerList.selectedIndex);

				var myObject:Object = new Object();
				myObject.namaLengkap = namalengkap;
				myObject.namaPengguna = namapengguna;
				myObject.kataSandi = katasandi;
				myObject.avatarPengguna = avatarpengguna;
				myObject.indeksAvatar = indeksavatar;

				rightButton.isEnabled = false;
				createLoadingLabel("Mohon tunggu...");
				patchData(Url.FIREBASE_PATCH_DATA_PENGGUNA_URL, myObject);
			}
			else
			{
				if(inputNamaLengkap.text.length < 4) inputNamaLengkap.errorString = "";
				if(inputNamaPengguna.text.length < 4) inputNamaPengguna.errorString = "";
				if(inputPassword.text.length < 4) inputPassword.errorString = "";

				createLoadingLabel("Input harus lebih dari tiga karakter...");
			}
		}

		private function patchData(url:String, object:Object):void
		{
			var uid:String = Main.mySo.data.uid + ".json";
			var patchMethod:String = "?x-http-method-override=PATCH";
			var addition:String = uid + patchMethod;

			var request:URLRequest = new URLRequest(url + addition);
			request.data = JSON.stringify(object);
			request.method = URLRequestMethod.POST;

			var loader:URLLoader = new URLLoader();
			loader.addEventListener(flash.events.Event.COMPLETE, postDataCompleteHAndler);
			loader.addEventListener(IOErrorEvent.IO_ERROR, errorHandler);
			loader.load(request);
		}

		private function postDataCompleteHAndler(event:flash.events.Event):void
		{
			event.currentTarget.removeEventListener(flash.events.Event.COMPLETE, postDataCompleteHAndler);

			var rawData:Object = JSON.parse(event.currentTarget.data);
			for(var parentKey:String in rawData)
			{
				trace(parentKey, rawData[parentKey]);
			}

			if(inputNamaLengkap.text.length > 2)
			{
				inputNamaLengkap.prompt = inputNamaLengkap.text;
				Main.mySo.data.namaLengkap = inputNamaLengkap.text;
				inputNamaLengkap.text = null;
			}
			
			if(inputNamaPengguna.text.length > 2)
			{
				inputNamaPengguna.prompt = inputNamaPengguna.text;
				Main.mySo.data.namaPengguna = inputNamaPengguna.text;
				inputNamaPengguna.text = null;
			}
			
			if(inputPassword.text.length > 2)
			{
				inputPassword.prompt = inputPassword.text;
				Main.mySo.data.kataSandi = inputPassword.text;
				inputPassword.text = null;
			}
			
			Main.mySo.data.avatar = ubahNilai(spinnerList.selectedIndex);
			Main.mySo.data.indexAvatar = String(spinnerList.selectedIndex);
			
			Main.profileName.text = Main.firstLetterUpperCase(Main.mySo.data.namaLengkap + "\n nilai: " + Main.mySo.data.nilai);
			Main.profileImage.source = Main.getImage(ubahNilai(spinnerList.selectedIndex));
			
			rightButton.isEnabled = true;
			createLoadingLabel("Berhasil...");
		}

		private function errorHandler(event:IOErrorEvent):void
		{
			rightButton.isEnabled = true;
			trace(event.currentTarget.data);
			createLoadingLabel("Tidak terkoneksi internet...");
		}
		
		private function createLoadingLabel(string:String):void
		{
			if(loading) loading.removeFromParent(true);
			
			var quad:Quad = new Quad(10, 10, 0x55D088);
			
			loading = new Label();
			loading.text = string;
			loading.minHeight = 20;
			loading.backgroundSkin = quad;
			loading.styleNameList.add("loading-center-label-bg");
			loading.layoutData = new AnchorLayoutData(NaN, 0, 0, 0, NaN, NaN);
			this.addChild(loading);
		}
		
		private function ubahNilai(nilai:int):String
		{
			var string:String;
			string = "avatar_" + String(nilai + 1) + "s";
			return string;
		}
		
		private function leftButton_triggeredHandler(event:starling.events.Event):void
		{
			Main.klik();
			
			dispatchEventWith(EventType.TOGGlE_LEFTDRAWER);
		}
	}
}