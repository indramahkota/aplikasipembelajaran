﻿package views
{
	import constants.EventType;

	import feathers.controls.Button;
	import feathers.controls.Header;
	import feathers.controls.ImageLoader;
	import feathers.controls.Label;
	import feathers.controls.LayoutGroup;
	import feathers.controls.PanelScreen;
	import feathers.controls.ScrollBarDisplayMode;
	import feathers.core.PopUpManager;
	import feathers.layout.AnchorLayout;
	import feathers.layout.AnchorLayoutData;
	import feathers.layout.VerticalLayout;
	import feathers.layout.VerticalLayoutData;

	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.system.System;

	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.textures.TextureAtlas;

	import utils.CustomButton;
	
	/**
	 * ...
	 * @author Indra Mahkota
	 */
	public class Pendahuluan extends PanelScreen
	{
		private var array:Array = new Array();
		
		private var atlasclick:TextureAtlas;
		private var gesermc:MovieClip;
		
		private var touchPointID:int = -1;
		private static const HELPER_POINT:Point = new Point();
		private static const HELPER_TOUCHES_VECTOR:Vector.<Touch> = new <Touch>[];
		
		protected var _data:NavigatorData;
		
		public function Pendahuluan()
		{
			super();
		}
		
		public function get data():NavigatorData  { return _data; }
		
		public function set data(value:NavigatorData):void  { _data = value; }
		
		override protected function initialize():void
		{
			this.title = "Pendahuluan";
			this.backButtonHandler = backHandler;
			this.headerFactory = customHeaderFactory;
			this.scrollBarDisplayMode = ScrollBarDisplayMode.FLOAT;
			this.addEventListener(Event.TRIGGERED, removePopUp);
			
			var layout:VerticalLayout = new VerticalLayout();
			layout.paddingBottom = 10;
			this.layout = layout;
			
			if(_data.pendahuluan) array = _data.pendahuluan;
			else {
				array = Main.getJsonAssets("pendahuluan");
				_data.pendahuluan = array;
			}
			
			var i:uint, len:int = array.length;
			
			for (i; i < len; ++i)
			{
				var lg:LayoutGroup = createLayotGroup();
				var bt:CustomButton = createButton(array[i].judul, array[i].icon);
				lg.addChild(bt);
				
				if(array[i].render == "ya")
				{
					var arrText:Array = new Array();
					arrText = array[i].text;
					
					var j:uint;
					var lenarrText:int = arrText.length;
					
					for (j; j < lenarrText; ++j)
					{
						var lgText:LayoutGroup = new LayoutGroup();
						lgText.layout = new AnchorLayout();
						lgText.layoutData = new VerticalLayoutData(100, NaN);
						
						var labelnum:Label = new Label();
						labelnum.text = String(j + 1) + ".";
						labelnum.layoutData = new AnchorLayoutData(20, 20, 0, 20, NaN, NaN);
						lgText.addChild(labelnum);
						
						var labelCont:Label = new Label();
						labelCont.text = arrText[j];
						labelCont.textRendererProperties.wordWrap = true;
						labelCont.layoutData = new AnchorLayoutData(20, 20, 0, 40, NaN, NaN);
						lgText.addChild(labelCont);
						
						lg.addChild(lgText);
					}
				}
				else
				{
					var lb:Label = createLabel(array[i].text);
					lg.addChild(lb);
				}
				this.addChild(lg);
			}
			
			if(_data.savedPendahuluanVSP)
			{
				this.verticalScrollPosition = _data.savedPendahuluanVSP;
			}
			
			if(Main.mySo.data.pendahuluantut1 == undefined)
			{
				atlasclick = Main.assets.getTextureAtlas("geser");
				gesermc = new MovieClip(atlasclick.getTextures("geseranimation_"));
				
				gesermc.scale = 50 / gesermc.width;
				
				PopUpManager.addPopUp(gesermc);
				Starling.juggler.add(gesermc);
				
				stage.addEventListener(TouchEvent.TOUCH, removePopUp);
			}
			
			super.initialize();
		}
		
		private function removePopUp(event:TouchEvent):void
		{
			var touch:Touch;
			var touches:Vector.<Touch> = event.getTouches(stage, null, HELPER_TOUCHES_VECTOR);
			
			if(touches.length == 0)
			{
				return;
			}
			
			if(touchPointID >= 0)
			{
				for each (var currentTouch:Touch in touches)
				{
					if(currentTouch.id == touchPointID)
					{
						touch = currentTouch;
						break;
					}
				}
				
				if(!touch)
				{
					HELPER_TOUCHES_VECTOR.length = 0;
					return;
				}
				
				if(touch.phase == TouchPhase.MOVED)
				{
					trace("[TouchPhase] Moves");
				}
				else if(touch.phase == TouchPhase.ENDED)
				{
					Main.mySo.data.pendahuluantut1 = "sudah";
					stage.removeEventListener(TouchEvent.TOUCH, removePopUp);
				}
			}
			else
			{
				for each (touch in touches)
				{
					if(touch.phase == TouchPhase.BEGAN)
					{
						if(PopUpManager.isPopUp(gesermc))
						{
							PopUpManager.removePopUp(gesermc, true);
						}
						else
						{
							Main.mySo.data.pendahuluantut1 = "sudah";
							stage.removeEventListener(TouchEvent.TOUCH, removePopUp);
						}
						break;
					}
				}
			}
			HELPER_TOUCHES_VECTOR.length = 0;
		}
		
		override public function dispose():void
		{
			if(array) array = null;
			
			if(gesermc)
			{
				gesermc.removeFromParent(true);
				Starling.juggler.remove(gesermc);
			}

			System.pauseForGCIfCollectionImminent(0);
			System.gc();
			
			super.dispose();
		}
		
		private function createLayotGroup():LayoutGroup
		{
			var image:Image = new Image(Main.roundQuadBackground);
			image.pixelSnapping = true;
			image.scale9Grid = new Rectangle(20, 20, 80, 80);

			var vl:VerticalLayout = new VerticalLayout();
			vl.paddingLeft = vl.paddingRight = 10;
			vl.paddingTop = 10;
			vl.paddingBottom = 60;
			
			var lg:LayoutGroup = new LayoutGroup();
			lg.backgroundSkin = image;
			lg.layout = vl;
			lg.layoutData = new VerticalLayoutData(100, NaN);
			return lg;
		}
		
		private function createButton(label:String, icon:String):CustomButton
		{
			var imgL:ImageLoader = new ImageLoader();
			imgL.touchable = false;
			imgL.source = Main.getImage(icon);
			imgL.pixelSnapping = true;
			imgL.setSize(25, 25);

			var button:CustomButton = new CustomButton();
			button.label = label;
			button.defaultIcon = imgL;
			button.styleNameList.add("headertest-button");
			button.layoutData = new VerticalLayoutData(100, NaN);
			return button;
		}
		
		private function createLabel(text:String):Label
		{
			var label:Label = new Label();
			label.text = text;
			label.paddingTop = label.paddingLeft = label.paddingRight = 20;
			label.textRendererProperties.wordWrap = true;
			label.layoutData = new VerticalLayoutData(100, NaN);
			return label;
		}
		
		private function createImageLoader():ImageLoader
		{
			var image:ImageLoader = new ImageLoader();
			image.source = Main.getImage("line_x");
			image.scale9Grid = new Rectangle(1, 1, 48, 48);
			image.paddingLeft = image.paddingRight = 20;
			image.pixelSnapping = true;
			image.layoutData = new VerticalLayoutData(100, NaN);
			return image;
		}
		
		private function customHeaderFactory():Header
		{
			var header:Header = new Header();
			
			var leftButton:Button = new Button();
			leftButton.styleNameList.add("back-button");
			leftButton.addEventListener(Event.TRIGGERED, leftButton_triggeredHandler);
			
			header.leftItems = new <DisplayObject>[leftButton];
			
			var rightButton:Button = new Button();
			rightButton.styleNameList.add("help-button");
			rightButton.addEventListener(Event.TRIGGERED, rightButton_triggeredHandler);
			
			header.rightItems = new <DisplayObject>[rightButton];
			
			return header;
		}
		
		private function backHandler():void
		{
			if(PopUpManager.isPopUp(gesermc))
			{
				PopUpManager.removePopUp(gesermc, true);
				Main.mySo.data.pendahuluantut1 = "sudah";
			}
			else
			{
				this.dispatchEventWith(Event.COMPLETE);
			}
		}
		
		private function rightButton_triggeredHandler(event:Event):void
		{
			Main.klik();
			
			_data.petunjuk = "petunjukpendahuluan";
			_data.savedPendahuluanVSP = this.verticalScrollPosition;
			this.dispatchEventWith(EventType.SHOW_PETUNJUK);
		}
		
		private function leftButton_triggeredHandler(event:Event):void
		{
			Main.klik();
			
			_data.savedPendahuluanVSP = this.verticalScrollPosition;
			this.dispatchEventWith(Event.COMPLETE);
		}
	}
}