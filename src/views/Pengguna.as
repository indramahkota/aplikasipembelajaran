﻿package views
{
	import constants.EventType;
	import constants.Url;

	import feathers.controls.Alert;
	import feathers.controls.Button;
	import feathers.controls.Header;
	import feathers.controls.ImageLoader;
	import feathers.controls.List;
	import feathers.controls.PanelScreen;
	import feathers.controls.ScrollBarDisplayMode;
	import feathers.controls.ScrollPolicy;
	import feathers.controls.renderers.DefaultListItemRenderer;
	import feathers.data.ArrayCollection;
	import feathers.data.IListCollection;
	import feathers.data.ListCollection;
	import feathers.events.FeathersEventType;
	import feathers.layout.AnchorLayout;
	import feathers.layout.AnchorLayoutData;
	import feathers.layout.VerticalLayout;

	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.geom.Rectangle;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestHeader;
	import flash.net.URLRequestMethod;
	import flash.system.System;

	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.events.Event;
	
	/**
	 * ...
	 * @author Indra Mahkota
	 */
	public class Pengguna extends PanelScreen
	{
		private var list:List;
		private var alert:Alert;
		private var pullView:MovieClip;
		private var collection:IListCollection;
		
		protected var _data:NavigatorData;
		
		public function Pengguna()
		{
			super();
		}
		
		public function get data():NavigatorData  { return _data; }
		
		public function set data(value:NavigatorData):void  { _data = value; }
		
		override protected function initialize():void
		{
			this.title = "Pengguna";
			this.layout = new AnchorLayout();
			this.backButtonHandler = backHandler;
			this.headerFactory = customHeaderFactory;
			
			pullView = new MovieClip(Main.assets.getTextureAtlas("AssetsImages").getTextures("Spinner"));
			pullView.addEventListener(FeathersEventType.PULLING, pullView_pullingHandler);
			
			var layoutList:VerticalLayout = new VerticalLayout();
			layoutList.paddingBottom = 10;
			
			list = new List();
			list.topPullView = pullView;
			list.hasElasticEdges = true;
			list.allowMultipleSelection = false;
			list.horizontalScrollPolicy = ScrollPolicy.OFF;
			list.scrollBarDisplayMode = ScrollBarDisplayMode.FLOAT;
			list.dataProvider = new ArrayCollection();
			list.layout = layoutList;
			list.layoutData = new AnchorLayoutData(0, 0, 0, 0);
			list.addEventListener(starling.events.Event.UPDATE, list_updateHandler);
			this.addChild(list);
			
			list.itemRendererFactory = function():DefaultListItemRenderer
			{
				var renderer:DefaultListItemRenderer = new DefaultListItemRenderer();
				renderer.iconSourceField = "icon";
				renderer.accessoryLabelField = "nilai";
				renderer.padding = 20;
				
				var image:Image = new Image(Main.roundQuadBackground);
				image.pixelSnapping = true;
				image.scale9Grid = new Rectangle(20, 20, 80, 80);
				image.height = 50;

				renderer.touchable = false;
				renderer.width = stage.stageWidth;				
				renderer.defaultSkin = image;

				renderer.iconLoaderFactory = function():ImageLoader
				{
					var loader:ImageLoader = new ImageLoader();
					loader.pixelSnapping = true;
					loader.setSize(40, 40);
					return loader;
				}
				
				return renderer;
			}
			
			konfigurasi();
			
			super.initialize();
		}
		
		override public function dispose():void
		{
			System.pauseForGCIfCollectionImminent(0);
			System.gc();
			
			super.dispose();
		}
		
		private function konfigurasi():void
		{
			if(Main.mySo.data.sessions == undefined)
			{
				collection = list.dataProvider;
				collection.removeAll();
				
				var newItem:Object = new Object();
				newItem.label = "Maaf, Anda belum terdaftar.";
				collection.push(newItem);
				
				list.dataProvider = collection;
				
				return;
			}
			
			if(_data.dataCollectionListPengguna)
			{
				list.dataProvider = _data.dataCollectionListPengguna;
				
				if(_data.savedPenggunaVSP)
					list.verticalScrollPosition = _data.savedPenggunaVSP;
			}
			else
			{
				getData(Url.FIREBASE_DATA_PENGGUNA_URL);
			}
		}

		private function getData(url:String):void
		{
			list.isTopPullViewActive = true;
			Starling.juggler.add(pullView);

			var header:URLRequestHeader = new URLRequestHeader("Content-Type", "application/json");
			
			var request:URLRequest = new URLRequest(url);
			request.method = URLRequestMethod.GET;
			request.requestHeaders.push(header);

			var loader:URLLoader = new URLLoader();
			loader.addEventListener(flash.events.Event.COMPLETE, getDataCompleteHAndler);
			loader.addEventListener(IOErrorEvent.IO_ERROR, errorHandler);
			loader.load(request);
		}

		private function getDataCompleteHAndler(event:flash.events.Event):void
		{
			list.isTopPullViewActive = false;
			Starling.juggler.remove(pullView);

			event.currentTarget.removeEventListener(flash.events.Event.COMPLETE, getDataCompleteHAndler);

			try{
				collection = list.dataProvider;
				collection.removeAll();

				var rawData:Object = JSON.parse(event.currentTarget.data);
				for(var parentKey:String in rawData)
				{
					var newItem:Object = new Object();
					newItem.label = Main.firstLetterUpperCase(rawData[parentKey]["namaLengkap"]);
					newItem.nilai = rawData[parentKey]["nilai"];
					newItem.icon = Main.getImage(rawData[parentKey]["avatarPengguna"]);
					collection.push(newItem);
				}

				list.dataProvider = collection;
				_data.dataCollectionListPengguna = list.dataProvider;
			}
			catch (err:*){
				trace("Error happen!!!");
			}
		}
		
		private function errorHandler(event:flash.events.IOErrorEvent):void
		{
			list.isTopPullViewActive = false;
			Starling.juggler.remove(pullView);
			
			alert = Alert.show("Tidak terkoneksi internet", "Pesan", new ListCollection([{label: "OK"}]));
			
			alert.addEventListener(starling.events.Event.CLOSE, function(event:starling.events.Event, data:Object):void
			{
				if(data.label == "OK")
				{
					Main.klik();
					
					alert.removeFromParent(true);
				}
			});
		}
		
		private function pullView_pullingHandler(event:starling.events.Event, ratio:Number):void
		{
			var totalFrames:int = pullView.numFrames;
			var frameIndex:int = Math.round(ratio * totalFrames);
			while (frameIndex >= totalFrames)
			{
				frameIndex -= totalFrames;
			}
			pullView.currentFrame = frameIndex;
		}
		
		private function list_updateHandler(event:starling.events.Event):void
		{
			if(Main.mySo.data.sessions == undefined)
			{
				//jika !sessions, force return;
				list.isTopPullViewActive = false;
				Starling.juggler.remove(pullView);
				
				return;
			}
			
			getData(Url.FIREBASE_DATA_PENGGUNA_URL);
		}
		
		private function customHeaderFactory():Header
		{
			var header:Header = new Header();
			
			var leftButton:Button = new Button();
			leftButton.styleNameList.add("back-button");
			leftButton.addEventListener(starling.events.Event.TRIGGERED, leftButton_triggeredHandler);
			
			header.leftItems = new <DisplayObject>[leftButton];
			
			var rightButton:Button = new Button();
			rightButton.styleNameList.add("help-button");
			rightButton.addEventListener(starling.events.Event.TRIGGERED, rightButton_triggeredHandler);
			
			header.rightItems = new <DisplayObject>[rightButton];
			
			return header;
		}
		
		private function backHandler():void
		{
			_data.savedPenggunaVSP = null;
			this.dispatchEventWith(starling.events.Event.COMPLETE);
		}
		
		private function leftButton_triggeredHandler(event:starling.events.Event):void
		{
			Main.klik();
			
			_data.savedPenggunaVSP = null;
			this.dispatchEventWith(starling.events.Event.COMPLETE);
		}
		
		private function rightButton_triggeredHandler(event:starling.events.Event):void
		{
			Main.klik();
			
			_data.petunjuk = "petunjukpengguna";
			_data.dataCollectionListPengguna = list.dataProvider;
			_data.savedPenggunaVSP = list.verticalScrollPosition;
			this.dispatchEventWith(EventType.SHOW_PETUNJUK);
		}
	}
}