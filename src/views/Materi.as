package views
{
	import constants.EventType;

	import feathers.controls.Button;
	import feathers.controls.Header;
	import feathers.controls.ImageLoader;
	import feathers.controls.LayoutGroup;
	import feathers.controls.PanelScreen;
	import feathers.controls.ScrollBarDisplayMode;
	import feathers.events.FeathersEventType;
	import feathers.layout.AnchorLayout;
	import feathers.layout.AnchorLayoutData;
	import feathers.layout.VerticalLayout;
	import feathers.layout.VerticalLayoutData;

	import flash.geom.Rectangle;
	import flash.utils.setTimeout;

	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.events.Event;
	import starling.textures.TextureAtlas;

	import utils.CustomButton;
	
	/**
	 * ...
	 * @author Indra Mahkota
	 */
	public class Materi extends PanelScreen
	{
		private var jenisVideo:String;
		
		private var pullView:MovieClip;
		private var arrayData:Array = [];

		private var tutoLayoutIndex:int = -1;
		private var atlasclick:TextureAtlas;
		private var clickmc:MovieClip;

		private var rex:RegExp = /[\s\r\n]+/gim;
		
		//"[\s\r\n]+"
		// \s (whitespace, spasi, tab, line-break)
		// \r (new line / macOS: end-of-line)
		// \n (new line / unix: end-of-line)
		// \r\n (new line / windows: end-of-line)
		// + (1 atau lebih, sebanyak-banyaknya)
		// g global, cari semua yang cocok.
		// i ignore case, huruf besar & huruf kecil sama aja
		// m multiline, cari di semua baris teks, jangan berenti biarpun ketemu karakter line-break.

		protected var _data:NavigatorData;
		
		public function Materi()
		{
			super();
		}
		
		public function get data():NavigatorData  { return _data; }
		
		public function set data(value:NavigatorData):void  { _data = value; }
		
		override protected function initialize():void
		{
			this.title = "Materi Pembelajaran";
			this.backButtonHandler = backHandler;
			this.headerFactory = customHeaderFactory;
			this.scrollBarDisplayMode = ScrollBarDisplayMode.FLOAT;

			var layout:VerticalLayout = new VerticalLayout();
			layout.paddingBottom = 10;
			this.layout = layout;

			pullView = new MovieClip(Main.assets.getTextureAtlas("AssetsImages").getTextures("Spinner"));
			pullView.addEventListener(FeathersEventType.PULLING, pullView_pullingHandler);

			this.topPullView = pullView;
			this.addEventListener(Event.UPDATE, updateHandler);

			arrayData = Main.getJsonAssets("materi");
			var i:uint, len:int = arrayData.length;

			if(Main.mySo.data.menumateritut0 == undefined)
			{
				tutoLayoutIndex = 0;
			}
			else if(Main.mySo.data.menumateritut1 == undefined)
			{
				tutoLayoutIndex = 1;
			}
			else if(Main.mySo.data.menumateritut2 == undefined)
			{
				tutoLayoutIndex = 2;
			}
			else if(Main.mySo.data.menumateritut3 == undefined)
			{
				tutoLayoutIndex = 3;
			}
			else
			{
				tutoLayoutIndex = len;
			}
			
			for (i; i < len; ++i)
			{
				var trick:LayoutGroup = new LayoutGroup();
				trick.layout = new AnchorLayout();
				trick.layoutData = new VerticalLayoutData(100, NaN);

				var lg:LayoutGroup = createLayotGroup();
				var bt:CustomButton = createButton(arrayData[i].nama, String(i));
				var img:ImageLoader = new ImageLoader();
				img.paddingLeft = img.paddingRight = 50;
				img.layoutData = new VerticalLayoutData(100, NaN);
				img.source = Main.getImage(arrayData[i].gambar);

				lg.addChild(bt);
				lg.addChild(img);
				trick.addChild(lg);

				if(tutoLayoutIndex == i)
				{
					atlasclick = Main.assets.getTextureAtlas("click");
					clickmc = new MovieClip(atlasclick.getTextures("clickanimation_"));
					clickmc.scale = 50 / clickmc.width;
					clickmc.touchable = false;
					clickmc.x = 20;
					trick.addChild(clickmc);
					Starling.juggler.add(clickmc);
				}

				if(i < tutoLayoutIndex)
				{
					var sudahCheck:ImageLoader = new ImageLoader();
					sudahCheck.source = Main.getImage("ceklist");
					sudahCheck.setSize(24, 24);
					sudahCheck.layoutData = new AnchorLayoutData(23, 23);
					trick.addChild(sudahCheck);
				}
				
				if(i > tutoLayoutIndex)
				{
					lg.touchable = false;
					var errorCheck:ImageLoader = new ImageLoader();
					errorCheck.source = Main.getImage("error_icon");
					errorCheck.setSize(24, 24);
					errorCheck.layoutData = new AnchorLayoutData(23, 23);
					trick.addChild(errorCheck);
				}

				this.addChild(trick);
			}

			if(Main.mySo.data.dominan != "kosong")
			{
				if(Main.mySo.data.dominan == "visual")
				{
					_data.randomMateri = "teks";
					jenisVideo = "teks";
				}
				else if(Main.mySo.data.dominan == "auditorial")
				{
					_data.randomMateri = "suara";
					jenisVideo = "suara";
				}
				else
				{
					var a:Number = Main.mySo.data.visual as Number;
					var b:Number = Main.mySo.data.auditorial as Number;

					if(a > b)
					{
						_data.randomMateri = "teks";
						jenisVideo = "teks";
					}
					else
					{
						_data.randomMateri = "suara";
						jenisVideo = "suara";
					}
				}
			}
			else if(_data.randomMateri != null)
			{
				jenisVideo = _data.randomMateri;
			}
			else
			{
				jenisVideo = "teks";
			}
			
			if(_data.savedMateriVSP)
			{
				this.verticalScrollPosition = _data.savedMateriVSP;
			}

			super.initialize();
		}
		
		override public function dispose():void
		{
			super.dispose();
		}
		
		private function loadData():void
		{
			this.isTopPullViewActive = true;
			Starling.juggler.add(pullView);
			
			if(_data.randomMateri != null)
			{
				if(_data.randomMateri == "teks") jenisVideo = "suara";
				else jenisVideo = "teks";
			}
			else
			{
				//langsung saja ke data kedua
				jenisVideo = "suara";
			}
			
			_data.randomMateri = jenisVideo;

			setTimeout(onCompleteLoadData, 300);
		}

		private function onCompleteLoadData():void
		{
			this.isTopPullViewActive = false;
			Starling.juggler.remove(pullView);
		}
		
		private function updateHandler(event:starling.events.Event):void
		{
			loadData();
		}
		
		private function pullView_pullingHandler(event:starling.events.Event, ratio:Number):void
		{
			var totalFrames:int = pullView.numFrames;
			var frameIndex:int = Math.round(ratio * totalFrames);
			while (frameIndex >= totalFrames)
			{
				frameIndex -= totalFrames;
			}
			pullView.currentFrame = frameIndex;
		}

		private function createLayotGroup():LayoutGroup
		{
			var image:Image = new Image(Main.roundQuadBackground);
			image.pixelSnapping = true;
			image.scale9Grid = new Rectangle(20, 20, 80, 80);

			var vl:VerticalLayout = new VerticalLayout();
			vl.paddingLeft = vl.paddingRight = 10;
			vl.paddingTop = 10;
			vl.paddingBottom = 30;
			
			var lg:LayoutGroup = new LayoutGroup();
			lg.backgroundSkin = image;
			lg.layout = vl;
			lg.layoutData = new AnchorLayoutData(0, 0, NaN, 0);
			return lg;
		}
		
		private function createButton(label:String, name:String):CustomButton
		{
			var button:CustomButton = new CustomButton();
			button.label = label;
			button.name = name;
			button.styleNameList.add("headertest-button");
			button.layoutData = new VerticalLayoutData(100, NaN);
			button.addEventListener(Event.TRIGGERED, button_triggeredHandler);
			return button;
		}

		private function customHeaderFactory():Header
		{
			var header:Header = new Header();
			
			var leftButton:Button = new Button();
			leftButton.styleNameList.add("back-button");
			leftButton.addEventListener(starling.events.Event.TRIGGERED, leftButton_triggeredHandler);
			
			header.leftItems = new <DisplayObject>[leftButton];
			
			var rightButton:Button = new Button();
			rightButton.styleNameList.add("help-button");
			rightButton.addEventListener(starling.events.Event.TRIGGERED, rightButton_triggeredHandler);
			
			header.rightItems = new <DisplayObject>[rightButton];
			
			return header;
		}

		private function button_triggeredHandler(event:Event):void
		{
			Main.klik();
			var materi:String = (event.target as CustomButton).name;

			if(materi == "0")
			{
				Main.mySo.data.menumateritut0 = "sudah";
			}			
			else if(materi == "1")
			{
				Main.mySo.data.menumateritut1 = "sudah";
			}
			else if(materi == "2")
			{
				Main.mySo.data.menumateritut2 = "sudah";
			}
			else if(materi == "3")
			{
				Main.mySo.data.menumateritut3 = "sudah";
				Main.mySo.data.menuutamatut3 = "sudah";
			}

			_data.jenisVideo = jenisVideo;
			_data.referensiMateri = materi;
			_data.savedMateriVSP = this.verticalScrollPosition;
			this.dispatchEventWith(EventType.SHOW_VIDEO_VIEWER);
		}
		
		private function goBack():void
		{
			this.dispatchEventWith(Event.COMPLETE);
		}
		
		private function backHandler():void
		{
			_data.savedMateriVSP = null;
			this.dispatchEventWith(Event.COMPLETE);
		}
		
		private function leftButton_triggeredHandler(event:Event):void
		{
			Main.klik();
			
			_data.savedMateriVSP = null;
			this.dispatchEventWith(Event.COMPLETE);
		}
		
		private function rightButton_triggeredHandler(event:Event):void
		{
			Main.klik();
			
			_data.petunjuk = "petunjukmateri";
			_data.savedMateriVSP = this.verticalScrollPosition;
			this.dispatchEventWith(EventType.SHOW_PETUNJUK);
		}
	}
}