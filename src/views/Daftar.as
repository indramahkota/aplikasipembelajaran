﻿package views
{
	import com.indramahkota.ane.localnotification.LocalNotificationExtension;
	import com.indramahkota.ane.toast.DurationEnum;
	import com.indramahkota.ane.toast.ToastExtension;

	import constants.EventType;
	import constants.Url;

	import feathers.controls.Alert;
	import feathers.controls.Button;
	import feathers.controls.ButtonState;
	import feathers.controls.DragGesture;
	import feathers.controls.ImageLoader;
	import feathers.controls.Label;
	import feathers.controls.LayoutGroup;
	import feathers.controls.PageIndicator;
	import feathers.controls.Screen;
	import feathers.controls.ScrollBarDisplayMode;
	import feathers.controls.ScrollContainer;
	import feathers.controls.ScrollPolicy;
	import feathers.controls.SpinnerList;
	import feathers.controls.TextInput;
	import feathers.controls.TextInputState;
	import feathers.controls.renderers.IListItemRenderer;
	import feathers.data.ArrayCollection;
	import feathers.data.ListCollection;
	import feathers.events.FeathersEventType;
	import feathers.layout.AnchorLayout;
	import feathers.layout.AnchorLayoutData;
	import feathers.layout.Direction;
	import feathers.layout.HorizontalLayout;
	import feathers.layout.HorizontalSpinnerLayout;
	import feathers.skins.ImageSkin;

	import flash.desktop.NativeApplication;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestHeader;
	import flash.net.URLRequestMethod;
	import flash.system.System;
	import flash.utils.setTimeout;

	import starling.animation.DelayedCall;
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Canvas;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.filters.DropShadowFilter;
	import starling.text.TextFormat;
	import starling.textures.RenderTexture;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;

	import utils.CustomButton;
	import utils.CustomListItemRenderer;
	import utils.RoundedQuad;
	
	/**
	 * ...
	 * @author Indra Mahkota
	 */
	public class Daftar extends Screen
	{
		private var alert:Alert;
		private var loading:Label;
		
		private var exitHandlerBoolean:Boolean = false;
		private var exitoastHandlerBoolean:Boolean = false;
		
		private var spinnerList:SpinnerList;
		private var tombolSelanjutnya:Button;
		private var inputNamaLengkap:TextInput;
		
		private var tombolDaftar:Button;
		private var inputNamaPengguna:TextInput;
		private var inputKataSandi:TextInput;

		private var indicator:PageIndicator;
		private var scrollerContainer:ScrollContainer;
		
		private var layoutGroupA:LayoutGroup;
		private var layoutGroupB:LayoutGroup;
		
		private var atlasclick:TextureAtlas;
		private var clickmc:MovieClip;
		private var tween:Tween;

		private var upTextureBtn:Texture;
		private var downTextureBtn:Texture;

		private var defaultTexture:Texture;
		private var focusTexture:Texture;
		private var errorTexture:Texture;
		
		private var textFormat:TextFormat = new TextFormat("SourceSansPro", 16);
		
		public function Daftar()
		{
			super();
		}
		
		override protected function initialize():void
		{
			this.layout = new AnchorLayout();
			this.backButtonHandler = exitHandler;
			
			scrollerContainer = new ScrollContainer();
			scrollerContainer.snapToPages = true;
			scrollerContainer.touchable = false;
			scrollerContainer.width = stage.stageWidth;
			scrollerContainer.height = stage.stageHeight;
			scrollerContainer.pageWidth = stage.stageWidth;
			scrollerContainer.pageHeight = stage.stageHeight;
			scrollerContainer.layout = new HorizontalLayout();
			scrollerContainer.verticalScrollPolicy = ScrollPolicy.OFF;
			scrollerContainer.scrollBarDisplayMode = ScrollBarDisplayMode.NONE;
			scrollerContainer.addEventListener(FeathersEventType.BEGIN_INTERACTION, startDrag);
			scrollerContainer.addEventListener(FeathersEventType.END_INTERACTION, stopDrag);
			this.addChild(scrollerContainer);

			if(Main.mySo.data.daftartut == "sudah")
				scrollerContainer.touchable = true;
			
			indicator = new PageIndicator();
			indicator.pageCount = 2;
			indicator.touchable = false;
			indicator.gap = indicator.padding = 4;
			indicator.direction = Direction.HORIZONTAL;
			indicator.layoutData = new AnchorLayoutData(NaN, NaN, 20, NaN, 0, NaN);
			indicator.normalSymbolFactory = function():DisplayObject
			{
				var image:Image = new Image(Main.getImage("page_disabled"));
				image.scale = 0.5;
				return image;
			};
			indicator.selectedSymbolFactory = function():DisplayObject
			{
				var image:Image = new Image(Main.getImage("page_selected"));
				image.scale = 0.5;
				return image;
			};
			this.addChild(indicator);
			
			layoutGroupA = new LayoutGroup();
			layoutGroupA.layout = new AnchorLayout();
			layoutGroupA.width = stage.stageWidth;
			layoutGroupA.height = stage.stageHeight;
			
			layoutGroupB = new LayoutGroup();
			layoutGroupB.layout = new AnchorLayout();
			layoutGroupB.width = stage.stageWidth;
			layoutGroupB.height = stage.stageHeight;
			
			var avatarMask:Canvas = new Canvas();
			avatarMask.drawCircle(60, 60, 60);
			var num:Number = (stage.stageWidth - 120) / 2;
			
			spinnerList = new SpinnerList();
			spinnerList.mask = avatarMask;
			spinnerList.layout = new HorizontalSpinnerLayout();
			spinnerList.layoutData = new AnchorLayoutData(NaN, num, NaN, num, 0, -130);
			spinnerList.dataProvider = new ArrayCollection([
				{thumbnail: Main.getImage("avatar_1s")}, 
				{thumbnail: Main.getImage("avatar_2s")}, 
				{thumbnail: Main.getImage("avatar_3s")}, 
				{thumbnail: Main.getImage("avatar_4s")}, 
				{thumbnail: Main.getImage("avatar_5s")}, 
				{thumbnail: Main.getImage("avatar_6s")}, 
				{thumbnail: Main.getImage("avatar_7s")}, 
				{thumbnail: Main.getImage("avatar_8s")}, 
				{thumbnail: Main.getImage("avatar_9s")}
			]);
			spinnerList.itemRendererFactory = function():IListItemRenderer
			{
				var itemRenderer:CustomListItemRenderer = new CustomListItemRenderer();
				itemRenderer.iconSourceField = "thumbnail";
				itemRenderer.hasLabelTextRenderer = false;
				itemRenderer.iconLoaderFactory = function():ImageLoader
				{
					var loader:ImageLoader = new ImageLoader();
					loader.pixelSnapping = true;
					loader.setSize(100, 100);
					return loader;
				}
				itemRenderer.padding = 10;
				itemRenderer.gap = 0;
				return itemRenderer;
			};
			layoutGroupA.addChild(spinnerList);

			defaultTexture = inputBackgroundSkin(0xd2d2d2);
			focusTexture = inputBackgroundSkin(0x00bfff);
			errorTexture = inputBackgroundSkin(0xff0000);
			
			var bgNamaLengkap:ImageSkin = new ImageSkin(defaultTexture);
			bgNamaLengkap.setTextureForState(TextInputState.FOCUSED, focusTexture);
			bgNamaLengkap.setTextureForState(TextInputState.ERROR, errorTexture);
			bgNamaLengkap.scale9Grid = new Rectangle(2, 2, 46, 46);

			var bgNamaPengguna:ImageSkin = new ImageSkin(defaultTexture);
			bgNamaPengguna.setTextureForState(TextInputState.FOCUSED, focusTexture);
			bgNamaPengguna.setTextureForState(TextInputState.ERROR, errorTexture);
			bgNamaPengguna.scale9Grid = new Rectangle(2, 2, 46, 46);

			var bgKataSandi:ImageSkin = new ImageSkin(defaultTexture);
			bgKataSandi.setTextureForState(TextInputState.FOCUSED, focusTexture);
			bgKataSandi.setTextureForState(TextInputState.ERROR, errorTexture);
			bgKataSandi.scale9Grid = new Rectangle(2, 2, 46, 46);

			var iconNamaLengkap:ImageSkin = new ImageSkin(Main.getImage("penggunaicon"));
			iconNamaLengkap.width = iconNamaLengkap.height = 18;
			iconNamaLengkap.alpha = 0.5;
			
			var iconPengguna:ImageSkin = new ImageSkin(Main.getImage("penggunaicon"));
			iconPengguna.width = iconPengguna.height = 18;
			iconPengguna.alpha = 0.5;
			
			var iconPassword:ImageSkin = new ImageSkin(Main.getImage("passwordicon"));
			iconPassword.width = iconPassword.height = 18;
			iconPassword.alpha = 0.5;
			
			inputNamaLengkap = new TextInput();
			inputNamaLengkap.prompt = "Nama Lengkap";
			inputNamaLengkap.maxChars = 25;
			inputNamaLengkap.restrict = "a-zA-Z ";
			inputNamaLengkap.fontStyles = textFormat;
			inputNamaLengkap.promptFontStyles = textFormat;
			inputNamaLengkap.width = stage.stageWidth / 2;
			inputNamaLengkap.defaultIcon = iconNamaLengkap;
			inputNamaLengkap.backgroundSkin = bgNamaLengkap;

			inputNamaLengkap.layoutData = new AnchorLayoutData(NaN, NaN, NaN, NaN, 0, -20);
			inputNamaLengkap.addEventListener(starling.events.Event.CHANGE, inputNamaLengkapUpdate);
			inputNamaLengkap.addEventListener(FeathersEventType.ENTER, inputNamaLengkaponEnter);
			layoutGroupA.addChild(inputNamaLengkap);

			upTextureBtn = createlightupBackground();
			downTextureBtn = createlightdownBackground();

			var lightSkinS:ImageSkin = new ImageSkin(upTextureBtn);
			lightSkinS.setTextureForState(ButtonState.DOWN, downTextureBtn);
			lightSkinS.scale9Grid = new Rectangle(20, 20, 80, 80);
			
			tombolSelanjutnya = new Button();
			tombolSelanjutnya.height = 65;
			tombolSelanjutnya.label = "SELANJUTNYA";
			tombolSelanjutnya.defaultSkin = lightSkinS;
			tombolSelanjutnya.width = stage.stageWidth / 2 + 24;
			tombolSelanjutnya.styleNameList.add("light-button");
			tombolSelanjutnya.layoutData = new AnchorLayoutData(NaN, NaN, NaN, NaN, 0, 60);
			tombolSelanjutnya.addEventListener(starling.events.Event.TRIGGERED, selanjutnya);
			layoutGroupA.addChild(tombolSelanjutnya);
			
			var tombolMasuk:CustomButton = new CustomButton();
			tombolMasuk.fontStyles = textFormat;
			tombolMasuk.label = "klik disini jika sudah ada akun!";
			
			tombolMasuk.validate();
			
			var paddingLR:Number = ((stage.stageWidth - tombolMasuk.width) / 2) - 2;
			tombolMasuk.layoutData = new AnchorLayoutData(NaN, paddingLR, NaN, paddingLR, 0, 130);
			tombolMasuk.addEventListener(starling.events.Event.TRIGGERED, masukscreen);
			layoutGroupA.addChild(tombolMasuk);
			
			inputNamaPengguna = new TextInput();
			inputNamaPengguna.prompt = "Nama Pengguna";
			inputNamaPengguna.maxChars = 25;
			inputNamaPengguna.restrict = "a-zA-Z0-9 ";
			inputNamaPengguna.fontStyles = textFormat;
			inputNamaPengguna.promptFontStyles = textFormat;
			inputNamaPengguna.width = stage.stageWidth / 2;
			inputNamaPengguna.defaultIcon = iconPengguna;
			inputNamaPengguna.backgroundSkin = bgNamaPengguna;
			inputNamaPengguna.layoutData = new AnchorLayoutData(NaN, NaN, NaN, NaN, 0, -70);
			inputNamaPengguna.addEventListener(FeathersEventType.ENTER, inputNamaPenggunaonEnter);
			inputNamaPengguna.addEventListener(starling.events.Event.CHANGE, inputNamaPenggunaUpdate);
			layoutGroupB.addChild(inputNamaPengguna);
			
			inputKataSandi = new TextInput();
			inputKataSandi.prompt = "Kata Sandi";
			inputKataSandi.maxChars = 25;
			inputKataSandi.restrict = "a-zA-Z0-9 ";
			inputKataSandi.fontStyles = textFormat;
			inputKataSandi.promptFontStyles = textFormat;
			inputKataSandi.width = stage.stageWidth / 2;
			inputKataSandi.defaultIcon = iconPassword;
			inputKataSandi.backgroundSkin = bgKataSandi;
			inputKataSandi.layoutData = new AnchorLayoutData(NaN, NaN, NaN, NaN, 0, -20);
			inputKataSandi.addEventListener(FeathersEventType.ENTER, inputKataSandionEnter);
			inputKataSandi.addEventListener(starling.events.Event.CHANGE, inputKataSandiUpdate);
			layoutGroupB.addChild(inputKataSandi);

			var lightSkinD:ImageSkin = new ImageSkin(upTextureBtn);
			lightSkinD.setTextureForState(ButtonState.DOWN, downTextureBtn);
			lightSkinD.scale9Grid = new Rectangle(20, 20, 80, 80);
			
			tombolDaftar = new Button();
			tombolDaftar.height = 65;
			tombolDaftar.label = "DAFTAR";
			tombolDaftar.defaultSkin = lightSkinD;
			tombolDaftar.width = stage.stageWidth / 2 + 24;
			tombolDaftar.styleNameList.add("light-button");
			tombolDaftar.layoutData = new AnchorLayoutData(NaN, NaN, NaN, NaN, 0, 60);
			tombolDaftar.addEventListener(starling.events.Event.TRIGGERED, onTombolDaftarTriggered);
			layoutGroupB.addChild(tombolDaftar);
			
			scrollerContainer.addChild(layoutGroupA);
			scrollerContainer.addChild(layoutGroupB);
			
			this.addEventListener(FeathersEventType.TRANSITION_IN_COMPLETE, startTutorial)
			super.initialize();
		}

		private function inputBackgroundSkin(color:uint):Texture
		{
			var quad:Quad = new Quad(50, 2, color);
			var matrix:Matrix = new Matrix();
			matrix.translate(0, 48);

			var rendertxture:RenderTexture = new RenderTexture(50, 50);
			rendertxture.draw(quad, matrix);
			return rendertxture;
		}

		private function onTombolDaftarTriggered(event:starling.events.Event):void
		{
			Main.klik();
			validasiInput();
		}

		private function inputNamaLengkaponEnter(event:starling.events.Event):void
		{
			trace("inputNamaLengkaponEnter");
			scrollerContainer.scrollToPageIndex(1, 0, 0.5);
			setTimeout(function():void
			{
				inputNamaPengguna.setFocus();
			}, 650);
		}

		private function inputNamaPenggunaonEnter(event:starling.events.Event):void
		{
			trace("inputNamaPenggunaonEnter");
			inputKataSandi.setFocus();
		}

		private function inputKataSandionEnter(event:starling.events.Event):void
		{
			trace("inputKataSandionEnter");
			inputKataSandi.clearFocus();
			validasiInput();
		}

		private function startTutorial(event:starling.events.Event):void
		{
			if(Main.mySo.data.daftartut != undefined) return;

			atlasclick = Main.assets.getTextureAtlas("click");
			clickmc = new MovieClip(atlasclick.getTextures("clickanimation_"));
			clickmc.touchable = false;
			clickmc.addEventListener(starling.events.Event.COMPLETE, clickComplete);
			clickmc.scale = 50 / clickmc.width;
			clickmc.x = ((stage.stageWidth - clickmc.width) / 2) + 40;
			clickmc.y = ((stage.stageHeight - clickmc.height) / 2) - 130;
			this.addChild(clickmc);
			Starling.juggler.add(clickmc);

			var next1Tween:Tween = new Tween(clickmc, 1);
			next1Tween.delay = 1;
			next1Tween.moveTo((stage.stageWidth - clickmc.width) / 2, ((stage.stageHeight - clickmc.height) / 2) - 20);
			next1Tween.onComplete = function():void
			{
				inputNamaLengkap.setFocus();
				setTimeout(function():void
				{
					Main.mySo.data.daftartut = "sudah";
					scrollerContainer.touchable = true;
					Starling.juggler.remove(clickmc);
					clickmc.removeFromParent(true);
					clickmc = null;
				}, 1000);
				Starling.juggler.remove(next1Tween);
			}

			function clickComplete():void
			{
				clickmc.stop();
				Starling.juggler.remove(clickmc);

				tween = new Tween(clickmc, 1);
				tween.moveTo(clickmc.x - 80, clickmc.y);
				tween.nextTween = next1Tween;
				tween.onComplete = function():void
				{
					spinnerList.scrollToPageIndex(1, 0.5);
					Starling.juggler.remove(tween);
				}
				Starling.juggler.add(tween);
			}
		}
		
		override public function dispose():void
		{
			textFormat = null;

			downTextureBtn.dispose();
			downTextureBtn = null;

			upTextureBtn.dispose();
			upTextureBtn = null;

			errorTexture.dispose();
			errorTexture = null;

			focusTexture.dispose();
			focusTexture = null;

			defaultTexture.dispose();
			defaultTexture = null;

			System.pauseForGCIfCollectionImminent(0);
			System.gc();

			super.dispose();
		}
		
		private function inputNamaLengkapUpdate():void
		{
			var nama:String = inputNamaLengkap.text;
			if(nama.charAt(0) == " ")
			{
				inputNamaLengkap.text = "";
				return;
			}
			var onlyonespace:RegExp = / {1,}/gi;			
			nama = nama.replace(onlyonespace, " ");
			inputNamaLengkap.text = nama;
		}
		
		private function inputNamaPenggunaUpdate():void
		{
			var nama:String = inputNamaPengguna.text;
			if(nama.charAt(0) == " ")
			{
				inputNamaPengguna.text = "";
				return;
			}
			var onlyonespace:RegExp = / {1,}/gi;			
			nama = nama.replace(onlyonespace, " ");
			inputNamaPengguna.text = nama;
		}

		private function inputKataSandiUpdate():void
		{
			var nama:String = inputKataSandi.text;
			if(nama.charAt(0) == " ")
			{
				inputKataSandi.text = "";
				return;
			}
			var onlyonespace:RegExp = / {1,}/gi;			
			nama = nama.replace(onlyonespace, " ");
			inputKataSandi.text = nama;
		}

		private function startDrag(event:starling.events.Event):void { }
		
		private function stopDrag(event:starling.events.Event):void
		{
			setTimeout(function():void
			{
				indicator.selectedIndex = scrollerContainer.horizontalPageIndex;
			}, 150);
		}
		
		private function selanjutnya():void
		{
			Main.klik();
			scrollerContainer.scrollToPageIndex(1, 0, 0.5);
			indicator.selectedIndex = 1;
		}
		
		private function masukscreen():void
		{
			this.dispatchEventWith(EventType.SHOW_MASUK_SCREEN);
		}

		private function validasiInput():void
		{
			tombolDaftar.isEnabled = false;

			if(inputNamaPengguna.text.indexOf(" ") >= 0 && inputKataSandi.text.indexOf(" ") >= 0)
			{
				inputNamaPengguna.errorString = "";
				inputKataSandi.errorString = "";
				createLoadingLabel("Karakter 'spasi' tidak diperbolehkan.");
				return;
			}

			if(inputNamaPengguna.text.indexOf(" ") >= 0)
			{
				inputNamaPengguna.errorString = "";
				createLoadingLabel("Karakter 'spasi' tidak diperbolehkan.");
				return;
			}
			
			if(inputKataSandi.text.indexOf(" ") >= 0)
			{
				inputKataSandi.errorString = "";
				createLoadingLabel("Karakter 'spasi' tidak diperbolehkan.");
				return;
			}

			if(inputKataSandi.errorString != null)
			{
				inputKataSandi.errorString = null;
			}

			if(inputNamaPengguna.errorString != null)
			{
				inputNamaPengguna.errorString = null;
			}
			
			if(inputNamaLengkap.text.length > 2 && inputNamaPengguna.text.length > 2 && inputKataSandi.text.length > 2)
			{
				createLoadingLabel("Mohon tunggu...");
				getData(Url.FIREBASE_DATA_PENGGUNA_URL);
			}
			else
			{
				tombolDaftar.isEnabled = true;

				if(inputNamaLengkap.text.length < 4) inputNamaLengkap.errorString = "";
				if(inputNamaPengguna.text.length < 4) inputNamaPengguna.errorString = "";
				if(inputKataSandi.text.length < 4) inputKataSandi.errorString = "";

				createLoadingLabel("Input harus lebih dari tiga karakter...");
			}
		}

		private function getData(url:String):void
		{
			var header:URLRequestHeader = new URLRequestHeader("Content-Type", "application/json");
			
			var request:URLRequest = new URLRequest(url);
			request.method = URLRequestMethod.GET;
			request.requestHeaders.push(header);

			var loader:URLLoader = new URLLoader();
			loader.addEventListener(flash.events.Event.COMPLETE, getDataCompleteHAndler);
			loader.addEventListener(IOErrorEvent.IO_ERROR, errorHandler);
			loader.load(request);
		}

		private function getDataCompleteHAndler(event:flash.events.Event):void
		{
			event.currentTarget.removeEventListener(flash.events.Event.COMPLETE, getDataCompleteHAndler);

			createLoadingLabel("Memeriksa nama pengguna...");

			var rawData:Object = JSON.parse(event.currentTarget.data);
			for(var parentKey:String in rawData)
			{
				for(var childKey:String in rawData[parentKey])
				{
					if(inputNamaPengguna.text == rawData[parentKey][childKey])
					{
						tombolDaftar.isEnabled = true;
						createLoadingLabel("Nama pengguna sudah digunakan...");
						return;
					}
				}
			}

			postData(Url.FIREBASE_DATA_PENGGUNA_URL);
		}

		private function postData(url:String):void
		{
			createLoadingLabel("Mendaftarkan nama pengguna...");

			var myObject:Object = new Object();
			myObject.namaLengkap = inputNamaLengkap.text;
			myObject.namaPengguna = inputNamaPengguna.text;
			myObject.kataSandi = inputKataSandi.text;
			myObject.visual = "kosong";
			myObject.auditorial = "kosong";
			myObject.kinestetik = "kosong";
			myObject.dominan = "kosong";
			myObject.avatarPengguna = ubahNilai(spinnerList.selectedIndex);
			myObject.indeksAvatar = String(spinnerList.selectedIndex);
			myObject.nilai = "kosong";
			myObject.status = "aktif";
			myObject.waktu = new Date();

			var request:URLRequest = new URLRequest(url);
			request.data = JSON.stringify(myObject);
			request.method = URLRequestMethod.POST;

			var loader:URLLoader = new URLLoader();
			loader.addEventListener(flash.events.Event.COMPLETE, postDataCompleteHAndler);
			loader.addEventListener(IOErrorEvent.IO_ERROR, errorHandler);
			loader.load(request);
		}

		private function postDataCompleteHAndler(event:flash.events.Event):void
		{
			tombolDaftar.isEnabled = true;
			createLoadingLabel("Anda telah terdaftar...");
			event.currentTarget.removeEventListener(flash.events.Event.COMPLETE, postDataCompleteHAndler);

			var rawData:Object = JSON.parse(event.currentTarget.data);
			for(var parentKey:String in rawData)
			{
				trace(parentKey, rawData[parentKey]);
				Main.mySo.data.uid = rawData[parentKey];
			}

			Main.mySo.data.sessions = "true";
			
			Main.mySo.data.namaLengkap = inputNamaLengkap.text;
			Main.mySo.data.namaPengguna = inputNamaPengguna.text;
			Main.mySo.data.kataSandi = inputKataSandi.text;
			Main.mySo.data.avatar = ubahNilai(spinnerList.selectedIndex);
			Main.mySo.data.indexAvatar = String(spinnerList.selectedIndex);
			
			Main.mySo.data.nilai = "kosong";
			Main.mySo.data.dominan = "kosong";
			
			Main.profileName.text = Main.firstLetterUpperCase(inputNamaLengkap.text + "\n nilai: kosong");
			Main.profileImage.source = Main.getImage(ubahNilai(spinnerList.selectedIndex));

			const txt:String = "Selamat datang, " + Main.firstLetterUpperCase(Main.mySo.data.namaLengkap) + ".";
			showNotification(txt);
			
			next();
		}

		private function errorHandler(event:IOErrorEvent):void
		{
			tombolDaftar.isEnabled = true;
			trace(event.currentTarget.data);
			if(event.currentTarget.data != "")
			{
				createLoadingLabel(event.currentTarget.data);
			}
			else
			{
				createLoadingLabel("Tidak terkoneksi internet...");
			
				alert = Alert.show("Apakah Anda ingin memainkan aplikasi ini secara offline?", "Pesan", new ListCollection([{label: "YA"}, {label: "TIDAK"}]));
				
				alert.addEventListener(starling.events.Event.CLOSE, function(event:starling.events.Event, data:Object):void
				{
					if(data.label == "YA")
					{
						Main.klik();
						alert.removeFromParent(true);
						alert = null;
						
						Main.keluarSession();
						
						Main.mySo.data.nilai = "kosong";
						Main.mySo.data.dominan = "kosong";
						
						Main.mySo.data.namaLengkap = inputNamaLengkap.text;
						Main.mySo.data.avatar = ubahNilai(spinnerList.selectedIndex);
						Main.profileName.text = Main.firstLetterUpperCase(inputNamaLengkap.text + "\n nilai: kosong");
						Main.profileImage.source = Main.getImage(ubahNilai(spinnerList.selectedIndex));
						
						showNotification("Anda belum terdaftar!!");
						next();
					}
					else
					{
						Main.klik();
						alert.removeFromParent(true);
						alert = null;
					}
				});
			}
		}
		
		private function createLoadingLabel(string:String):void
		{
			if(loading) loading.removeFromParent(true);
			
			var quad:Quad = new Quad(10, 10, 0x55D088);
			
			loading = new Label();
			loading.text = string;
			loading.minHeight = 20;
			loading.backgroundSkin = quad;
			loading.styleNameList.add("loading-center-label-bg");
			loading.layoutData = new AnchorLayoutData(NaN, 0, 0, 0, NaN, NaN);
			this.addChild(loading);
		}
		
		private function ubahNilai(nilai:int):String
		{
			var string:String;
			string = "avatar_" + String(nilai + 1) + "s";
			return string;
		}
		
		private function next():void
		{
			Main.changeScreenUsingList = true;
			var drawersEvent:starling.events.Event = new starling.events.Event(EventType.UBAH_DRAWERS, true, DragGesture.EDGE);
			dispatchEvent(drawersEvent);
			this.dispatchEventWith(EventType.SHOW_MENU_UTAMA);
		}
		
		private function exitHandler():void
		{
			exitHandlerBoolean = !exitHandlerBoolean;
			if(exitHandlerBoolean || exitoastHandlerBoolean)
			{
				if(!exitoastHandlerBoolean)
				{
					if(ToastExtension.isSupported)
					{
						var toast:ToastExtension = new ToastExtension();
						toast.setText("Tekan sekali lagi untuk keluar");
						toast.setDuration(DurationEnum.LENGTH_LONG);
						toast.show();
					}
					var delayedCall:DelayedCall = new DelayedCall(function():void
					{
						Starling.juggler.remove(delayedCall);
						exitHandlerBoolean = !exitHandlerBoolean;
					}, 3);
					Starling.juggler.add(delayedCall);
				}
				else
				{
					exitHandlerBoolean = !exitHandlerBoolean;
				}
			}
			else
			{
				NativeApplication.nativeApplication.exit();
			}
		}

		private function showNotification(str:String):void
		{
			if(LocalNotificationExtension.isSupported)
			{
				var notif:LocalNotificationExtension = new LocalNotificationExtension();
				if(inputNamaLengkap.text.length > 0)
					notif.show("Pesan", str, "Pesan baru");
				else
					notif.show();
			}
		}

		private function createlightdownBackground():Texture
		{
			var downdBg:RoundedQuad = new RoundedQuad(5, 100, 100, 0xd34836);
			downdBg.filter = new DropShadowFilter(4, Math.PI / 2, 0, 0.25, 2, 1);
			downdBg.x = downdBg.y = 10;

			var downuBg:RoundedQuad = new RoundedQuad(5, 96, 96, 0);
			downuBg.x = downuBg.y = 12;

			var spr:Sprite = new Sprite();
			spr.addChild(downdBg);
			spr.addChild(downuBg);
			
			var bnd:Rectangle = spr.bounds;
			var rendertxture:RenderTexture = new RenderTexture(bnd.width + 20, bnd.height + 20);
			rendertxture.draw(spr);
			return rendertxture;
		}

		private function createlightupBackground():Texture
		{
			var downdBg:RoundedQuad = new RoundedQuad(5, 100, 100, 0xffffff);
			downdBg.x = downdBg.y = 10;

			var downuBg:RoundedQuad = new RoundedQuad(5, 96, 96, 0);
			downuBg.x = downuBg.y = 12;

			var spr:Sprite = new Sprite();
			spr.addChild(downdBg);
			spr.addChild(downuBg);
			
			var bnd:Rectangle = spr.bounds;
			var rendertxture:RenderTexture = new RenderTexture(bnd.width + 20, bnd.height + 20);
			rendertxture.draw(spr);
			return rendertxture;
		}
	}
}