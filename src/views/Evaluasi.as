package views
{
	import constants.EventType;
	import constants.Url;

	import feathers.controls.Button;
	import feathers.controls.Header;
	import feathers.controls.ImageLoader;
	import feathers.controls.Label;
	import feathers.controls.LayoutGroup;
	import feathers.controls.List;
	import feathers.controls.PanelScreen;
	import feathers.controls.ScrollPolicy;
	import feathers.controls.renderers.DefaultListItemRenderer;
	import feathers.data.ListCollection;
	import feathers.layout.AnchorLayout;
	import feathers.layout.AnchorLayoutData;
	import feathers.layout.FlowLayout;
	import feathers.layout.HorizontalAlign;
	import feathers.layout.VerticalAlign;
	import feathers.layout.VerticalLayout;
	import feathers.layout.VerticalLayoutData;

	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.geom.Rectangle;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.system.System;

	import renderers.EvaluasiRenderer;

	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.events.Event;
	import starling.textures.SubTexture;

	import utils.CustomButton;
	
	/**
	 * ...
	 * @author Indra Mahkota
	 */
	public class Evaluasi extends PanelScreen
	{
		private var curState:String;
		
		private var soalList:List;
		private var jawabanList:List;
		private var arraySoal:Array = new Array();
		private var jawabanArray:Array = new Array();
		
		private var nomorSoal:Label;
		private var infoLabel:Label;
		
		private var benar:int = 0;
		private var salah:int = 0;
		private var nomor:int = 1;
		private var totalSoal:int;
		
		private var layoutgroupKet:LayoutGroup;

		private var topRoundedRect:SubTexture;
		private var botomRoundedRect:SubTexture;

		private var topRect:Rectangle = new Rectangle(0, 0, 120, 60);
		private var bottomRect:Rectangle = new Rectangle(0, 60, 120, 60);
		
		protected var _data:NavigatorData;
		
		public function Evaluasi()
		{
			super();
		}
		
		public function get data():NavigatorData  { return _data; }
		
		public function set data(value:NavigatorData):void  { _data = value; }
		
		override protected function initialize():void
		{
			this.title = "Evaluasi";
			this.layout = new AnchorLayout();
			this.backButtonHandler = backHandler;
			this.headerFactory = customHeaderFactory;

			topRoundedRect = new SubTexture(Main.roundQuadBackground, topRect);
			botomRoundedRect = new SubTexture(Main.roundQuadBackground, bottomRect);
			
			layoutgroupKet = createLayotGroup();
			layoutgroupKet.layout = new AnchorLayout();
			layoutgroupKet.layoutData = new AnchorLayoutData(0, 0, NaN, 0, NaN, NaN);
			
			nomorSoal = new Label();
			nomorSoal.text = "No. " + nomor;
			nomorSoal.paddingTop = 20;
			nomorSoal.layoutData = new AnchorLayoutData(NaN, NaN, NaN, 30, NaN, NaN);
			layoutgroupKet.addChild(nomorSoal);
			
			infoLabel = new Label();
			infoLabel.text = "Benar: " + benar + "  Salah: " + salah;
			infoLabel.paddingTop = 20;
			infoLabel.layoutData = new AnchorLayoutData(NaN, 30, NaN, NaN, NaN, NaN);
			layoutgroupKet.addChild(infoLabel);

			var lineKet:Quad = new Quad(stage.stageWidth - 20, 1, 0xEDEDF0);
			lineKet.x = 10;
			lineKet.y = 60;
			layoutgroupKet.addChild(lineKet);
			
			var layoutForList:VerticalLayout = new VerticalLayout();
			layoutForList.hasVariableItemDimensions = true;
			
			soalList = new List();
			soalList.addEventListener(EventType.PENYIMPANAN, simpanData);
			soalList.addEventListener(EventType.SELESAI, soalSelesai);
			
			soalList.horizontalScrollPolicy = ScrollPolicy.OFF;
			soalList.verticalScrollPolicy = ScrollPolicy.OFF;
			soalList.itemRendererType = EvaluasiRenderer;
			soalList.layout = layoutForList;
			
			soalList.typicalItem = {"no": "1", "soal": "A", "jawaban": "A", "urutan": "1"};
			soalList.layoutData = new AnchorLayoutData(0, 0, 0, 0, NaN, NaN);
			
			this.addChild(soalList);
			this.addChild(layoutgroupKet);
			
			//--------------------------------------
			// Konfigurasi
			//--------------------------------------
			
			if(_data.curState)
			{
				jawabanArray = _data.jawabanArray;
				soalSelesai();
			}
			else
			{
				if(_data.arraySoal)
				{
					jawabanArray = _data.jawabanArray;
					soalList.dataProvider = _data.arraySoal;
					benar = _data.benar;
					salah = _data.salah;
					nomor = _data.nomor;
					totalSoal = _data.totalSoal;
				}
				else
				{
					arraySoal = Main.getJsonAssets("evaluasi");
					
					arraySoal.sort(function(a:*, b:*):int
					{
						return (Math.random() > .5) ? 1 : -1;
					});
					
					var i:uint, len:int = arraySoal.length;
					totalSoal = len;
					
					for (i; i < len; ++i)
					{
						arraySoal[i].urutan = String(i + 1);
						arraySoal[i].totalSoal = len;
					}
					
					soalList.dataProvider = new ListCollection(arraySoal);
					arraySoal = null;
				}
				
				nomorSoal.text = "No. " + nomor + "/" + totalSoal;
				infoLabel.text = "Benar: " + benar + "  Salah: " + salah;
			}
			
			super.initialize();
		}
		
		override public function dispose():void
		{
			if(jawabanArray || arraySoal)
			{
				jawabanArray = null;
				arraySoal = null;
			}
			
			System.pauseForGCIfCollectionImminent(0);
			System.gc();
			
			super.dispose();
		}
		
		private function simpanData(event:starling.events.Event):void
		{
			jawabanArray.push(event.data);
			nomor++;
			
			if(nomor <= totalSoal) nomorSoal.text = "No. " + nomor + "/" + totalSoal;
			
			if(event.data.keterangan == "benar")
				benar++;
			else
				salah++;
			
			infoLabel.text = "Benar: " + benar + "  Salah: " + salah;
		}
		
		private function soalSelesai():void
		{
			this.removeChildren(0, -1, true);
			
			soalList.removeEventListener(EventType.PENYIMPANAN, simpanData);
			soalList.removeEventListener(EventType.SELESAI, soalSelesai);
			
			var layoutatas:VerticalLayout = new VerticalLayout();
			this.layout = layoutatas;
			
			curState = "hasil";
			soalList.dataProvider = null;
			soalList.removeFromParent(true);
			
			var total:int, jlhSoal:int, keteranganIcon:String;
			var i:uint, len:int = jawabanArray.length;

			//index untuk membentuk posisi 3 kolom
			var fixedArr:Array = [];
			var index:Array = [1, 6, 11, 2, 7, 12, 3, 8, 13, 4, 9, 14, 5, 10, 15];
			
			for (i; i < len; ++i)
			{
				fixedArr.push(jawabanArray[index[i] - 1])
				var obj:Object = jawabanArray[i];
				total += int(obj.nilai);
				jlhSoal++;
			}
			
			var nilai:Number = (total / jlhSoal) * 100;
			keteranganIcon = hitungNilai(nilai);
			
			if(!_data.curState)
			{
				var helper:String = nilai.toFixed(2);
				var intHelper:int = helper.indexOf(".");
				
				if(helper.substring(intHelper) == ".00")
				{
					helper = helper.slice(0, intHelper);
				}
				else
				{
					helper = helper.replace(".", ",");
				}
				
				Main.mySo.data.nilai = helper;
				Main.profileName.text = Main.firstLetterUpperCase(Main.mySo.data.namaLengkap + "\n nilai: " + helper);
				
				postData(Url.FIREBASE_DATA_NILAI_URL);
				patchData(Url.FIREBASE_PATCH_DATA_PENGGUNA_URL);
			}
			
			var layoutList:FlowLayout = new FlowLayout();
			layoutList.horizontalAlign = HorizontalAlign.CENTER;
			layoutList.paddingTop = 20;

			var jawabanListBg:Image = new Image( topRoundedRect );
			jawabanListBg.pixelSnapping = true;
			jawabanListBg.scale9Grid = new Rectangle(20, 20, 80, 30);
			
			jawabanList = new List();
			jawabanList.touchable = false;
			jawabanList.layout = layoutList;
			jawabanList.layoutData = new VerticalLayoutData(100, NaN);
			jawabanList.backgroundSkin = jawabanListBg;
			this.addChild(jawabanList);
			
			jawabanList.itemRendererFactory = function():DefaultListItemRenderer
			{
				var renderer:DefaultListItemRenderer = new DefaultListItemRenderer();
				renderer.isQuickHitAreaEnabled = true;
				renderer.iconSourceField = "icon";
				renderer.paddingRight = 20;
				renderer.width = 0.99 * (stage.stageWidth - 24) / 3;
				renderer.height = 50;
				
				renderer.iconLoaderFactory = function():ImageLoader
				{
					var loader:ImageLoader = new ImageLoader();
					loader.pixelSnapping = true;
					loader.setSize(20, 20);
					return loader;
				}
				
				renderer.labelFunction = function(item:Object):String
				{
					var string:String;
					if(item.urutan)
						string = item.urutan + ".  " + item.pilihan;
					else
						string = item.nilai;
					return string;
				};
				
				return renderer;
			}
			
			jawabanList.horizontalScrollPolicy = ScrollPolicy.OFF;
			jawabanList.dataProvider = new ListCollection(fixedArr);
			this.addChild(jawabanList);

			var imgS:Image = new Image( botomRoundedRect );
			imgS.pixelSnapping = true;
			imgS.scale9Grid = new Rectangle(20, 10, 80, 30);
			imgS.height = 60;

			var buttonLgLayout:VerticalLayout = new VerticalLayout();
			buttonLgLayout.paddingLeft = buttonLgLayout.paddingRight = 10;
			buttonLgLayout.paddingTop = 10;
			buttonLgLayout.paddingBottom = 20;
			buttonLgLayout.horizontalAlign = HorizontalAlign.CENTER;
			buttonLgLayout.verticalAlign = VerticalAlign.MIDDLE;

			var Buttonlg:LayoutGroup = new LayoutGroup();
			Buttonlg.backgroundSkin = imgS;
			Buttonlg.layout = buttonLgLayout;
			Buttonlg.layoutData = new VerticalLayoutData(100, NaN);
			this.addChild(Buttonlg);
			
			var imgL:ImageLoader = new ImageLoader();
			imgL.source = Main.getImage(keteranganIcon);
			imgL.pixelSnapping = true;
			imgL.setSize(20, 20);

			var button:CustomButton = new CustomButton();
			button.gap = 10;
			button.label = "Nilai yang diperoleh adalah " + nilai.toFixed(2).replace(".", ",");
			button.defaultIcon = imgL;
			button.styleNameList.add("headertest-button");
			Buttonlg.addChild(button);
			
			var jawabanListFadeIn:Tween = new Tween(jawabanList, 0.3);
			jawabanListFadeIn.fadeTo(1);
			Starling.juggler.add(jawabanListFadeIn);
		}
		
		private function hitungNilai(total:Number):String
		{
			var string:String;
			
			if(total >= 90) string = "sangatmemuaskan";
			if(total >= 80 && total < 90) string = "memuaskan";
			if(total >= 70 && total < 80) string = "sedang";
			if(total >= 60 && total < 70) string = "kurang";
			if(total < 60) string = "sangatkurang";
			
			return string;
		}
		
		private function createLayotGroup():LayoutGroup
		{
			var thsimage:Image = new Image( topRoundedRect );
			thsimage.pixelSnapping = true;
			thsimage.scale9Grid = new Rectangle(20, 20, 80, 30);
			thsimage.height = 61;
			
			var lg:LayoutGroup = new LayoutGroup();
			lg.backgroundSkin = thsimage;
			lg.layout = new VerticalLayout();
			lg.layoutData = new VerticalLayoutData(100, NaN);
			return lg;
		}
		
		private function createLabel(text:String):Label
		{
			var label:Label = new Label();
			label.text = text;
			label.padding = 20;
			label.textRendererProperties.wordWrap = true;
			label.layoutData = new VerticalLayoutData(100, NaN);
			return label;
		}

		private function postData(url:String):void
		{
			var myObject:Object = new Object();
			myObject.namaPengguna = Main.mySo.data.namaPengguna;
			myObject.nilai = Main.mySo.data.nilai;
			myObject.waktu = new Date();

			var request:URLRequest = new URLRequest(url);
			request.data = JSON.stringify(myObject);
			request.method = URLRequestMethod.POST;

			var loader:URLLoader = new URLLoader();
			loader.addEventListener(flash.events.Event.COMPLETE, postDataCompleteHAndler);
			loader.addEventListener(IOErrorEvent.IO_ERROR, errorHandler);
			loader.load(request);
		}

		private function patchData(url:String):void
		{
			var uid:String = Main.mySo.data.uid + ".json";
			var patchMethod:String = "?x-http-method-override=PATCH";
			var addition:String = uid + patchMethod;

			var myObject:Object = new Object();
			myObject.nilai = Main.mySo.data.nilai;

			var request:URLRequest = new URLRequest(url + addition);
			request.data = JSON.stringify(myObject);
			request.method = URLRequestMethod.POST;

			var loader:URLLoader = new URLLoader();
			loader.addEventListener(flash.events.Event.COMPLETE, postDataCompleteHAndler);
			loader.addEventListener(IOErrorEvent.IO_ERROR, errorHandler);
			loader.load(request);
		}

		private function postDataCompleteHAndler(event:flash.events.Event):void
		{
			event.currentTarget.removeEventListener(flash.events.Event.COMPLETE, postDataCompleteHAndler);

			var rawData:Object = JSON.parse(event.currentTarget.data);
			for(var parentKey:String in rawData)
			{
				trace(parentKey, rawData[parentKey]);
			}
		}

		private function errorHandler(event:IOErrorEvent):void
		{
			trace(event.currentTarget.data);
		}
		
		private function customHeaderFactory():Header
		{
			var header:Header = new Header();
			
			var leftButton:Button = new Button();
			leftButton.styleNameList.add("back-button");
			leftButton.addEventListener(starling.events.Event.TRIGGERED, goBack);
			
			header.leftItems = new <DisplayObject>[leftButton];
			
			var rightButton:Button = new Button();
			rightButton.styleNameList.add("help-button");
			rightButton.addEventListener(starling.events.Event.TRIGGERED, rightButton_triggeredHandler);
			
			header.rightItems = new <DisplayObject>[rightButton];
			
			return header;
		}
		
		private function backHandler():void
		{
			_data.benar = null;
			_data.salah = null;
			_data.nomor = null;
			_data.totalSoal = null;
			
			_data.curState = null;
			_data.arraySoal = null;
			_data.jawabanArray = null;
			this.dispatchEventWith(starling.events.Event.COMPLETE);
		}
		
		private function goBack():void
		{
			Main.klik();
			
			_data.benar = null;
			_data.salah = null;
			_data.nomor = null;
			_data.totalSoal = null;
			
			_data.curState = null;
			_data.arraySoal = null;
			_data.jawabanArray = null;
			this.dispatchEventWith(starling.events.Event.COMPLETE);
		}
		
		private function rightButton_triggeredHandler(event:starling.events.Event):void
		{
			Main.klik();
			
			_data.benar = benar;
			_data.salah = salah;
			_data.nomor = nomor;
			_data.totalSoal = totalSoal;
			
			_data.curState = curState;
			_data.arraySoal = soalList.dataProvider;
			_data.jawabanArray = jawabanArray;
			_data.petunjuk = "petunjukevaluasi";
			this.dispatchEventWith(EventType.SHOW_PETUNJUK);
		}
	}
}