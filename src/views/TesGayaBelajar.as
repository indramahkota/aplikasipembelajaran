package views
{
	import constants.EventType;
	import constants.Url;

	import feathers.controls.Button;
	import feathers.controls.ButtonState;
	import feathers.controls.Header;
	import feathers.controls.ImageLoader;
	import feathers.controls.Label;
	import feathers.controls.LayoutGroup;
	import feathers.controls.List;
	import feathers.controls.PanelScreen;
	import feathers.controls.ScrollPolicy;
	import feathers.data.ListCollection;
	import feathers.layout.AnchorLayout;
	import feathers.layout.AnchorLayoutData;
	import feathers.layout.VerticalLayout;
	import feathers.layout.VerticalLayoutData;
	import feathers.skins.ImageSkin;

	import flash.events.IOErrorEvent;
	import flash.geom.Rectangle;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.system.System;

	import renderers.TesRenderer;

	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.filters.DropShadowFilter;
	import starling.textures.RenderTexture;
	import starling.textures.Texture;

	import utils.CustomButton;
	import utils.RoundedQuad;
	
	/**
	 * ...
	 * @author Indra Mahkota
	 */
	public class TesGayaBelajar extends PanelScreen
	{
		private var curState:String;
		
		private var soalList:List;
		private var hasilGroup:LayoutGroup;
		
		private var arraySoal:Array = new Array();
		private var arrayHasil:Array = new Array();
		private var jawabanArray:Array = new Array();
		
		private var ulangiButton:Button;
		private var upTextureBtn:Texture;
		private var downTextureBtn:Texture;
		
		protected var _data:NavigatorData;
		
		public function TesGayaBelajar()
		{
			super();
		}
		
		public function get data():NavigatorData  { return _data; }
		
		public function set data(value:NavigatorData):void  { _data = value; }
		
		override protected function initialize():void
		{
			this.title = "Pertanyaan";
			this.layout = new AnchorLayout();
			this.backButtonHandler = backHandler;
			this.headerFactory = customHeaderFactory;
			
			//--------------------------------------
			// Konfigurasi
			//--------------------------------------
			
			arrayHasil = Main.getJsonAssets("hasiltesgayabelajar");
			
			if(_data.curState == "hasil")
			{
				menentukanTextGayaBelajar();
			}
			else if(_data.curState == "pertanyaan")
			{
				jawabanArray = _data.jawabanArray;
				tampilkanSoal(_data.arraySoal);
			}
			else
			{
				if(Main.mySo.data.dominan != "kosong")
				{
					menentukanTextGayaBelajar();
				}
				else
				{
					initSoal();
				}
			}
			
			super.initialize();
		}
		
		override public function dispose():void
		{
			if(arraySoal || arrayHasil || jawabanArray)
			{
				arraySoal = null;
				arrayHasil = null;
				jawabanArray = null;
			}
			
			if(downTextureBtn != null)
			{
				downTextureBtn.dispose();
				downTextureBtn = null;
			}

			if(upTextureBtn != null)
			{
				upTextureBtn.dispose();
				upTextureBtn = null;
			}

			System.pauseForGCIfCollectionImminent(0);
			System.gc();
			
			super.dispose();
		}
		
		private function menentukanTextGayaBelajar():void
		{
			var keteranganText:String;
			
			if(Main.mySo.data.dominan == "visual")
			{
				keteranganText = arrayHasil[0].visual;
			}
			else if(Main.mySo.data.dominan == "auditorial")
			{
				keteranganText = arrayHasil[0].auditorial;
			}
			else
			{
				keteranganText = arrayHasil[0].kinestetik;
			}
			
			tampilkanKeterangan(keteranganText);
		}
		
		private function initSoal():void
		{
			arraySoal = Main.getJsonAssets("tesgayabelajar");
			
			arraySoal.sort(function(a:*, b:*):int
			{
				return (Math.random() > .5) ? 1 : -1;
			});
			
			var i:uint, len:int = arraySoal.length;
			
			for (i; i < len; ++i)
			{
				arraySoal[i].urutan = String(i + 1);
			}
			
			var listCollection:ListCollection = new ListCollection(arraySoal);
			
			tampilkanSoal(listCollection);
		}
		
		private function tampilkanSoal(data:ListCollection):void
		{
			curState = "pertanyaan";
			this.title = "Pertanyaan";
			
			var layoutForList:VerticalLayout = new VerticalLayout();
			layoutForList.hasVariableItemDimensions = true;
			
			soalList = new List();
			soalList.addEventListener(EventType.PENYIMPANAN, simpanData);
			soalList.addEventListener(EventType.SELESAI, soalSelesai);
			
			soalList.horizontalScrollPolicy = ScrollPolicy.OFF;
			soalList.verticalScrollPolicy = ScrollPolicy.OFF;
			soalList.itemRendererType = TesRenderer;
			soalList.layout = layoutForList;
			
			soalList.typicalItem = {"jenis": "visual", "tipe": "positif", "soal": "1. Pertanyaan", "urutan": "1"};
			soalList.layoutData = new AnchorLayoutData(0, 0, 0, 0, NaN, NaN);
			
			soalList.dataProvider = data;
			this.addChild(soalList);
		}
		
		private function tampilkanKeterangan(str:String):void
		{
			curState = "hasil";
			this.title = "Hasil";
			
			var image:Image = new Image(Main.roundQuadBackground);
			image.pixelSnapping = true;
			image.scale9Grid = new Rectangle(20, 20, 80, 80);

			var vl:VerticalLayout = new VerticalLayout();
			vl.paddingLeft = vl.paddingRight = 10;
			vl.paddingTop = 10;
			vl.paddingBottom = 40;
			
			hasilGroup = new LayoutGroup();
			hasilGroup.backgroundSkin = image;
			hasilGroup.layoutData = new AnchorLayoutData(0, 0, NaN, 0, NaN, NaN);
			hasilGroup.layout = vl;
			
			var bt:CustomButton = createButton("Gaya belajar Anda yaitu " + Main.firstLetterUpperCase(Main.mySo.data.dominan));

			var layoutIsi:LayoutGroup = new LayoutGroup();
			layoutIsi.layout = new AnchorLayout();
			layoutIsi.layoutData = new VerticalLayoutData(100, NaN);

			var ima:Image = new Image(Main.getImage(Main.mySo.data.dominan + "img"));

			const scale:Number = 100 / ima.width;
			ima.scale = scale;

			ima.x = 20;
			ima.pixelSnapping = true;
			layoutIsi.addChild(ima);

			var lb:Label = createLabel(str);
			lb.x = ima.width + 20;
			lb.width = stage.stageWidth - (46 + ima.width);
			layoutIsi.addChild(lb);
			
			var layoutTombol:LayoutGroup = new LayoutGroup();
			layoutTombol.layout = new AnchorLayout();
			layoutTombol.layoutData = new VerticalLayoutData(100, NaN);

			upTextureBtn = createlightupBackground();
			downTextureBtn = createlightdownBackground();

			var lightSkin:ImageSkin = new ImageSkin(upTextureBtn);
			lightSkin.setTextureForState(ButtonState.DOWN, downTextureBtn);
			lightSkin.scale9Grid = new Rectangle(20, 20, 80, 80);
			
			ulangiButton = new Button();
			ulangiButton.label = "Ulangi";
			ulangiButton.height = 65;
			ulangiButton.paddingLeft = 20;
			ulangiButton.paddingRight = 20;
			ulangiButton.defaultSkin = lightSkin;
			ulangiButton.addEventListener(starling.events.Event.TRIGGERED, Button_triggeredHandler);
			ulangiButton.layoutData = new AnchorLayoutData(0, 10, NaN, NaN, NaN, NaN);
			ulangiButton.styleNameList.add("light-button");
			layoutTombol.addChild(ulangiButton);
			
			hasilGroup.addChild(bt);
			hasilGroup.addChild(layoutIsi);
			hasilGroup.addChild(layoutTombol);
			this.addChild(hasilGroup);
		}
		
		private function simpanData(event:starling.events.Event):void
		{
			jawabanArray.push(event.data);
		}
		
		private function soalSelesai():void
		{
			soalList.removeEventListener(EventType.PENYIMPANAN, simpanData);
			soalList.removeEventListener(EventType.SELESAI, soalSelesai);
			
			soalList.dataProvider = null;
			this.removeChild(soalList, true);
			
			var visual:int, auditorial:int, kinestetik:int;
			var keterangan:String, gayaBelajarDominan:String;
			
			arrayHasil = Main.getJsonAssets("hasiltesgayabelajar");
			
			var i:uint, len:int = jawabanArray.length;
			
			for (i; i < len; ++i)
			{
				var object:Object = jawabanArray[i];
				
				if(object.jenis == "visual" && object.tipe == "positif")
					visual += nilaiPositif(int(object.pilihan));
				else if(object.jenis == "visual" && object.tipe == "negatif")
					visual += nilaiNegatif(int(object.pilihan));
				else if(object.jenis == "auditorial" && object.tipe == "positif")
					auditorial += nilaiPositif(int(object.pilihan));
				else if(object.jenis == "auditorial" && object.tipe == "negatif")
					auditorial += nilaiNegatif(int(object.pilihan));
				else if(object.jenis == "kinestetik" && object.tipe == "positif")
					kinestetik += nilaiPositif(int(object.pilihan));
				else if(object.jenis == "kinestetik" && object.tipe == "negatif")
					kinestetik += nilaiNegatif(int(object.pilihan));
			}
			
			var vis:Number = visual / 21;
			var audi:Number = auditorial / 30;
			var kine:Number = kinestetik / 27;
			
			if(vis > audi)
			{
				if(vis > kine)
				{
					gayaBelajarDominan = "visual";
					keterangan = arrayHasil[0].visual;
				}
				else
				{
					gayaBelajarDominan = "kinestetik";
					keterangan = arrayHasil[0].kinestetik;
				}
			}
			else if(audi > kine)
			{
				gayaBelajarDominan = "auditorial";
				keterangan = arrayHasil[0].auditorial;
			}
			else
			{
				gayaBelajarDominan = "kinestetik";
				keterangan = arrayHasil[0].kinestetik;
			}
			
			var visTemp:Number = vis * 100;
			var audiTemp:Number = audi * 100;
			var kineTemp:Number = kine * 100;
			
			Main.mySo.data.visual = String(visTemp.toFixed(2));
			Main.mySo.data.auditorial = String(audiTemp.toFixed(2));
			Main.mySo.data.kinestetik = String(kineTemp.toFixed(2));
			
			Main.mySo.data.dominan = gayaBelajarDominan;
			
			tampilkanKeterangan(keterangan);

			postData(Url.FIREBASE_DATA_GAYABELAJAR_URL);
			patchData(Url.FIREBASE_PATCH_DATA_PENGGUNA_URL);
		}

		private function postData(url:String):void
		{
			var myObject:Object = new Object();
			myObject.namaPengguna = Main.mySo.data.namaPengguna;
			myObject.visual = Main.mySo.data.visual;
			myObject.auditorial = Main.mySo.data.auditorial;
			myObject.kinestetik = Main.mySo.data.kinestetik;
			myObject.dominan = Main.mySo.data.dominan;
			myObject.waktu = new Date();

			var request:URLRequest = new URLRequest(url);
			request.data = JSON.stringify(myObject);
			request.method = URLRequestMethod.POST;

			var loader:URLLoader = new URLLoader();
			loader.addEventListener(flash.events.Event.COMPLETE, postDataCompleteHAndler);
			loader.addEventListener(IOErrorEvent.IO_ERROR, errorHandler);
			loader.load(request);
		}

		private function patchData(url:String):void
		{
			var uid:String = Main.mySo.data.uid + ".json";
			var patchMethod:String = "?x-http-method-override=PATCH";
			var addition:String = uid + patchMethod;

			var myObject:Object = new Object();
			myObject.visual = Main.mySo.data.visual;
			myObject.auditorial = Main.mySo.data.auditorial;
			myObject.kinestetik = Main.mySo.data.kinestetik;
			myObject.dominan = Main.mySo.data.dominan;

			var request:URLRequest = new URLRequest(url + addition);
			request.data = JSON.stringify(myObject);
			request.method = URLRequestMethod.POST;

			var loader:URLLoader = new URLLoader();
			loader.addEventListener(flash.events.Event.COMPLETE, postDataCompleteHAndler);
			loader.addEventListener(IOErrorEvent.IO_ERROR, errorHandler);
			loader.load(request);
		}

		private function postDataCompleteHAndler(event:flash.events.Event):void
		{
			event.currentTarget.removeEventListener(flash.events.Event.COMPLETE, postDataCompleteHAndler);

			var rawData:Object = JSON.parse(event.currentTarget.data);
			for(var parentKey:String in rawData)
			{
				trace(parentKey, rawData[parentKey]);
			}
		}

		private function errorHandler(event:IOErrorEvent):void
		{
			trace(event.currentTarget.data);
		}
		
		private function Button_triggeredHandler(event:starling.events.Event):void
		{
			if(hasilGroup)
			{
				hasilGroup.removeFromParent(true);
			}
			
			initSoal();
		}
		
		private function createButton(label:String):CustomButton
		{
			var button:CustomButton = new CustomButton();
			button.label = label;
			button.styleNameList.add("headertest-button");
			button.layoutData = new VerticalLayoutData(100, NaN);
			return button;
		}
		
		private function createLabel(text:String):Label
		{
			var label:Label = new Label();
			label.text = text;
			label.padding = 20;
			label.textRendererProperties.wordWrap = true;
			label.layoutData = new VerticalLayoutData(100, NaN);
			return label;
		}
		
		private function createImageLoader():ImageLoader
		{
			var image:ImageLoader = new ImageLoader();
			image.source = Main.getImage("line_x");
			image.scale9Grid = new Rectangle(1, 1, 48, 48);
			image.paddingLeft = image.paddingRight = 20;
			image.pixelSnapping = true;
			image.layoutData = new VerticalLayoutData(100, NaN);
			return image;
		}
		
		private function nilaiPositif(i:int):int
		{
			var arr:Array = [3, 2, 1, 0];
			
			return arr[i];
		}
		
		private function nilaiNegatif(i:int):int
		{
			var arr:Array = [0, 1, 2, 3];
			
			return arr[i];
		}
		
		private function customHeaderFactory():Header
		{
			var header:Header = new Header();
			
			var leftButton:Button = new Button();
			leftButton.styleNameList.add("back-button");
			leftButton.addEventListener(starling.events.Event.TRIGGERED, goBack);
			
			header.leftItems = new <DisplayObject>[leftButton];
			
			var rightButton:Button = new Button();
			rightButton.styleNameList.add("help-button");
			rightButton.addEventListener(starling.events.Event.TRIGGERED, rightButton_triggeredHandler);
			
			header.rightItems = new <DisplayObject>[rightButton];
			
			return header;
		}
		
		private function backHandler():void
		{
			_data.curState = null;
			_data.arraySoal = null;
			_data.jawabanArray = null;
			this.dispatchEventWith(starling.events.Event.COMPLETE);
		}
		
		private function goBack():void
		{
			Main.klik();
			
			_data.curState = null;
			_data.arraySoal = null;
			_data.jawabanArray = null;
			this.dispatchEventWith(starling.events.Event.COMPLETE);
		}
		
		private function rightButton_triggeredHandler(event:starling.events.Event):void
		{
			Main.klik();
			
			_data.curState = curState;
			
			if(soalList)
			{
				_data.arraySoal = soalList.dataProvider;
			}
			
			_data.jawabanArray = jawabanArray;
			_data.petunjuk = "petunjuktesgayabelajar";
			this.dispatchEventWith(EventType.SHOW_PETUNJUK);
		}

		private function createlightdownBackground():Texture
		{
			var downdBg:RoundedQuad = new RoundedQuad(5, 100, 100, 0xd34836);
			downdBg.filter = new DropShadowFilter(4, Math.PI / 2, 0, 0.25, 2, 1);
			downdBg.x = downdBg.y = 10;

			var downuBg:RoundedQuad = new RoundedQuad(5, 96, 96, 0);
			downuBg.x = downuBg.y = 12;

			var spr:Sprite = new Sprite();
			spr.addChild(downdBg);
			spr.addChild(downuBg);
			
			var bnd:Rectangle = spr.bounds;
			var rendertxture:RenderTexture = new RenderTexture(bnd.width + 20, bnd.height + 20);
			rendertxture.draw(spr);
			return rendertxture;
		}

		private function createlightupBackground():Texture
		{
			var downdBg:RoundedQuad = new RoundedQuad(5, 100, 100, 0xffffff);
			downdBg.x = downdBg.y = 10;

			var downuBg:RoundedQuad = new RoundedQuad(5, 96, 96, 0);
			downuBg.x = downuBg.y = 12;

			var spr:Sprite = new Sprite();
			spr.addChild(downdBg);
			spr.addChild(downuBg);
			
			var bnd:Rectangle = spr.bounds;
			var rendertxture:RenderTexture = new RenderTexture(bnd.width + 20, bnd.height + 20);
			rendertxture.draw(spr);
			return rendertxture;
		}
	}
}