﻿package views
{
	import feathers.controls.Button;
	import feathers.controls.Header;
	import feathers.controls.ImageLoader;
	import feathers.controls.Label;
	import feathers.controls.LayoutGroup;
	import feathers.controls.PanelScreen;
	import feathers.layout.AnchorLayout;
	import feathers.layout.AnchorLayoutData;
	import feathers.layout.VerticalLayout;
	import feathers.layout.VerticalLayoutData;

	import flash.geom.Rectangle;
	import flash.system.System;

	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.events.Event;

	import utils.CustomButton;
	import feathers.controls.ScrollBarDisplayMode;
	
	/**
	 * ...
	 * @author Indra Mahkota
	 */
	public class Petunjuk extends PanelScreen
	{
		private var label:Label;
		private var array:Array = new Array();
		
		protected var _data:NavigatorData;
		
		public function Petunjuk()
		{
			super();
		}
		
		public function get data():NavigatorData  { return _data; }
		
		public function set data(value:NavigatorData):void  { _data = value; }
		
		override protected function initialize():void
		{
			this.title = "Petunjuk";
			this.backButtonHandler = backHandler;
			this.headerFactory = customHeaderFactory;
			this.scrollBarDisplayMode = ScrollBarDisplayMode.FLOAT;
			
			var layout:VerticalLayout = new VerticalLayout();
			layout.paddingBottom = 10;
			this.layout = layout;
			
			array = Main.getJsonAssets(_data.petunjuk);
			
			var i:uint, arrlen:int = array.length;

			for (i; i < arrlen; ++i)
			{
				var lg:LayoutGroup = createLayotGroup();
				
				var bt:CustomButton = createButton(array[i].judul);
				lg.addChild(bt);
				
				if(array[i].render == "ya")
				{
					var arrText:Array = new Array();
					arrText = array[i].text;
				
					var j:uint;
					var lenarrText:int = arrText.length;
					
					for (j; j < lenarrText; ++j)
					{
						var lgText:LayoutGroup = new LayoutGroup();
						lgText.layout = new AnchorLayout();
						lgText.layoutData = new VerticalLayoutData(100, NaN);
						
						var labelnum:Label = new Label();
						labelnum.text = String(j + 1) + ".";
						labelnum.layoutData = new AnchorLayoutData(20, 20, 0, 20, NaN, NaN);
						lgText.addChild(labelnum);
						
						var labelCont:Label = new Label();
						labelCont.text = arrText[j];
						labelCont.textRendererProperties.wordWrap = true;
						labelCont.layoutData = new AnchorLayoutData(20, 20, 0, 40, NaN, NaN);
						lgText.addChild(labelCont);
						
						lg.addChild(lgText);
					}
				}
				else
				{
					var lb:Label = createLabel(array[i].text);
					lg.addChild(lb);
				}
				this.addChild(lg);
			}
			
			super.initialize();
		}
		
		private function createLayotGroup():LayoutGroup
		{
			var image:Image = new Image(Main.roundQuadBackground);
			image.pixelSnapping = true;
			image.scale9Grid = new Rectangle(20, 20, 80, 80);

			var vl:VerticalLayout = new VerticalLayout();
			vl.paddingLeft = vl.paddingRight = 10;
			vl.paddingTop = 10;
			vl.paddingBottom = 60;
			
			var lg:LayoutGroup = new LayoutGroup();
			lg.backgroundSkin = image;
			lg.layout = vl;
			lg.layoutData = new VerticalLayoutData(100, NaN);
			return lg;
		}
		
		private function createButton(label:String):CustomButton
		{
			var button:CustomButton = new CustomButton();
			button.label = label;
			button.styleNameList.add("headertest-button");
			button.layoutData = new VerticalLayoutData(100, NaN);
			return button;
		}
		
		private function createLabel(text:String):Label
		{
			var label:Label = new Label();
			label.text = text;
			label.paddingLeft = label.paddingRight = label.paddingTop = 20;
			label.textRendererProperties.wordWrap = true;
			label.layoutData = new VerticalLayoutData(100, NaN);
			return label;
		}
		
		override public function dispose():void
		{
			if(array) array = null;

			System.pauseForGCIfCollectionImminent(0);
			System.gc();
			
			super.dispose();
		}
		
		private function customHeaderFactory():Header
		{
			var header:Header = new Header();
			
			var leftButton:Button = new Button();
			leftButton.styleNameList.add("back-button");
			leftButton.addEventListener(Event.TRIGGERED, goBack);
			
			header.leftItems = new <DisplayObject>[leftButton];
			
			return header;
		}
		
		private function backHandler():void
		{
			_data.petunjuk = null;
			this.dispatchEventWith(Event.COMPLETE);
		}
		
		private function goBack():void
		{
			Main.klik();
			
			_data.petunjuk = null;
			this.dispatchEventWith(Event.COMPLETE);
		}
	}
}