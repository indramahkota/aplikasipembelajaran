package views
{
	import constants.EventType;

	import feathers.controls.Button;
	import feathers.controls.ButtonState;
	import feathers.controls.Header;
	import feathers.controls.ImageLoader;
	import feathers.controls.Label;
	import feathers.controls.PanelScreen;
	import feathers.controls.ToggleButton;
	import feathers.events.FeathersEventType;
	import feathers.layout.AnchorLayout;
	import feathers.skins.ImageSkin;

	import flash.events.AsyncErrorEvent;
	import flash.events.NetStatusEvent;
	import flash.events.SecurityErrorEvent;
	import flash.filesystem.File;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.net.NetConnection;
	import flash.net.NetStream;
	import flash.system.System;
	import flash.utils.setTimeout;

	import starling.core.Starling;
	import starling.display.BlendMode;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.MeshBatch;
	import starling.display.Quad;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.filters.DropShadowFilter;
	import starling.textures.Texture;

	import utils.RoundedQuad;
	
	/**
	 * ...
	 * @author Indra Mahkota
	 */
	public class VideoViewer extends PanelScreen
	{
		private var ns:NetStream;
		private var nc:NetConnection;
		
		private var file:File;
		private var image:Image;
		private var texture:Texture;
		
		private var bgimage:RoundedQuad;
		private var cgimage:Quad;
		
		private var fsButton:Button;
		private var teksWaktu:Label;
		private var togglebtn:ToggleButton;
		private var bgSlider:ImageLoader;
		private var upSlider:ImageLoader;
		private var buttonHandler:Button;
		
		private var timerWIdth:Number;
		private var timerHeight:Number;
		private var seekWidth:Number;
		
		private var isPlay:Boolean;
		private var duration:Number = 0;
		private var totalDuration:Number;

		private var arrayData:Array = [];
		
		private var touchPointID:int = -1;
		
		private var pointInBounds:Point = new Point();
		private static const HELPER_POINT:Point = new Point();
		private static const HELPER_TOUCHES_VECTOR:Vector.<Touch> = new <Touch>[];
		
		protected var _data:NavigatorData;
		
		public function VideoViewer()
		{
			super();
		}
		
		public function get data():NavigatorData  { return _data; }
		
		public function set data(value:NavigatorData):void  { _data = value; }
		
		override protected function initialize():void
		{
			this.layout = new AnchorLayout();
			this.backButtonHandler = backHandler;
			this.headerFactory = customHeaderFactory;

			var referensiData:int;
			if(_data.referensiMateri)
			{
				referensiData = int(_data.referensiMateri);
			}

			arrayData = Main.getJsonAssets("materi");

			var objek:Object = arrayData[referensiData];

			this.title = objek.nama;
			totalDuration = objek.totalduration;
			file = File.applicationDirectory.resolvePath(objek.urlvideo);
			
			Starling.current.skipUnchangedFrames = false;
			
			bgimage = new RoundedQuad(5, stage.stageWidth - 20, 351, 0xFFFFFF);
			bgimage.filter = new DropShadowFilter(4, Math.PI / 2, 0, 0.25, 2, 1);
			bgimage.touchable = false;
			bgimage.x = 10;
			bgimage.y = 10;
			this.addChild(bgimage);
			
			cgimage = new Quad(stage.stageWidth - 20, 1, 0xEDEDF0);
			cgimage.touchable = false;
			cgimage.x = 10;
			cgimage.y = 310;
			this.addChild(cgimage);

			var fsSkin:ImageSkin = new ImageSkin(Main.getImage("fsenter-up"));
			fsSkin.setTextureForState(ButtonState.DOWN, Main.getImage("fsenter-down"));
			fsSkin.width = fsSkin.height = 25;
			
			fsButton = new Button();
			fsButton.defaultSkin = fsSkin;
			fsButton.y = 310 + ((50 - 25) / 2);
			fsButton.x = stage.stageWidth - (25 + 20);
			fsButton.addEventListener(Event.TRIGGERED, fulscreenHandler);
			this.addChild(fsButton);
			
			var skin:ImageSkin = new ImageSkin();
			skin.setTextureForState(ButtonState.UP, Main.getImage("play-up"));
			skin.setTextureForState(ButtonState.DOWN, Main.getImage("play-down"));
			skin.setTextureForState(ButtonState.HOVER, Main.getImage("play-up"));
			skin.setTextureForState(ButtonState.UP_AND_SELECTED, Main.getImage("pause-up"));
			skin.setTextureForState(ButtonState.DOWN_AND_SELECTED, Main.getImage("pause-down"));
			skin.setTextureForState(ButtonState.HOVER_AND_SELECTED, Main.getImage("pause-up"));
			skin.width = skin.height = 25;
			
			togglebtn = new ToggleButton();
			togglebtn.x = 20;
			togglebtn.y = 310 + ((50 - 25) / 2);
			togglebtn.touchable = false;
			togglebtn.hasLabelTextRenderer = false;
			togglebtn.addEventListener(Event.TRIGGERED, togglebtnHandler);
			togglebtn.defaultIcon = skin;
			this.addChild(togglebtn);
			
			teksWaktu = new Label();
			teksWaktu.text = "00:00 / 00:00";
			teksWaktu.validate();
			timerWIdth = teksWaktu.width;
			timerHeight = teksWaktu.height;
			teksWaktu.x = stage.stageWidth - (timerWIdth + 55);
			teksWaktu.y = 310 + (((50 - timerHeight) / 2) - 1);
			teksWaktu.touchable = false;
			this.addChild(teksWaktu);
			
			seekWidth = stage.stageWidth - (70 + 25 + timerWIdth + 25);
			
			bgSlider = new ImageLoader();
			bgSlider.source = Main.getImage("seek-bg");
			bgSlider.scale9Grid = new Rectangle(4, 4, 1, 1);
			bgSlider.height = 12;
			bgSlider.width = seekWidth;
			bgSlider.x = 55;
			bgSlider.y = 310 + ((50 - 12) / 2);
			bgSlider.touchable = false;
			this.addChild(bgSlider);
			
			upSlider = new ImageLoader();
			upSlider.source = Main.getImage("seek-up");
			upSlider.scale9Grid = new Rectangle(4, 4, 1, 20);
			upSlider.height = 12;
			upSlider.x = 55;
			upSlider.y = 310 + ((50 - 12) / 2);
			upSlider.touchable = false;
			this.addChild(upSlider);
			
			var skinbuttonHandler:ImageSkin = new ImageSkin(Main.getImage("seek-bg"));
			skinbuttonHandler.scale9Grid = new Rectangle(4, 4, 1, 1);
			skinbuttonHandler.height = 12;
			
			buttonHandler = new Button();
			buttonHandler.defaultSkin = skinbuttonHandler;
			buttonHandler.height = 50;
			buttonHandler.width = seekWidth;
			buttonHandler.alpha = 0;
			buttonHandler.x = 55;
			buttonHandler.y = 310;
			buttonHandler.addEventListener(TouchEvent.TOUCH, progressbuttonHandler);
			this.addChild(buttonHandler);

			this.addEventListener(FeathersEventType.RESIZE, resizeHandler);

			if(_data.isPlay)
			{
				isPlay = _data.isPlay;
			}

			if(_data.seekVideo)
			{
				duration = _data.seekVideo;
			}
			
			setupConnection();
			super.initialize();
		}

		private function resizeHandler(event:Event):void
		{
			seekWidth = stage.stageWidth - (70 + 25 + timerWIdth + 25);
			bgimage.fWidth = stage.stageWidth - 20;
			cgimage.width = stage.stageWidth - 20;

			fsButton.y = 310 + ((50 - 25) / 2);
			fsButton.x = stage.stageWidth - (25 + 20);

			teksWaktu.x = stage.stageWidth - (timerWIdth + 55);
			teksWaktu.y = 310 + (((50 - timerHeight) / 2) - 1);

			bgSlider.width = seekWidth;
			bgSlider.x = 55;
			bgSlider.y = 310 + ((50 - 12) / 2);

			upSlider.x = 55;
			upSlider.y = 310 + ((50 - 12) / 2);

			teksWaktu.text = textDuration(duration) + " / " + textDuration(totalDuration);
			updateTextWidth();
				
			upSlider.width = (duration / totalDuration) * seekWidth;

			buttonHandler.width = seekWidth;
			buttonHandler.x = 55;
			buttonHandler.y = 310;

			var quada:Quad = new Quad(stage.stageWidth, stage.stageHeight, 0xEDEDF0);
			var quadb:Quad = new Quad(stage.stageWidth, 110, 0x197483);
			
			var meshBatch:MeshBatch = new MeshBatch();
			meshBatch.addMesh(quada);
			meshBatch.addMesh(quadb);
			
			this.backgroundSkin = meshBatch;
		}

		override public function dispose():void
		{
			if(ns != null)
			{
				ns.close();
				ns = null;
			}
			
			if(nc != null)
			{
				nc.close();
				nc = null;
			}
			
			if(image != null)
			{
				image.dispose();
				image = null;
			}

			if(bgimage != null)
			{
				bgimage.filter.dispose();
				bgimage.filter = null;
				bgimage.dispose();
				bgimage = null;
			}
			
			if(texture != null)
			{
				texture.dispose();
				texture = null;
			}
			
			if(this.hasEventListener(Event.ENTER_FRAME))
			{
				this.removeEventListener(Event.ENTER_FRAME, updateHandler);
			}
			
			System.pauseForGCIfCollectionImminent(0);
			System.gc();

			super.dispose();
		}
		
		private function setupConnection():void
		{
			nc = new NetConnection();
			nc.addEventListener(NetStatusEvent.NET_STATUS, onNetStatus);
			nc.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
			nc.connect(null);
		}
		
		private function setupStream():void
		{
			ns = new NetStream(nc);
			ns.client = new VideoPlayerNetStreamClient(
					onMetaDataHandler, onCuePointHandler,
					onXMPData);
			
			ns.addEventListener(NetStatusEvent.NET_STATUS, onNetStatus);
			ns.addEventListener(AsyncErrorEvent.ASYNC_ERROR, asyncErrorHandler);
			
			texture = Texture.fromNetStream(ns, 1, onTextureComplete);
			
			texture.root.onRestore = function():void
			{
				texture.root.attachNetStream(ns, onTextureComplete);
				ns.play(file.url);

				duration = _data.seekVideo;
			}
			
			ns.play(file.url);
		}

		private function onMetaDataHandler(metaData:Object):void
		{
			totalDuration = Number(metaData.duration);
			teksWaktu.text = "00:00 / " + textDuration(totalDuration);
			updateTextWidth();

			trace("Total duration: " + Number(metaData.duration));
		}

		private function onCuePointHandler(item:Object):void
		{
			trace("[CuePoint] onCuePoint");
		}

		private function onXMPData(item:Object):void
		{
			trace("[XMPData] onXMPData");
		}
		
		private function onTextureComplete():void
		{
			if(this.contains(image))
			{
				removeChild(image);
			}

			image = new Image(texture);
			image.blendMode = BlendMode.NONE;
			image.touchable = false;
			image.visible = false;

			var scale:Number;
			var stgVideoScale:Number = (stage.stageWidth - 20) / 290;
			var videoScale:Number = image.width / image.height;

			//2 kemungkinan: portrait/landscape
			//yang diikuti adalah container

			if(stgVideoScale > videoScale)
			{
				scale = 299 / image.height;
				image.scaleX = image.scaleY = scale;
				image.x = ((stage.stageWidth - 20) - image.width) / 2;
				image.y = 10;
			}
			else if(stgVideoScale < videoScale)
			{
				scale = (stage.stageWidth - 20) / image.width;
				image.scaleX = image.scaleY = scale;
				image.x = 10;
				image.y = ((299 - image.height) / 2) + 10;
			}
			else
			{
				image.x = 10;
				image.y = 10;
			}

			addChild(image);
			setTimeout(munculkanVideo, 150);
		}
		
		private function securityErrorHandler(event:SecurityErrorEvent):void
		{
			trace("securityErrorHandler: " + event);
		}
		
		private function asyncErrorHandler(event:AsyncErrorEvent):void
		{
			trace("asyncErrorHandler: " + event);
		}
		
		private function onNetStatus(e:NetStatusEvent):void 
		{
			switch (e.info.code)
			{
				case "NetStream.Play.Start": 

					if(_data.seekVideo)
					{
						ns.seek(_data.seekVideo);
					}
					else
					{
						ns.seek(duration);
					}

					if(isPlay)
					{
						ns.resume();
						togglebtn.isSelected = isPlay;
						this.addEventListener(Event.ENTER_FRAME, updateHandler);
					}
					else
					{
						ns.pause();
					}

					teksWaktu.text = textDuration(duration) + " / " + textDuration(totalDuration);
					upSlider.width = (duration / totalDuration) * seekWidth;
					updateTextWidth();
					
					break;
					
				case "NetStream.Play.Stop": 

					duration = 0;
					isPlay = false;
					togglebtn.isSelected = false;

					this.removeEventListener(Event.ENTER_FRAME, updateHandler);

					ns.play(file.url);
					image.visible = false;
					setTimeout(munculkanVideo, 150);
					
					break;
					
				case "NetConnection.Connect.Success": 
					setupStream();
					
					break;
					
				case "NetStream.Play.StreamNotFound": 
					trace(e.info.code, "Unable to locate video data ");
					break;
					
				default : 
					break;
			}
		}
		
		private function munculkanVideo():void
		{
			if(!image) return;//jika image belum dibuat, return;
			image.visible = true;
			togglebtn.touchable = true;
		}
		
		private function togglebtnHandler(event:Event):void
		{
			var toggle:ToggleButton = ToggleButton(event.currentTarget);
			
			if(!toggle.isSelected)
			{
				isPlay = true;
				ns.resume();
				this.addEventListener(Event.ENTER_FRAME, updateHandler);
			}
			else
			{
				isPlay = false;
				ns.pause();
				this.removeEventListener(Event.ENTER_FRAME, updateHandler);
			}
		}
		
		private function progressbuttonHandler(event:TouchEvent):void
		{
			if(!buttonHandler.isEnabled)
			{
				touchPointID = -1;
				return;
			}
			
			var touch:Touch;
			var touches:Vector.<Touch> = event.getTouches(this, null, HELPER_TOUCHES_VECTOR);
			
			if(touches.length == 0)
			{
				return;
			}
			
			if(touchPointID >= 0)
			{
				for each (var currentTouch:Touch in touches)
				{
					if(currentTouch.id == touchPointID)
					{
						touch = currentTouch;
						break;
					}
				}
				
				if(!touch)
				{
					HELPER_TOUCHES_VECTOR.length = 0;
					return;
				}
				
				if(touch.phase == TouchPhase.MOVED)
				{
					touch.getLocation(buttonHandler, HELPER_POINT);
					
					var isInBounds:Boolean = buttonHandler.hitTest(HELPER_POINT) != null;
					
					if(isInBounds || buttonHandler.keepDownStateOnRollOut)
					{
						pointInBounds = touch.getLocation(buttonHandler);
						toucMovedHandler(pointInBounds.x);
					}
				}
				else if(touch.phase == TouchPhase.ENDED)
				{
					touchPointID = -1;
					
					pointInBounds = touch.getLocation(buttonHandler);
					touchEndHandler(pointInBounds.x, isInBounds);
				}
			}
			else
			{
				for each (touch in touches)
				{
					if(touch.phase == TouchPhase.BEGAN)
					{
						touchPointID = touch.id;
						
						pointInBounds = touch.getLocation(buttonHandler);
						touchBeganHandler(pointInBounds.x);
						
						break;
					}
				}
			}
			HELPER_TOUCHES_VECTOR.length = 0;
		}
		
		private function touchBeganHandler(position:Number):void
		{
			this.removeEventListener(Event.ENTER_FRAME, updateHandler);
			ns.pause();
			
			updateVideo(position);
		}
		
		private function toucMovedHandler(position:Number):void
		{
			updateVideo(position);
		}
		
		private function touchEndHandler(position:Number, inBounds:Boolean):void
		{
			if(togglebtn.isSelected)
			{
				this.addEventListener(Event.ENTER_FRAME, updateHandler);
				ns.resume();
			}
			
			if(!inBounds) return;
			
			updateVideo(position);
		}
		
		private function updateVideo(position:Number):void
		{
			var ratio:Number = position / seekWidth;
			
			upSlider.width = ratio * seekWidth;
			ns.seek(ratio * totalDuration);
			teksWaktu.text = textDuration(ratio * totalDuration) + " / " + textDuration(totalDuration);
		}
		
		private function updateHandler(event:Event):void
		{
			duration = ns.time;
			teksWaktu.text = textDuration(duration) + " / " + textDuration(totalDuration);
			updateTextWidth();
			
			var ratio:Number = duration / totalDuration;
			upSlider.width = ratio * seekWidth;
		}
		
		private function textDuration(duration:Number):String
		{
			var detik:int = Math.floor(duration % 60);
			var menit:int = Math.floor((duration % 3600) / 60);
			
			var detik_text:String = detik < 10 ? detik_text = ("0" + String(detik)) : detik_text = String(detik);
			var menit_text:String = menit < 10 ? menit_text = ("0" + String(menit)) : menit_text = String(menit);
			
			return menit_text + ":" + detik_text;
		}
		
		private function updateTextWidth():void
		{
			var tempNum:Number;
			teksWaktu.validate();
			tempNum = teksWaktu.width;
			
			if(tempNum != timerWIdth)
			{
				timerWIdth = tempNum;
				teksWaktu.x = stage.stageWidth - (timerWIdth + 20);
				
				seekWidth = stage.stageWidth - (40 + timerWIdth + 45);
				
				bgSlider.width = seekWidth;
				upSlider.width = seekWidth;
				buttonHandler.width = seekWidth;
			}
		}
		
		private function customHeaderFactory():Header
		{
			var header:Header = new Header();
			
			var leftButton:Button = new Button();
			leftButton.styleNameList.add("back-button");
			leftButton.addEventListener(Event.TRIGGERED, leftButton_triggeredHandler);
			
			header.leftItems = new <DisplayObject>[leftButton];
			
			var rightButton:Button = new Button();
			rightButton.styleNameList.add("help-button");
			rightButton.addEventListener(Event.TRIGGERED, rightButton_triggeredHandler);
			
			header.rightItems = new <DisplayObject>[rightButton];
			
			return header;
		}
		
		private function backHandler():void
		{
			_data.isPlay = null;
			_data.seekVideo = null;

			Starling.current.skipUnchangedFrames = true;
			this.dispatchEventWith(Event.COMPLETE);
		}
		
		private function leftButton_triggeredHandler(event:Event):void
		{
			Main.klik();
			
			_data.isPlay = null;
			_data.seekVideo = null;

			Starling.current.skipUnchangedFrames = true;
			this.dispatchEventWith(Event.COMPLETE);
		}
		
		private function rightButton_triggeredHandler(event:Event):void
		{
			Main.klik();

			stroreDataHandler();
			
			_data.petunjuk = "petunjukmateri";
			this.dispatchEventWith(EventType.SHOW_PETUNJUK);
		}

		private function fulscreenHandler(event:Event):void
		{
			stroreDataHandler();

			this.dispatchEventWith(EventType.SHOW_VIDEO_VIEWER_FS);
		}

		private function stroreDataHandler():void
		{
			_data.seekVideo = ns.time;
			_data.isPlay = isPlay;
		}
	}
}

dynamic class VideoPlayerNetStreamClient
{
	public function VideoPlayerNetStreamClient(onMetaDataCallback:Function,
		onCuePointCallback:Function, onXMPDataCallback:Function)
	{
		this.onMetaDataCallback = onMetaDataCallback;
		this.onCuePointCallback = onCuePointCallback;
		this.onXMPDataCallback = onXMPDataCallback;
	}

	public var onMetaDataCallback:Function;

	public var onCuePointCallback:Function;

	public function onMetaData(metadata:Object):void
	{
		this.onMetaDataCallback(metadata);
	}

	public function onCuePoint(cuePoint:Object):void
	{
		this.onCuePointCallback(cuePoint);
	}

	public function onXMPData(xmpData:Object):void
	{
		this.onXMPDataCallback(xmpData);
	}
}