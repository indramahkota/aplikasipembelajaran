﻿package views
{
	import com.indramahkota.ane.toast.DurationEnum;
	import com.indramahkota.ane.toast.ToastExtension;

	import constants.EventType;

	import feathers.controls.Button;
	import feathers.controls.Header;
	import feathers.controls.ImageLoader;
	import feathers.controls.Label;
	import feathers.controls.LayoutGroup;
	import feathers.controls.PanelScreen;
	import feathers.controls.ScrollContainer;
	import feathers.controls.TabBar;
	import feathers.controls.ToggleButton;
	import feathers.data.ArrayCollection;
	import feathers.layout.AnchorLayout;
	import feathers.layout.AnchorLayoutData;
	import feathers.layout.FlowLayout;
	import feathers.layout.HorizontalAlign;
	import feathers.layout.VerticalLayout;
	import feathers.layout.VerticalLayoutData;

	import flash.desktop.NativeApplication;
	import flash.geom.Rectangle;
	import flash.system.System;

	import starling.animation.DelayedCall;
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Canvas;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.display.Quad;
	import starling.events.Event;
	import starling.filters.DropShadowFilter;
	import starling.text.TextFormat;
	import starling.textures.TextureAtlas;
	import starling.utils.Align;

	import utils.CustomButton;
	import utils.QuadSection;
	import utils.RoundedQuad;
	import starling.display.MeshBatch;
	
	/**
	 * ...
	 * @author Indra Mahkota
	 */
	public class MenuUtama extends PanelScreen
	{
		private var tabs:TabBar;
		private var scrollerContainer:ScrollContainer;
		
		private var layoutgroupGrafikBat:LayoutGroup;
		private var layoutgroupGrafikLing:LayoutGroup;
		
		private var array:Array = new Array();
		private var exitHandlerBoolean:Boolean = false;
		private var exitoastHandlerBoolean:Boolean = false;
		
		private var atlasclick:TextureAtlas;
		private var clickmc:MovieClip;

		private var dropShadowQuad:Quad;
		private var switchIndicator:Quad;
		
		private var gayabelajar:Array = ["Visual", "Auditorial", "Kinestetik"];
		private var colorArray:Array = [0xFFFF33, 0x79DCF4, 0xFFCC33];
		
		protected var _data:NavigatorData;
		
		public function MenuUtama()
		{
			super();
		}
		
		public function get data():NavigatorData  { return _data; }
		
		public function set data(value:NavigatorData):void  { _data = value; }
		
		override protected function initialize():void
		{
			this.title = "Halaman Utama";
			this.backButtonHandler = exitHandler;
			this.menuButtonHandler = menuHandler;
			this.headerFactory = customHeaderFactory;
			this.layout = new AnchorLayout();
			
			var layout:VerticalLayout = new VerticalLayout();
			layout.paddingBottom = 10;
			
			scrollerContainer = new ScrollContainer();
			scrollerContainer.layout = layout;
			scrollerContainer.layoutData = new AnchorLayoutData(45, 0, 0, 0);
			this.addChild(scrollerContainer);

			dropShadowQuad = new Quad(stage.stageWidth, 45, 0x197483);
			dropShadowQuad.filter = new DropShadowFilter(4, Math.PI / 2, 0, 0.25, 2, 1);
			this.addChild(dropShadowQuad);

			tabs = new TabBar();
			tabs.height = 45;
			tabs.layoutData = new AnchorLayoutData(0, 0, NaN, 0);
			tabs.dataProvider = new ArrayCollection(
			[
				{ label: "Pencapaian" },
				{ label: "Aktivitas" }
			]);
			tabs.tabFactory = function():ToggleButton
			{
				var tab:ToggleButton = new ToggleButton();
				tab.downSkin = new Quad(10, 10, 0x298d9d);
				tab.selectedDownSkin = new Quad(10, 10, 0x298d9d);
				tab.fontStyles = new TextFormat("SourceSansPro", 18, 0xFFFFFF);
				return tab;
			};
			tabs.addEventListener(Event.CHANGE, tabs_changeHandler);
			tabs.addEventListener(Event.TRIGGERED, function():void
			{
				Main.klik();
			});
			this.addChild(tabs);

			switchIndicator = new Quad(stage.stageWidth / 2, 4);
			switchIndicator.y = 41;
			this.addChild(switchIndicator);

			if(_data.dasboard)
			{
				if(_data.dasboard == 1) switchIndicator.x = stage.stageWidth / 2;
				tabs.selectedIndex = _data.dasboard;
				tampilkanScreen(_data.dasboard);
			}
			else
			{
				drawGrafik();
			}
			
			if(_data.scrollerContainerVSP)
			{
				scrollerContainer.verticalScrollPosition = _data.scrollerContainerVSP;
			}

			if(Main.mySo.data.menuutamatut0 == undefined)
			{
				atlasclick = Main.assets.getTextureAtlas("click");
				clickmc = new MovieClip(atlasclick.getTextures("clickanimation_"));
				clickmc.touchable = false;
				clickmc.scale = 50 / clickmc.width;
				clickmc.x = (stage.stageWidth / 2) + 20;
				this.addChild(clickmc);
				Starling.juggler.add(clickmc);
			}

			if(tabs.selectedIndex == 1)
			{
				checkToDisplayTut();

				if(Main.mySo.data.menuutamatut0 == "sudah" &&
				Main.mySo.data.menuutamatut1 == undefined)
				{
					tampilkanTutorial(0);
				}
				else if(Main.mySo.data.menuutamatut1 == "sudah" &&
					Main.mySo.data.menuutamatut2 == undefined)
				{
					tampilkanTutorial(1);
				}
				else if(Main.mySo.data.menuutamatut2 == "sudah" &&
					Main.mySo.data.menuutamatut3 == undefined)
				{
					tampilkanTutorial(2);
				}
				else if(Main.mySo.data.menuutamatut3 == "sudah" &&
					Main.mySo.data.menuutamatut4 == undefined)
				{
					tampilkanTutorial(3);
				}
				else if(Main.mySo.data.menuutamatut4 == "sudah" &&
					Main.mySo.data.menuutamatut5 == undefined)
				{
					tampilkanTutorial(4);
				}
				else if(Main.mySo.data.menuutamatut5 == "sudah" &&
					Main.mySo.data.menuutamatut6 == undefined)
				{
					tampilkanTutorial(5);
				}
			}

			function tampilkanTutorial(index:int):void
			{
				var laygp:LayoutGroup = scrollerContainer.getChildAt(index) as LayoutGroup;

				atlasclick = Main.assets.getTextureAtlas("click");
				clickmc = new MovieClip(atlasclick.getTextures("clickanimation_"));
				clickmc.touchable = false;
				clickmc.scale = 50 / clickmc.width;
				clickmc.x = 20;
				laygp.addChild(clickmc);
				Starling.juggler.add(clickmc);
			}
			
			super.initialize();
		}
		
		override public function dispose():void
		{
			if(array) array = null;
			
			if(clickmc)
			{
				clickmc.removeFromParent(true);
				Starling.juggler.remove(clickmc);
			}

			if(dropShadowQuad != null)
			{
				dropShadowQuad.filter.dispose();
				dropShadowQuad.filter = null;
				dropShadowQuad.dispose();
				dropShadowQuad = null;
			}

			System.pauseForGCIfCollectionImminent(0);
			System.gc();
			
			super.dispose();
		}

		private function checkToDisplayTut():void
		{
			var arr:Array = [1, 0, 0, 0, 0, 0];

			if(Main.mySo.data.menuutamatut0 == "sudah")
			{
				toggleBlockTouchable(0, true);
			}

			if(Main.mySo.data.menuutamatut1 == undefined)
			{
				toggleBlockTouchable(1, false);
			}
			else
			{
				arr[1] = 1;
				toggleBlockTouchable(1, true);
			}
			
			if(Main.mySo.data.menuutamatut2 == undefined)
			{
				toggleBlockTouchable(2, false);
			}
			else
			{
				arr[2] = 1;
				toggleBlockTouchable(2, true);
			}
			
			if(Main.mySo.data.menuutamatut3 == undefined)
			{
				toggleBlockTouchable(3, false);
			}
			else
			{
				arr[3] = 1;
				toggleBlockTouchable(3, true);
			}

			if(Main.mySo.data.menuutamatut4 == undefined)
			{
				toggleBlockTouchable(4, false);
			}
			else
			{
				arr[4] = 1;
				toggleBlockTouchable(4, true);
			}

			if(Main.mySo.data.menuutamatut5 == undefined)
			{
				toggleBlockTouchable(5, false);
			}
			else
			{
				arr[5] = 1;
				toggleBlockTouchable(5, true);
			}

			var netralIndex:int = 0;
			for(var g:int = 0; g < arr.length; g++)
			{
				if(arr[g] == 1)
				{
					netralIndex = g;
				}
			}

			var layp:LayoutGroup = scrollerContainer.getChildAt(netralIndex) as LayoutGroup;

			if(layp.numChildren > 1)
			{
				layp.removeChildAt(1);
			}

			if(Main.mySo.data.menuutamatut6 == "sudah")
			{
				toggleBlockTouchable(5, true);
			}

			function toggleBlockTouchable(index:int, value:Boolean):void
			{
				var layp:LayoutGroup = scrollerContainer.getChildAt(index) as LayoutGroup;
				layp.touchable = value;

				if(value)
				{
					var sudahCheck:ImageLoader = new ImageLoader();
					sudahCheck.source = Main.getImage("ceklist");
					sudahCheck.setSize(24, 24);
					sudahCheck.layoutData = new AnchorLayoutData(23, 23);
					layp.addChild(sudahCheck);
				}
				else
				{
					var errorCheck:ImageLoader = new ImageLoader();
					errorCheck.source = Main.getImage("error_icon");
					errorCheck.setSize(24, 24);
					errorCheck.layoutData = new AnchorLayoutData(23, 23);
					layp.addChild(errorCheck);
				}
			}
		}
		
		private function tabs_changeHandler(event:Event):void
		{
			var tabs:TabBar = TabBar(event.currentTarget);
			tampilkanScreen(tabs.selectedIndex);
			var switchIndicatorTween:Tween = new Tween(switchIndicator,0.3);

			if(tabs.selectedIndex == 1)
			{
				checkToDisplayTut();
				// switchIndicator.x = stage.stageWidth / 2;
				switchIndicatorTween.moveTo(stage.stageWidth / 2, 41);
				switchIndicatorTween.onComplete = function():void
				{
					Starling.juggler.remove(switchIndicatorTween);
				}
				Starling.juggler.add(switchIndicatorTween);

				if(Main.mySo.data.menuutamatut0 == undefined)
				{
					if(clickmc && clickmc.alpha == 0) clickmc.alpha = 1;

					var tween:Tween = new Tween(clickmc, 0.5);
					tween.moveTo(30, 50);
					tween.onComplete = function():void
					{
						clickmc.removeFromParent(true);
						Main.mySo.data.menuutamatut0 = "sudah";

						tampilkanTutorial(0);
						Starling.juggler.remove(tween);
					}
					Starling.juggler.add(tween);
				}
				else if(Main.mySo.data.menuutamatut0 == "sudah" &&
				Main.mySo.data.menuutamatut1 == undefined)
				{
					tampilkanTutorial(0);
				}
				else if(Main.mySo.data.menuutamatut1 == "sudah" &&
				Main.mySo.data.menuutamatut2 == undefined)
				{
					tampilkanTutorial(1);
				}
				else if(Main.mySo.data.menuutamatut2 == "sudah" &&
				Main.mySo.data.menuutamatut3 == undefined)
				{
					tampilkanTutorial(2);
				}
				else if(Main.mySo.data.menuutamatut3 == "sudah" &&
				Main.mySo.data.menuutamatut4 == undefined)
				{
					tampilkanTutorial(3);
				}
				else if(Main.mySo.data.menuutamatut4 == "sudah" &&
				Main.mySo.data.menuutamatut5 == undefined)
				{
					tampilkanTutorial(4);
				}
				else if(Main.mySo.data.menuutamatut5 == "sudah" &&
				Main.mySo.data.menuutamatut6 == undefined)
				{
					tampilkanTutorial(5);
				}
			}
			else
			{
				switchIndicatorTween.moveTo(0, 41);
				switchIndicatorTween.onComplete = function():void
				{
					Starling.juggler.remove(switchIndicatorTween);
				}
				Starling.juggler.add(switchIndicatorTween);
				if(clickmc && clickmc.y != 0) clickmc.alpha = 0;
			}

			function tampilkanTutorial(index:int):void
			{
				var laygp:LayoutGroup = scrollerContainer.getChildAt(index) as LayoutGroup;

				atlasclick = Main.assets.getTextureAtlas("click");
				clickmc = new MovieClip(atlasclick.getTextures("clickanimation_"));
				clickmc.touchable = false;
				clickmc.scale = 50 / clickmc.width;
				clickmc.x = 20;
				laygp.addChild(clickmc);
				Starling.juggler.add(clickmc);
			}

			scrollerContainer.verticalScrollPosition = scrollerContainer.minVerticalScrollPosition;
		}

		private function tampilkanScreen(i:int):void
		{
			if(scrollerContainer.numChildren >= 0)
			{
				scrollerContainer.removeChildren(0, -1, true);
			}
			
			i == 0 ? drawGrafik() : drawAktivitas();
		}
		
		private function drawAktivitas():void
		{
			if(_data.menuutama)
			{
				array = _data.menuutama;
			}
			else
			{
				array = Main.getJsonAssets("menuutama");
				_data.menuutama = array;
			}
			
			var i:uint, len:int = array.length;
			
			for (i; i < len; ++i)
			{
				var trick:LayoutGroup = new LayoutGroup();
				trick.layout = new AnchorLayout();
				trick.layoutData = new VerticalLayoutData(100, NaN);

				var lg:LayoutGroup = createLayotGroup();
				var lb:Label = createLabel(array[i].label);
				var bt:CustomButton = createButton(array[i].judul, array[i].icon, array[i].tujuan);
				
				lg.addChild(bt);
				lg.addChild(lb);
				trick.addChild(lg);

				scrollerContainer.addChild(trick);
			}
		}
		
		private function drawGrafik():void
		{
			if(Main.mySo.data.dominan != "kosong")
			{
				layoutgroupGrafikLing = createLayotGroup();
				layoutgroupGrafikLing.layout = new AnchorLayout();
				layoutgroupGrafikLing.minHeight = 290;
				layoutgroupGrafikLing.width = stage.stageWidth;
				scrollerContainer.addChild(layoutgroupGrafikLing);
				
				var flowLayout:FlowLayout = new FlowLayout();
				flowLayout.horizontalAlign = HorizontalAlign.CENTER;
				flowLayout.padding = 20;
				flowLayout.gap = 15;
				
				var colorViewLayout:LayoutGroup = new LayoutGroup();
				colorViewLayout.layout = flowLayout;
				//colorViewLayout.backgroundSkin = new Quad(10, 10, 0xf80503); //debug
				colorViewLayout.layoutData = new AnchorLayoutData(290, 10, NaN, 10);
				layoutgroupGrafikLing.addChild(colorViewLayout);
				
				var btnGrafikLing:CustomButton = createButton("Gaya Belajar", "pie-chart", "tidakada");
				btnGrafikLing.x = 10;
				btnGrafikLing.y = 10;
				btnGrafikLing.width = stage.stageWidth - 20;
				layoutgroupGrafikLing.addChild(btnGrafikLing);
				
				var dataArray:Array = [];
				dataArray.push(
					Number(Main.mySo.data.visual), 
					Number(Main.mySo.data.auditorial), 
					Number(Main.mySo.data.kinestetik)
				);
				
				const xobject:Number = ((stage.stageWidth) - 200) / 2;
				
				var quadescMask:Canvas = new Canvas();
				quadescMask.drawCircle(100, 100, 100);
				var quadesc:Quad = new Quad(200, 200, colorArray[0]);
				quadesc.mask = quadescMask;
				quadesc.x = xobject;
				quadesc.y = 80;
				layoutgroupGrafikLing.addChild(quadesc);
				
				var total:Number = dataArray[0] + dataArray[1] + dataArray[2];
				var counter:Number = 1;
				var sudut:Array = [];
				var persentasi:Number;

				var avatarMask:Canvas = new Canvas();
				avatarMask.drawCircle(100, 100, 100);

				var mbLink:MeshBatch = new MeshBatch();
				mbLink.x = xobject;
				mbLink.y = 80;
				mbLink.mask = avatarMask;
				layoutgroupGrafikLing.addChild(mbLink);
				
				for (var k:int = 0; k < 3; ++k)
				{
					var quadSection:QuadSection = new QuadSection(200, 200, colorArray[k + 1]);
					quadSection.clockwise = false;
					quadSection.ratio = counter - (dataArray[k] / total);
					mbLink.addMesh(quadSection);
					
					counter = quadSection.ratio;
					sudut.push(-1 * (counter * 360) * (Math.PI / 180));
					
					var layKeterangan:LayoutGroup = new LayoutGroup();
					layKeterangan.layout = new AnchorLayout();
					layKeterangan.height = 50;
					
					var quadder:RoundedQuad = new RoundedQuad(4, 20, 20, colorArray[k]);
					layKeterangan.addChild(quadder);
					
					persentasi = (dataArray[k] / total) * 100;

					var percentText:Label = new Label();
					percentText.x = 25;
					percentText.text = persentasi.toFixed(2).replace(".", ",") + " %";
					layKeterangan.addChild(percentText);
					
					var ketText:Label = new Label();
					ketText.y = 22;
					ketText.text = gayabelajar[k];
					layKeterangan.addChild(ketText);
					
					colorViewLayout.addChild(layKeterangan);

					var garisgrafling:Quad = new Quad(1, 100);
					garisgrafling.alignPivot(Align.CENTER, Align.BOTTOM);
					garisgrafling.x = ((stage.stageWidth) - 1) / 2;
					garisgrafling.y = 180;
					garisgrafling.rotation = sudut[k];
					layoutgroupGrafikLing.addChildAt(garisgrafling, 4 + k);
				}
			}
			
			//--------------------------------------------------------------
			
			if(Main.mySo.data.nilai != "kosong")
			{
				layoutgroupGrafikBat = createLayotGroup();
				layoutgroupGrafikBat.layout = new AnchorLayout();
				layoutgroupGrafikBat.layoutData = new VerticalLayoutData(100, NaN);
				layoutgroupGrafikBat.minHeight = 370;
				layoutgroupGrafikBat.width = stage.stageWidth;
				scrollerContainer.addChild(layoutgroupGrafikBat);
				
				var paddingTp:Number = 80;
				var paddingLR:Number = 50;
				var chartHeight:Number = 250;
				var chartWidth:Number = stage.stageWidth - (2 * paddingLR);
				
				var labesto:Label = new Label();
				labesto.text = "Nilai (" + Main.mySo.data.nilai + ")";
				labesto.layoutData = new AnchorLayoutData(chartHeight + paddingTp + 20, NaN, 20, NaN, 0, NaN);
				layoutgroupGrafikBat.addChild(labesto);
				
				var qTegakKanan:Quad = new Quad(1, chartHeight, 0xEDEDF0);
				qTegakKanan.alpha = 0.75;
				qTegakKanan.x = paddingLR;
				qTegakKanan.y = paddingTp;
				layoutgroupGrafikBat.addChild(qTegakKanan);
				
				var qTegakKiri:Quad = new Quad(1, chartHeight, 0xEDEDF0);
				qTegakKiri.alpha = 0.75;
				qTegakKiri.x = stage.stageWidth - paddingLR;
				qTegakKiri.y = paddingTp;
				layoutgroupGrafikBat.addChild(qTegakKiri);
				
				var skalaPenomoran:int = 100;
				var yPosition:int = paddingTp;
				for (var i:int = 0; i < 6; i++)
				{
					var labe:Label = new Label();
					labe.text = String(skalaPenomoran);
					labe.x = 30;
					labe.y = yPosition - 10;
					labe.styleNameList.add("video-center-label");
					skalaPenomoran -= 20;
					layoutgroupGrafikBat.addChild(labe);
					
					var quad:Quad = new Quad(chartWidth, 1, 0xEDEDF0);
					quad.alpha = 0.75;
					quad.x = paddingLR;
					quad.y = yPosition;
					
					yPosition += 50;
					layoutgroupGrafikBat.addChild(quad);
				}
				
				var Leftpad:Number = 51;
				var LebarGrafik:Number = chartWidth - 1;
				
				yPosition = paddingTp;
				
				var nilai:Number = Number(String(Main.mySo.data.nilai).replace(",", "."));
				nilai == 0 ? nilai = (1/100) : nilai = (nilai/100);
				var percentage:Number = nilai * chartHeight;
				
				var quadr:Quad = new Quad(LebarGrafik, percentage, 0xA09FED);
				quadr.alpha = 0.5;
				quadr.x = Leftpad;
				quadr.y = chartHeight + yPosition;
				quadr.alignPivot("left", "bottom");
				
				var quads:Quad = new Quad(2, percentage - 2, 0xA09FED);
				quads.x = Leftpad;
				quads.y = chartHeight + yPosition;
				quads.alignPivot("left", "bottom");
				
				var quadt:Quad = new Quad(2, percentage - 2, 0xA09FED);
				quadt.x = Leftpad + LebarGrafik - 2;
				quadt.y = chartHeight + yPosition;
				quadt.alignPivot("left", "bottom");
				
				var quadu:Quad = new Quad(LebarGrafik, 2, 0xA09FED);
				quadu.x = Leftpad;
				quadu.y = (chartHeight + yPosition + 2) - percentage;
				quadu.alignPivot("left", "bottom");
				
				layoutgroupGrafikBat.addChild(quadr);
				layoutgroupGrafikBat.addChild(quads);
				layoutgroupGrafikBat.addChild(quadt);
				layoutgroupGrafikBat.addChild(quadu);
				
				var btnGrafik:CustomButton = createButton("Nilai", "chart_icon", "tidakada");
				btnGrafik.x = 10;
				btnGrafik.y = 10;
				btnGrafik.width = stage.stageWidth - 20;
				layoutgroupGrafikBat.addChild(btnGrafik);
			}
			
			if(Main.mySo.data.dominan == "kosong" && Main.mySo.data.nilai == "kosong")
			{
				var layoutgroupKet:LayoutGroup = createLayotGroup();
				layoutgroupKet.layout = new AnchorLayout();
				layoutgroupKet.layoutData = new VerticalLayoutData(100, NaN);
				scrollerContainer.addChild(layoutgroupKet);

				var ima:Image = new Image(Main.getImage("dasboardpencapaian"));

				const scale:Number = 100 / ima.width;
				ima.scale = scale;

				ima.x = 20;
				ima.y = 20;
				ima.pixelSnapping = true;
				layoutgroupKet.addChild(ima);

				var keterangan:String;
				if(Main.mySo.data.namaLengkap != undefined && Main.mySo.data.namaLengkap != "Offline Mode")
				{
					var strNama:String = Main.firstLetterUpperCase(Main.mySo.data.namaLengkap);
					var strArray:Array = strNama.split(' ');
					keterangan = "Maaf " + strArray[0] + ", data kamu tidak tersedia.";
				}
				else
				{
					keterangan = "Maaf, data kamu tidak tersedia.";
				}

				layoutgroupKet.minHeight = ima.bounds.bottom + 20;
				
				var label:Label = createLabel(keterangan + " Dimohon untuk menekan tombol aktivitas!");
				label.layoutData = new AnchorLayoutData(0, 0, 0, 117, 0);
				layoutgroupKet.addChild(label);
			}
		}
		
		private function createLayotGroup():LayoutGroup
		{
			var image:Image = new Image(Main.roundQuadBackground);
			image.pixelSnapping = true;
			image.scale9Grid = new Rectangle(20, 20, 80, 80);

			var vl:VerticalLayout = new VerticalLayout();
			vl.paddingLeft = vl.paddingRight = 10;
			vl.paddingTop = 10;
			vl.paddingBottom = 60;
			
			var lg:LayoutGroup = new LayoutGroup();
			lg.backgroundSkin = image;
			lg.layout = vl;
			lg.layoutData = new AnchorLayoutData(0, 0, NaN, 0);
			return lg;
		}
		
		private function createButton(label:String, icon:String, tujuan:String):CustomButton
		{
			var imgL:ImageLoader = new ImageLoader();
			imgL.source = Main.getImage(icon);
			imgL.pixelSnapping = true;
			imgL.setSize(32, 32);
			
			var button:CustomButton = new CustomButton();
			button.label = label;
			button.name = tujuan;
			button.defaultIcon = imgL;
			button.styleNameList.add("headertest-button");
			button.layoutData = new VerticalLayoutData(100, NaN);
			button.addEventListener(Event.TRIGGERED, Button_triggeredHandler);
			return button;
		}
		
		private function createLabel(text:String):Label
		{
			var label:Label = new Label();
			label.text = text;
			label.paddingLeft = label.paddingRight = label.paddingTop = 20;
			label.textRendererProperties.wordWrap = true;
			label.layoutData = new VerticalLayoutData(100, NaN);
			return label;
		}
		
		private function Button_triggeredHandler(event:Event):void
		{
			var location:String;
			if((event.target as CustomButton).name == "tidakada") return;

			if((event.target as CustomButton).name == "showPendahuluan")
			{
				location = "showPendahuluan";
				Main.mySo.data.menuutamatut1 = "sudah";
			}			
			else if((event.target as CustomButton).name == "showTesGayaBelajar")
			{
				Main.mySo.data.menuutamatut2 = "sudah";
			}
			/* else if((event.target as CustomButton).name == "showMateri")
			{
				Main.mySo.data.menuutamatut3 = "sudah";
			} */
			else if((event.target as CustomButton).name == "showSimulasi")
			{
				Main.mySo.data.menuutamatut4 = "sudah";
			}
			else if((event.target as CustomButton).name == "showEvaluasi")
			{
				Main.mySo.data.menuutamatut5 = "sudah";
			}
			else if((event.target as CustomButton).name == "showPengguna")
			{
				Main.mySo.data.menuutamatut6 = "sudah";
			}

			Main.klik();
			exitoastHandlerBoolean = true;
			_data.dasboard = tabs.selectedIndex;
			_data.scrollerContainerVSP = scrollerContainer.verticalScrollPosition;
			
			this.dispatchEventWith((event.target as CustomButton).name);
		}
		
		private function customHeaderFactory():Header
		{
			var header:Header = new Header();
			
			var leftButton:Button = new Button();
			leftButton.styleNameList.add("menu-button");
			leftButton.addEventListener(Event.TRIGGERED, leftButton_triggeredHandler);
			
			header.leftItems = new <DisplayObject>[leftButton];
			
			var rightButton:Button = new Button();
			rightButton.styleNameList.add("help-button");
			rightButton.addEventListener(Event.TRIGGERED, rightButton_triggeredHandler);
			
			header.rightItems = new <DisplayObject>[rightButton];
			
			return header;
		}
		
		private function exitHandler():void
		{
			exitHandlerBoolean = !exitHandlerBoolean;
			
			if(exitHandlerBoolean || exitoastHandlerBoolean)
			{
				if(!exitoastHandlerBoolean)
				{
					if(ToastExtension.isSupported)
					{
						var toast:ToastExtension = new ToastExtension();
						toast.setText("Tekan sekali lagi untuk keluar");
						toast.setDuration(DurationEnum.LENGTH_LONG);
						toast.show();
					}
					
					var delayedCall:DelayedCall = new DelayedCall(function():void
					{
						Starling.juggler.remove(delayedCall);
						exitHandlerBoolean = !exitHandlerBoolean;
					}, 3);
					Starling.juggler.add(delayedCall);
				}
				else
				{
					exitHandlerBoolean = !exitHandlerBoolean;
				}
			}
			else
			{
				NativeApplication.nativeApplication.exit();
			}
		}
		
		private function menuHandler():void
		{
			_data.dasboard = tabs.selectedIndex;
			_data.savedMenuUtamaVSP = this.verticalScrollPosition;
			this.dispatchEventWith(EventType.TOGGlE_LEFTDRAWER);
		}
		
		private function rightButton_triggeredHandler(event:Event):void
		{
			Main.klik();
			
			exitoastHandlerBoolean = true;
			_data.petunjuk = "petunjukbelajarpecahan";
			_data.dasboard = tabs.selectedIndex;
			_data.scrollerContainerVSP = scrollerContainer.verticalScrollPosition;
			this.dispatchEventWith(EventType.SHOW_PETUNJUK);
		}
		
		private function leftButton_triggeredHandler(event:Event):void
		{
			Main.klik();
			
			_data.scrollerContainerVSP = null;
			_data.dasboard = tabs.selectedIndex;
			this.dispatchEventWith(EventType.TOGGlE_LEFTDRAWER);
		}
	}
}