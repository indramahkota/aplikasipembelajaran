﻿package views
{
	import com.indramahkota.ane.toast.DurationEnum;
	import com.indramahkota.ane.toast.ToastExtension;

	import constants.EventType;

	import feathers.controls.Button;
	import feathers.controls.Header;
	import feathers.controls.ImageLoader;
	import feathers.controls.Label;
	import feathers.controls.LayoutGroup;
	import feathers.controls.List;
	import feathers.controls.PanelScreen;
	import feathers.controls.ScrollBarDisplayMode;
	import feathers.controls.ScrollPolicy;
	import feathers.controls.renderers.DefaultListItemRenderer;
	import feathers.data.ListCollection;
	import feathers.layout.HorizontalAlign;
	import feathers.layout.HorizontalLayout;
	import feathers.layout.VerticalLayout;
	import feathers.layout.VerticalLayoutData;

	import flash.desktop.NativeApplication;
	import flash.geom.Rectangle;
	import flash.system.System;

	import starling.animation.DelayedCall;
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.events.Event;

	import utils.CustomButton;
	import utils.CustomListItemRenderer;
	
	/**
	 * ...
	 * @author Indra Mahkota
	 */
	public class Tentang extends PanelScreen
	{
		/** Semua layout yang digunakan. */
		private var currLayout:VerticalLayout;
		private var keteranganLayout:VerticalLayout;
		private var profilLayout:HorizontalLayout;

		/** Semua LayoutGroup yang digunakan. */
		private var profilPengembang_lg:LayoutGroup;
		private var alatPengembangan_lg:LayoutGroup;
		private var profil_lg:LayoutGroup;
		private var keterangan_lg:LayoutGroup;

		/** Card 1: Profil Pengembang. */
		private var tombolJudul_pp:CustomButton;
		private var fotoProfil_pp:ImageLoader;
		private var labelNama_pp:Label;
		private var labelEmail_pp:Label;
		private var garisPembatas_pp:ImageLoader;

		/** Card 2: Alat Pengembangan. */
		private var tombolJudul_ap:CustomButton;
		private var listAlat_ap:List;

		/** Pengaplikasian klik kembali dua kali untuk keluar. */
		private var exitHandlerBoolean:Boolean = false;
		private var exitoastHandlerBoolean:Boolean = false;
		
		/** Berbagi data antar class yang berada dalam navigator. */
		protected var _data:NavigatorData;
		
		public function Tentang()
		{
			super();
		}
		
		public function get data():NavigatorData  { return _data; }
		
		public function set data(value:NavigatorData):void  { _data = value; }
		
		override protected function initialize():void
		{
			this.title = "Tentang";
			this.backButtonHandler = exitHandler;
			this.menuButtonHandler = menuHandler;
			this.headerFactory = customHeaderFactory;
			this.scrollBarDisplayMode = ScrollBarDisplayMode.FLOAT;
			
			currLayout = new VerticalLayout();
			currLayout.paddingBottom = 10;
			this.layout = currLayout;

			// Card 1
			profilPengembang_lg = createLayotGroup();
			this.addChild(profilPengembang_lg);

			tombolJudul_pp = createButton("Profil Pengembang");
			profilPengembang_lg.addChild(tombolJudul_pp);

			profilLayout = new HorizontalLayout();
			profilLayout.paddingLeft = 20;
			profilLayout.paddingRight = 20;
			profilLayout.paddingTop = 20;
			profilLayout.gap = 10;

			profil_lg = new LayoutGroup();
			profil_lg.layout = profilLayout;
			profil_lg.layoutData = new VerticalLayoutData(100, NaN);
			//profil_lg.backgroundSkin = new Quad(10, 10, 0xf8030); // Debug
			profilPengembang_lg.addChild(profil_lg);

			fotoProfil_pp = new ImageLoader();
			fotoProfil_pp.source = Main.getImage("avatar_3s");
			fotoProfil_pp.setSize(55, 55);
			profil_lg.addChild(fotoProfil_pp);

			keteranganLayout = new VerticalLayout();
			keteranganLayout.gap = 2;

			keterangan_lg = new LayoutGroup();
			keterangan_lg.layout = keteranganLayout;
			profil_lg.addChild(keterangan_lg);

			labelNama_pp = new Label();
			labelNama_pp.text = "Indra Mahkota";
			keterangan_lg.addChild(labelNama_pp);

			labelEmail_pp = new Label();
			labelEmail_pp.text = "indramahkota1@gmail.com";
			keterangan_lg.addChild(labelEmail_pp);

			//Card 1
			alatPengembangan_lg = createLayotGroup();
			this.addChild(alatPengembangan_lg);

			tombolJudul_ap = createButton("Alat Pengembangan Aplikasi");
			alatPengembangan_lg.addChild(tombolJudul_ap);

			listAlat_ap = new List();
			listAlat_ap.isSelectable = false;
			listAlat_ap.horizontalScrollPolicy = ScrollPolicy.OFF;
			listAlat_ap.verticalScrollPolicy = ScrollPolicy.OFF;
			listAlat_ap.layoutData = new VerticalLayoutData(100, NaN);
			listAlat_ap.dataProvider = new ListCollection(
			[
				{label: "Visual Studio Code", icon: Main.getImage("vscodeicon")}, 
				{label: "Adobe AIR SDK & Compiler", icon: Main.getImage("adobeairicon")}, 
				{label: "ActionScript 3.0", icon: Main.getImage("as3icon")},
				{label: "Starling Framework", icon: Main.getImage("starlingicon")},
				{label: "Feathers User Interface", icon: Main.getImage("feathersicon")}
			]);
			listAlat_ap.itemRendererFactory = function():DefaultListItemRenderer
			{
				var renderer:CustomListItemRenderer = new CustomListItemRenderer();
				renderer.paddingLeft = 20;
				renderer.labelField = "label";
				renderer.iconSourceField = "icon";
				renderer.defaultSkin = new Quad(10, 10, 0xffffff);
				renderer.iconLoaderFactory = function():ImageLoader
				{
					var loader:ImageLoader = new ImageLoader();
					loader.pixelSnapping = true;
					loader.setSize(32, 32);
					return loader;
				}
				return renderer;
			}
			alatPengembangan_lg.addChild(listAlat_ap);

			/* tombolJudul_ap = createButton("Alat Pengembangan Aplikasi");
			profilPengembang_lg.addChild(tombolJudul_ap);

			listAlat_ap = new List();
			listAlat_ap.isSelectable = false;
			listAlat_ap.paddingBottom = 50;
			listAlat_ap.horizontalScrollPolicy = ScrollPolicy.OFF;
			listAlat_ap.verticalScrollPolicy = ScrollPolicy.OFF;
			listAlat_ap.layoutData = new VerticalLayoutData(100, NaN);
			listAlat_ap.dataProvider = new ListCollection(
			[
				{label: "Visual Studio Code", icon: Main.getImage("vscodeicon")}, 
				{label: "Adobe AIR SDK & Compiler", icon: Main.getImage("adobeairicon")}, 
				{label: "ActionScript 3.0", icon: Main.getImage("as3icon")},
				{label: "Starling Framework", icon: Main.getImage("starlingicon")},
				{label: "Feathers User Interface", icon: Main.getImage("feathersicon")}
			]);
			listAlat_ap.itemRendererFactory = function():DefaultListItemRenderer
			{
				var renderer:CustomListItemRenderer = new CustomListItemRenderer();
				renderer.paddingLeft = 20;
				renderer.labelField = "label";
				renderer.iconSourceField = "icon";
				renderer.iconLoaderFactory = function():ImageLoader
				{
					var loader:ImageLoader = new ImageLoader();
					loader.pixelSnapping = true;
					loader.setSize(32, 32);
					return loader;
				}
				return renderer;
			}
			profilPengembang_lg.addChild(listAlat_ap); */

			if(_data.savedTentangVSP)
			{
				this.verticalScrollPosition = _data.savedTentangVSP;
			}
			
			super.initialize();
		}
		
		override public function dispose():void
		{
			System.pauseForGCIfCollectionImminent(0);
			System.gc();
			
			super.dispose();
		}
		
		private function createLayotGroup():LayoutGroup
		{
			var image:Image = new Image(Main.roundQuadBackground);
			image.pixelSnapping = true;
			image.scale9Grid = new Rectangle(20, 20, 80, 80);
			
			var layout:VerticalLayout = new VerticalLayout();
			layout.horizontalAlign = HorizontalAlign.CENTER;
			layout.paddingTop = 10;
			layout.paddingRight = layout.paddingLeft = 10;
			layout.paddingBottom = 50;
			
			var lg:LayoutGroup = new LayoutGroup();
			lg.backgroundSkin = image;
			lg.layout = layout;
			lg.layoutData = new VerticalLayoutData(100, NaN);
			return lg;
		}
		
		private function createButton(label:String):CustomButton
		{
			var button:CustomButton = new CustomButton();
			button.label = label;
			button.styleNameList.add("headertest-button");
			button.layoutData = new VerticalLayoutData(100, NaN);
			return button;
		}
		
		private function createLabel(text:String):Label
		{
			var label:Label = new Label();
			label.text = text;
			label.padding = 20;
			label.textRendererProperties.wordWrap = true;
			label.layoutData = new VerticalLayoutData(100, NaN);
			return label;
		}
		
		private function createImageLoader():ImageLoader
		{
			var image:ImageLoader = new ImageLoader();
			image.source = Main.getImage("line_x");
			image.scale9Grid = new Rectangle(1, 1, 48, 48);
			image.paddingLeft = image.paddingRight = 20;
			image.pixelSnapping = true;
			image.layoutData = new VerticalLayoutData(100, NaN);
			return image;
		}
		
		private function customHeaderFactory():Header
		{
			var header:Header = new Header();
			
			var leftButton:Button = new Button();
			leftButton.styleNameList.add("menu-button");
			leftButton.addEventListener(Event.TRIGGERED, leftButton_triggeredHandler);
			
			header.leftItems = new <DisplayObject>[leftButton];
			
			var rightButton:Button = new Button();
			rightButton.styleNameList.add("help-button");
			rightButton.addEventListener(Event.TRIGGERED, rightButton_triggeredHandler);
			
			header.rightItems = new <DisplayObject>[rightButton];
			
			return header;
		}
		
		private function exitHandler():void
		{
			exitHandlerBoolean = !exitHandlerBoolean;
			
			if(exitHandlerBoolean || exitoastHandlerBoolean)
			{
				if(!exitoastHandlerBoolean)
				{
					if(ToastExtension.isSupported)
					{
						var toast:ToastExtension = new ToastExtension();
						toast.setText("Tekan sekali lagi untuk keluar");
						toast.setDuration(DurationEnum.LENGTH_LONG);
						toast.show();
					}
					
					var delayedCall:DelayedCall = new DelayedCall(function():void
					{
						Starling.juggler.remove(delayedCall);
						exitHandlerBoolean = !exitHandlerBoolean;
					}, 3);
					Starling.juggler.add(delayedCall);
				}
				else
				{
					exitHandlerBoolean = !exitHandlerBoolean;
				}
			}
			else
			{
				NativeApplication.nativeApplication.exit();
			}
		}
		
		private function menuHandler():void
		{
			this.dispatchEventWith(EventType.TOGGlE_LEFTDRAWER);
		}
		
		private function leftButton_triggeredHandler(event:Event):void
		{
			Main.klik();
			
			this.dispatchEventWith(EventType.TOGGlE_LEFTDRAWER);
		}
		
		private function rightButton_triggeredHandler(event:Event):void
		{
			Main.klik();
			
			exitoastHandlerBoolean = true;
			_data.petunjuk = "petunjuktentang";
			this.dispatchEventWith(EventType.SHOW_PETUNJUK);
		}
	}
}