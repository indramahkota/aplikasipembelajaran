package views
{
	import feathers.controls.Button;
	import feathers.controls.ButtonState;
	import feathers.controls.ImageLoader;
	import feathers.controls.Label;
	import feathers.controls.Screen;
	import feathers.controls.ToggleButton;
	import feathers.events.FeathersEventType;
	import feathers.layout.AnchorLayout;
	import feathers.skins.ImageSkin;

	import flash.display.StageDisplayState;
	import flash.display.StageOrientation;
	import flash.events.AsyncErrorEvent;
	import flash.events.NetStatusEvent;
	import flash.events.SecurityErrorEvent;
	import flash.events.TimerEvent;
	import flash.filesystem.File;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.net.NetConnection;
	import flash.net.NetStream;
	import flash.system.System;
	import flash.utils.Timer;
	import flash.utils.setTimeout;

	import starling.core.Starling;
	import starling.display.BlendMode;
	import starling.display.Image;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.textures.Texture;

	import utils.RoundedQuad;
	
	/**
	 * ...
	 * @author Indra Mahkota
	 */
	public class VideoViewerfs extends Screen
	{
		private var ns:NetStream;
		private var nc:NetConnection;
		
		private var file:File;
		private var image:Image;
		private var texture:Texture;
		
		private var teksWaktu:Label;
		private var togglebtn:ToggleButton;
		private var bgSlider:ImageLoader;
		private var upSlider:ImageLoader;
		private var buttonHandler:Button;
		
		private var timerWIdth:Number;
		private var timerHeight:Number;
		private var seekWidth:Number;
		
		private var isPlay:Boolean;
		private var duration:Number = 0;
		private var totalDuration:Number;

		private var arrayData:Array = [];

		private var fsButton:Button;
		private var background:RoundedQuad;
		private var isVisible:Boolean;
		
		private var timer:Timer;
		private var touchPointID:int = -1;
		
		private var pointInBounds:Point = new Point();
		private static const HELPER_POINT:Point = new Point();
		private static const HELPER_TOUCHES_VECTOR:Vector.<Touch> = new <Touch>[];
		
		protected var _data:NavigatorData;
		
		public function VideoViewerfs()
		{
			super();
		}
		
		public function get data():NavigatorData  { return _data; }
		
		public function set data(value:NavigatorData):void  { _data = value; }
		
		override protected function initialize():void
		{
			this.layout = new AnchorLayout();
			this.backButtonHandler = backHandler;

			if(Main.statusbar) Main.statusbar.visible = false;
			Starling.current.nativeStage.setOrientation(StageOrientation.ROTATED_RIGHT)
			Starling.current.nativeStage.displayState = StageDisplayState.FULL_SCREEN_INTERACTIVE;
			
			var referensiData:int;
			if(_data.referensiMateri)
			{
				referensiData = int(_data.referensiMateri);
			}

			arrayData = Main.getJsonAssets("materi");

			var objek:Object = arrayData[referensiData];

			totalDuration = objek.totalduration;
			file = File.applicationDirectory.resolvePath(objek.urlvideo);
			
			background = new RoundedQuad(25, stage.stageWidth - 40, 50, 0x00D2D2);
			background.x = 20;
			background.y = stage.stageHeight - 50;
			this.addChild(background);
			
			var fsSkin:ImageSkin = new ImageSkin(Main.getImage("fsexit-up"));
			fsSkin.setTextureForState(ButtonState.DOWN, Main.getImage("fsexit-down"));
			fsSkin.width = fsSkin.height = 25;
			
			fsButton = new Button();
			fsButton.defaultSkin = fsSkin;
			fsButton.y = stage.stageHeight - 35;
			fsButton.x = stage.stageWidth - 75;
			fsButton.addEventListener(Event.TRIGGERED, fulscreenHandler);
			this.addChild(fsButton);

			var skin:ImageSkin = new ImageSkin();
			skin.setTextureForState(ButtonState.UP, Main.getImage("play-up"));
			skin.setTextureForState(ButtonState.DOWN, Main.getImage("play-down"));
			skin.setTextureForState(ButtonState.HOVER, Main.getImage("play-up"));
			skin.setTextureForState(ButtonState.UP_AND_SELECTED, Main.getImage("pause-up"));
			skin.setTextureForState(ButtonState.DOWN_AND_SELECTED, Main.getImage("pause-down"));
			skin.setTextureForState(ButtonState.HOVER_AND_SELECTED, Main.getImage("pause-up"));
			skin.width = skin.height = 25;
			
			togglebtn = new ToggleButton();
			togglebtn.x = 50;
			togglebtn.y = stage.stageHeight - 35;
			togglebtn.hasLabelTextRenderer = false;
			togglebtn.addEventListener(Event.TRIGGERED, togglebtnHandler);
			togglebtn.defaultIcon = skin;
			this.addChild(togglebtn);
			
			teksWaktu = new Label();
			teksWaktu.text = "00:00 / 00:00";
			teksWaktu.validate();

			timerWIdth = teksWaktu.width;
			timerHeight = teksWaktu.height;

			teksWaktu.x = stage.stageWidth - (timerWIdth + 85);
			teksWaktu.y = stage.stageHeight -  (timerHeight + 10 + ((25 - timerHeight)/2) + 1);
			teksWaktu.touchable = false;
			this.addChild(teksWaktu);
			
			seekWidth = stage.stageWidth - (130 + 25 + timerWIdth + 25);
			
			bgSlider = new ImageLoader();
			bgSlider.source = Main.getImage("seek-bg");
			bgSlider.scale9Grid = new Rectangle(4, 4, 1, 1);
			bgSlider.pixelSnapping = true;
			bgSlider.height = 12;
			bgSlider.width = seekWidth;
			
			bgSlider.x = 85;
			bgSlider.y = stage.stageHeight - (10 + 12 + ((25 - 12)/2));
			
			bgSlider.touchable = false;
			this.addChild(bgSlider);
			
			upSlider = new ImageLoader();
			upSlider.source = Main.getImage("seek-up");
			upSlider.scale9Grid = new Rectangle(4, 4, 1, 20);
			upSlider.pixelSnapping = true;
			upSlider.height = 12;

			upSlider.x = 85;
			upSlider.y = stage.stageHeight - (10 + 12 + ((25 - 12)/2));

			upSlider.touchable = false;
			this.addChild(upSlider);
			
			var skinbuttonHandler:ImageSkin = new ImageSkin(Main.getImage("seek-bg"));
			skinbuttonHandler.scale9Grid = new Rectangle(4, 4, 1, 1);
			skinbuttonHandler.height = 12;
			
			buttonHandler = new Button();
			buttonHandler.defaultSkin = skinbuttonHandler;
			buttonHandler.height = 50;
			buttonHandler.width = seekWidth;
			buttonHandler.alpha = 0;

			buttonHandler.x = 85;
			buttonHandler.y = stage.stageHeight - (10 + 12 + ((25 - 12)/2) + 3);

			buttonHandler.addEventListener(TouchEvent.TOUCH, progressbuttonHandler);
			this.addChild(buttonHandler);

			background.alpha = 0;
			fsButton.alpha = 0;
			togglebtn.alpha = 0;
			teksWaktu.alpha = 0;
			bgSlider.alpha = 0;
			upSlider.alpha = 0;

			togglebtn.touchable = false;
			fsButton.touchable = false;
			buttonHandler.touchable = false;

			this.addEventListener(FeathersEventType.RESIZE, resizeHandler);

			if(_data.isPlay)
			{
				isPlay = _data.isPlay;
			}

			if(_data.seekVideo)
			{
				duration = _data.seekVideo;
			}
			
			setupConnection();
			super.initialize();
		}

		private function resizeHandler(event:Event):void
		{
			background.fRadius = 25;
			background.fHeight = 50;
			background.fWidth = stage.stageWidth - 40;
			background.y = stage.stageHeight - 51;

			fsButton.y = stage.stageHeight - 35;
			fsButton.x = stage.stageWidth - 75;

			togglebtn.y = stage.stageHeight - 35;

			teksWaktu.y = stage.stageHeight -  (timerHeight + 10 + ((25 - timerHeight)/2) + 1);
			teksWaktu.x = stage.stageWidth - (timerWIdth + 85);

			seekWidth = stage.stageWidth - (130 + 25 + timerWIdth + 25);

			bgSlider.width = seekWidth;
			bgSlider.y = stage.stageHeight - (10 + 12 + ((25 - 12)/2));

			upSlider.width = seekWidth;
			upSlider.y = stage.stageHeight - (10 + 12 + ((25 - 12)/2));

			teksWaktu.text = textDuration(duration) + " / " + textDuration(totalDuration);
			updateTextWidth();
				
			upSlider.width = (duration / totalDuration) * seekWidth;

			buttonHandler.width = seekWidth;
			buttonHandler.y = stage.stageHeight - (10 + 12 + ((25 - 12)/2) + 3);
		}
		
		override public function dispose():void
		{
			if(ns != null)
			{
				ns.close();
				ns = null;
			}
			
			if(nc != null)
			{
				nc.close();
				nc = null;
			}
			
			if(image != null)
			{
				image.dispose();
				image = null;
			}
			
			if(texture != null)
			{
				texture.dispose();
				texture = null;
			}
			
			if(this.hasEventListener(Event.ENTER_FRAME))
			{
				this.removeEventListener(Event.ENTER_FRAME, updateHandler);
			}
			
			System.pauseForGCIfCollectionImminent(0);
			System.gc();

			super.dispose();
		}
		
		private function setupConnection():void
		{
			nc = new NetConnection();
			nc.addEventListener(NetStatusEvent.NET_STATUS, onNetStatus);
			nc.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
			nc.connect(null);
		}
		
		private function setupStream():void
		{
			ns = new NetStream(nc);
			ns.client = new VideoPlayerNetStreamClient(
					onMetaDataHandler, onCuePointHandler,
					onXMPData);

			ns.addEventListener(NetStatusEvent.NET_STATUS, onNetStatus);
			ns.addEventListener(AsyncErrorEvent.ASYNC_ERROR, asyncErrorHandler);
						
			texture = Texture.fromNetStream(ns, 1, onTextureComplete);
			
			texture.root.onRestore = function():void
			{
				texture.root.attachNetStream(ns, onTextureComplete);
				ns.play(file.url);

				duration = _data.seekVideo;
			}
			
			ns.play(file.url);
		}

		private function onMetaDataHandler(metaData:Object):void
		{
			totalDuration = Number(metaData.duration);
			teksWaktu.text = "00:00 / " + textDuration(totalDuration);
			updateTextWidth();

			trace("Total duration: " + Number(metaData.duration));
		}

		private function onCuePointHandler(item:Object):void
		{
			trace("[CuePoint] onCuePoint");
		}

		private function onXMPData(item:Object):void
		{
			trace("[XMPData] onXMPData");
		}
		
		private function onTextureComplete():void
		{
			if(this.contains(image))
			{
				removeChild(image);
			}

			image = new Image(texture);
			image.blendMode = BlendMode.NONE;
			image.visible = false;
			image.addEventListener(TouchEvent.TOUCH, interactionHandler);
			
			var scale:Number;
			var stgVideoScale:Number = stage.stageWidth / stage.stageHeight;
			var videoScale:Number = image.width / image.height;

			//2 kemungkinan: portrait/landscape
			//yang diikuti adalah container

			if(stgVideoScale > videoScale)
			{
				scale = stage.stageHeight / image.height;
				image.scaleX = image.scaleY = scale;
				image.x = (stage.stageWidth - image.width) / 2;
			}
			else if(stgVideoScale < videoScale)
			{
				scale = stage.stageWidth / image.width;
				image.scaleX = image.scaleY = scale;
				image.y = (stage.stageHeight - image.height) / 2;
			}

			addChildAt(image, 0);
			setTimeout(munculkanVideo, 150);
		}

		private function interactionHandler(event:TouchEvent):void
		{
			var touch:Touch;
			var touches:Vector.<Touch> = event.getTouches(image, null, HELPER_TOUCHES_VECTOR);
			
			if(touchPointID >= 0)
			{
				for each (var currentTouch:Touch in touches)
				{
					if(currentTouch.id == touchPointID)
					{
						touch = currentTouch;
						break;
					}
				}
				
				if(!touch)
				{
					HELPER_TOUCHES_VECTOR.length = 0;
					return;
				}
				
				if(touch.phase == TouchPhase.MOVED)
				{
					touch.getLocation(image, HELPER_POINT);
					var isInBounds:Boolean = image.hitTest(HELPER_POINT) !== null;
					
					if(isInBounds && !isVisible)
					{
						aktifkanKontrol();
					}
				}
				else if(touch.phase == TouchPhase.ENDED)
				{
					touchPointID = -1;

					if(timer != null)
					{
						timer.stop();
						timer = null;
					}

					timer = new Timer(1000, 3);
					timer.addEventListener(TimerEvent.TIMER_COMPLETE, nonAktifkanKontrol);
					timer.start();
				}
			}
			else
			{
				for each (touch in touches)
				{
					if(touch.phase == TouchPhase.BEGAN && !isVisible)
					{
						aktifkanKontrol();
						touchPointID = touch.id;
					}
				}
			}
			HELPER_TOUCHES_VECTOR.length = 0;
		}

		private function aktifkanKontrol():void
		{
			isVisible = true;
			background.alpha = 0.75;
			fsButton.alpha = 1;
			togglebtn.alpha = 1;
			teksWaktu.alpha = 1;
			bgSlider.alpha = 1;
			upSlider.alpha = 1;

			if(timer != null)
			{
				timer.stop();
			}

			togglebtn.touchable = true;
			fsButton.touchable = true;
			buttonHandler.touchable = true;
		}

		private function nonAktifkanKontrol(event:TimerEvent):void
		{
			isVisible = false;
			background.alpha = 0;
			fsButton.alpha = 0;
			togglebtn.alpha = 0;
			teksWaktu.alpha = 0;
			bgSlider.alpha = 0;
			upSlider.alpha = 0;

			togglebtn.touchable = false;
			fsButton.touchable = false;
			buttonHandler.touchable = false;

			if(timer != null)
			{
				timer.removeEventListener(TimerEvent.TIMER_COMPLETE, nonAktifkanKontrol);
				timer = null;
			}
		}
		
		private function securityErrorHandler(event:SecurityErrorEvent):void
		{
			trace("securityErrorHandler: " + event);
		}
		
		private function asyncErrorHandler(event:AsyncErrorEvent):void
		{
			trace("asyncErrorHandler: " + event);
		}
		
		private function onNetStatus(e:NetStatusEvent):void 
		{
			switch (e.info.code)
			{
				case "NetStream.Play.Start": 
					
					if(_data.seekVideo)
					{
						ns.seek(_data.seekVideo);
					}
					else
					{
						ns.seek(duration);
					}

					if(isPlay)
					{
						ns.resume();
						togglebtn.isSelected = isPlay;
						this.addEventListener(Event.ENTER_FRAME, updateHandler);
					}
					else
					{
						ns.pause();
					}

					teksWaktu.text = textDuration(duration) + " / " + textDuration(totalDuration);
					upSlider.width = (duration / totalDuration) * seekWidth;
					updateTextWidth();
					
					break;
					
				case "NetStream.Play.Stop": 

					duration = 0;
					isPlay = false;
					togglebtn.isSelected = false;

					this.removeEventListener(Event.ENTER_FRAME, updateHandler);

					ns.play(file.url);
					image.visible = false;
					setTimeout(munculkanVideo, 150);
					
					break;
					
				case "NetConnection.Connect.Success": 
					setupStream();
					
					break;
					
				case "NetStream.Play.StreamNotFound": 
					trace(e.info.code, "Unable to locate video data ");
					break;
					
				default : 
					break;
			}
		}
		
		private function munculkanVideo():void
		{
			if(!image) return;//jika image belum dibuat, return;
			image.visible = true;
			togglebtn.touchable = true;
		}
		
		private function togglebtnHandler(event:Event):void
		{
			var toggle:ToggleButton = ToggleButton(event.currentTarget);
			
			if(!toggle.isSelected)
			{
				isPlay = true;
				ns.resume();
				this.addEventListener(Event.ENTER_FRAME, updateHandler);
			}
			else
			{
				isPlay = false;
				ns.pause();
				this.removeEventListener(Event.ENTER_FRAME, updateHandler);
			}
		}
		
		private function progressbuttonHandler(event:TouchEvent):void
		{
			if(!buttonHandler.isEnabled)
			{
				touchPointID = -1;
				return;
			}
			
			var touch:Touch;
			var touches:Vector.<Touch> = event.getTouches(this, null, HELPER_TOUCHES_VECTOR);
			
			if(touches.length == 0)
			{
				return;
			}
			
			if(touchPointID >= 0)
			{
				for each (var currentTouch:Touch in touches)
				{
					if(currentTouch.id == touchPointID)
					{
						touch = currentTouch;
						break;
					}
				}
				
				if(!touch)
				{
					HELPER_TOUCHES_VECTOR.length = 0;
					return;
				}
				
				if(touch.phase == TouchPhase.MOVED)
				{
					touch.getLocation(buttonHandler, HELPER_POINT);
					
					var isInBounds:Boolean = buttonHandler.hitTest(HELPER_POINT) != null;
					
					if(isInBounds || buttonHandler.keepDownStateOnRollOut)
					{
						pointInBounds = touch.getLocation(buttonHandler);
						toucMovedHandler(pointInBounds.x);
						aktifkanKontrol();
					}
				}
				else if(touch.phase == TouchPhase.ENDED)
				{
					touchPointID = -1;
					
					pointInBounds = touch.getLocation(buttonHandler);
					touchEndHandler(pointInBounds.x, isInBounds);

					if(timer != null)
					{
						timer.stop();
						timer = null;
					}

					timer = new Timer(1000, 3);
					timer.addEventListener(TimerEvent.TIMER_COMPLETE, nonAktifkanKontrol);
					timer.start();
				}
			}
			else
			{
				for each (touch in touches)
				{
					if(touch.phase == TouchPhase.BEGAN)
					{
						touchPointID = touch.id;

						aktifkanKontrol();
						
						pointInBounds = touch.getLocation(buttonHandler);
						touchBeganHandler(pointInBounds.x);
						
						break;
					}
				}
			}
			HELPER_TOUCHES_VECTOR.length = 0;
		}
		
		private function touchBeganHandler(position:Number):void
		{
			this.removeEventListener(Event.ENTER_FRAME, updateHandler);
			ns.pause();
			
			updateVideo(position);
		}
		
		private function toucMovedHandler(position:Number):void
		{
			updateVideo(position);
		}
		
		private function touchEndHandler(position:Number, inBounds:Boolean):void
		{
			if(togglebtn.isSelected)
			{
				this.addEventListener(Event.ENTER_FRAME, updateHandler);
				ns.resume();
			}
			
			if(!inBounds) return;
			
			updateVideo(position);
		}
		
		private function updateVideo(position:Number):void
		{
			var ratio:Number = position / seekWidth;
			
			upSlider.width = ratio * seekWidth;
			ns.seek(ratio * totalDuration);
			teksWaktu.text = textDuration(ratio * totalDuration) + " / " + textDuration(totalDuration);
		}
		
		private function updateHandler(event:Event):void
		{
			duration = ns.time;

			teksWaktu.text = textDuration(duration) + " / " + textDuration(totalDuration);
			updateTextWidth();
			
			var ratio:Number = duration / totalDuration;
			upSlider.width = ratio * seekWidth;
		}
		
		private function textDuration(duration:Number):String
		{
			var detik:int = Math.floor(duration % 60);
			var menit:int = Math.floor((duration % 3600) / 60);
			
			var detik_text:String = detik < 10 ? detik_text = ("0" + String(detik)) : detik_text = String(detik);
			var menit_text:String = menit < 10 ? menit_text = ("0" + String(menit)) : menit_text = String(menit);
			
			return menit_text + ":" + detik_text;
		}
		
		private function updateTextWidth():void
		{
			var tempNum:Number;
			teksWaktu.validate();
			tempNum = teksWaktu.width;
			
			if(tempNum != timerWIdth)
			{
				timerWIdth = tempNum;
				teksWaktu.x = stage.stageWidth - (timerWIdth + 20);
				
				seekWidth = stage.stageWidth - (40 + timerWIdth + 45);
				
				bgSlider.width = seekWidth;
				upSlider.width = seekWidth;
				buttonHandler.width = seekWidth;
			}
		}

		private function fulscreenHandler(event:Event):void
		{
			stroreDataHandler();

			if(Main.statusbar) Main.statusbar.visible = true;
			Starling.current.nativeStage.setOrientation(StageOrientation.DEFAULT);
			Starling.current.nativeStage.displayState = StageDisplayState.NORMAL;
			this.dispatchEventWith(Event.COMPLETE);
		}
		
		private function backHandler():void
		{
			stroreDataHandler();

			if(Main.statusbar) Main.statusbar.visible = true;
			Starling.current.nativeStage.setOrientation(StageOrientation.DEFAULT);
			Starling.current.nativeStage.displayState = StageDisplayState.NORMAL;
			this.dispatchEventWith(Event.COMPLETE);
		}

		private function stroreDataHandler():void
		{
			_data.seekVideo = ns.time;
			_data.isPlay = isPlay;
		}
	}
}

dynamic class VideoPlayerNetStreamClient
{
	public function VideoPlayerNetStreamClient(onMetaDataCallback:Function,
		onCuePointCallback:Function, onXMPDataCallback:Function)
	{
		this.onMetaDataCallback = onMetaDataCallback;
		this.onCuePointCallback = onCuePointCallback;
		this.onXMPDataCallback = onXMPDataCallback;
	}

	public var onMetaDataCallback:Function;

	public var onCuePointCallback:Function;

	public function onMetaData(metadata:Object):void
	{
		this.onMetaDataCallback(metadata);
	}

	public function onCuePoint(cuePoint:Object):void
	{
		this.onCuePointCallback(cuePoint);
	}

	public function onXMPData(xmpData:Object):void
	{
		this.onXMPDataCallback(xmpData);
	}
}