﻿package views
{
	import com.indramahkota.ane.toast.DurationEnum;
	import com.indramahkota.ane.toast.ToastExtension;

	import constants.EventType;
	import constants.Url;

	import feathers.controls.Alert;
	import feathers.controls.Button;
	import feathers.controls.ButtonState;
	import feathers.controls.DragGesture;
	import feathers.controls.Label;
	import feathers.controls.Screen;
	import feathers.controls.TextInput;
	import feathers.controls.TextInputState;
	import feathers.data.ListCollection;
	import feathers.events.FeathersEventType;
	import feathers.layout.AnchorLayout;
	import feathers.layout.AnchorLayoutData;
	import feathers.skins.ImageSkin;

	import flash.desktop.NativeApplication;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestHeader;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.system.System;

	import starling.animation.DelayedCall;
	import starling.core.Starling;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.filters.DropShadowFilter;
	import starling.text.TextFormat;
	import starling.textures.RenderTexture;
	import starling.textures.Texture;

	import utils.CustomButton;
	import utils.RoundedQuad;
	
	/**
	 * ...
	 * @author Indra Mahkota
	 */
	public class Masuk extends Screen
	{
		private var alert:Alert;
		private var loading:Label;
		
		private var tombolMasuk:Button;
		private var inputNamaPengguna:TextInput;
		private var inputKataSandi:TextInput;
		
		private var exitHandlerBoolean:Boolean = false;
		private var exitoastHandlerBoolean:Boolean = false;

		private var upTextureBtn:Texture;
		private var downTextureBtn:Texture;

		private var defaultTexture:Texture;
		private var focusTexture:Texture;
		private var errorTexture:Texture;
		
		private var urlL:URLLoader;
		private var urlR:URLRequest;
		private var urlV:URLVariables;
		private var textFormat:TextFormat = new TextFormat("SourceSansPro", 16);
		
		public function Masuk()
		{
			super();
		}
		
		override protected function initialize():void
		{
			this.layout = new AnchorLayout();
			this.backButtonHandler = exitHandler;

			defaultTexture = inputBackgroundSkin(0xd2d2d2);
			focusTexture = inputBackgroundSkin(0x00bfff);
			errorTexture = inputBackgroundSkin(0xff0000);
			
			var bgNamaPengguna:ImageSkin = new ImageSkin(defaultTexture);
			bgNamaPengguna.setTextureForState(TextInputState.FOCUSED, focusTexture);
			bgNamaPengguna.setTextureForState(TextInputState.ERROR, errorTexture);
			bgNamaPengguna.scale9Grid = new Rectangle(2, 2, 46, 46);

			var bgKataSandi:ImageSkin = new ImageSkin(defaultTexture);
			bgKataSandi.setTextureForState(TextInputState.FOCUSED, focusTexture);
			bgKataSandi.setTextureForState(TextInputState.ERROR, errorTexture);
			bgKataSandi.scale9Grid = new Rectangle(2, 2, 46, 46);

			var iconPengguna:ImageSkin = new ImageSkin(Main.getImage("penggunaicon"));
			iconPengguna.width = iconPengguna.height = 18 
			iconPengguna.alpha = 0.5;
			
			var iconPassword:ImageSkin = new ImageSkin(Main.getImage("passwordicon"));
			iconPassword.width = iconPassword.height = 18;
			iconPassword.alpha = 0.5;
			
			inputNamaPengguna = new TextInput();
			inputNamaPengguna.prompt = "Nama Pengguna";
			inputNamaPengguna.maxChars = 25;
			inputNamaPengguna.restrict = "a-zA-Z0-9 ";
			inputNamaPengguna.fontStyles = textFormat;
			inputNamaPengguna.promptFontStyles = textFormat;
			inputNamaPengguna.width = stage.stageWidth / 2;
			inputNamaPengguna.defaultIcon = iconPengguna;
			inputNamaPengguna.backgroundSkin = bgNamaPengguna;
			inputNamaPengguna.layoutData = new AnchorLayoutData(NaN, NaN, NaN, NaN, 0, -70);
			inputNamaPengguna.addEventListener(FeathersEventType.ENTER, inputNamaPenggunaonEnter);
			this.addChild(inputNamaPengguna);
			
			inputKataSandi = new TextInput();
			inputKataSandi.prompt = "Kata Sandi";
			inputKataSandi.maxChars = 25;
			inputKataSandi.restrict = "a-zA-Z0-9 ";
			inputKataSandi.displayAsPassword = true;
			inputKataSandi.fontStyles = textFormat;
			inputKataSandi.promptFontStyles = textFormat;
			inputKataSandi.width = stage.stageWidth / 2;
			inputKataSandi.defaultIcon = iconPassword;
			inputKataSandi.backgroundSkin = bgKataSandi;
			inputKataSandi.layoutData = new AnchorLayoutData(NaN, NaN, NaN, NaN, 0, -20);
			inputKataSandi.addEventListener(FeathersEventType.ENTER, inputKataSandiaonEnter);
			this.addChild(inputKataSandi);

			upTextureBtn = createlightupBackground();
			downTextureBtn = createlightdownBackground();

			var lightSkin:ImageSkin = new ImageSkin(upTextureBtn);
			lightSkin.setTextureForState(ButtonState.DOWN, downTextureBtn);
			lightSkin.scale9Grid = new Rectangle(20, 20, 80, 80);
			
			tombolMasuk = new Button();
			tombolMasuk.height = 65;
			tombolMasuk.label = "MASUK";
			tombolMasuk.defaultSkin = lightSkin;
			tombolMasuk.width = stage.stageWidth / 2 + 24;
			tombolMasuk.styleNameList.add("light-button");
			tombolMasuk.layoutData = new AnchorLayoutData(NaN, NaN, NaN, NaN, 0, 60);
			tombolMasuk.addEventListener(starling.events.Event.TRIGGERED, onTombolMasukTriggered);
			this.addChild(tombolMasuk);
			
			var tombolDaftar:CustomButton = new CustomButton();
			tombolDaftar.fontStyles = textFormat;
			tombolDaftar.label = "klik disini untuk membuat akun!";
			
			tombolDaftar.validate();
			
			var paddingLR:Number = ((stage.stageWidth - tombolDaftar.width) / 2) - 2;
			tombolDaftar.layoutData = new AnchorLayoutData(NaN, paddingLR, NaN, paddingLR, 0, 130);
			tombolDaftar.addEventListener(starling.events.Event.TRIGGERED, daftarscreen);
			this.addChild(tombolDaftar);
			
			super.initialize();
		}
		
		override public function dispose():void
		{
			textFormat = null;

			downTextureBtn.dispose();
			downTextureBtn = null;

			upTextureBtn.dispose();
			upTextureBtn = null;

			errorTexture.dispose();
			errorTexture = null;

			focusTexture.dispose();
			focusTexture = null;

			defaultTexture.dispose();
			defaultTexture = null;

			System.pauseForGCIfCollectionImminent(0);
			System.gc();
			
			super.dispose();
		}

		private function inputBackgroundSkin(color:uint):Texture
		{
			var quad:Quad = new Quad(50, 2, color);
			var matrix:Matrix = new Matrix();
			matrix.translate(0, 48);

			var rendertxture:RenderTexture = new RenderTexture(50, 50);
			rendertxture.draw(quad, matrix);
			return rendertxture;
		}

		private function onTombolMasukTriggered(event:starling.events.Event):void
		{
			Main.klik();
			validasiInput();
		}

		private function inputNamaPenggunaonEnter(event:starling.events.Event):void
		{
			trace("inputNamaPenggunaonEnter");
			inputKataSandi.setFocus();
		}

		private function inputKataSandiaonEnter(event:starling.events.Event):void
		{
			trace("inputKataSandiaonEnter");
			inputKataSandi.clearFocus();
			validasiInput();
		}
		
		private function daftarscreen():void
		{
			this.dispatchEventWith(EventType.SHOW_DAFTAR_SCREEN);
		}

		private function validasiInput():void
		{
			tombolMasuk.isEnabled = false;

			if(inputKataSandi.errorString != null)
			{
				inputKataSandi.errorString = null;
			}

			if(inputNamaPengguna.errorString != null)
			{
				inputNamaPengguna.errorString = null;
			}

			if(inputNamaPengguna.text.length > 2 && inputKataSandi.text.length > 2)
			{
				createLoadingLabel("Mohon tunggu...");
				getData(Url.FIREBASE_DATA_PENGGUNA_URL);
			}
			else
			{
				tombolMasuk.isEnabled = true;

				if(inputNamaPengguna.text.length < 4) inputNamaPengguna.errorString = "";
				if(inputKataSandi.text.length < 4) inputKataSandi.errorString = "";

				createLoadingLabel("Input harus lebih dari tiga karakter...");
				inputKataSandi.addEventListener(FeathersEventType.ENTER, inputKataSandiaonEnter);
			}
		}

		private function getData(url:String):void
		{
			var header:URLRequestHeader = new URLRequestHeader("Content-Type", "application/json");
			
			var request:URLRequest = new URLRequest(url);
			request.method = URLRequestMethod.GET;
			request.requestHeaders.push(header);

			var loader:URLLoader = new URLLoader();
			loader.addEventListener(flash.events.Event.COMPLETE, getDataCompleteHAndler);
			loader.addEventListener(IOErrorEvent.IO_ERROR, errorHandler);
			loader.load(request);
		}

		private function getDataCompleteHAndler(event:flash.events.Event):void
		{
			event.currentTarget.removeEventListener(flash.events.Event.COMPLETE, getDataCompleteHAndler);

			createLoadingLabel("Memeriksa nama pengguna...");

			var rawData:Object = JSON.parse(event.currentTarget.data);
			for(var parentKey:String in rawData)
			{
				if(inputNamaPengguna.text == rawData[parentKey]["namaPengguna"]
					&& inputKataSandi.text == rawData[parentKey]["kataSandi"])
				{
					var obj:Object = rawData[parentKey];
					
					Main.mySo.data.sessions = "true";
					
					Main.mySo.data.uid = parentKey;
					Main.mySo.data.namaLengkap = obj.namaLengkap;
					Main.mySo.data.namaPengguna = obj.namaPengguna;
					Main.mySo.data.kataSandi = obj.kataSandi;
					Main.mySo.data.visual = obj.visual;
					Main.mySo.data.auditorial = obj.auditorial;
					Main.mySo.data.kinestetik = obj.kinestetik;
					Main.mySo.data.dominan = obj.dominan;
					Main.mySo.data.nilai = obj.nilai;
					Main.mySo.data.avatar = obj.avatarPengguna;
					Main.mySo.data.indexAvatar = obj.indeksAvatar;

					if(obj.hasOwnProperty("sharedObject"))
					{
						var newObject:Object = obj.sharedObject;
						for(var prop:String in newObject)
						{
							Main.mySo.data[prop] = newObject[prop];
						}
					}
					
					Main.profileName.text = Main.firstLetterUpperCase(obj.namaLengkap + "\n nilai: " + obj.nilai);
					Main.profileImage.source = Main.getImage(obj.avatarPengguna);
					
					next();

					return;
				}
			}

			tombolMasuk.isEnabled = true;
			createLoadingLabel("Nama pengguna atau kata sandi salah.");
		}

		private function errorHandler(event:IOErrorEvent):void
		{
			tombolMasuk.isEnabled = true;
			trace(event.currentTarget.data);
			if(event.currentTarget.data != "")
			{
				createLoadingLabel(event.currentTarget.data);
			}
			else
			{
				createLoadingLabel("Tidak terkoneksi internet...");

				alert = Alert.show("Apakah Anda ingin memainkan aplikasi ini secara offline?", "Pesan", new ListCollection([{label: "YA"}, {label: "TIDAK"}]));
				
				alert.addEventListener(starling.events.Event.CLOSE, function(event:starling.events.Event, data:Object):void
				{
					if(data.label == "YA")
					{
						Main.klik();
						
						alert.removeFromParent(true);
						alert = null;
						
						Main.keluarSession();

						Main.mySo.data.nilai = "kosong";
						Main.mySo.data.dominan = "kosong";
						Main.mySo.data.namaLengkap = "Offline Mode";
						Main.profileName.text = "Offline Mode \n Nilai: Kosong";
						Main.profileImage.source = Main.getImage("profil_icon");
						
						next();
					}
					else
					{
						Main.klik();
						
						alert.removeFromParent(true);
						alert = null;
					}
				});
			}
		}
		
		private function createLoadingLabel(string:String):void
		{
			if(loading) loading.removeFromParent(true);
			
			var quad:Quad = new Quad(10, 10, 0x55D088);
			
			loading = new Label();
			loading.text = string;
			loading.minHeight = 20;
			loading.backgroundSkin = quad;
			loading.styleNameList.add("loading-center-label-bg");
			loading.layoutData = new AnchorLayoutData(NaN, 0, 0, 0, NaN, NaN);
			this.addChild(loading);
		}
		
		private function ubahNilai(nilai:int):String
		{
			var string:String;
			string = "avatar_" + String(nilai + 1) + "s";
			return string;
		}
		
		private function next():void
		{
			Main.changeScreenUsingList = true;
			
			var drawersEvent:starling.events.Event = new starling.events.Event(EventType.UBAH_DRAWERS, true, DragGesture.EDGE);
			dispatchEvent(drawersEvent);
			
			this.dispatchEventWith(EventType.SHOW_MENU_UTAMA);
		}
		
		private function exitHandler():void
		{
			exitHandlerBoolean = !exitHandlerBoolean;
			
			if(exitHandlerBoolean || exitoastHandlerBoolean)
			{
				if(!exitoastHandlerBoolean)
				{
					if(ToastExtension.isSupported)
					{
						var toast:ToastExtension = new ToastExtension();
						toast.setText("Tekan sekali lagi untuk keluar");
						toast.setDuration(DurationEnum.LENGTH_LONG);
						toast.show();
					}
					
					var delayedCall:DelayedCall = new DelayedCall(function():void
					{
						Starling.juggler.remove(delayedCall);
						exitHandlerBoolean = !exitHandlerBoolean;
					}, 3);
					Starling.juggler.add(delayedCall);
				}
				else
				{
					exitHandlerBoolean = !exitHandlerBoolean;
				}
			}
			else
			{
				NativeApplication.nativeApplication.exit();
			}
		}

		private function createlightdownBackground():Texture
		{
			var downdBg:RoundedQuad = new RoundedQuad(5, 100, 100, 0xd34836);
			downdBg.filter = new DropShadowFilter(4, Math.PI / 2, 0, 0.25, 2, 1);
			downdBg.x = downdBg.y = 10;

			var downuBg:RoundedQuad = new RoundedQuad(5, 96, 96, 0);
			downuBg.x = downuBg.y = 12;

			var spr:Sprite = new Sprite();
			spr.addChild(downdBg);
			spr.addChild(downuBg);
			
			var bnd:Rectangle = spr.bounds;
			var rendertxture:RenderTexture = new RenderTexture(bnd.width + 20, bnd.height + 20);
			rendertxture.draw(spr);
			return rendertxture;
		}

		private function createlightupBackground():Texture
		{
			var downdBg:RoundedQuad = new RoundedQuad(5, 100, 100, 0xffffff);
			downdBg.x = downdBg.y = 10;

			var downuBg:RoundedQuad = new RoundedQuad(5, 96, 96, 0);
			downuBg.x = downuBg.y = 12;

			var spr:Sprite = new Sprite();
			spr.addChild(downdBg);
			spr.addChild(downuBg);
			
			var bnd:Rectangle = spr.bounds;
			var rendertxture:RenderTexture = new RenderTexture(bnd.width + 20, bnd.height + 20);
			rendertxture.draw(spr);
			return rendertxture;
		}
	}
}