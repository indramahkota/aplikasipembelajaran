﻿package themes
{
	import feathers.controls.Alert;
	import feathers.controls.Button;
	import feathers.controls.ButtonGroup;
	import feathers.controls.ButtonState;
	import feathers.controls.DataGrid;
	import feathers.controls.Header;
	import feathers.controls.Label;
	import feathers.controls.List;
	import feathers.controls.PanelScreen;
	import feathers.controls.Radio;
	import feathers.controls.ScrollBarDisplayMode;
	import feathers.controls.ScrollPolicy;
	import feathers.controls.SimpleScrollBar;
	import feathers.controls.TextInput;
	import feathers.controls.TextInputState;
	import feathers.controls.ToggleButton;
	import feathers.controls.ToggleSwitch;
	import feathers.controls.TrackLayoutMode;
	import feathers.controls.renderers.DefaultDataGridCellRenderer;
	import feathers.controls.renderers.DefaultDataGridHeaderRenderer;
	import feathers.controls.renderers.DefaultListItemRenderer;
	import feathers.controls.text.StageTextTextEditor;
	import feathers.controls.text.TextBlockTextRenderer;
	import feathers.core.FeathersControl;
	import feathers.core.ITextEditor;
	import feathers.core.ITextRenderer;
	import feathers.core.PopUpManager;
	import feathers.layout.Direction;
	import feathers.layout.HorizontalAlign;
	import feathers.layout.RelativePosition;
	import feathers.layout.VerticalAlign;
	import feathers.media.PlayPauseToggleButton;
	import feathers.media.SeekSlider;
	import feathers.skins.ImageSkin;
	import feathers.themes.StyleNameFunctionTheme;

	import flash.geom.Rectangle;
	import flash.text.SoftKeyboardType;
	import flash.text.engine.ElementFormat;
	import flash.text.engine.FontDescription;
	import flash.text.engine.FontLookup;
	import flash.text.engine.FontPosture;
	import flash.text.engine.FontWeight;
	import flash.text.engine.LineJustification;
	import flash.text.engine.SpaceJustifier;

	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.MeshBatch;
	import starling.display.Quad;
	import starling.text.TextFormat;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	import starling.utils.Color;
	
	/**
	 * ...
	 * @author Indra Mahkota
	 */
	public class CustomTheme extends StyleNameFunctionTheme
	{
		[Embed(source = "/../fonts/SourceSansPro-Regular.ttf", fontFamily = "SourceSansPro", mimeType = "application/x-font", embedAsCFF = "true")]
		protected static const SourceSansPro_Regular:Class;
		
		[Embed(source = "/../fonts/SourceSansPro-Semibold.ttf", fontFamily = "SourceSansPro", fontWeight = "bold", mimeType = "application/x-font", embedAsCFF = "true")]
		protected static const SourceSansPro_Bold:Class;
		
		protected var atlas:TextureAtlas;
		protected static const fontName:String = "SourceSansPro";
		
		protected var extrasmallFontSize:int = 14;
		protected var smallFontSize:int = 16;
		protected var regularFontSize:int = 18;
		protected var normalFontSize:int = 20;
		protected var largeFontSize:int = 22;
		protected var extralargeFontSize:int = 24;
		
		protected var menuButtonTexture:Texture;
		protected var helpButtonTexture:Texture;
		protected var backButtonTexture:Texture;
		protected var closeButtonTexture:Texture;
		protected var checkButtonTexture:Texture;
		
		protected var radioUpTexture:Texture;
		protected var radioDownTexture:Texture;
		protected var radioSelectedUpTexture:Texture;
		protected var radioSelectedDownTexture:Texture;
		
		protected var scrollBarTexture:Texture;
		protected var inputErrorIcon:Texture;
		
		//[--
		protected var focusIndicatorSkinTexture:Texture;
		protected var backgroundSkinTexture:Texture;
		protected var backgroundDisabledSkinTexture:Texture;
		protected var buttonUpSkinTexture:Texture;
		protected var buttonDownSkinTexture:Texture;
		protected var buttonDisabledSkinTexture:Texture;
		
		protected var playPauseButtonPlayUpIconTexture:Texture;
		protected var playPauseButtonPauseUpIconTexture:Texture;
		protected var playPauseButtonPlayDownIconTexture:Texture;
		protected var playPauseButtonPauseDownIconTexture:Texture;
		
		protected var overlayPlayPauseButtonPlayUpIconTexture:Texture;
		protected var overlayPlayPauseButtonPlayDownIconTexture:Texture;
		
		protected var seekSliderProgressSkinTexture:Texture;
		//--]
		
		protected var generalFontStyle:TextFormat;
		protected var buttonFontStyles:TextFormat;
		protected var centerButtonFontStyles:TextFormat;
		protected var centerNameButtonFontStyles:TextFormat;
		protected var largeFontStyles:TextFormat;
		protected var headerFontStyles:TextFormat;
		protected var nameLabelFontStyles:TextFormat;
		protected var loadingLabelFontStyles:TextFormat;
		protected var loadingLabelCenterFontStyles:TextFormat;
		protected var loadingLabelCenterBgFontStyles:TextFormat;
		protected var normalLabelFontStyles:TextFormat;
		protected var titleLabelFontStyles:TextFormat;
		protected var disabledLabelFontStyles:TextFormat;
		protected var videoLabelCenterFontStyles:TextFormat;
		
		//[--
		protected var lightUIFontStyles:TextFormat;
		protected var lightDisabledUIFontStyles:TextFormat;
		protected var selectedUIFontStyles:TextFormat;
		//--]
		
		protected static const GREY_COLOR:uint = 0xD2D2D2;
		protected static const HEADER_COLOR:uint = 0x197483;//0xA09FED;
		protected static const LIGHT_BUTTON_UP:uint = 0x211B1F;
		
		protected static const MENU_BUTTON:String = "menu-button";
		protected static const HELP_BUTTON:String = "help-button";
		protected static const BACK_BUTTON:String = "back-button";
		protected static const CLOSE_BUTTON:String = "close-button";
		
		protected static const CHECK_BUTTON:String = "check-button";
		protected static const ALERT_BUTTON:String = "alert-button";
		protected static const LIGHT_BUTTON:String = "light-button";
		protected static const LOGOUT_BUTTON:String = "logout-button";
		protected static const SCROLLBAR_BUTTON:String = "scrollbar-button";
		
		protected static const HEADERTEST_BUTTON:String = "headertest-button";
		
		protected static const NAME_LABEL:String = "name-label";
		protected static const TITLE_LABEL:String = "title-label";
		protected static const LOADING_LABEL_CENTER:String = "loading-center-label";
		protected static const LOADING_LABEL_CENTER_BG:String = "loading-center-label-bg";
		protected static const VIDEO_LABEL_CENTER:String = "video-center-label";
		
		protected static const TEXT_INPUT_NUMBER:String = "text-input-number";
		
		//[--
		protected var gridSize:int = 44;
		protected var controlSize:int = 28;
		protected var focusPaddingSize:int = -1;
		
		protected static const FORWARD_BUTTON_SCALE9_GRID:Rectangle = new Rectangle(3, 0, 1, 28);
		protected static const FOCUS_INDICATOR_SCALE_9_GRID:Rectangle = new Rectangle(5, 5, 1, 1);
		protected static const DEFAULT_BACKGROUND_SCALE9_GRID:Rectangle = new Rectangle(4, 4, 1, 1);
		protected static const BUTTON_SCALE9_GRID:Rectangle = new Rectangle(4, 4, 1, 20);
		//--]
		
		public function CustomTheme()
		{
			super();
			initialize();
		}
		
		//-------------------------
		// Disposes the atlas before calling super.dispose()
		//-------------------------
		
		override public function dispose():void
		{
			if(atlas)
			{
				//if anything is keeping a reference to the texture, we don't
				//want it to keep a reference to the theme too.
				atlas.texture.root.onRestore = null;
				atlas.dispose();
				atlas = null;
			}
			
			//don't forget to call super.dispose()!
			super.dispose();
		}
		
		//-------------------------
		// Initializes the theme
		//-------------------------
		
		protected function initialize():void
		{
			initializeFonts();
			initializeTextures();
			initializeGlobals();
			initializeStyleProviders();
		}
		
		//-------------------------
		// Initializes fonts
		//-------------------------
		
		protected function initializeFonts():void
		{
			nameLabelFontStyles = new TextFormat(fontName, normalFontSize, Color.WHITE, HorizontalAlign.LEFT);
			
			generalFontStyle = new TextFormat(fontName, smallFontSize, Color.BLACK, HorizontalAlign.LEFT);
			
			buttonFontStyles = new TextFormat(fontName, regularFontSize, Color.WHITE, HorizontalAlign.CENTER);
			
			centerButtonFontStyles = new TextFormat(fontName, smallFontSize, Color.BLACK, HorizontalAlign.CENTER);
			
			centerNameButtonFontStyles = new TextFormat(fontName, normalFontSize, Color.BLACK, HorizontalAlign.CENTER);
			centerNameButtonFontStyles.bold = true;
			
			largeFontStyles = new TextFormat(fontName, largeFontSize, Color.WHITE, HorizontalAlign.CENTER);
			largeFontStyles.bold = true;
			
			headerFontStyles = new TextFormat(fontName, normalFontSize, Color.WHITE, HorizontalAlign.LEFT);
			headerFontStyles.bold = true;
			
			loadingLabelFontStyles = new TextFormat(fontName, smallFontSize, Color.RED, HorizontalAlign.JUSTIFY);
			loadingLabelFontStyles.leading = 2;
			
			videoLabelCenterFontStyles = new TextFormat(fontName, 12, Color.BLACK, HorizontalAlign.CENTER);
			
			loadingLabelCenterFontStyles = new TextFormat(fontName, smallFontSize, Color.RED, HorizontalAlign.CENTER);
			
			loadingLabelCenterBgFontStyles = new TextFormat(fontName, smallFontSize, Color.WHITE, HorizontalAlign.CENTER);
			
			normalLabelFontStyles = new TextFormat(fontName, smallFontSize, Color.BLACK, HorizontalAlign.JUSTIFY);
			normalLabelFontStyles.leading = 2;
			
			titleLabelFontStyles = new TextFormat(fontName, regularFontSize, Color.BLACK, HorizontalAlign.JUSTIFY);
			titleLabelFontStyles.leading = 2;
			
			disabledLabelFontStyles = new TextFormat(fontName, smallFontSize, GREY_COLOR, HorizontalAlign.JUSTIFY);
			disabledLabelFontStyles.leading = 2;
			
			//[--
			lightUIFontStyles = new TextFormat(fontName, smallFontSize, Color.WHITE, HorizontalAlign.LEFT, VerticalAlign.TOP);
			
			lightDisabledUIFontStyles = new TextFormat(fontName, smallFontSize, GREY_COLOR, HorizontalAlign.LEFT, VerticalAlign.TOP);
			
			selectedUIFontStyles = new TextFormat(fontName, smallFontSize, Color.WHITE, HorizontalAlign.LEFT, VerticalAlign.TOP);
			//--]
		}
		
		//-------------------------
		// Initializes texture
		//-------------------------
		
		protected function initializeTextures():void
		{
			atlas = Main.assets.getTextureAtlas("AssetsThemes");
			
			menuButtonTexture = atlas.getTexture("menu");
			helpButtonTexture = atlas.getTexture("help");
			backButtonTexture = atlas.getTexture("back");
			closeButtonTexture = atlas.getTexture("close");
			checkButtonTexture = atlas.getTexture("check");
			radioUpTexture = atlas.getTexture("radioup");
			radioDownTexture = atlas.getTexture("radiodown");
			radioSelectedUpTexture = atlas.getTexture("radioselectedup");
			radioSelectedDownTexture = atlas.getTexture("radioselecteddown");
			scrollBarTexture = atlas.getTexture("scroll");
			inputErrorIcon = atlas.getTexture("input-error-icon");
			
			//[--
			focusIndicatorSkinTexture = atlas.getTexture("focus-indicator-skin0000");
			backgroundSkinTexture = atlas.getTexture("background-skin0000");
			backgroundDisabledSkinTexture = atlas.getTexture("background-disabled-skin0000");
			buttonUpSkinTexture = atlas.getTexture("button-up-skin0000");
			buttonDownSkinTexture = atlas.getTexture("button-down-skin0000");
			buttonDisabledSkinTexture = atlas.getTexture("button-disabled-skin0000");
			
			playPauseButtonPlayUpIconTexture = atlas.getTexture("play-pause-toggle-button-play-up-icon0000");
			playPauseButtonPauseUpIconTexture = atlas.getTexture("play-pause-toggle-button-pause-up-icon0000");
			playPauseButtonPlayDownIconTexture = atlas.getTexture("play-pause-toggle-button-play-down-icon0000");
			playPauseButtonPauseDownIconTexture = atlas.getTexture("play-pause-toggle-button-pause-down-icon0000");
			
			overlayPlayPauseButtonPlayUpIconTexture = atlas.getTexture("overlay-play-pause-toggle-button-play-up-icon0000");
			overlayPlayPauseButtonPlayDownIconTexture = atlas.getTexture("overlay-play-pause-toggle-button-play-down-icon0000");
			
			seekSliderProgressSkinTexture = atlas.getTexture("seek-slider-progress-skin0000");
			//--]
		}
		
		//-------------------------
		// Initializes global variables
		//-------------------------
		
		protected function initializeGlobals():void
		{
			FeathersControl.defaultTextRendererFactory = textRendererFactory;
			FeathersControl.defaultTextEditorFactory = textEditorFactory;
			PopUpManager.overlayFactory = popUpOverlayFactory;
		}
		
		//defaultTextRendererFactory
		private static function textRendererFactory():ITextRenderer
		{
			return new TextBlockTextRenderer();
		}
		
		//specific label renderer factory
		private static function specifictextRendererFactory():ITextRenderer
		{
			var textRenderer:TextBlockTextRenderer = new TextBlockTextRenderer();
			textRenderer.styleProvider = null;
			textRenderer.textAlign = HorizontalAlign.LEFT;
			
			var fontDesc:FontDescription = new FontDescription(fontName, FontWeight.NORMAL, FontPosture.NORMAL);
			fontDesc.fontLookup = FontLookup.EMBEDDED_CFF;
			
			var spaceJustifier:SpaceJustifier = new SpaceJustifier("en", LineJustification.ALL_BUT_MANDATORY_BREAK);
            //spaceJustifier.letterSpacing = true;
			
			textRenderer.elementFormat = new ElementFormat(fontDesc, 18, Color.BLACK);
			textRenderer.textJustifier = spaceJustifier;
			return textRenderer;
		}
		
		//defaultTextEditorFactory
		private static function textEditorFactory():ITextEditor
		{
			return new StageTextTextEditor();
		}
		
		//popUpOverlayFactory
		private static function popUpOverlayFactory():DisplayObject
		{
			var quad:Quad = new Quad(3, 3, Color.BLACK);
			quad.alpha = 0.5;
			return quad;
		}
		
		//-------------------------
		// Sets global style providers for all components
		//-------------------------		
		
		protected function initializeStyleProviders():void
		{
			getStyleProviderForClass(Button).setFunctionForStyleName(MENU_BUTTON, setMenuButtonStyles);
			getStyleProviderForClass(Button).setFunctionForStyleName(HELP_BUTTON, setHelpButtonStyles);
			getStyleProviderForClass(Button).setFunctionForStyleName(BACK_BUTTON, setBackButtonStyles);
			getStyleProviderForClass(Button).setFunctionForStyleName(CLOSE_BUTTON, setCloseButtonStyles);
			getStyleProviderForClass(Button).setFunctionForStyleName(CHECK_BUTTON, setCheckButtonStyles);
			getStyleProviderForClass(Button).setFunctionForStyleName(ALERT_BUTTON, setAlertButtonStyles);
			getStyleProviderForClass(Button).setFunctionForStyleName(LIGHT_BUTTON, setLightButtonStyles);
			getStyleProviderForClass(Button).setFunctionForStyleName(SCROLLBAR_BUTTON, setScrollBarButtonStyles);
			getStyleProviderForClass(Button).setFunctionForStyleName(HEADERTEST_BUTTON, setHeaderTestButtonStyles);
			getStyleProviderForClass(Button).setFunctionForStyleName(LOGOUT_BUTTON, setHeaderLoginButtonStyles);
			
			getStyleProviderForClass(Label).setFunctionForStyleName(NAME_LABEL, setNameStyles);
			getStyleProviderForClass(Label).setFunctionForStyleName(TITLE_LABEL, setTitleStyles);
			getStyleProviderForClass(Label).setFunctionForStyleName(LOADING_LABEL_CENTER, setLoadingLabelCenterStyles);
			getStyleProviderForClass(Label).setFunctionForStyleName(LOADING_LABEL_CENTER_BG, setLoadingLabelBgCenterStyles);
			getStyleProviderForClass(Label).setFunctionForStyleName(VIDEO_LABEL_CENTER, setVideoLabelCenterStyles);
			
			
			//getStyleProviderForClass(Button).defaultStyleFunction = setButtonStyles;
			getStyleProviderForClass(Radio).defaultStyleFunction = setRadioStyles;
			getStyleProviderForClass(Alert).defaultStyleFunction = setAlertStyles;
			getStyleProviderForClass(Header).defaultStyleFunction = setHeaderStyles;
			getStyleProviderForClass(List).defaultStyleFunction = setListStyles;
			getStyleProviderForClass(Label).defaultStyleFunction = setLabelStyles;
			getStyleProviderForClass(PanelScreen).defaultStyleFunction = setPanelScreenStyles;
			getStyleProviderForClass(SimpleScrollBar).defaultStyleFunction = setSimpleScrollBarStyles;
			getStyleProviderForClass(DefaultListItemRenderer).defaultStyleFunction = setDefaultListItemRendererStyles;
			
			//text input
			getStyleProviderForClass(TextInput).defaultStyleFunction = setTextInputStyles;
			getStyleProviderForClass(TextInput).setFunctionForStyleName(TEXT_INPUT_NUMBER, setTextInputNumber);
			
			//toggle switch
			getStyleProviderForClass(ToggleSwitch).defaultStyleFunction = setToggleSwitchStyles;
			getStyleProviderForClass(Button).setFunctionForStyleName(ToggleSwitch.DEFAULT_CHILD_STYLE_NAME_THUMB, setSimpleButtonStyles);
			getStyleProviderForClass(ToggleButton).setFunctionForStyleName(ToggleSwitch.DEFAULT_CHILD_STYLE_NAME_THUMB, setSimpleButtonStyles);
			getStyleProviderForClass(Button).setFunctionForStyleName(ToggleSwitch.DEFAULT_CHILD_STYLE_NAME_ON_TRACK, setToggleSwitchTrackStyles);
			
			//playpause
			this.getStyleProviderForClass(PlayPauseToggleButton).defaultStyleFunction = this.setPlayPauseToggleButtonStyles;
			this.getStyleProviderForClass(PlayPauseToggleButton).setFunctionForStyleName(PlayPauseToggleButton.ALTERNATE_STYLE_NAME_OVERLAY_PLAY_PAUSE_TOGGLE_BUTTON, this.setOverlayPlayPauseToggleButtonStyles);
			
			//seek slider
			this.getStyleProviderForClass(SeekSlider).defaultStyleFunction = this.setSeekSliderStyles;
			this.getStyleProviderForClass(Button).setFunctionForStyleName(SeekSlider.DEFAULT_CHILD_STYLE_NAME_THUMB, this.setSeekSliderThumbStyles);
			this.getStyleProviderForClass(Button).setFunctionForStyleName(SeekSlider.DEFAULT_CHILD_STYLE_NAME_MINIMUM_TRACK, this.setSeekSliderMinimumTrackStyles);
			this.getStyleProviderForClass(Button).setFunctionForStyleName(SeekSlider.DEFAULT_CHILD_STYLE_NAME_MAXIMUM_TRACK, this.setSeekSliderMaximumTrackStyles);
			
			//data grid
			this.getStyleProviderForClass(DataGrid).defaultStyleFunction = this.setDataGridStyles;
			this.getStyleProviderForClass(DefaultDataGridCellRenderer).defaultStyleFunction = this.setDataGridCellRendererStyles;
			this.getStyleProviderForClass(DefaultDataGridHeaderRenderer).defaultStyleFunction = this.setDataGridHeaderStyles;
		}
		
		//-------------------------
		// Label
		//-------------------------
		
		protected function setLoadingLabelCenterStyles(label:Label):void
		{
			label.fontStyles = loadingLabelCenterFontStyles;
		}
		
		protected function setLoadingLabelBgCenterStyles(label:Label):void
		{
			label.fontStyles = loadingLabelCenterBgFontStyles;
			label.fontStyles.size = 12;
		}
		
		protected function setLabelStyles(label:Label):void
		{
			label.fontStyles = normalLabelFontStyles;
			label.textRendererFactory = specifictextRendererFactory;
		}
		
		protected function setTitleStyles(label:Label):void
		{
			label.fontStyles = titleLabelFontStyles;
			label.textRendererFactory = function ():ITextRenderer
			{
				var textRenderer:TextBlockTextRenderer = new TextBlockTextRenderer();
				textRenderer.styleProvider = null;
				textRenderer.textAlign = HorizontalAlign.LEFT;
				
				var fontDesc:FontDescription = new FontDescription(fontName, FontWeight.NORMAL, FontPosture.NORMAL);
				fontDesc.fontLookup = FontLookup.EMBEDDED_CFF;
				
				var spaceJustifier:SpaceJustifier = new SpaceJustifier("en", LineJustification.ALL_BUT_MANDATORY_BREAK);
				//spaceJustifier.letterSpacing = true;
				
				textRenderer.elementFormat = new ElementFormat(fontDesc, 18, Color.BLACK);
				textRenderer.textJustifier = spaceJustifier;
				return textRenderer;
			}
		}
		
		protected function setNameStyles(label:Label):void
		{
			label.fontStyles = buttonFontStyles;
		}
		
		protected function setVideoLabelCenterStyles(label:Label):void
		{
			label.fontStyles = videoLabelCenterFontStyles;
		}
		
		//-------------------------
		// Radio
		//-------------------------
		
		protected function setRadioStyles(radio:Radio):void
		{
			var icon:ImageSkin = new ImageSkin(radioUpTexture);
			icon.selectedTexture = radioSelectedUpTexture;
			icon.setTextureForState(ButtonState.DOWN, radioDownTexture);
			icon.setTextureForState(ButtonState.DOWN_AND_SELECTED, radioSelectedDownTexture);
			icon.height = 25;
			icon.width = 25;

			radio.defaultIcon = icon;
			radio.gap = 5;
			radio.horizontalAlign = HorizontalAlign.LEFT;
			radio.labelFactory = function():ITextRenderer
			{
				var renderer:TextBlockTextRenderer = new TextBlockTextRenderer();
				
				var fontDesc:FontDescription = new FontDescription(fontName, FontWeight.NORMAL, FontPosture.NORMAL);
				fontDesc.fontLookup = FontLookup.EMBEDDED_CFF;
			
				renderer.elementFormat = new ElementFormat(fontDesc, 18, Color.BLACK);
				return renderer;
			};
		}
		
		//-------------------------
		// Alert
		//-------------------------
		
		protected function setAlertStyles(alert:Alert):void
		{
			alert.padding = 10;
			alert.maxWidth = 280;
			alert.minHeight = 50;
			alert.backgroundSkin =new Quad(10, 10);
			alert.fontStyles = generalFontStyle;
			
			alert.buttonGroupFactory = function():ButtonGroup
			{
				var group:ButtonGroup = new ButtonGroup();
				group.customButtonStyleName = ALERT_BUTTON;
				group.gap = 10;
				group.padding = 10;
				group.direction = Direction.HORIZONTAL;
				return group;
			};
			
			alert.headerFactory = function():Header
			{
				var header:Header = new Header();
				header.backgroundSkin = new Quad(100, 10, HEADER_COLOR);
				header.minHeight = 50;
				header.padding = 10;
				header.gap = 10;
				header.titleAlign = HorizontalAlign.LEFT;
				header.fontStyles = headerFontStyles;
				return header;
			};
		}
		
		//-------------------------
		// Button
		//-------------------------
		
		protected function setButtonStyles(button:Button):void
		{
			button.height = 70;
			button.paddingLeft = button.paddingRight = 20;
			button.paddingTop = button.paddingBottom = 20;
			button.gap = 10;
			
			button.horizontalAlign = HorizontalAlign.LEFT;
			button.fontStyles = generalFontStyle;
			button.fontStyles.size = 18;
		}
		
		protected function setHeaderTestButtonStyles(button:Button):void
		{
			button.height = 50;
			button.paddingLeft = button.paddingRight = 20;
			button.gap = 10;
			
			button.horizontalAlign = HorizontalAlign.LEFT;
			button.fontStyles = generalFontStyle;
			button.fontStyles.size = 18;
		}
		
		protected function setHeaderLoginButtonStyles(button:Button):void
		{
			button.fontStyles = centerButtonFontStyles;
			button.fontStyles.color = 0xFFFFFF;
		}
		
		protected function setAlertButtonStyles(button:Button):void
		{
			button.height = 40;
			button.defaultSkin = new Quad(38, 38, LIGHT_BUTTON_UP);
			button.downSkin = new Quad(40, 40, 0xADD8E6);
			button.fontStyles = buttonFontStyles;
		}
		
		protected function setLightButtonStyles(button:Button):void
		{
			button.fontStyles = buttonFontStyles;
		}
		
		protected function setMenuButtonStyles(button:Button):void
		{
			var skin:ImageSkin = new ImageSkin(menuButtonTexture);
			skin.pixelSnapping = true;
			skin.width = 25;
			skin.height = 25;
			
			button.defaultSkin = skin;
			button.hasLabelTextRenderer = false;
		}
		
		protected function setHelpButtonStyles(button:Button):void
		{
			var skin:ImageSkin = new ImageSkin(helpButtonTexture);
			skin.pixelSnapping = true;
			skin.width = 25;
			skin.height = 25;
			
			button.defaultSkin = skin;
			button.hasLabelTextRenderer = false;
		}
		
		protected function setBackButtonStyles(button:Button):void
		{
			var skin:ImageSkin = new ImageSkin(backButtonTexture);
			skin.pixelSnapping = true;
			skin.width = 25;
			skin.height = 25;
			
			button.defaultSkin = skin;
			button.hasLabelTextRenderer = false;
		}
		
		protected function setCloseButtonStyles(button:Button):void
		{
			var skin:ImageSkin = new ImageSkin(closeButtonTexture);
			skin.pixelSnapping = true;
			skin.width = 25;
			skin.height = 25;
			
			button.defaultSkin = skin;
			button.hasLabelTextRenderer = false;
		}
		
		protected function setCheckButtonStyles(button:Button):void
		{
			var skin:ImageSkin = new ImageSkin(checkButtonTexture);
			skin.pixelSnapping = true;
			skin.width = 25;
			skin.height = 25;
			
			button.defaultSkin = skin;
			button.hasLabelTextRenderer = false;
		}
		
		protected function setScrollBarButtonStyles(button:Button):void
		{
			var defaultSkin:Image = new Image(scrollBarTexture);
			defaultSkin.pixelSnapping = true;
			defaultSkin.scale9Grid = new Rectangle(0, 4, 5, 4);

			button.defaultSkin = defaultSkin;
			button.scale = 0.4;
			button.hasLabelTextRenderer = false;
			button.alpha = 0.5;
		}
		
		//-------------------------
		// Header
		//-------------------------
		
		protected function setHeaderStyles(header:Header):void
		{
			header.paddingTop = 40;
			header.paddingBottom = 10;
			header.paddingLeft = 10;
			header.paddingRight = 10;
			header.gap = 10;
			header.backgroundSkin = new Quad(10, 75, HEADER_COLOR);
			header.titleAlign = HorizontalAlign.LEFT;
			header.fontStyles = headerFontStyles;
		}
		
		//-------------------------
		// List
		//-------------------------
		
		protected function setListStyles(list:List):void
		{
			list.hasElasticEdges = false;
			list.scrollBarDisplayMode = ScrollBarDisplayMode.NONE;
		}
		
		//-------------------------
		// Simple ScrollBar
		//-------------------------
		
		protected function setSimpleScrollBarStyles(scrollBar:SimpleScrollBar):void
		{
			if(scrollBar.direction == Direction.VERTICAL)
			{
				scrollBar.paddingTop = 10;
				scrollBar.paddingBottom = 20;
				scrollBar.paddingLeft = 2
				scrollBar.paddingRight = 2;
				scrollBar.customThumbStyleName = SCROLLBAR_BUTTON;
			}
		}
		
		//-------------------------
		// PanelScreen
		//-------------------------
		
		protected function setPanelScreenStyles(screen:PanelScreen):void
		{
			var quada:Quad = new Quad(starling.stage.stageWidth, starling.stage.stageHeight, 0xEDEDF0);
			var quadb:Quad = new Quad(starling.stage.stageWidth, 120, 0x197483); //0xA09FED;
			
			var meshBatch:MeshBatch = new MeshBatch();
			meshBatch.addMesh(quada);
			meshBatch.addMesh(quadb);
			
			screen.backgroundSkin = meshBatch;
			
			//screen.hasElasticEdges = false;
			screen.horizontalScrollPolicy = ScrollPolicy.OFF;
			screen.scrollBarDisplayMode = ScrollBarDisplayMode.NONE;
		}
		
		//-------------------------
		// DefaultListItemRenderer
		//-------------------------
		
		protected function setDefaultListItemRendererStyles(renderer:DefaultListItemRenderer):void
		{
			var downSkin:Quad = new Quad(3, 3, GREY_COLOR);
			downSkin.alpha = 0.25;
			
			renderer.paddingLeft = 10;
			renderer.paddingRight = 10;
			renderer.paddingTop = 10;
			renderer.paddingBottom = 10;
			renderer.minHeight = 32;
			renderer.gap = 10;
			renderer.accessoryGap = Number.POSITIVE_INFINITY;
			renderer.iconPosition = RelativePosition.LEFT;
			renderer.accessoryPosition = RelativePosition.RIGHT;
			renderer.downSkin = downSkin;
			renderer.defaultSelectedSkin = downSkin;
			renderer.horizontalAlign = HorizontalAlign.LEFT;
			renderer.fontStyles = generalFontStyle;
			renderer.iconLabelFontStyles = generalFontStyle;
			renderer.accessoryLabelFontStyles = generalFontStyle;
		}
		
		//-------------------------
		// TextInput
		//-------------------------
		
		protected function setTextInputStyles(textInput:TextInput):void
		{
			var errorIcon:Image = new Image(inputErrorIcon);
			errorIcon.width = errorIcon.height = 12;

			textInput.setIconForState(TextInputState.ERROR, errorIcon);
			textInput.paddingTop = textInput.paddingBottom = 20;
			textInput.paddingLeft = textInput.paddingRight = 10;
			textInput.fontStyles = generalFontStyle;
			textInput.promptFontStyles = generalFontStyle;
			textInput.textEditorFactory = function():ITextEditor
			{
				var editor:StageTextTextEditor = new StageTextTextEditor();
				editor.multiline = false;
				return editor;
			};
			textInput.promptFactory = function():ITextRenderer
			{
				var renderer:TextBlockTextRenderer = new TextBlockTextRenderer();
				renderer.alpha = 0.5;
				return renderer;
			};
		}
		
		protected function setTextInputNumber(textInput:TextInput):void
		{
			textInput.paddingTop = textInput.paddingBottom = 20;
			textInput.paddingLeft = textInput.paddingRight = 10;
			textInput.fontStyles = generalFontStyle;
			textInput.promptFontStyles = generalFontStyle;
			textInput.textEditorFactory = function():ITextEditor
			{
				var editor:StageTextTextEditor = new StageTextTextEditor();
				editor.multiline = false;
				editor.softKeyboardType = SoftKeyboardType.NUMBER;
				return editor;
			};
			textInput.promptFactory = function():ITextRenderer
			{
				var renderer:TextBlockTextRenderer = new TextBlockTextRenderer();
				renderer.alpha = 0.5;
				return renderer;
			};
		}
		
		//
		protected function setSimpleButtonStyles(button:Button):void
		{
			var skin:ImageSkin = new ImageSkin(buttonUpSkinTexture);
			skin.setTextureForState(ButtonState.DOWN, buttonDownSkinTexture);
			skin.setTextureForState(ButtonState.DISABLED, buttonDisabledSkinTexture);
			skin.scale9Grid = BUTTON_SCALE9_GRID;
			skin.width = controlSize;
			skin.height = controlSize;
			skin.minWidth = controlSize;
			skin.minHeight = controlSize;

			button.defaultSkin = skin;
			button.hasLabelTextRenderer = false;
			button.minTouchWidth = gridSize;
			button.minTouchHeight = gridSize;
		}
		
		//-------------------------
		// ToggleSwitch
		//-------------------------
		
		protected function setToggleSwitchStyles(toggle:ToggleSwitch):void
		{
			var focusIndicatorSkin:Image = new Image(focusIndicatorSkinTexture);
			focusIndicatorSkin.scale9Grid = FOCUS_INDICATOR_SCALE_9_GRID;

			toggle.focusIndicatorSkin = focusIndicatorSkin;
			toggle.focusPadding = focusPaddingSize;
			toggle.trackLayoutMode = TrackLayoutMode.SINGLE;
			toggle.offLabelFontStyles = lightUIFontStyles;
			toggle.offLabelDisabledFontStyles = lightDisabledUIFontStyles;
			toggle.onLabelFontStyles = selectedUIFontStyles;
			toggle.onLabelDisabledFontStyles = lightDisabledUIFontStyles;
		}
		
		//see Shared section for thumb styles
		
		protected function setToggleSwitchTrackStyles(track:Button):void
		{
			var skin:ImageSkin = new ImageSkin(backgroundSkinTexture);
			skin.disabledTexture = backgroundDisabledSkinTexture;
			skin.scale9Grid = DEFAULT_BACKGROUND_SCALE9_GRID;
			skin.width = Math.round(controlSize * 2.5);
			skin.height = controlSize;
			track.defaultSkin = skin;
			track.hasLabelTextRenderer = false;
		}
		
		//-------------------------
		// Play Pause Toggle Button
		//-------------------------
		
		protected function setPlayPauseToggleButtonStyles(button:PlayPauseToggleButton):void
		{
			var skin:Quad = new Quad(10, 10);
			skin.alpha = 0;
			
			var icon:ImageSkin = new ImageSkin(this.playPauseButtonPlayUpIconTexture);
			icon.selectedTexture = this.playPauseButtonPauseUpIconTexture;
			icon.setTextureForState(ButtonState.DOWN, this.playPauseButtonPlayDownIconTexture);
			icon.setTextureForState(ButtonState.DOWN_AND_SELECTED, this.playPauseButtonPauseDownIconTexture);
			icon.scale = 0.5;

			button.defaultSkin = skin;
			button.defaultIcon = icon;
			button.hasLabelTextRenderer = false;
			button.minTouchWidth = 32;
			button.minTouchHeight = 32;
		}
		
		protected function setOverlayPlayPauseToggleButtonStyles(button:PlayPauseToggleButton):void
		{
			var icon:ImageSkin = new ImageSkin(null);
			icon.setTextureForState(ButtonState.UP, this.overlayPlayPauseButtonPlayUpIconTexture);
			icon.setTextureForState(ButtonState.HOVER, this.overlayPlayPauseButtonPlayUpIconTexture);
			icon.setTextureForState(ButtonState.DOWN, this.overlayPlayPauseButtonPlayDownIconTexture);
			icon.scale = 0.5;
			button.defaultIcon = icon;
			button.hasLabelTextRenderer = false;
			
			var overlaySkin:Quad = new Quad(1, 1, 0x1a1816);
			overlaySkin.alpha = 0.2;
			button.upSkin = overlaySkin;
			button.hoverSkin = overlaySkin;
		}
		
		//-------------------------
		// SeekSlider
		//-------------------------

		protected function setSeekSliderStyles(slider:SeekSlider):void
		{
			slider.trackLayoutMode = TrackLayoutMode.SPLIT;
			slider.showThumb = false;

			var progressSkin:Image = new Image(this.seekSliderProgressSkinTexture);
			progressSkin.scale9Grid = new Rectangle(4, 4, 1, 1);
			progressSkin.width = 12;
			progressSkin.height = 12;
			slider.progressSkin = progressSkin;
		}

		protected function setSeekSliderThumbStyles(thumb:Button):void
		{
			thumb.defaultSkin = new Quad(6, 6);
			thumb.hasLabelTextRenderer = false;
			thumb.minTouchWidth = 44;
			thumb.minTouchHeight = 44;
		}

		protected function setSeekSliderMinimumTrackStyles(track:Button):void
		{
			var defaultSkin:ImageSkin = new ImageSkin(this.buttonUpSkinTexture);
			defaultSkin.scale9Grid = new Rectangle(4, 4, 1, 20);
			defaultSkin.width = 156;
			defaultSkin.height = 12;
			defaultSkin.minWidth = 156;
			defaultSkin.minHeight = 12;
			track.defaultSkin = defaultSkin;
			track.hasLabelTextRenderer = false;
			track.minTouchHeight = 44;
		}

		protected function setSeekSliderMaximumTrackStyles(track:Button):void
		{
			var defaultSkin:ImageSkin = new ImageSkin(this.backgroundSkinTexture);
			defaultSkin.scale9Grid = new Rectangle(4, 4, 1, 1);
			defaultSkin.width = 156;
			defaultSkin.height = 12;
			defaultSkin.minHeight = 12;
			track.defaultSkin = defaultSkin;
			track.hasLabelTextRenderer = false;
			track.minTouchHeight = 44;
		}
		
		//-------------------------
		// DataGrid
		//-------------------------

		protected function setDataGridStyles(grid:DataGrid):void
		{
			grid.backgroundSkin = new Quad(10, 10, 0xC0C0C0);
			grid.columnResizeSkin = new Quad(10, 10, 0x00FF80);
			grid.headerDividerFactory = this.dataGridHeaderDividerFactory;
			grid.verticalDividerFactory = this.dataGridVerticalDividerFactory;
		}
		
		protected function dataGridHeaderDividerFactory():DisplayObject
		{
			var skin:Quad = new Quad(2, 2, 0xFFFFFF);
			return skin;
		}

		protected function dataGridVerticalDividerFactory():DisplayObject
		{
			var skin:Quad = new Quad(2, 2, 0xA09FED);
			return skin;
		}
		
		protected function setDataGridCellRendererStyles(cellRenderer:DefaultDataGridCellRenderer):void
		{
			var skin:Quad = new Quad(10, 10, 0xFFFFFF);
			cellRenderer.defaultSkin = skin;
			cellRenderer.fontStyles = normalLabelFontStyles;
			cellRenderer.iconLabelFontStyles = normalLabelFontStyles;
			cellRenderer.accessoryLabelFontStyles = normalLabelFontStyles;
			cellRenderer.horizontalAlign = HorizontalAlign.LEFT;
			cellRenderer.paddingTop = 10;
			cellRenderer.paddingBottom = 10;
			cellRenderer.paddingLeft = 20;
			cellRenderer.paddingRight = 10;
			cellRenderer.gap = 10;
			cellRenderer.minGap = 10;
			cellRenderer.iconPosition = RelativePosition.LEFT;
			cellRenderer.accessoryGap = Number.POSITIVE_INFINITY;
			cellRenderer.minAccessoryGap = 10;
			cellRenderer.accessoryPosition = RelativePosition.RIGHT;
			cellRenderer.minTouchWidth = 40;
			cellRenderer.minTouchHeight = 40;
		}
		
		protected function setDataGridHeaderStyles(headerRenderer:DefaultDataGridHeaderRenderer):void
		{
			headerRenderer.backgroundSkin = new Quad(10, 10, 0xA09FED);
			headerRenderer.fontStyles = buttonFontStyles;
			headerRenderer.padding = 10;
		}
	}
}