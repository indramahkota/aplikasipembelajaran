﻿package
{
	import constants.EventType;
	import constants.ScreenID;
	import constants.Url;

	import feathers.controls.DragGesture;
	import feathers.controls.Drawers;
	import feathers.controls.ImageLoader;
	import feathers.controls.Label;
	import feathers.controls.LayoutGroup;
	import feathers.controls.List;
	import feathers.controls.ScrollPolicy;
	import feathers.controls.StackScreenNavigator;
	import feathers.controls.StackScreenNavigatorItem;
	import feathers.controls.renderers.DefaultListItemRenderer;
	import feathers.data.ListCollection;
	import feathers.layout.AnchorLayout;
	import feathers.layout.AnchorLayoutData;
	import feathers.layout.HorizontalAlign;
	import feathers.layout.RelativeDepth;
	import feathers.layout.VerticalAlign;
	import feathers.layout.VerticalLayout;
	import feathers.motion.Cover;
	import feathers.motion.Reveal;
	import feathers.motion.Slide;

	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.filesystem.File;
	import flash.geom.Rectangle;
	import flash.net.SharedObject;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.system.System;
	import flash.utils.setTimeout;

	import starling.assets.AssetManager;
	import starling.core.Starling;
	import starling.display.MeshBatch;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.extensions.animate.AnimAssetManager;
	import starling.extensions.animate.Animation;
	import starling.filters.DropShadowFilter;
	import starling.textures.RenderTexture;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;

	import themes.CustomTheme;

	import utils.CustomButton;
	import utils.CustomListItemRenderer;
	import utils.RoundedQuad;

	import views.Daftar;
	import views.Evaluasi;
	import views.Masuk;
	import views.Materi;
	import views.MenuUtama;
	import views.Pendahuluan;
	import views.Pengaturan;
	import views.Pengguna;
	import views.Petunjuk;
	import views.Simulasi;
	import views.Tentang;
	import views.TesGayaBelajar;
	import views.VideoViewer;
	import views.VideoViewerfs;
	
	/**
	 * ...
	 * @author Indra Mahkota
	 */
	public class Main extends Sprite
	{
		private var file:File;
		private var list:List;
		private var drawers:Drawers;
		private var intro:Animation;
		private var overlaySkin:Quad;
		private var logOutButton:CustomButton;
		private var navigator:StackScreenNavigator;
		private var navigatorData:NavigatorData;
		public static var assetsIntro:AnimAssetManager;
		
		public static var profileName:Label;
		public static var assets:AssetManager;
		public static var profileImage:ImageLoader;
		public static var changeScreenUsingList:Boolean = true;

		public static var roundQuadBackground:Texture;
		
		public static var statusbar:Quad;
		public static var mySo:SharedObject = SharedObject.getLocal("mediapembelajaran");
		
		public function Main()
		{
			file = File.applicationDirectory;

			assetsIntro = new AnimAssetManager();
			assetsIntro.enqueue(file.resolvePath("assets/animations/intro/"));
			assetsIntro.loadQueue(startIntroAnimation);

			roundQuadBackground = createRoundQuadBackground();
			roundQuadBackground.root.onRestore = function():void
			{
				roundQuadBackground = createRoundQuadBackground();
			}
		}

		private function startIntroAnimation():void
		{
			intro = assetsIntro.createAnimation("intro");
			intro.addEventListener(starling.events.Event.COMPLETE, loadAssets);
			intro.scaleX = intro.scaleY = (stage.stageWidth - 150) / intro.width;
			intro.x = ((stage.stageWidth - intro.width) / 2);
			intro.y = ((stage.stageHeight - intro.height) / 2);
			intro.touchable = intro.loop = false;
			this.addChild(intro);
			Starling.juggler.add(intro);
		}

		private function loadAssets():void
		{
			intro.removeEventListener(starling.events.Event.COMPLETE, loadAssets);
			assetsIntro.dispose();
			assetsIntro = null;
			
			assets = new AssetManager();
			assets.enqueue(
				file.resolvePath("assets/images"), 
				file.resolvePath("assets/jsons"), 
				file.resolvePath("assets/sounds")
			);
			assets.loadQueue(function():void
			{
				System.pauseForGCIfCollectionImminent(0);
				System.gc();
				setTimeout(start, 150);
			});	
		}
		
		private function start():void
		{
			new CustomTheme();
			navigatorData = new NavigatorData();
			navigator = new StackScreenNavigator();
			
			var daftar:StackScreenNavigatorItem = new StackScreenNavigatorItem(Daftar);
			daftar.setScreenIDForPushEvent(EventType.SHOW_MENU_UTAMA, ScreenID.MENU_UTAMA);
			daftar.setScreenIDForPushEvent(EventType.SHOW_MASUK_SCREEN, ScreenID.MASUKSCREEN);
			daftar.pushTransition = Slide.createSlideRightTransition();
			navigator.addScreen(ScreenID.DAFTARSCREEN, daftar);
			
			var masuk:StackScreenNavigatorItem = new StackScreenNavigatorItem(Masuk);
			masuk.setScreenIDForPushEvent(EventType.SHOW_MENU_UTAMA, ScreenID.MENU_UTAMA);
			masuk.setScreenIDForPushEvent(EventType.SHOW_DAFTAR_SCREEN, ScreenID.DAFTARSCREEN);
			masuk.pushTransition = Slide.createSlideLeftTransition();
			navigator.addScreen(ScreenID.MASUKSCREEN, masuk);
			
			var menuUtama:StackScreenNavigatorItem = new StackScreenNavigatorItem(MenuUtama);
			menuUtama.properties.data = navigatorData;
			menuUtama.setScreenIDForPushEvent(EventType.SHOW_PETUNJUK, ScreenID.PETUNJUK);
			menuUtama.setScreenIDForPushEvent(EventType.SHOW_PENDAHULUAN, ScreenID.PENDAHULUAN);
			menuUtama.setScreenIDForPushEvent(EventType.SHOW_TES_GAYA_BELAJAR, ScreenID.TESGAYABELAJAR);
			menuUtama.setScreenIDForPushEvent(EventType.SHOW_MATERI, ScreenID.MATERI);
			menuUtama.setScreenIDForPushEvent(EventType.SHOW_SIMULASI, ScreenID.SIMULASI);
			menuUtama.setScreenIDForPushEvent(EventType.SHOW_EVALUASI, ScreenID.EVALUASI);
			menuUtama.setScreenIDForPushEvent(EventType.SHOW_PENGGUNA, ScreenID.PENGGUNA);
			navigator.addScreen(ScreenID.MENU_UTAMA, menuUtama);
			
			var pengaturan:StackScreenNavigatorItem = new StackScreenNavigatorItem(Pengaturan);
			pengaturan.properties.data = navigatorData;
			pengaturan.setScreenIDForPushEvent(EventType.SHOW_PETUNJUK, ScreenID.PETUNJUK);
			navigator.addScreen(ScreenID.PENGATURAN, pengaturan);
			
			var tentang:StackScreenNavigatorItem = new StackScreenNavigatorItem(Tentang);
			tentang.properties.data = navigatorData;
			tentang.setScreenIDForPushEvent(EventType.SHOW_PETUNJUK, ScreenID.PETUNJUK);
			navigator.addScreen(ScreenID.TENTANG, tentang);
			
			var pendahuluan:StackScreenNavigatorItem = new StackScreenNavigatorItem(Pendahuluan);
			pendahuluan.properties.data = navigatorData;
			pendahuluan.addPopEvent(starling.events.Event.COMPLETE);
			pendahuluan.pushTransition = Slide.createSlideLeftTransition();
			pendahuluan.popTransition = Slide.createSlideRightTransition();
			pendahuluan.setScreenIDForPushEvent(EventType.SHOW_PETUNJUK, ScreenID.PETUNJUK);
			pendahuluan.setScreenIDForPushEvent(EventType.SHOW_TES_GAYA_BELAJAR, ScreenID.TESGAYABELAJAR);
			navigator.addScreen(ScreenID.PENDAHULUAN, pendahuluan);
			
			var testGayaBelajar:StackScreenNavigatorItem = new StackScreenNavigatorItem(TesGayaBelajar);
			testGayaBelajar.properties.data = navigatorData;
			testGayaBelajar.addPopEvent(starling.events.Event.COMPLETE);
			testGayaBelajar.setScreenIDForPushEvent(EventType.SHOW_PETUNJUK, ScreenID.PETUNJUK);
			testGayaBelajar.pushTransition = Slide.createSlideLeftTransition();
			testGayaBelajar.popTransition = Slide.createSlideRightTransition();
			navigator.addScreen(ScreenID.TESGAYABELAJAR, testGayaBelajar);
			
			var materi:StackScreenNavigatorItem = new StackScreenNavigatorItem(Materi);
			materi.properties.data = navigatorData;
			materi.addPopEvent(starling.events.Event.COMPLETE);
			materi.setScreenIDForPushEvent(EventType.SHOW_PETUNJUK, ScreenID.PETUNJUK);
			materi.setScreenIDForPushEvent(EventType.SHOW_VIDEO_VIEWER, ScreenID.VIDEOVIEWER);
			materi.pushTransition = Slide.createSlideLeftTransition();
			materi.popTransition = Slide.createSlideRightTransition();
			navigator.addScreen(ScreenID.MATERI, materi);

			var simulasi:StackScreenNavigatorItem = new StackScreenNavigatorItem(Simulasi);
			simulasi.properties.data = navigatorData;
			simulasi.addPopEvent(starling.events.Event.COMPLETE);
			simulasi.setScreenIDForPushEvent(EventType.SHOW_PETUNJUK, ScreenID.PETUNJUK);
			simulasi.pushTransition = Slide.createSlideLeftTransition();
			simulasi.popTransition = Slide.createSlideRightTransition();
			navigator.addScreen(ScreenID.SIMULASI, simulasi);
			
			var evaluasi:StackScreenNavigatorItem = new StackScreenNavigatorItem(Evaluasi);
			evaluasi.properties.data = navigatorData;
			evaluasi.addPopEvent(starling.events.Event.COMPLETE);
			evaluasi.setScreenIDForPushEvent(EventType.SHOW_PETUNJUK, ScreenID.PETUNJUK);
			evaluasi.pushTransition = Slide.createSlideLeftTransition();
			evaluasi.popTransition = Slide.createSlideRightTransition();
			navigator.addScreen(ScreenID.EVALUASI, evaluasi);
			
			var pengguna:StackScreenNavigatorItem = new StackScreenNavigatorItem(Pengguna);
			pengguna.properties.data = navigatorData;
			pengguna.addPopEvent(starling.events.Event.COMPLETE);
			pengguna.pushTransition = Slide.createSlideLeftTransition();
			pengguna.popTransition = Slide.createSlideRightTransition();
			pengguna.setScreenIDForPushEvent(EventType.SHOW_PETUNJUK, ScreenID.PETUNJUK);
			navigator.addScreen(ScreenID.PENGGUNA, pengguna);
			
			var petunjuk:StackScreenNavigatorItem = new StackScreenNavigatorItem(Petunjuk);
			petunjuk.properties.data = navigatorData;
			petunjuk.addPopEvent(starling.events.Event.COMPLETE);
			petunjuk.pushTransition = Cover.createCoverUpTransition();
			petunjuk.popTransition = Reveal.createRevealDownTransition();
			navigator.addScreen(ScreenID.PETUNJUK, petunjuk);
			
			var videoviewer:StackScreenNavigatorItem = new StackScreenNavigatorItem(VideoViewer);
			videoviewer.properties.data = navigatorData;
			videoviewer.addPopEvent(starling.events.Event.COMPLETE);
			videoviewer.setScreenIDForPushEvent(EventType.SHOW_PETUNJUK, ScreenID.PETUNJUK);
			videoviewer.setScreenIDForPushEvent(EventType.SHOW_VIDEO_VIEWER_FS, ScreenID.VIDEOVIEWERFS);
			videoviewer.pushTransition = Slide.createSlideLeftTransition();
			videoviewer.popTransition = Slide.createSlideRightTransition();
			navigator.addScreen(ScreenID.VIDEOVIEWER, videoviewer);

			var videoviewerfs:StackScreenNavigatorItem = new StackScreenNavigatorItem(VideoViewerfs);
			videoviewerfs.properties.data = navigatorData;
			videoviewerfs.addPopEvent(starling.events.Event.COMPLETE);
			navigator.addScreen(ScreenID.VIDEOVIEWERFS, videoviewerfs);

			const stW:Number = stage.stageWidth;
			const stH:Number = stage.stageHeight;

			var layer1:Quad = new Quad((85 / 100) * stW, stH);
			var layer2:Quad = new Quad((85 / 100) * stW, (35 / 100) * stH, 0x197483);

			var meshbatch:MeshBatch = new MeshBatch();
			meshbatch.addMesh(layer1);
			meshbatch.addMesh(layer2);

			var leftDrawers:LayoutGroup = new LayoutGroup();
			leftDrawers.layout = new AnchorLayout();
			leftDrawers.backgroundSkin = meshbatch;
			leftDrawers.width = (85 / 100) * stW;

			var pI:Number = (((35 / 100) * stH) - 144) / 2;
			
			profileImage = new ImageLoader();
			profileImage.scale = 0.5;
			profileImage.touchable = false;
			profileImage.pixelSnapping = true;
			profileImage.layoutData = new AnchorLayoutData(pI, NaN, NaN, NaN, 0, NaN);
			leftDrawers.addChild(profileImage);
			
			profileName = new Label();
			profileName.touchable = false;
			profileName.styleNameList.add("name-label");
			profileName.layoutData = new AnchorLayoutData(pI + 94, NaN, NaN, NaN, 0, NaN);
			leftDrawers.addChild(profileName);
			
			list = new List();
			list.horizontalScrollPolicy = ScrollPolicy.OFF;
			list.verticalScrollPolicy = ScrollPolicy.OFF;
			list.layoutData = new AnchorLayoutData((35 / 100) * stH, 0, NaN, 0);
			list.dataProvider = new ListCollection([
				{label: "Halaman Utama", screen: ScreenID.MENU_UTAMA, icon: getImage("home_icon")}, 
				{label: "Pengaturan", screen: ScreenID.PENGATURAN, icon: getImage("pengaturan_icon")}, 
				{label: "Tentang", screen: ScreenID.TENTANG, icon: getImage("tentang_icon")}
			]);
			list.itemRendererFactory = function():DefaultListItemRenderer
			{
				var renderer:CustomListItemRenderer = new CustomListItemRenderer();
				renderer.paddingLeft = 20;
				renderer.labelField = "label";
				renderer.iconSourceField = "icon";
				renderer.defaultSkin = new Quad(10, 10, 0xffffff);
				renderer.iconLoaderFactory = function():ImageLoader
				{
					var loader:ImageLoader = new ImageLoader();
					loader.pixelSnapping = true;
					loader.setSize(32, 32);
					return loader;
				}
				return renderer;
			}
			list.selectedIndex = 0;
			list.addEventListener(starling.events.Event.CHANGE, list_changeHandler);
			leftDrawers.addChild(list);
			
			logOutButton = new CustomButton();
			logOutButton.label = "KELUAR";
			logOutButton.padding = 10;
			logOutButton.defaultSkin = new Quad(20, 20, 0x197483);
			logOutButton.styleNameList.add("logout-button");
			logOutButton.layoutData = new AnchorLayoutData(NaN, 10, 5, 10, NaN, NaN);
			logOutButton.addEventListener(starling.events.Event.TRIGGERED, LogOutbuttonHandler);
			leftDrawers.addChild(logOutButton);
			
			overlaySkin = new Quad(100, 100, 0x0);
			overlaySkin.alpha = 0.5;
			
			drawers = new Drawers();
			drawers.content = navigator;
			drawers.overlaySkin = overlaySkin;
			drawers.openGesture = DragGesture.NONE;
			drawers.openMode = RelativeDepth.ABOVE;
			drawers.addEventListener(EventType.UBAH_DRAWERS, ubahDrawers);
			drawers.leftDrawerToggleEventType = EventType.TOGGlE_LEFTDRAWER;
			drawers.leftDrawer = leftDrawers;
			this.addChild(drawers);

			if(isDebugBuild())
			{
				statusbar = new Quad(stage.stageWidth, 25, 0x0);
				statusbar.alpha = 0.25;
				this.addChild(statusbar);
			}
			
			if(mySo.data.sessions != undefined || mySo.data.sessions == "true")
			{
				profileName.text = firstLetterUpperCase(mySo.data.namaLengkap + "\n nilai: " + mySo.data.nilai);
				profileImage.source = getImage(mySo.data.avatar);
				drawers.openGesture = DragGesture.EDGE;
				navigator.rootScreenID = ScreenID.MENU_UTAMA;
			}
			else
			{
				profileName.text = firstLetterUpperCase("belum terdaftar (offline) \n nilai: kosong");
				profileImage.source = getImage("profil_icon");
				navigator.rootScreenID = ScreenID.DAFTARSCREEN;
			}

			Starling.juggler.remove(intro);
			this.removeChild(intro, true);
			intro.dispose();
			intro = null;
		}

		private function isDebugBuild():Boolean
		{
			var stackTrace:String = new Error().getStackTrace();
			return (stackTrace && stackTrace.search(/:[0-9]+]$/m) > -1);
		}
		
		private function list_changeHandler(event:starling.events.Event):void
		{
			klik();
			drawers.toggleLeftDrawer();
			if(changeScreenUsingList)
			{
				var screen:String = list.selectedItem.screen;
				navigator.pushScreen(screen);
			}
		}
		
		private function LogOutbuttonHandler(event:starling.events.Event):void
		{
			klik();
			drawers.toggleLeftDrawer();
			keluarSession();
			
			for(var prop:String in navigatorData)
			{
				trace("[NavigatorData] delete: " + prop + " -> " + navigatorData[prop]);
				delete navigatorData[prop];
			}
			
			list.selectedIndex = 0;
			changeScreenUsingList = false;
			drawers.openGesture = DragGesture.NONE;
			navigator.pushScreen(ScreenID.MASUKSCREEN);
		}
		
		private function ubahDrawers(event:starling.events.Event):void
		{
			drawers.openGesture = event.data as String;
		}
		
		public static function getJsonAssets(string:String):Array
		{
			var array:Array = [];
			var object:Object = assets.getObject("config");
			var newObject:Object = object[string];
			
			for(var key:String in newObject)
			{
				array.push(newObject[key]);
			}
			return array;
		}
		
		public static function firstLetterUpperCase(strData:String):String
		{
			var strArray:Array = strData.split(' ');
			var newArray:Array = [];
			
			for(var str:String in strArray)
			{
				newArray.push(strArray[str].charAt(0).toUpperCase() + strArray[str].slice(1));
			}
			return newArray.join(' ');
		}
		
		public static function klik():void
		{
			if(mySo.data.SuaraTombol == "on" || mySo.data.SuaraTombol == undefined)
			{
				assets.getSound("Klik").play();
			}
		}
		
		public static function getImage(string:String):Texture
		{
			var textureAtlas:TextureAtlas = assets.getTextureAtlas("AssetsImages");
			var texture:Texture = textureAtlas.getTexture(string);
			return texture;
		}

		private function createRoundQuadBackground():Texture
		{
			var spr:Sprite = new Sprite();
			var qsh:RoundedQuad = new RoundedQuad(5, 100, 100, 0xFFFFFF);
			qsh.filter = new DropShadowFilter(4, Math.PI / 2, 0, 0.25, 2, 1);
			qsh.x = qsh.y = 10;
			spr.addChild(qsh);
			
			var bnd:Rectangle = spr.bounds;
			var rendertxture:RenderTexture = new RenderTexture(bnd.width + 20, bnd.height + 20);
			rendertxture.draw(spr);
			return rendertxture;
		}
		
		public static function keluarSession():void
		{
			var arrayProfile:Array = ["uid","auditorial","avatar","dominan","indexAvatar",
			"kataSandi","kinestetik","namaLengkap","namaPengguna",
			"nilai","status","visual","waktu","sessions"];

			var localSo:Object = mySo.data;

			if(mySo.data.uid != undefined)
			{
				var uid:String = mySo.data.uid;

				for(var prop:String in localSo)
				{
					if(findInArray(prop) > -1)
					{
						delete mySo.data[prop];
					}
				}

				function findInArray(str:String):int
				{
					var i:uint, len:int = arrayProfile.length;

					for(i; i < len; ++i){
						if(arrayProfile[i] == str){
							trace("found it at index: " + i);
							return i;
						}
					}
					return -1;
				}

				var newObject:Object = mySo.data;
				patchData(Url.FIREBASE_PATCH_DATA_PENGGUNA_URL, uid, newObject);
			}

			mySo.clear();
		}
		
		public static function checksession():void
		{
			for(var prop:String in mySo.data)
			{
				trace("[SharedObject] data:" + prop + " -> " + mySo.data[prop]);
			}
		}

		public static function patchData(url:String, id:String, object:Object):void
		{
			var uid:String = id + "/sharedObject.json";
			var patchMethod:String = "?x-http-method-override=PATCH";
			var addition:String = uid + patchMethod;

			var request:URLRequest = new URLRequest(url + addition);
			request.data = JSON.stringify(object);
			request.method = URLRequestMethod.POST;

			var loader:URLLoader = new URLLoader();
			loader.addEventListener(flash.events.Event.COMPLETE, postDataCompleteHAndler);
			loader.addEventListener(IOErrorEvent.IO_ERROR, errorHandler);
			loader.load(request);
		}

		public static function postDataCompleteHAndler(event:flash.events.Event):void
		{
			event.currentTarget.removeEventListener(flash.events.Event.COMPLETE, postDataCompleteHAndler);

			var rawData:Object = JSON.parse(event.currentTarget.data);
			for(var parentKey:String in rawData)
			{
				trace(parentKey, rawData[parentKey]);
			}
		}

		public static function errorHandler(event:IOErrorEvent):void
		{
			trace(event.currentTarget.data);
		}
	}
}
