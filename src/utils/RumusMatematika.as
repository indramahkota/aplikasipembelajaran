package utils 
{
	import com.sevenson.math.display.mathml.MathML;

	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	import flash.text.TextFormat;

	import starling.core.Starling;
	import starling.display.Image;
	import starling.textures.Texture;
	
	/**
	 * ...
	 * @author Indra Mahkota
	 */
	public class RumusMatematika extends Image 
	{
		private var texture:Texture;
		
		private var contentScale:Number = 1;
		private var tfF:TextFormat = new TextFormat();
		
		public function RumusMatematika(text:String, name:String)
		{
			contentScale = Starling.current.contentScaleFactor;
			texture = createTexture(text, name);
			super(texture);
		}
		
		private function createTexture(text:String, name:String):Texture
		{
			if(Main.assets.getTextureNames(name).length != 0)
			{
				texture = Main.assets.getTexture(name);
			}
			else
			{
				tfF.font = "Calibri";
				tfF.color = 0x000000;
				tfF.size = contentScale * 14;
			
				var xml:XML = new XML(text);
				var sprt:Sprite = MathML.parse(xml, tfF);
				var bmp:Bitmap = createBitmap(sprt);
				
				texture = Texture.fromBitmap(bmp, false, true);
				
				Main.assets.addAsset(name, texture);
			}
			
			return texture;
		}
		
		private function createBitmap(target:DisplayObject):Bitmap
		{
			if(!target.parent)
			{
				var tempSprite:Sprite = new Sprite();
				tempSprite.addChild(target);
			}
			
			var rect:Rectangle = target.parent.getBounds(target.parent);
			
			var matrix:Matrix = new Matrix();
			matrix.translate(-rect.x, -rect.y);
			
			var bmp:BitmapData = new BitmapData(rect.width + (contentScale * 5), rect.height, true, 0);
			bmp.draw(target.parent, matrix);
			
			var duplicate:Bitmap = new Bitmap(bmp);
			return duplicate;
		}
	}
}