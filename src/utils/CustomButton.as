package utils
{
	import feathers.controls.Button;

	import flash.geom.Point;

	import starling.animation.Transitions;
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	/**
	 * ...
	 * @author Indra Mahkota
	 */
	public class CustomButton extends Button
	{
		private var tween:Tween;
		protected var radialEffect:RoundedQuad;
		private var touchPointID:int = -1;
		
		private static const HELPER_POINT:Point = new Point();
		private static const HELPER_TOUCHES_VECTOR:Vector.<Touch> = new <Touch>[];
		
		public function CustomButton()
		{
			this.addEventListener(TouchEvent.TOUCH, touchHandler);
		}
		
		protected function touchHandler(event:TouchEvent):void
		{
			mask = new Quad(bounds.width, bounds.height);
			
			if(!_isEnabled)
			{
				touchPointID = -1;
				return;
			}
			
			var touch:Touch;
			var isInBounds:Boolean;
			var touches:Vector.<Touch> = event.getTouches(this, null, HELPER_TOUCHES_VECTOR);
			
			if(touches.length == 0)
			{
				return;
			}
			
			if(touchPointID >= 0)
			{
				for each (var currentTouch:Touch in touches)
				{
					if(currentTouch.id == touchPointID)
					{
						touch = currentTouch;
						break;
					}
				}
				
				if(!touch)
				{
					HELPER_TOUCHES_VECTOR.length = 0;
					return;
				}
				
				if(touch.phase == TouchPhase.MOVED)
				{
					touch.getLocation(this, HELPER_POINT);
					isInBounds = this.hitTest(HELPER_POINT) !== null;					
				}
				else if(touch.phase == TouchPhase.ENDED)
				{
					touchPointID = -1;
					
					if(isInBounds)
					{
						if(tween != null && !tween.isComplete)
						{
							tween.onComplete = function():void
							{
								radialEffect.dispose();
								Starling.juggler.remove(tween);
							};
						}
					}
				}
			}
			else
			{
				for each (touch in touches)
				{
					if(touch.phase == TouchPhase.BEGAN)
					{
						touchPointID = touch.id;
						if(radialEffect != null) radialEffect.dispose();
						createcircleTween(touch.getLocation(this));
						break;
					}
				}
			}
			HELPER_TOUCHES_VECTOR.length = 0;
		}
		
		private function createcircleTween(position:Point):void
		{
			const wh:Number = 100;
			radialEffect = new RoundedQuad(wh / 2, wh, wh, 0xd2d2d2);
			radialEffect.alpha = 0.5;
			radialEffect.touchable = false;
			
			radialEffect.x = position.x, radialEffect.y = position.y;
			
			radialEffect.width = bounds.width * 2;
			const finalScale:Number = radialEffect.scaleX;
			
			radialEffect.scaleX = radialEffect.scaleY = 0;
			radialEffect.alignPivot();
			
			if(this.defaultSkin != null)
			{
				addChildAt(radialEffect, 1);
			}
			else
			{
				addChildAt(radialEffect, 0);
			}
			
			var tweenDuration:Number = finalScale;
			tweenDuration = (tweenDuration < 0.8) ? 0.8 : ((tweenDuration > 1.6) ? 1.6 : tweenDuration);
			
			Starling.juggler.remove(tween);
			
			tween = new Tween(radialEffect, tweenDuration, Transitions.EASE_OUT);
			tween.fadeTo(0);
			tween.scaleTo(finalScale);
			Starling.juggler.add(tween);
		}
	}
}