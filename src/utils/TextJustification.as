package utils
{
	import com.sevenson.math.display.mathml.MathML;

	import feathers.controls.ImageLoader;
	import feathers.controls.Label;
	import feathers.controls.LayoutGroup;
	import feathers.layout.FlowLayout;
	import feathers.layout.VerticalAlign;

	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	import flash.text.TextFormat;

	import starling.core.Starling;
	import starling.textures.Texture;
	
	/**
	 * ...
	 * @author Indra Mahkota
	 */
	public class TextJustification extends LayoutGroup 
	{
		private var text:String;
		private var kontainerwidth:Number;
		private var flowLayout:FlowLayout;
		private var pattern:RegExp = /_/g;
		
		private var lencharStr:int = 0;
		private var padding:Number = 0;
		private var hitungbaris:Number = 0;
		private var hitunglebar:Number = 0;
		private var panjangsementara:Number = 0;
		
		private var pembilang:String;
		private var penyebut:String;
		private var xmlString:String;
		private var imageloader:ImageLoader;
		private var labelSoal:Label;
		private var contentScale:Number = 1;
		
		private var strArr:Array = [];
		private var objectSementara:Array = [];
		private var tempArrayBaris:Array = [];
		private var arrayPanjang:Array = [];
		private var arrayObject:Array = [];
		private var arrayPadding:Array = [];
		
		private var tfF:TextFormat = new TextFormat();
		
		public function TextJustification(txt:String, wdt:Number)
		{
			text = txt;
			kontainerwidth = wdt;
			contentScale = Starling.current.contentScaleFactor;
			
			super();
		}
		
		override protected function initialize():void
		{
			tfF.font = "Calibri";
			tfF.color = 0x000000;
			tfF.size = contentScale * 12;
			
			flowLayout = new FlowLayout();
			flowLayout.rowVerticalAlign = VerticalAlign.MIDDLE;
			this.layout = flowLayout;
			
			strArr = text.split(' ');
			lencharStr = strArr.length;
			
			for (var i:int = 0; i < lencharStr; ++i)
			{
				if(hitunglebar < width)
				{
					if(strArr[i].substr(0,4) == "pec(")
					{
						imageloader = createImage(strArr[i]);
						imageloader.validate();
						
						panjangsementara = imageloader.width;
						hitunglebar += panjangsementara;
						
						arrayObject.push(imageloader);
						tempArrayBaris.push(imageloader);
					}
					else
					{
						labelSoal = createLabel(strArr[i]);
						labelSoal.validate();
						
						panjangsementara = labelSoal.width;
						hitunglebar += panjangsementara;
						
						arrayObject.push(labelSoal);
						tempArrayBaris.push(labelSoal);
					}
				}
				else
				{
					objectSementara.push(tempArrayBaris[tempArrayBaris.length - 1]);
					
					tempArrayBaris.removeAt(tempArrayBaris.length - 1);
					
					padding = (width - (hitunglebar - panjangsementara)) / (tempArrayBaris.length - 1);
					
					arrayPadding.push(0);
					
					for (var j:int = 1; j < tempArrayBaris.length; ++j)
					{
						arrayPadding.push(padding * 0.99);
					}
					
					hitunglebar = panjangsementara;
					tempArrayBaris = objectSementara;
					
					objectSementara.length = 0;
					hitungbaris += 1;
					
					if(strArr[i].substr(0,4) == "pec(")
					{
						imageloader = createImage(strArr[i]);
						imageloader.validate();
						
						panjangsementara = imageloader.width;
						hitunglebar += panjangsementara;
						
						arrayObject.push(imageloader);
						tempArrayBaris.push(imageloader);
					}
					else
					{
						labelSoal = createLabel(strArr[i]);
						labelSoal.validate();
						
						panjangsementara = labelSoal.width;
						hitunglebar += panjangsementara;
						
						arrayObject.push(labelSoal);
						tempArrayBaris.push(labelSoal);
					}
				}
				
				if(i == (lencharStr - 1))
				{
					if(hitunglebar > width)
					{
						tempArrayBaris.removeAt(tempArrayBaris.length - 1);
						
						padding = (width - (hitunglebar - panjangsementara)) / (tempArrayBaris.length);
						
						arrayPadding.push(0);
						
						for (var c:int = 0; c < tempArrayBaris.length - 1; ++c)//berdasarkan bug yang terakhir kali ditemukan
						{
							arrayPadding.push(padding * 0.99);
						}
						
						arrayPadding.push(0);
					}
					else
					{
						for (var l:int = 0; l < tempArrayBaris.length + 1; ++l)
						{
							arrayPadding.push(0);
						}
					}
					
					for (var k:int = 0; k < arrayObject.length; ++k)
					{
						if(arrayObject[k] is ImageLoader)
						{
							arrayObject[k].paddingLeft = contentScale * arrayPadding[k];
							arrayObject[k].paddingTop = contentScale * 2;
						}
						else
						{
							arrayObject[k].paddingLeft = arrayPadding[k];
						}
						
						this.addChild(arrayObject[k]);
					}
				}
			}
			
			super.initialize();
		}
		
		private function createImage(text:String):ImageLoader
		{
			pembilang = text.slice(4, text.indexOf("/")).replace(pattern, " ");
			penyebut = text.slice((text.indexOf("/") + 1), text.indexOf(")")).replace(pattern, " ");
			
			xmlString = "<math><mfrac><mn>" + pembilang + "</mn><mn>" + penyebut + "</mn></mfrac></math>";
			var xml:XML = new XML(xmlString);
			
			var sprt:Sprite = MathML.parse(xml, tfF);
			var bmp:Bitmap = createBitmap(sprt);
			
			var img:ImageLoader = new ImageLoader();
			img.source = Texture.fromBitmap(bmp, false, true);
			img.scale = (1 / contentScale);
			return img;
		}
		
		private function createBitmap(target:DisplayObject):Bitmap
		{
			if(!target.parent)
			{
				var tempSprite:Sprite = new Sprite();
				tempSprite.addChild(target);
			}
			
			var rect:Rectangle = target.parent.getBounds(target.parent);
			
			var matrix:Matrix = new Matrix();
			matrix.translate(-rect.x, -rect.y);
			
			var bmp:BitmapData = new BitmapData(rect.width + (contentScale * 5), rect.height, true, 0);
			bmp.draw(target.parent, matrix);
			
			var duplicate:Bitmap = new Bitmap(bmp);
			return duplicate;
		}
		
		private function createLabel(text:String):Label
		{
			var label:Label = new Label();
			label.text = text + " ";
			return label;
		}
	}
}