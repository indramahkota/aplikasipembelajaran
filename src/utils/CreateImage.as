package utils 
{
	import com.sevenson.math.display.mathml.MathML;

	import feathers.controls.ImageLoader;
	import feathers.controls.Label;
	import feathers.controls.LayoutGroup;
	import feathers.layout.FlowLayout;
	import feathers.layout.VerticalAlign;

	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	import flash.text.TextFormat;

	import starling.core.Starling;
	import starling.display.Image;
	import starling.textures.RenderTexture;
	import starling.textures.Texture;
	
	/**
	 * ...
	 * @author Indra Mahkota
	 */
	public class CreateImage extends Image 
	{
		private var flowLayout:FlowLayout;
		private var pattern:RegExp = /_/g;
		
		private var pembilang:String;
		private var penyebut:String;
		private var xmlString:String;
		private var imageloader:ImageLoader;
		private var labelSoal:Label;
		
		private var texture:Texture;
		private var layoutgroup:LayoutGroup;
		private var rendertexture:RenderTexture;
		
		private var contentScale:Number = 1;
		
		private var lencharStr:int = 0;
		private var padding:Number = 0;
		//private var hitungbaris:Number = 0;
		private var hitunglebar:Number = 0;
		private var panjangsementara:Number = 0;
		
		private var strArr:Array = [];
		private var objectSementara:Array = [];
		private var tempArrayBaris:Array = [];
		private var arrayPanjang:Array = [];
		private var arrayObject:Array = [];
		private var arrayPadding:Array = [];
		
		private var tfF:TextFormat = new TextFormat();

		private var textureFromBitmap:Array = [];
		
		public function CreateImage(text:String, width:Number)
		{
			contentScale = Starling.current.contentScaleFactor;
			rendertexture = textJustification(text, width);

			var i:uint, len:int = textureFromBitmap.length;

			for(i; i < len; ++i)
			{
				(textureFromBitmap[i] as Texture).root.dispose();
			}

			super(rendertexture);
		}
		
		private function textJustification(text:String, width:Number):RenderTexture
		{
			tfF.font = "Calibri";
			tfF.color = 0x000000;
			tfF.size = contentScale * 12;
			
			flowLayout = new FlowLayout();
			flowLayout.rowVerticalAlign = VerticalAlign.MIDDLE;
			
			layoutgroup = new LayoutGroup();
			layoutgroup.layout = flowLayout;
			layoutgroup.width = width;
			
			strArr = text.split(' ');
			lencharStr = strArr.length;
			
			for (var i:int = 0; i < lencharStr; ++i)
			{
				if(hitunglebar < width)
				{
					if(strArr[i].substr(0,4) == "pec(")
					{
						imageloader = createImage(strArr[i]);
						imageloader.validate();
						
						panjangsementara = imageloader.width;
						hitunglebar += panjangsementara;
						
						arrayObject.push(imageloader);
						tempArrayBaris.push(imageloader);
					}
					else
					{
						labelSoal = createLabel(strArr[i]);
						labelSoal.validate();
						
						panjangsementara = labelSoal.width;
						hitunglebar += panjangsementara;
						
						arrayObject.push(labelSoal);
						tempArrayBaris.push(labelSoal);
					}
				}
				else
				{
					//handle baris
					//item tempArrayBaris jumlah lebarnya sudah melewati batas width
					//dengan membuat array "objectSementara" dan memasukkan item terakhir dari tempArrayBaris ke objectSementara maka item tersebut tidak akan hilang nantinya
					objectSementara.push(tempArrayBaris[tempArrayBaris.length - 1]);

					//dengan menghapus item terakhir dari tempArrayBaris maka satu baris telah kita buat
					tempArrayBaris.removeAt(tempArrayBaris.length - 1);
					
					//dengan mengurangi width dengan hitunglebar dan panjangsementara maka akan didapatkan nilai padding = (hasil pengurangan / semua object yang ada di baris satu dikurang satu karena padding pertama = 0)
					padding = (width - (hitunglebar - panjangsementara)) / (tempArrayBaris.length - 1);
					
					//padding item pertama = 0
					arrayPadding.push(0);
					
					//padding item selanjutnya = padding dikali 0,99 untuk mengantisipasi kesalahan penjumlahan yang mengakibatkan flow layout salah menghitung barisnya
					for (var j:int = 1; j < tempArrayBaris.length; ++j)
					{
						arrayPadding.push(padding * 0.99);
					}
					
					//menyimpan panjangsementara yang merupakan object yang telah dibuang dari baris satu dan akan dimasukkan di baris kedua
					hitunglebar = panjangsementara;

					//menyimpan objectsementara yang merupakan object yang telah dibuang dari baris satu dan akan dimasukkan di baris kedua
					tempArrayBaris = objectSementara;
					
					//menghapus object sementara karena item sudah dipindahkan kebaris kedua
					objectSementara.length = 0;

					//hitungbaris += 1;//untuk debug saja melihat berapa baris yang dibuat
					
					//handle i yang sekarang
					//karena i tetap berjalan dan i yang sekarang tidak melewati tahap atas membuat imageloader dan label jadi kita tangkap nilainya disini
					if(strArr[i].substr(0,4) == "pec(")
					{
						imageloader = createImage(strArr[i]);
						imageloader.validate();
						
						panjangsementara = imageloader.width;
						hitunglebar += panjangsementara;
						
						arrayObject.push(imageloader);
						tempArrayBaris.push(imageloader);
					}
					else
					{
						labelSoal = createLabel(strArr[i]);
						labelSoal.validate();
						
						panjangsementara = labelSoal.width;
						hitunglebar += panjangsementara;
						
						arrayObject.push(labelSoal);
						tempArrayBaris.push(labelSoal);
					}
				}
				
				//untuk mengeksekusi tempArrayBaris dan finishing pemunculan semua item di flowLayout
				//jika i-nya sudah sampai pada item yang terakhir dan semua item sudah dibuat imageloadernya dan labelnya
				if(i == (lencharStr - 1))
				{
					//mengecek apakah hitunglebar pada baris terakhir lebih besar dari width
					if(hitunglebar > width)
					{
						//jika hitunglebar berlebih, kita akan menghapus item terakhir dan menghitung padding kembali
						tempArrayBaris.removeAt(tempArrayBaris.length - 1);
						
						padding = (width - (hitunglebar - panjangsementara)) / (tempArrayBaris.length);
						
						arrayPadding.push(0);
						
						for (var c:int = 0; c < tempArrayBaris.length - 1; ++c)//letak bug terakhir yang membuat comment ini muncul dan sangat menjengkelkannnnn shiiit. ternyata aku kelebihan satu indeks
						{
							arrayPadding.push(padding * 0.99);
						}
						
						arrayPadding.push(0);
					}
					else
					{
						for (var l:int = 0; l < tempArrayBaris.length + 1; ++l)
						{
							arrayPadding.push(0);
						}
					}
					
					for (var k:int = 0; k < arrayObject.length; ++k)
					{
						if(arrayObject[k] is ImageLoader)
						{
							arrayObject[k].paddingLeft = contentScale * arrayPadding[k];
							arrayObject[k].paddingTop = contentScale * 2;
						}
						else
						{
							arrayObject[k].paddingLeft = arrayPadding[k];
						}
						
						layoutgroup.addChild(arrayObject[k]);
					}
				}
			}
			
			layoutgroup.validate();
			var bounds:Rectangle = layoutgroup.getBounds(layoutgroup);
			
			var matrix:Matrix = new Matrix();
			matrix.translate(-bounds.x, -bounds.y);
			
			rendertexture = new RenderTexture(bounds.width, bounds.height);
			rendertexture.draw(layoutgroup, matrix);
			return rendertexture;
		}
		
		private function createImage(text:String):ImageLoader
		{
			pembilang = text.slice(4, text.indexOf("/")).replace(pattern, " ");
			penyebut = text.slice((text.indexOf("/") + 1), text.indexOf(")")).replace(pattern, " ");
			
			xmlString = "<math><mfrac><mn>" + pembilang + "</mn><mn>" + penyebut + "</mn></mfrac></math>";
			texture = createTexture(xmlString, text);
			
			var img:ImageLoader = new ImageLoader();
			img.source = texture;
			img.scale = (1 / contentScale);
			
			return img;
		}
		
		private function createTexture(text:String, name:String):Texture
		{
			var xml:XML = new XML(text);
			var sprt:Sprite = MathML.parse(xml, tfF);
			var bmp:Bitmap = createBitmap(sprt);
			texture = Texture.fromBitmap(bmp, false, true);

			textureFromBitmap.push(texture);
			
			return texture;
		}
		
		private function createBitmap(target:DisplayObject):Bitmap
		{
			if(!target.parent)
			{
				var tempSprite:Sprite = new Sprite();
				tempSprite.addChild(target);
			}
			
			var rect:Rectangle = target.parent.getBounds(target.parent);
			
			var matrix:Matrix = new Matrix();
			matrix.translate(-rect.x, -rect.y);
			
			var bmp:BitmapData = new BitmapData(rect.width + (contentScale * 5), rect.height, true, 0);
			bmp.draw(target.parent, matrix);
			
			var duplicate:Bitmap = new Bitmap(bmp);
			return duplicate;
		}
		
		private function createLabel(text:String):Label
		{
			var label:Label = new Label();
			label.text = text + " ";
			return label;
		}
	}
}