package utils
{
	import feathers.controls.LayoutGroup;
	import feathers.layout.AnchorLayout;

	import starling.display.Canvas;
	import starling.display.Quad;
	import starling.utils.Align;
	
	/**
	 * ...
	 * @author Indra Mahkota
	 */
	public class PieChart extends LayoutGroup
	{
		private var pembilang:int;
		private var penyebut:int;

		public function PieChart(pemb:int, peny:int)
		{
			pembilang = pemb;
			penyebut = peny;
			super();
		}
		
		override protected function initialize():void
		{
			this.layout = new AnchorLayout();

			var quadSectionLayout:LayoutGroup = new LayoutGroup();
			var quadLineLayout:LayoutGroup = new LayoutGroup();

			this.addChild(quadSectionLayout);
			this.addChild(quadLineLayout);

			var avatarMask0:Canvas = new Canvas();
			avatarMask0.drawCircle(51, 51, 51);

			var avatarMask:Canvas = new Canvas();
			avatarMask.drawCircle(50, 50, 50);

			var quadSection0:Quad = new Quad(102, 102, 0x0);
			quadSection0.alignPivot();
			quadSection0.mask = avatarMask0;
			
			var quadSection1:Quad = new Quad(100, 100, 0xF80330);
			quadSection1.alignPivot();
			quadSection1.mask = avatarMask;

			var quadSection2:QuadSection = new QuadSection(100, 100, 0xFFFF33);
			quadSection2.alignPivot();
			quadSection2.mask = avatarMask;
			quadSection2.ratio = pembilang / penyebut;
			
			quadSectionLayout.addChild(quadSection0);
			quadSectionLayout.addChild(quadSection1);
			quadSectionLayout.addChild(quadSection2);

			if(pembilang == 1 && penyebut == 1) return;

			var counter:Number = 1;
			var pengurang:Number = 1 / penyebut;

			for (var k:int = 0; k < penyebut; ++k)
			{
				var quadLine:Quad = new Quad(1, 50, 0x0);
				quadLine.alignPivot(Align.CENTER, Align.BOTTOM);
				quadLine.rotation = -1 * (counter * 360) * (Math.PI / 180);
				
				quadLineLayout.addChild(quadLine);

				counter = counter - pengurang;
			}
		}
	}
}