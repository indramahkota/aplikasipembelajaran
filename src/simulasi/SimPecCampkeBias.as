package simulasi
{
	import feathers.controls.Label;
	import feathers.controls.LayoutGroup;
	import feathers.controls.TextInput;
	import feathers.layout.AnchorLayout;
	import feathers.layout.AnchorLayoutData;
	import feathers.layout.VerticalLayout;
	import feathers.layout.VerticalLayoutData;
	import feathers.skins.ImageSkin;

	import flash.geom.Matrix;
	import flash.geom.Rectangle;

	import starling.display.Image;
	import starling.display.Quad;
	import starling.events.Event;
	import starling.text.TextFormat;
	import starling.textures.RenderTexture;
	import starling.textures.Texture;

	import utils.TextJustification;
	
	/**
	 * ...
	 * @author Indra Mahkota
	 */
	public class SimPecCampkeBias extends LayoutGroup
	{
		private var lgs:LayoutGroup;
		
		private var inputA:TextInput;
		private var inputB:TextInput;
		private var inputC:TextInput;

		private var text1:TextJustification;
		private var text2:TextJustification;
		private var text3:TextJustification;
		
		private var Yposition:Number;
		private var ControlHeigh:Number;
		
		private static var fixed_height:Number;
		private var defaultTexture:Texture;
		
		private var textFormat:TextFormat = new TextFormat("Calibri", 16);
		
		public function SimPecCampkeBias()
		{
			super();
		}
		
		override protected function initialize():void
		{
			var layout:VerticalLayout = new VerticalLayout();
			layout.paddingTop = 20;
			layout.paddingLeft = 10;
			layout.paddingRight = 10;
			layout.gap = 10;
			this.layout = layout;

			defaultTexture = inputBackgroundSkin(0xd2d2d2);

			var bgInputA:ImageSkin = new ImageSkin(defaultTexture);
			bgInputA.scale9Grid = new Rectangle(2, 2, 46, 46);

			var image:Image = new Image(Main.roundQuadBackground);
			image.pixelSnapping = true;
			image.scale9Grid = new Rectangle(20, 20, 80, 80);
			this.backgroundSkin = image;

			this.minHeight = stage.stageHeight - 85;
			this.width = stage.stageWidth;
			
			//var judul:Label = createLabel("Masukkan bilangan a, b, dan c dengan c ≠ 0 untuk mendapatkan bentuk pecahan biasa.");
			var judul:Label = createLabel("Mendapatkan bentuk pecahan biasa.");
			this.addChild(judul);
			
			lgs = createLayoutGroupWithAnchorLayout();
			this.addChild(lgs);
			
			inputA = new TextInput();
			inputA.prompt = "b";
			inputA.maxChars = 3;
			inputA.restrict = "0-9";
			inputA.fontStyles = textFormat;
			inputA.promptFontStyles = textFormat;
			inputA.width = 50;
			inputA.backgroundSkin = bgInputA;
			inputA.layoutData = new AnchorLayoutData(0, NaN, NaN, NaN, 10, NaN);
			inputA.styleNameList.add("text-input-number");
			lgs.addChild(inputA);
			
			inputA.validate();
			var inputAHeight:Number = inputA.height;
			
			inputB = new TextInput();
			inputB.prompt = "c";
			inputB.maxChars = 3;
			inputB.restrict = "0-9";
			inputB.fontStyles = textFormat;
			inputB.promptFontStyles = textFormat;
			inputB.width = 50;
			inputB.layoutData = new AnchorLayoutData(inputAHeight, NaN, NaN, NaN, 10, NaN);
			inputB.styleNameList.add("text-input-number");
			lgs.addChild(inputB);
			
			inputC = new TextInput();
			inputC.prompt = "a";
			inputC.maxChars = 3;
			inputC.restrict = "0-9";
			inputC.fontStyles = textFormat;
			inputC.promptFontStyles = textFormat;
			inputC.width = 50;
			inputC.layoutData = new AnchorLayoutData(inputAHeight / 2, NaN, NaN, NaN, -40, NaN);
			inputC.styleNameList.add("text-input-number");
			lgs.addChild(inputC);
			
			inputA.addEventListener(Event.CHANGE, selanjutnya);
			inputB.addEventListener(Event.CHANGE, selanjutnya);
			inputC.addEventListener(Event.CHANGE, selanjutnya);
			
			Yposition = (2 * inputAHeight + 10);
		}
		
		override public function dispose():void
		{
			defaultTexture.dispose();
			super.dispose();
		}

		private function inputBackgroundSkin(color:uint):Texture
		{
			var quad:Quad = new Quad(50, 2, color);
			var matrix:Matrix = new Matrix();
			matrix.translate(0, 48);

			var rendertxture:RenderTexture = new RenderTexture(50, 50);
			rendertxture.draw(quad, matrix);
			return rendertxture;
		}
				
		private function selanjutnya():void
		{
			if(lgs.contains(text1)) text1.removeFromParent(true);
			if(lgs.contains(text2)) text2.removeFromParent(true);
			if(lgs.contains(text3)) text3.removeFromParent(true);
			
			if(inputA.text.length > 0 && inputB.text.length > 0 && inputC.text.length > 0)
			{
				var a:int = int(inputA.text);
				var b:int = int(inputB.text);
				var c:int = int(inputC.text);
				var d:int;
				var e:int;
				var f:int;
				var fpb:int = 1;
				
				for (var i:int = 1; i <= a && i <= b; ++i)
				{
					if(a % i == 0 && b % i == 0)
					{
						fpb = i;
					}
				}
				
				var str1:String;
				var str2:String;
				var str3:String;
				
				if(b == 0)
				{
					text1 = new TextJustification("c tidak boleh 0.", stage.stageWidth - 60);
					text1.layoutData = new AnchorLayoutData(Yposition, NaN, NaN, NaN, 0, NaN);
					lgs.addChild(text1);
				}
				else if(a == 0)
				{
					str1 = c + " pec(" + a + "/" + b + ") = " + c + " + pec(" + a + "/" + b + ") = " + c + " + 0 = " + c;
					
					text1 = new TextJustification(str1, stage.stageWidth - 60);
					text1.layoutData = new AnchorLayoutData(Yposition, NaN, NaN, NaN, 0, NaN);
					lgs.addChild(text1);
				}
				else if(a == b)
				{
					str1 = c + " pec(" + a + "/" + b + ") = " + c + " + pec(" + a + "/" + b + ") = " + c + " + 1 = " + (c + 1);
					
					text1 = new TextJustification(str1, stage.stageWidth - 60);
					text1.layoutData = new AnchorLayoutData(Yposition, NaN, NaN, NaN, 0, NaN);
					lgs.addChild(text1);
				}
				else
				{
					if(a % b == 0)
					{
						str1 = c + " pec(" + a + "/" + b + ") = " + c + " + pec(" + a + "/" + b + ") = " + c + " + " + (a / b) + " = " + (c + (a / b));
						
						text1 = new TextJustification(str1, stage.stageWidth - 60);
						text1.layoutData = new AnchorLayoutData(Yposition, NaN, NaN, NaN, 0, NaN);
						lgs.addChild(text1);
					}
					else if(fpb != 1)
					{
						str1 = c + " pec(" + a + "/" + b + ") = pec(" + b + "_x_" + c + "_+_" + a + "/" + b + ") = pec(" + ((b * c) + a) + "/" + b + ") = pec(" + ((b * c) + a) + "_:_" + fpb + "/" + b + "_:_" + fpb + ") = pec(" + (((b * c) + a) / fpb) + "/" + (b / fpb) + ")";
						
						text1 = new TextJustification(str1, stage.stageWidth - 60);
						text1.layoutData = new AnchorLayoutData(Yposition, NaN, NaN, NaN, 0, NaN);
						//text1.width = stage.stageWidth - 60;
						lgs.addChild(text1);
						
						/* text1.validate();
						ControlHeigh = text1.height;
						
						str2 = "atau";
						
						text2 = new TextJustification(str2, stage.stageWidth - 60);
						text2.layoutData = new AnchorLayoutData(Yposition + ControlHeigh + 10, NaN, NaN, NaN, 0, NaN);
						text2.width = stage.stageWidth - 60;
						lgs.addChild(text2);
						
						text2.validate();
						ControlHeigh += text2.height;
						
						str3 = c + " pec(" + a + "/" + b + ") = " + c + " + pec(" + a + "/" + b + ") =  pec(" + (c * b) + "/" + b + ") + pec(" + a + "/" + b + ") = pec(" + ((b * c) + a) + "/" + b + ") = pec(" + ((b * c) + a) + "_:_" + fpb + "/" + b + "_:_" + fpb + ") = pec(" + (((b * c) + a) / fpb) + "/" + (b / fpb) + ")";
						
						text3 = new TextJustification(str3, stage.stageWidth - 60);
						text3.layoutData = new AnchorLayoutData(Yposition + (ControlHeigh + 10), NaN, NaN, NaN, 0, NaN);
						text3.width = stage.stageWidth - 60;
						lgs.addChild(text3); */
					}
					else
					{
						str1 = c + " pec(" + a + "/" + b + ") = pec(" + b + "_x_" + c + "_+_" + a + "/" + b + ") = pec(" + ((b * c) + a) + "/" + b + ")";
						
						text1 = new TextJustification(str1, stage.stageWidth - 60);
						text1.layoutData = new AnchorLayoutData(Yposition, NaN, NaN, NaN, 0, NaN);
						lgs.addChild(text1);
						
						/* text1.validate();
						ControlHeigh = text1.height;
						
						str2 = "atau";
						
						text2 = new TextJustification(str2, stage.stageWidth - 60);
						text2.layoutData = new AnchorLayoutData(Yposition + ControlHeigh + 10, NaN, NaN, NaN, 0, NaN);
						lgs.addChild(text2);
						
						text2.validate();
						ControlHeigh += text2.height;
						
						str3 = c + " pec(" + a + "/" + b + ") = " + c + " + pec(" + a + "/" + b + ") =  pec(" + (c * b) + "/" + b + ") + pec(" + a + "/" + b + ") = pec(" + ((b * c) + a) + "/" + b + ")";
						
						text3 = new TextJustification(str3, stage.stageWidth - 60);
						text3.layoutData = new AnchorLayoutData(Yposition + (ControlHeigh + 10), NaN, NaN, NaN, 0, NaN);
						lgs.addChild(text3); */
					}
				}
			}
		}
		
		private function createLayoutGroupWithAnchorLayout():LayoutGroup
		{
			var lgs:LayoutGroup = new LayoutGroup();
			lgs.layout = new AnchorLayout();
			lgs.layoutData = new VerticalLayoutData(100, NaN);
			return lgs;
		}
		
		private function createLabel(text:String):Label
		{
			var label:Label = new Label();
			label.text = text;
			
			label.paddingLeft = label.paddingRight = 20;
			label.textRendererProperties.wordWrap = true;
			label.layoutData = new VerticalLayoutData(100, NaN);
			return label;
		}
	}
}