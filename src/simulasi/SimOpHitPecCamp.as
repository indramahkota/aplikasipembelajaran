package simulasi
{
	import feathers.controls.Label;
	import feathers.controls.LayoutGroup;
	import feathers.controls.TextInput;
	import feathers.layout.AnchorLayout;
	import feathers.layout.AnchorLayoutData;
	import feathers.layout.VerticalLayout;
	import feathers.layout.VerticalLayoutData;
	import feathers.skins.ImageSkin;

	import flash.geom.Matrix;
	import flash.geom.Rectangle;

	import starling.display.Image;
	import starling.display.Quad;
	import starling.events.Event;
	import starling.text.TextFormat;
	import starling.textures.RenderTexture;
	import starling.textures.Texture;

	import utils.TextJustification;
	
	/**
	 * ...
	 * @author Indra Mahkota
	 */
	public class SimOpHitPecCamp extends LayoutGroup
	{
		private var lgs:LayoutGroup;
		
		private var inputA:TextInput;
		private var inputB:TextInput;
		private var inputC:TextInput;
		private var inputD:TextInput;
		private var inputE:TextInput;
		private var inputF:TextInput;
		private var inputG:TextInput;

		private var text1:TextJustification;
		private var text2:TextJustification;

		private var jawaban:LayoutGroup;
		private var angk1:int;
		private var angk2:int;
		private var angk3:int;
		private var angk4:int;
		
		private var Yposition:Number;
		private var ControlHeigh:Number;
		
		private static var fixed_height:Number;
		private var defaultTexture:Texture;
		
		private var textFormat:TextFormat = new TextFormat("Calibri", 16);
		
		public function SimOpHitPecCamp()
		{
			super();
		}
		
		override protected function initialize():void
		{
			var layout:VerticalLayout = new VerticalLayout();
			layout.paddingTop = 20;
			layout.paddingLeft = 10;
			layout.paddingRight = 10;
			layout.gap = 10;
			this.layout = layout;

			defaultTexture = inputBackgroundSkin(0xd2d2d2);

			var bgInputA:ImageSkin = new ImageSkin(defaultTexture);
			bgInputA.scale9Grid = new Rectangle(2, 2, 46, 46);

			var bgInputC:ImageSkin = new ImageSkin(defaultTexture);
			bgInputA.scale9Grid = new Rectangle(2, 2, 46, 46);

			var image:Image = new Image(Main.roundQuadBackground);
			image.pixelSnapping = true;
			image.scale9Grid = new Rectangle(20, 20, 80, 80);
			this.backgroundSkin = image;

			this.minHeight = stage.stageHeight - 85;
			this.width = stage.stageWidth;
			
			//var judul:Label = createLabel("Masukkan bilangan a, b, c, d, e, dan f serta ubah tanda ... menjadi tanda + - x atau : dengan syarat c dan f ≠ 0 untuk mengoperasikan pecahan campuran.");
			var judul:Label = createLabel("Mengoperasikan pecahan campuran.");
			this.addChild(judul);
			
			lgs = createLayoutGroupWithAnchorLayout();
			this.addChild(lgs);

			jawaban = createLayoutGroupWithAnchorLayout();
			this.addChild(jawaban);
			
			inputA = new TextInput();
			inputA.prompt = "b";
			inputA.maxChars = 3;
			inputA.restrict = "0-9";
			inputA.fontStyles = textFormat;
			inputA.promptFontStyles = textFormat;
			inputA.width = 50;
			inputA.backgroundSkin = bgInputA;
			inputA.layoutData = new AnchorLayoutData(0, NaN, NaN, NaN, -50, NaN);
			inputA.styleNameList.add("text-input-number");
			lgs.addChild(inputA);
			
			inputA.validate();
			var inputBHeight:Number = inputA.height;
			
			inputB = new TextInput();
			inputB.prompt = "c";
			inputB.maxChars = 3;
			inputB.restrict = "0-9";
			inputB.fontStyles = textFormat;
			inputB.promptFontStyles = textFormat;
			inputB.width = 50;
			inputB.layoutData = new AnchorLayoutData(inputBHeight, NaN, NaN, NaN, -50, NaN);
			inputB.styleNameList.add("text-input-number");
			lgs.addChild(inputB);
			
			inputC = new TextInput();
			inputC.prompt = "e";
			inputC.maxChars = 3;
			inputC.restrict = "0-9";
			inputC.fontStyles = textFormat;
			inputC.promptFontStyles = textFormat;
			inputC.width = 50;
			inputC.backgroundSkin = bgInputC;
			inputC.layoutData = new AnchorLayoutData(0, NaN, NaN, NaN, 80, NaN);
			inputC.styleNameList.add("text-input-number");
			lgs.addChild(inputC);
			
			inputD = new TextInput();
			inputD.prompt = "f";
			inputD.maxChars = 3;
			inputD.restrict = "0-9";
			inputD.fontStyles = textFormat;
			inputD.promptFontStyles = textFormat;
			inputD.width = 50;
			inputD.layoutData = new AnchorLayoutData(inputBHeight, NaN, NaN, NaN, 80, NaN);
			inputD.styleNameList.add("text-input-number");
			lgs.addChild(inputD);
			
			inputE = new TextInput();
			inputE.prompt = "...";
			inputE.maxChars = 1;
			inputE.restrict = "x+-:";
			inputE.fontStyles = textFormat;
			inputE.promptFontStyles = textFormat;
			inputE.width = 50;
			inputE.layoutData = new AnchorLayoutData(NaN, NaN, NaN, NaN, 0, 0);
			inputE.styleNameList.add("text-input-number");
			lgs.addChild(inputE);
			
			inputF = new TextInput();
			inputF.prompt = "a";
			inputF.maxChars = 3;
			inputF.restrict = "0-9";
			inputF.fontStyles = textFormat;
			inputF.promptFontStyles = textFormat;
			inputF.width = 50;
			inputF.layoutData = new AnchorLayoutData(inputBHeight / 2, NaN, NaN, NaN, -95, NaN);
			inputF.styleNameList.add("text-input-number");
			lgs.addChild(inputF);
			
			inputG = new TextInput();
			inputG.prompt = "d";
			inputG.maxChars = 3;
			inputG.restrict = "0-9";
			inputG.fontStyles = textFormat;
			inputG.promptFontStyles = textFormat;
			inputG.width = 50;
			inputG.layoutData = new AnchorLayoutData(inputBHeight / 2, NaN, NaN, NaN, 35, NaN);
			inputG.styleNameList.add("text-input-number");
			lgs.addChild(inputG);
			
			inputA.addEventListener(Event.CHANGE, selanjutnya);
			inputB.addEventListener(Event.CHANGE, selanjutnya);
			inputC.addEventListener(Event.CHANGE, selanjutnya);
			inputD.addEventListener(Event.CHANGE, selanjutnya);
			inputE.addEventListener(Event.CHANGE, selanjutnya);
			inputF.addEventListener(Event.CHANGE, selanjutnya);
			inputG.addEventListener(Event.CHANGE, selanjutnya);
		}
		
		override public function dispose():void
		{
			defaultTexture.dispose();
			super.dispose();
		}

		private function inputBackgroundSkin(color:uint):Texture
		{
			var quad:Quad = new Quad(50, 2, color);
			var matrix:Matrix = new Matrix();
			matrix.translate(0, 48);

			var rendertxture:RenderTexture = new RenderTexture(50, 50);
			rendertxture.draw(quad, matrix);
			return rendertxture;
		}
				
		private function selanjutnya():void
		{
			if(jawaban.contains(text1)) text1.removeFromParent(true);
			if(jawaban.contains(text2)) text2.removeFromParent(true);
			
			if(inputA.text.length > 0 && inputB.text.length > 0 && inputC.text.length > 0 && inputD.text.length > 0
				&& inputE.text.length > 0 && inputF.text.length > 0 && inputG.text.length > 0)
			{
				var str:String;
				var penyebut1:int = int(inputB.text);
				var penyebut2:int = int(inputD.text);

				var bool:Boolean = ((penyebut1 == 0 && penyebut2 == 0) || (penyebut1 == 0) || (penyebut2 == 0));

				if(!bool)
				{
					str = "Ubah pecahan campuran diatas menjadi pecahan biasa " + ubahKePecahanBiasa(int(inputA.text), int(inputB.text), int(inputF.text)) + " dan " + ubahKePecahanBiasa1(int(inputC.text), int(inputD.text), int(inputG.text)) + " lalu operasikan seperti dibawah ini.";
					
					text1 = new TextJustification(str, stage.stageWidth - 60);
					text1.layoutData = new AnchorLayoutData(NaN, NaN, NaN, NaN, 0, NaN);
					text1.width = stage.stageWidth - 60;
					jawaban.addChild(text1);
					
					text1.validate();
					var gap:Number = text1.height + 8;
					
					var str1:String = operasikanPecahan(angk1, angk2, angk3, angk4, inputE.text);
					
					text2 = new TextJustification(str1, stage.stageWidth - 60);
					text2.layoutData = new AnchorLayoutData(gap, NaN, NaN, NaN, 0, NaN);
					text2.width = stage.stageWidth - 60;
					jawaban.addChild(text2);

					return;
				}

				if(penyebut1 == 0 && penyebut2 == 0)
				{
					str = "nilai b dan d tidak boleh 0.";
				}
				else if(penyebut1 == 0)
				{
					str = "nilai b tidak boleh 0.";
				}
				else if(penyebut2 == 0)
				{
					str = "nilai d tidak boleh 0.";
				}
				else
				{
					str = "maaf tidak memenuhi.";
				}

				text1 = new TextJustification(str, stage.stageWidth - 60);
				text1.layoutData = new AnchorLayoutData(NaN, NaN, NaN, NaN, 0, NaN);
				jawaban.addChild(text1);
			}
		}

		private function ubahKePecahanBiasa(a:int, b:int, c:int):String
		{
			//c bilbul, a pembilang, b penyebut.
			var string:String;			
			var fpb:int = cariFpb(a, b);
			
			if(a == 0)
			{
				string = String(c);
				angk1 = c;
				angk2 = 1;
			}
			else if(a == b)
			{
				string = String(c + 1);
				angk1 = c + 1;
				angk2 = 1;
			}
			else
			{
				if(a % b == 0)
				{
					string = String(c + (a / b));
					angk1 = c + (a / b);
					angk2 = 1;
				}
				else if(fpb != 1)
				{
					string = "pec(" + (((b * c) + a) / fpb) + "/" + (b / fpb) + ")";
					angk1 = (((b * c) + a) / fpb);
					angk2 = (b / fpb);
				}
				else
				{
					string = "pec(" + ((b * c) + a) + "/" + b + ")";
					angk1 = ((b * c) + a);
					angk2 = b;
				}
			}
			
			return string;
		}

		private function ubahKePecahanBiasa1(a:int, b:int, c:int):String
		{
			//c bilbul, a pembilang, b penyebut.
			var string:String;			
			var fpb:int = cariFpb(a, b);
			
			if(a == 0)
			{
				string = String(c);
				angk3 = c;
				angk4 = 1;
			}
			else if(a == b)
			{
				string = String(c + 1);
				angk3 = c + 1;
				angk4 = 1;
			}
			else
			{
				if(a % b == 0)
				{
					string = String(c + (a / b));
					angk3 = c + (a / b);
					angk4 = 1;
				}
				else if(fpb != 1)
				{
					string = "pec(" + (((b * c) + a) / fpb) + "/" + (b / fpb) + ")";
					angk3 = (((b * c) + a) / fpb);
					angk4 = (b / fpb);
				}
				else
				{
					string = "pec(" + ((b * c) + a) + "/" + b + ")";
					angk3 = ((b * c) + a);
					angk4 = b;
				}
			}
			
			return string;
		}

		private function operasikanPecahan(a:int, b:int, c:int, d:int, tanda:String):String
		{
			//a, c pembilang; b, d penyebut.
			var string:String;
			
			var i:int, m:int, n:int, o:int, p:int, q:int, r:int;
			
			var fpb:int = 1, fpb1:int = 1, kpk:int = 1;

			//-----------------------------------------------------------------------------//
			//---------------------------- penjumlahan pecahan ----------------------------//
			//-----------------------------------------------------------------------------//
			
			if(tanda == "+")
			{
				if(a == 0 && c == 0)
				{
					string = "= 0 + 0 = 0";
				}
				else if(a == 0)
				{
					fpb = cariFpb(c, d);
					
					if(c == d)
					{
						string = "= 0 + 1 = 1";
					}
					else if(c > d)
					{
						if(c % d == 0)
						{
							string = "= 0 + " + (c / d) + " = " + (c / d);
						}
						else
						{
							m = c % d;
							n = c - m;
							o = n / d;
							
							if(fpb != 1)
							{
								string = "= 0 + pec(" + c + "_:_" + fpb + "/" + d + "_:_" + fpb + ") = 0 + pec(" + (c / fpb) + "/" + (d / fpb) + ") = pec(" + (c / fpb) + "/" + (d / fpb) + ") = " + o + " pec(" + (m / fpb) + "/" + (d / fpb) + ")";
							}
							else
							{
								string = "= 0 + pec(" + c + "/" + d + ") = pec(" + c + "/" + d + ") = " + o + " pec(" + m + "/" + d + ")";
							}
						}
					}
					else
					{
						if(fpb != 1)
						{
							string = "= 0 + pec(" + c + "_:_" + fpb + "/" + d + "_:_" + fpb + ") = 0 + pec(" + (c / fpb) + "/" + (d / fpb) + ") = pec(" + (c / fpb) + "/" + (d / fpb) + ")";
						}
						else
						{
							string = "= 0 + pec(" + c + "/" + d + ") = pec(" + c + "/" + d + ")";
						}
					}
				}
				else if(c == 0)
				{
					fpb = cariFpb(a, b);
					
					if(a == b)
					{
						string = "= 1 + 0 = 1";
					}
					else if(a > b)
					{
						if(a % b == 0)
						{
							string = "= " + (a / b) + " + 0 = " + (a / b);
						}
						else
						{
							m = a % b;
							n = a - m;
							o = n / b;
							
							if(fpb != 1)
							{
								string = "= pec(" + a + "_:_" + fpb + "/" + b + "_:_" + fpb + ") + 0 = pec(" + (a / fpb) + "/" + (b / fpb) + ") + 0 = pec(" + (a / fpb) + "/" + (b / fpb) + ") = " + o + " pec(" + (m / fpb) + "/" + (b / fpb) + ")";
							}
							else
							{
								string = "= pec(" + a + "/" + b + ") + 0 = pec(" + a + "/" + b + ") = " + o + " pec(" + m + "/" + b + ")";
							}
						}
					}
					else
					{
						if(fpb != 1)
						{
							string = "= pec(" + a + "_:_" + fpb + "/" + b + "_:_" + fpb + ") + 0 = pec(" + (a / fpb) + "/" + (b / fpb) + ") + 0 = pec(" + (a / fpb) + "/" + (b / fpb) + ")";
						}
						else
						{
							string = "= pec(" + a + "/" + b + ") + 0 = pec(" + a + "/" + b + ")";
						}
					}
				}
				else if(a == b)
				{
					fpb = cariFpb(c, d);
					
					if(c == d)
					{
						string = "= 1 + 1 = 2";
					}
					else if(c > d)
					{
						if(c % d == 0)
						{
							string = "= 1 + " + (c / d) + " = " + (1 + (c / d));
						}
						else
						{
							m = c % d;
							n = c - m;
							o = n / d;
							
							if(fpb != 1)
							{
								string = "= 1 + pec(" + c + "_:_" + fpb + "/" + d + "_:_" + fpb + ") = 1 + pec(" + (c / fpb) + "/" + (d / fpb) + ") = " + 1 + " + " + o + " pec(" + (m / fpb) + "/" + (d / fpb) + ") = " + (1 + o) + " pec(" + (m / fpb) + "/" + (d / fpb) + ")";
							}
							else
							{
								string = "= 1 + pec(" + c + "/" + d + ") = " + 1 + " + " + o + " pec(" + m + "/" + d + ") = " + (1 + o) + " pec(" + m + "/" + d + ")";
							}
						}
					}
					else
					{
						if(fpb != 1)
						{
							string = "= 1 + pec(" + c + "_:_" + fpb + "/" + d + "_:_" + fpb + ") = 1 + pec(" + (c / fpb) + "/" + (d / fpb) + ") = 1 pec(" + (c / fpb) + "/" + (d / fpb) + ")";
						}
						else
						{
							string = "= 1 + pec(" + c + "/" + d + ") = 1 pec(" + c + "/" + d + ")";
						}
					}
				}
				else if(c == d)
				{
					fpb = cariFpb(a, b);
					
					if(a == b)
					{
						string = "= 1 + 1 = 2";
					}
					else if(a > b)
					{
						if(a % b == 0)
						{
							string = "= " + (a / b) + " + 1 = " + ((a / b) + 1);
						}
						else
						{
							m = a % b;
							n = a - m;
							o = n / b;
							
							if(fpb != 1)
							{
								string = "= pec(" + a + "_:_" + fpb + "/" + b + "_:_" + fpb + ") + 1 = pec(" + (a / fpb) + "/" + (b / fpb) + ") + 1 = " + o + " pec(" + (m / fpb) + "/" + (d / fpb) + ") + 1 = " + (o + 1) + " pec(" + (m / fpb) + "/" + (d / fpb) + ")";
							}
							else
							{
								string = "= pec(" + a + "/" + b + ") + 1 = " + o + " pec(" + m + "/" + b + ") + 1 = " + (o + 1) + " pec(" + m + "/" + b + ")";
							}
						}
					}
					else
					{
						if(fpb != 1)
						{
							string = "= pec(" + a + "_:_" + fpb + "/" + b + "_:_" + fpb + ") + 1 = pec(" + (a / fpb) + "/" + (b / fpb) + ") + 1 = 1 pec(" + (a / fpb) + "/" + (b / fpb) + ")";
						}
						else
						{
							string = "= pec(" + a + "/" + b + ") + 1 = 1 pec(" + a + "/" + b + ")";
						}
					}
				}
				else if(a % b == 0)
				{
					fpb = cariFpb(c, d);
					
					if(c == d)
					{
						string = "= " + (a / b) + " 1 = " + (a / b) + 1;
					}
					else if(c > d)
					{
						if(c % d == 0)
						{
							string = "= " + (a / b) + " + " + (c / d) + " = " + ((a / b) + c / d);
						}
						else
						{
							m = c % d;
							n = c - m;
							o = n / d;
							
							if(fpb != 1)
							{
								string = "= " + (a / b) + " + pec(" + c + "_:_" + fpb + "/" + d + "_:_" + fpb + ") = " + (a / b) + " + pec(" + (c / fpb) + "/" + (d / fpb) + ") = " + (a / b) + " + " + o + " pec(" + (m / fpb) + "/" + (d / fpb) + ") = " + ((a / b) + o) + " pec(" + (m / fpb) + "/" + (d / fpb) + ")";
							}
							else
							{
								string = "= " + (a / b) + " + pec(" + c + "/" + d + ") = " + (a / b) + " + " + o + " pec(" + m + "/" + d + ") = " + ((a / b) + o) + " pec(" + m + "/" + d + ")";
							}
						}
					}
					else
					{
						if(fpb != 1)
						{
							string = "= " + (a / b) + " + pec(" + c + "_:_" + fpb + "/" + d + "_:_" + fpb + ") = " + (a / b) + " pec(" + (c / fpb) + "/" + (d / fpb) + ")";
						}
						else
						{
							string = "= " + (a / b) + " + pec(" + c + "/" + d + ") = " + (a / b) + " pec(" + c + "/" + d + ")";
						}
					}
				}
				else if(c % d == 0)
				{
					fpb = cariFpb(a, b);
					
					if(a == b)
					{
						string = "= 1 + " + (c / d) + " = " + (1 + (c / d));
					}
					else if(a > b)
					{
						if(a % b == 0)
						{
							string = "= " + (a / b) + " + " + (c / d) + " = " + (( a / b) + 1);
						}
						else
						{
							m = a % b;
							n = a - m;
							o = n / b;
							
							if(fpb != 1)
							{
								string = "= pec(" + a + "_:_" + fpb + "/" + b + "_:_" + fpb + ") + " + (c / d) + " = pec(" + (a / fpb) + "/" + (b / fpb) + ") + " + (c / d) + " = " + o + " pec(" + (m / fpb) + "/" + (b / fpb) + ") + " + (c / d) + " = " + (o + (c / d)) + " pec(" + (m / fpb) + "/" + (b / fpb) + ")";
							}
							else
							{
								string = "= pec(" + a + "/" + b + ") + " + (c / d) + " = " + o + " pec(" + m + "/" + b + ") + " + (c / d) + " = " + (o + (c / d)) + " pec(" + m + "/" + b + ")";
							}
						}
					}
					else
					{
						if(fpb != 1)
						{
							string = "= pec(" + a + "_:_" + fpb + "/" + b + "_:_" + fpb + ") + " + (c / d) + " = pec(" + (a / fpb) + "/" + (b / fpb) + ") + " + (c / d) + " = " + (c / d) + " pec(" + (a / fpb) + "/" + (b / fpb) + ")";
						}
						else
						{
							string = "= pec(" + a + "/" + b + ") + " + (c / d) + " = " + (c / d) + " pec(" + a + "/" + b + ")";
						}
					}
				}
				else if(b == d)
				{
					m = a + c;
					
					fpb = cariFpb(m, b);
					
					if(m == b)
					{
						string = "= pec(" + a + "_+_" + c + "/" + b + ") = pec(" + m + "/" + b + ") = 1";
					}
					else if(m > b)
					{
						if(m % b == 0)
						{
							string = "= pec(" + a + "_+_" + c + "/" + b + ") = pec(" + m + "/" + b + ") = " + (m / b);
						}
						else
						{
							n = m % b;
							o = m - n;
							p = o / b;
							
							if(fpb != 1)
							{
								string = "= pec(" + a + "_+_" + c + "/" + b + ") = pec(" + m + "/" + b + ") = " + "pec(" + m + "_:_" + fpb + "/" + b + "_:_" + fpb + ") = pec(" + (m / fpb) + "/" + (b / fpb) + ") = " + p + " pec(" + (n / fpb) + "/" + (b / fpb) + ")"; 
							}
							else
							{
								string = "= pec(" + a + "_+_" + c + "/" + b + ") = pec(" + m + "/" + b + ") = " + p + " pec(" + n + "/" + b + ")"; 
							}
						}
					}
					else
					{
						n = m % b;
						o = m - n;
						p = o / b;
						
						if(fpb != 1)
						{
							string = "= pec(" + a + "_+_" + c + "/" + b + ") = pec(" + m + "/" + b + ") = " + "pec(" + m + "_:_" + fpb + "/" + b + "_:_" + fpb + ") = pec(" + (m / fpb) + "/" + (b / fpb) + ")";
						}
						else
						{
							string = "= pec(" + a + "_+_" + c + "/" + b + ") = pec(" + m + "/" + b + ")";
						}
					}
				}
				else if(b != d)
				{
					fpb = cariFpb(b, d);
					
					kpk = b * d / fpb;
					
					m = kpk / b * a;
					n = kpk / d * c;
					
					o = m + n;
					
					fpb1 = cariFpb(o, kpk);
					
					string = "KPK(" + b + ", " + d + ") = " + kpk + ", maka pec(" + a + "/" + b + ") + pec(" + c + "/" + d + ") = pec(" + m + "/" + kpk + ") + pec(" + n + "/" + kpk + ") = pec(" + o + "/" + kpk + ")";
					
					if(o == kpk)
					{
						string += " = 1";
					}
					else if(o > kpk)
					{
						if(o % kpk == 0)
						{
							string += " = pec(" + o + "/" + kpk + ") = " + (o / kpk);
						}
						else
						{
							p = o % kpk;
							q = o - p;
							r = q / kpk;
							
							if(fpb1 != 1)
							{
								string += " = pec(" + o + "_:_" + fpb1 + "/" + kpk + "_:_" + fpb1 + ") = pec(" + (o / fpb1) + "/" + (kpk / fpb1) + ") = " + r + " pec(" + (p / fpb1) + "/" + (kpk / fpb1) + ")";
							}
							else
							{
								string += " = " + r + " pec(" + (p / fpb1) + "/" + (kpk / fpb1) + ")";
							}
						}
					}
					else
					{
						p = o % kpk;
						q = o - p;
						r = q / kpk;
						
						if(fpb1 != 1)
						{
							string += " = pec(" + o + "_:_" + fpb1 + "/" + kpk + "_:_" + fpb1 + ") = pec(" + (o / fpb1) + "/" + (kpk / fpb1) + ")";
						}
					}
				}
				else
				{
					string = "tidak memenuhi";
				}
			}
			
			//-----------------------------------------------------------------------------//
			//---------------------------- pengurangan pecahan ----------------------------//
			//-----------------------------------------------------------------------------//
			
			else if(tanda == "-")
			{
				if(a == 0 && c == 0)
				{
					string = "= 0 - 0 = 0";
				}
				else if(a == 0)//fix
				{
					fpb = cariFpb(c, d);
					
					if(c == d)
					{
						string = "= 0 - 1 = -1";
					}
					else if(c > d)
					{
						if(c % d == 0)
						{
							string = "= 0 - " + (c / d) + " = " + (0 - (c / d));
						}
						else
						{
							m = c % d;
							n = c - m;
							o = n / d;
							
							if(fpb != 1)
							{
								string = "= 0 - pec(" + c + "_:_" + fpb + "/" + d + "_:_" + fpb + ") = 0 - pec(" + (c / fpb) + "/" + (d / fpb) + ") =  - pec(" + (c / fpb) + "/" + (d / fpb) + ") = " + (0 - o) + " pec(" + (m / fpb) + "/" + (d / fpb) + ")";
							}
							else
							{
								string = "= 0 - pec(" + c + "/" + d + ") = " + (0 - o) + " pec(" + m + "/" + d + ")";
							}
						}
					}
					else
					{
						if(fpb != 1)
						{
							string = "= 0 - pec(" + c + "_:_" + fpb + "/" + d + "_:_" + fpb + ") = 0 - pec(" + (c / fpb) + "/" + (d / fpb) + ") = - pec(" + (c / fpb) + "/" + (d / fpb) + ")";
						}
						else
						{
							string = "= 0 - pec(" + c + "/" + d + ") = - pec(" + c + "/" + d + ")";
						}
					}
				}
				else if(c == 0)//fix
				{
					fpb = cariFpb(a, b);
					
					if(a == b)
					{
						string = "= 1 - 0 = 1";
					}
					else if(a > b)
					{
						if(a % b == 0)
						{
							string = "= " + (a / b) + " - 0 = " + (a / b);
						}
						else
						{
							m = a % b;
							n = a - m;
							o = n / b;
							
							if(fpb != 1)
							{
								string = "= pec(" + a + "_:_" + fpb + "/" + b + "_:_" + fpb + ") - 0 = pec(" + (a / fpb) + "/" + (b / fpb) + ") - 0 = pec(" + (a / fpb) + "/" + (b / fpb) + ") = " + o + " pec(" + (m / fpb) + "/" + (b / fpb) + ")";
							}
							else
							{
								string = "= pec(" + a + "/" + b + ") - 0 = " + (-1 * o) + " pec(" + m + "/" + b + ")";
							}
						}
					}
					else
					{
						if(fpb != 1)
						{
							string = "= pec(" + a + "_:_" + fpb + "/" + b + "_:_" + fpb + ") - 0 = pec(" + (a / fpb) + "/" + (b / fpb) + ") - 0  = pec(" + (a / fpb) + "/" + (b / fpb) + ")";
						}
						else
						{
							string = "= pec(" + a + "/" + b + ") - 0 = pec(" + a + "/" + b + ")";
						}
					}
				}
				else if(a == b)//fix
				{
					string = "= 1 - pec(" + c + "/" + d + ") = pec(" + d + "/" + d + ") - pec(" + c + "/" + d + ") = pec(" + d + "_-_" + c + "/" + d + ") = pec(" + (d - c) + "/" + d + ")";
					
					if(c == d)
					{
						string = "= 1 - 1 = 0";
					}
					else if(c > d)
					{
						if(c % d == 0)
						{
							string = "= " + 1 + " - " + (c / d) + " = " + (1 - (c / d));
						}
						else
						{
							m = -1 * (d - c);
							
							fpb = cariFpb(m, d);
							
							if(m == d)
							{
								//skip
							}
							else if(m > d)
							{
								if(m % d == 0)
								{
									//skip
								}
								else
								{
									n = m % d;
									o = m - n;
									p = o / d;
									
									if(fpb != 1)
									{
										string += " = pec(" + (-1 * m) + "_:_" + fpb + "/" + d + "_:_" + fpb + ") = pec(" + ((-1 * m) / fpb) + "/" + (d / fpb) + ") = " + (-1 * p) + " pec(" + (n / fpb) + "/" +  (d / fpb) + ")";
									}
									else
									{
										string += " = - " + p + " pec(" + n + "/" +  d + ")";
									}
								}
							}
							else
							{
								if(fpb != 1)
								{
									string += " = - pec(" + m + "_:_" + fpb + "/" + d + "_:_" + fpb + ") = - pec(" + (m / fpb) + "/" + (d / fpb) + ")";
								}
								else
								{
									string += " =  - pec(" + (-1 * (d - c)) + "/" + d + ")";
								}
							}
						}
					}
					else
					{
						m = d - c;
						
						fpb = cariFpb(m, d);
						
						if(m == d)
						{
							//skip
						}
						else if(m > d)
						{
							if(m % d == 0)
							{
								//skip
							}
							else
							{
								n = m % d;
								o = m - n;
								p = o / d;
								
								if(fpb != 1)
								{
									string += " = pec(" + m + "_:_" + fpb + "/" + d + "_:_" + fpb + ") = pec(" + (m / fpb) + "/" + (d / fpb) + ") = " + p + " pec(" + (n / fpb) + "/" +  (d / fpb) + ")";
								}
								else
								{
									string += " = " + p + " pec(" + n + "/" +  d + ")";
								}
							}
						}
						else
						{
							if(fpb != 1)
							{
								string += " = pec(" + m + "_:_" + fpb + "/" + d + "_:_" + fpb + ") = pec(" + (m / fpb) + "/" + (d / fpb) + ")";
							}
						}
					}
				}
				else if(c == d)//fix
				{
					string = "= pec(" + a + "/" + b + ") - 1 = pec(" + a + "/" + b + ") - pec(" + b + "/" + b + ") = pec(" + a + "_-_" + b + "/" + b + ") = pec(" + (a - b) + "/" + b + ")";
					
					if(a == b)
					{
						string = "= 1 - 1 = 0";
					}
					else if(a > b)
					{
						if(a % b == 0)
						{
							string = "= " + (a / b) + " - 1 = " + ((a / b) - 1);
						}
						else
						{
							m = a - b;
							
							fpb = cariFpb(m, b);
							
							if(m == b)
							{
								//skip
							}
							else if(m > b)
							{
								if(m % b == 0)
								{
									//skip
								}
								else
								{
									n = m % b;
									o = m - n;
									p = o / b;
									
									if(fpb != 1)
									{
										string += " = pec(" + m + "_:_" + fpb + "/" + b + "_:_" + fpb + ") = pec(" + (m / fpb) + "/" + (b / fpb) + ") = " + p + " pec(" + (n / fpb) + "/" +  (b / fpb) + ")";
									}
									else
									{
										string += " = " + p + " pec(" + n + "/" +  b + ")";
									}
								}
							}
							else
							{
								if(fpb != 1)
								{
									string += " = pec(" + m + "_:_" + fpb + "/" + b + "_:_" + fpb + ") = pec(" + (m / fpb) + "/" + (b / fpb) + ")";
								}
							}
						}
					}
					else
					{
						m = b - a;
						
						fpb = cariFpb(m, b);
						
						if(m == b)
						{
							//skip 
						}
						else if(m > b)
						{
							if(m % d == 0)
							{
								//skip
							}
							else
							{
								n = m % b;
								o = m - n;
								p = o / b;
								
								if(fpb != 1)
								{
									string += " = - pec(" + m + "_:_" + fpb + "/" + b + "_:_" + fpb + ") = - pec(" + (m / fpb) + "/" + (b / fpb) + ") = " + (-1 * p) + " pec(" + (n / fpb) + "/" +  (b / fpb) + ")";
								}
								else
								{
									string += " = - " + p + " pec(" + n + "/" +  b + ")";
								}
							}
						}
						else
						{
							if(fpb != 1)
							{
								string += " = - pec(" + m + "_:_" + fpb + "/" + b + "_:_" + fpb + ") = - pec(" + (m / fpb) + "/" + (b / fpb) + ")";
							}
							else
							{
								string += " = - pec(" + (-1 * (a - b)) + "/" + b + ")"
							}
						}
					}
				}
				else if(a % b == 0)//fix, minus case a/b = c/d || c berkelipatan dengan d
				{
					string = "= " + (a / b) + " - pec(" + c + "/" + d + ") = pec(" + ((a / b) * d) + "/" + d + ") - pec(" + c + "/" + d + ") = pec(" + ((a / b) * d) + "_-_" + c + "/" + d + ") = pec(" + (((a / b) * d) - c) + "/" + d + ")";
					//string atas fix
					
					if(c == d)
					{
						string = "= " + (a / b) + " - 1 = " + ((a / b) - 1);
					}
					else if(c > d) //bisa positif || bisa negatif
					{
						m = (((a / b) * d) - c);
						
						if(m < 0) //pecahan negatif
						{
							//untuk mencari fpb m harus dijadikan positif karena memakai algoritma sederhana.
							m = Math.sqrt(Math.pow(m, 2));
							fpb = cariFpb(m, d);
							
							if(m % d == 0)
							{
								string += " = - " + (m / d);
							}
							else
							{
								if(m == d)
								{
									string += " = - 1";
								}
								else if(m > d)
								{
									n = m % d;
									o = m - n;
									p = o / d;
									
									if(fpb != 1)
									{
										string += " = - pec(" + m + "_:_" + fpb + "/" + d + "_:_" + fpb + ") = - pec(" + (m / fpb) + "/" + (d / fpb) + ") = - " + p + " pec(" + (n / fpb) + "/" + (d / fpb) + ")";
									}
									else
									{
										string +=  " = - pec(" + m + "/" + d + ") = - " + p + " pec(" + n + "/" + d + ")";
									}
								}
								else
								{
									if(fpb != 1)
									{
										string += " = - pec(" + m + "_:_" + fpb + "/" + d + "_:_" + fpb + ") = - pec(" + (m / fpb) + "/" + (d / fpb) + ")";
									}
									else
									{
										string += " = - pec(" + m + "/" + d + ")";
									}
								}
							}
						}
						else
						{
							fpb = cariFpb(m, d);
							
							if(m % d == 0)
							{
								string += " = " + (m / d);
							}
							else
							{
								if(m == d)
								{
									string += " = 1";
								}
								else if(m > d)
								{
									n = m % d;
									o = m - n;
									p = o / d;
									
									if(fpb != 1)
									{
										string += " = pec(" + m + "_:_" + fpb + "/" + d + "_:_" + fpb + ") = pec(" + (m / fpb) + "/" + (d / fpb) + ") = " + p + " pec(" + (n / fpb) + "/" + (d / fpb) + ")";
									}
									else
									{
										string +=  " = " + p + " pec(" + n + "/" + d + ")";
									}
								}
								else
								{
									if(fpb != 1)
									{
										string += " = pec(" + m + "_:_" + fpb + "/" + d + "_:_" + fpb + ") = pec(" + (m / fpb) + "/" + (d / fpb) + ")";
									}
								}
							}
						}
					}
					else //fix //pasti positif karena case ini nilai a/b > 1 dan dikurang bilangan < 1.
					{
						m = (((a /b) * d) - c);
						
						fpb = cariFpb(m, d);
						
						if(m == d)
						{
							//skip
						}
						else if(m > d)
						{
							if(m % d == 0)
							{
								//skip
							}
							else
							{
								n = m % d;
								o = m - n;
								p = o / d;
								
								if(fpb != 1)
								{
									string += " = pec(" + m + "_:_" + fpb + "/" + d + "_:_" + fpb + ") = pec(" + (m / fpb) + "/" + (d / fpb) + ") = " + p + " pec(" + (n / fpb) + "/" +  (d / fpb) + ")";
								}
								else
								{
									string += " = " + p + " pec(" + n + "/" +  d + ")";
								}
							}
						}
						else
						{
							if(fpb != 1)
							{
								string += " = pec(" + m + "_:_" + fpb + "/" + d + "_:_" + fpb + ") = pec(" + (m / fpb) + "/" + (d / fpb) + ")";
							}
						}
					}
				}
				else if(c % d == 0)//fix, minus case a/b = c/d || a berkelipatan dengan b
				{
					string = "= pec(" + a + "/" + b + ") - " + (c / d) + " = pec(" + a + "/" + b + ") - pec(" + (b * (c / d)) + "/" + b + ") = pec(" + a + "_-_" + (b * (c / d)) + "/" + b + ") = pec(" + (a - (b * (c / d))) + "/" + b + ")";
					//string atas fix
					
					if(a == b)
					{
						string = "= 1 - " + (c / d) + " = " + (1 - (a / b));
					}
					else if(a > b) //bisa positif || bisa negatif
					{
						m = (a - (b * (c / d)));
						
						if(m < 0) //pecahan negatif
						{
							//untuk mencari fpb m harus dijadikan positif karena memakai algoritma sederhana.
							m = Math.sqrt(Math.pow(m, 2));
							fpb = cariFpb(m, b);
							
							if(m % b == 0)
							{
								string += " = - " + (m / b);
							}
							else
							{
								if(m == b)
								{
									string += " = - 1";
								}
								else if(m > b)
								{
									n = m % b;
									o = m - n;
									p = o / b;
									
									if(fpb != 1)
									{
										string += " = - pec(" + m + "_:_" + fpb + "/" + b + "_:_" + fpb + ") = - pec(" + (m / fpb) + "/" + (b / fpb) + ") = - " + p + " pec(" + (n / fpb) + "/" + (b / fpb) + ")";
									}
									else
									{
										string +=  " = - pec(" + m + "/" + b + ") = - " + p + " pec(" + n + "/" + b + ")";
									}
								}
								else
								{
									if(fpb != 1)
									{
										string += " = - pec(" + m + "_:_" + fpb + "/" + b + "_:_" + fpb + ") = - pec(" + (m / fpb) + "/" + (b / fpb) + ")";
									}
									else
									{
										string += " = - pec(" + m + "/" + b + ")";
									}
								}
							}
						}
						else
						{
							fpb = cariFpb(m, b);
							
							if(m % b == 0)
							{
								string += " = " + (m / b);
							}
							else
							{
								if(m == b)
								{
									string += " = 1";
								}
								else if(m > b)
								{
									n = m % b;
									o = m - n;
									p = o / b;
									
									if(fpb != 1)
									{
										string += " = pec(" + m + "_:_" + fpb + "/" + b + "_:_" + fpb + ") = pec(" + (m / fpb) + "/" + (b / fpb) + ") = " + p + " pec(" + (n / fpb) + "/" + (b / fpb) + ")";
									}
									else
									{
										string +=  " = " + p + " pec(" + n + "/" + b + ")";
									}
								}
								else
								{
									if(fpb != 1)
									{
										string += " = pec(" + m + "_:_" + fpb + "/" + b + "_:_" + fpb + ") = pec(" + (m / fpb) + "/" + (b / fpb) + ")";
									}
								}
							}
						}
					}
					else //karena (a < b), nilai a/b adalah (0 <= x <= 1) dan dikurang dengan c/d > 1 bernilai negatif.
					{
						m = (a - (b * (c / d)));
						
						m = Math.sqrt(Math.pow(m, 2));
						fpb = cariFpb(m, b);
						
						if(m == b)
						{
							//skip
						}
						else if(m > b)
						{
							if(m % b == 0)
							{
								//skip
							}
							else
							{
								n = m % b;
								o = m - n;
								p = o / b;
								
								if(fpb != 1)
								{
									string += " = - pec(" + m + "_:_" + fpb + "/" + b + "_:_" + fpb + ") = - pec(" + (m / fpb) + "/" + (b / fpb) + ") = - " + p + " pec(" + (n / fpb) + "/" +  (b / fpb) + ")";
								}
								else
								{
									string += " = - " + p + " pec(" + n + "/" +  b + ")";
								}
							}
						}
						else
						{
							if(fpb != 1)
							{
								string += " = - pec(" + m + "_:_" + fpb + "/" + b + "_:_" + fpb + ") = - pec(" + (m / fpb) + "/" + (b / fpb) + ")";
							}
						}
					}
				}
				else if(b == d)//fix
				{
					string = "= pec(" + a + "_-_" + c + "/" + b + ") = pec(" + (a - c) + "/" + b + ")";
					
					m = a - c;
					
					if(m < 0) //pecahan negatif
					{
						//untuk mencari fpb m harus dijadikan positif karena memakai algoritma sederhana.
						m = Math.sqrt(Math.pow(m, 2));
						fpb = cariFpb(m, b);
						
						if(m % b == 0)
						{
							string += " = - " + (m / b);
						}
						else
						{
							if(m == b)
							{
								string += " = - 1";
							}
							else if(m > b)
							{
								n = m % b;
								o = m - n;
								p = o / b;
								
								if(fpb != 1)
								{
									string += " = - pec(" + m + "_:_" + fpb + "/" + b + "_:_" + fpb + ") = - pec(" + (m / fpb) + "/" + (b / fpb) + ") = - " + p + " pec(" + (n / fpb) + "/" + (b / fpb) + ")";
								}
								else
								{
									string +=  " = - pec(" + m + "/" + b + ") = - " + p + " pec(" + n + "/" + b + ")";
								}
							}
							else
							{
								if(fpb != 1)
								{
									string += " = - pec(" + m + "_:_" + fpb + "/" + b + "_:_" + fpb + ") = - pec(" + (m / fpb) + "/" + (b / fpb) + ")";
								}
								else
								{
									string += " = - pec(" + m + "/" + b + ")";
								}
							}
						}
					}
					else
					{
						fpb = cariFpb(m, b);
						
						if(m % b == 0)
						{
							string += " = " + (m / b);
						}
						else
						{
							if(m == b)
							{
								string += " = 1";
							}
							else if(m > b)
							{
								n = m % b;
								o = m - n;
								p = o / b;
								
								if(fpb != 1)
								{
									string += " = pec(" + m + "_:_" + fpb + "/" + b + "_:_" + fpb + ") = pec(" + (m / fpb) + "/" + (b / fpb) + ") = " + p + " pec(" + (n / fpb) + "/" + (b / fpb) + ")";
								}
								else
								{
									string +=  " = " + p + " pec(" + n + "/" + b + ")";
								}
							}
							else
							{
								if(fpb != 1)
								{
									string += " = pec(" + m + "_:_" + fpb + "/" + b + "_:_" + fpb + ") = pec(" + (m / fpb) + "/" + (b / fpb) + ")";
								}
								else
								{
									string += " = pec(" + m + "/" + b + ")";
								}
							}
						}
					}
				}
				else if(b != d)//fix
				{
					fpb1 = cariFpb(b, d);
					
					kpk = b * d / fpb1;
					
					m = (kpk / b * a) - (kpk / d * c);
					
					string = "KPK(" + b + ", " + d + ") = " + kpk + ", maka pec(" + a + "/" + b + ") - pec(" + c + "/" + d + ") = pec(" + (kpk / b * a) + "/" + kpk + ") - pec(" + (kpk / d * c) + "/" + kpk + ") = pec(" + m + "/" + kpk + ")";
					
					if(m < 0) //pecahan negatif
					{
						//untuk mencari fpb m harus dijadikan positif karena memakai algoritma sederhana.
						m = Math.sqrt(Math.pow(m, 2));
						fpb = cariFpb(m, kpk);
						
						if(m % kpk == 0)
						{
							string += " = - " + (m / kpk);
						}
						else
						{
							if(m == kpk)
							{
								string += " = - 1";
							}
							else if(m > kpk)
							{
								n = m % kpk;
								o = m - n;
								p = o / kpk;
								
								if(fpb != 1)
								{
									string += " = - pec(" + m + "_:_" + fpb + "/" + kpk + "_:_" + fpb + ") = - pec(" + (m / fpb) + "/" + (kpk / fpb) + ") = - " + p + " pec(" + (n / fpb) + "/" + (kpk / fpb) + ")";
								}
								else
								{
									string +=  " = - pec(" + m + "/" + kpk + ") = - " + p + " pec(" + n + "/" + kpk + ")";
								}
							}
							else
							{
								if(fpb != 1)
								{
									string += " = - pec(" + m + "_:_" + fpb + "/" + kpk + "_:_" + fpb + ") = - pec(" + (m / fpb) + "/" + (kpk / fpb) + ")";
								}
								else
								{
									string += " = - pec(" + m + "/" + kpk + ")";
								}
							}
						}
					}
					else
					{
						fpb = cariFpb(m, kpk);
						
						if(m % b == 0)
						{
							string += " = " + (m / kpk);
						}
						else
						{
							if(m == kpk)
							{
								string += " = 1";
							}
							else if(m > kpk)
							{
								n = m % kpk;
								o = m - n;
								p = o / kpk;
								
								if(fpb != 1)
								{
									string += " = pec(" + m + "_:_" + fpb + "/" + kpk + "_:_" + fpb + ") = pec(" + (m / fpb) + "/" + (kpk / fpb) + ") = " + p + " pec(" + (n / fpb) + "/" + (kpk / fpb) + ")";
								}
								else
								{
									string +=  " = " + p + " pec(" + n + "/" + kpk + ")";
								}
							}
							else
							{
								if(fpb != 1)
								{
									string += " = pec(" + m + "_:_" + fpb + "/" + kpk + "_:_" + fpb + ") = pec(" + (m / fpb) + "/" + (kpk / fpb) + ")";
								}
								else
								{
									string += " = pec(" + m + "/" + kpk + ")";
								}
							}
						}
					}
				}
				else
				{
					string = "tidak memenuhi";
				}
			}
			
			//-----------------------------------------------------------------------------//
			//----------------------------- perkalian pecahan -----------------------------//
			//-----------------------------------------------------------------------------//
			
			else if(tanda == "x")
			{
				m = a * c;
				n = b * d;
				
				fpb = cariFpb(m, n);
				
				string = "= pec(" + a + "_x_" + c + "/" + b + "_x_" + d + ") = pec(" + m + "/" + n + ")";
				
				if(m == 0)
				{
					string += " = 0";
				}
				else if(m == n)
				{
					string += " = 1";
				}
				else if(m > n)
				{
					if(m % n == 0)
					{
						string += " = " + (m / n); 
					}
					else
					{
						o = m % n;
						p = m - o;
						q = p / n;
						
						if(fpb != 1)
						{
							string += " = pec(" + m + "_:_" + fpb + "/" + n + "_:_" + fpb + ") = pec(" + (m / fpb) + "/" + (n / fpb) + ") = " + q +  " pec(" + (o / fpb) + "/" + (n / fpb) + ")";
						}
						else
						{
							string += " = " + q +  " pec(" + o + "/" + n + ")";
						}
					}
				}
				else
				{
					if(fpb != 1)
					{
						string += " = pec(" + m + "_:_" + fpb + "/" + n + "_:_" + fpb + ") = pec(" + (m / fpb) + "/" + (n / fpb) + ")";
					}
					else
					{
						//nothing to do;
					}
				}
			}
			
			//-----------------------------------------------------------------------------//
			//----------------------------- pembagian pecahan -----------------------------//
			//-----------------------------------------------------------------------------//
			
			else if(tanda == ":")
			{
				string = "= pec(" + a + "/" + b + ") : pec(" + c + "/" + d + ") = pec(" + a + "/" + b + ") x pec(" + d + "/" + c + ") ";
				string += operasikanPecahan(a, b, d, c, "x");
			}
			else
			{
				string = "tidak memenuhi";
			}
			
			return string;
		}

		private function cariFpb(a:int, b:int):int
		{
			var fpb:int;
			
			for (var i:int = 1; i <= a && i <= b; ++i)
			{
				if(a % i == 0 && b % i == 0)
				{
					fpb = i;
				}
			}
			
			return fpb;
		}
		
		private function createLayoutGroupWithAnchorLayout():LayoutGroup
		{
			var lgs:LayoutGroup = new LayoutGroup();
			lgs.layout = new AnchorLayout();
			lgs.layoutData = new VerticalLayoutData(100, NaN);
			return lgs;
		}
		
		private function createLabel(text:String):Label
		{
			var label:Label = new Label();
			label.text = text;
			
			label.paddingLeft = label.paddingRight = 20;
			label.textRendererProperties.wordWrap = true;
			label.layoutData = new VerticalLayoutData(100, NaN);
			return label;
		}
	}
}