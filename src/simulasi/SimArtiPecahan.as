package simulasi
{
	import feathers.controls.Label;
	import feathers.controls.LayoutGroup;
	import feathers.controls.TextInput;
	import feathers.layout.AnchorLayout;
	import feathers.layout.AnchorLayoutData;
	import feathers.layout.VerticalLayout;
	import feathers.layout.VerticalLayoutData;
	import feathers.skins.ImageSkin;

	import flash.geom.Matrix;
	import flash.geom.Rectangle;

	import starling.display.Image;
	import starling.display.Quad;
	import starling.events.Event;
	import starling.text.TextFormat;
	import starling.textures.RenderTexture;
	import starling.textures.Texture;

	import utils.PieChart;
	
	/**
	 * ...
	 * @author Indra Mahkota
	 */
	public class SimArtiPecahan extends LayoutGroup
	{
		private var lgs:LayoutGroup;
		
		private var inputA:TextInput;
		private var inputB:TextInput;
		private var pieChart:PieChart;
		private var keterangan:Label;

		private var yPosition:Number;
		
		private static var fixed_height:Number;
		private var defaultTexture:Texture;
		
		private var textFormat:TextFormat = new TextFormat("Calibri", 16);
		private var textFormat1:TextFormat = new TextFormat("SourceSansPro", 18);
		
		public function SimArtiPecahan()
		{
			super();
		}
		
		override protected function initialize():void
		{
			var layout:VerticalLayout = new VerticalLayout();
			layout.paddingTop = 20;
			layout.paddingLeft = 10;
			layout.paddingRight = 10;
			layout.gap = 10;
			this.layout = layout;

			defaultTexture = inputBackgroundSkin(0xd2d2d2);

			var bgInputA:ImageSkin = new ImageSkin(defaultTexture);
			bgInputA.scale9Grid = new Rectangle(2, 2, 46, 46);

			var image:Image = new Image(Main.roundQuadBackground);
			image.pixelSnapping = true;
			image.scale9Grid = new Rectangle(20, 20, 80, 80);
			this.backgroundSkin = image;

			this.minHeight = stage.stageHeight - 85;
			this.width = stage.stageWidth;
			
			//var judul:Label = createLabel("Masukkan bilangan a dan b dengan b ≠ 0 untuk merepresentasikan pecahan dalam bentuk diagram lingkaran.");
			var judul:Label = createLabel("Merepresentasikan pecahan dalam bentuk diagram lingkaran.");
			this.addChild(judul);
			
			lgs = createLayoutGroupWithAnchorLayout();
			this.addChild(lgs);
			
			inputA = new TextInput();
			inputA.prompt = "a";
			inputA.maxChars = 3;
			inputA.restrict = "0-9";
			inputA.fontStyles = textFormat;
			inputA.promptFontStyles = textFormat;
			inputA.width = 100;
			inputA.backgroundSkin = bgInputA;
			inputA.layoutData = new AnchorLayoutData(0, NaN, NaN, NaN, 0, NaN);
			inputA.styleNameList.add("text-input-number");
			lgs.addChild(inputA);
			
			inputA.validate();
			var inputAHeight:Number = inputA.height;
			
			inputB = new TextInput();
			inputB.prompt = "b";
			inputB.maxChars = 3;
			inputB.restrict = "0-9";
			inputB.fontStyles = textFormat;
			inputB.promptFontStyles = textFormat;
			inputB.width = 100;
			inputB.layoutData = new AnchorLayoutData(inputAHeight, NaN, NaN, NaN, 0, NaN);
			inputB.styleNameList.add("text-input-number");
			lgs.addChild(inputB);
			
			inputA.addEventListener(Event.CHANGE, selanjutnya);
			inputB.addEventListener(Event.CHANGE, selanjutnya);
			
			yPosition = (2 * inputAHeight + 10);

			super.initialize();
		}
		
		override public function dispose():void
		{
			defaultTexture.dispose();
			super.dispose();
		}

		private function inputBackgroundSkin(color:uint):Texture
		{
			var quad:Quad = new Quad(50, 2, color);
			var matrix:Matrix = new Matrix();
			matrix.translate(0, 48);

			var rendertxture:RenderTexture = new RenderTexture(50, 50);
			rendertxture.draw(quad, matrix);
			return rendertxture;
		}
				
		private function selanjutnya():void
		{
			if(lgs.contains(pieChart)) pieChart.removeFromParent(true);
			if(lgs.contains(keterangan)) keterangan.removeFromParent(true);
			
			if(inputA.text.length > 0 && inputB.text.length > 0)
			{
				var str:String;
				var pembilang:int = int(inputA.text);
				var penyebut:int = int(inputB.text);
				var bool:Boolean = (penyebut === 0 || penyebut > 100 || pembilang > penyebut);

				if(!bool)
				{
				pieChart = new PieChart(pembilang, penyebut);
				pieChart.y = 1.5 * yPosition;
				pieChart.x = (lgs.width - pieChart.width) / 2;
					lgs.addChild(pieChart);
					return;
				}
				
				if(penyebut === 0)
				{
					str = "b tidak boleh 0.";
				}
				else if(penyebut > 100)
				{
					str = "maaf, b hanya boleh diisi bilangan 1 sampai 100.";
				}
				else if(pembilang > penyebut)
				{
					str = "maaf, hanya pecahan dengan pembilang lebih kecil dari penyebut yang diperbolehkan.";
				}
				else
				{
					str = "maaf, tidak memenuhi.";
				}
				
				keterangan = new Label();
				keterangan.styleProvider = null;
				keterangan.text = str;
				keterangan.y = yPosition;
				keterangan.fontStyles = textFormat1;
				keterangan.width = stage.stageWidth - 26;
				keterangan.paddingLeft = keterangan.paddingRight = 20;
				keterangan.textRendererProperties.wordWrap = true;
				keterangan.layoutData = new VerticalLayoutData(100, NaN);
				lgs.addChild(keterangan);
			}
		}
		
		private function createLayoutGroupWithAnchorLayout():LayoutGroup
		{
			var lgs:LayoutGroup = new LayoutGroup();
			lgs.layout = new AnchorLayout();
			lgs.layoutData = new VerticalLayoutData(100, NaN);
			return lgs;
		}
		
		private function createLabel(text:String):Label
		{
			var label:Label = new Label();
			label.text = text;
			
			label.paddingLeft = label.paddingRight = 20;
			label.textRendererProperties.wordWrap = true;
			label.layoutData = new VerticalLayoutData(100, NaN);
			return label;
		}
	}
}