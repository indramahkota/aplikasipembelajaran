package simulasi
{
	import feathers.controls.Label;
	import feathers.controls.LayoutGroup;
	import feathers.controls.TextInput;
	import feathers.layout.AnchorLayout;
	import feathers.layout.AnchorLayoutData;
	import feathers.layout.VerticalLayout;
	import feathers.layout.VerticalLayoutData;
	import feathers.skins.ImageSkin;

	import flash.geom.Matrix;
	import flash.geom.Rectangle;

	import starling.display.Image;
	import starling.display.Quad;
	import starling.events.Event;
	import starling.text.TextFormat;
	import starling.textures.RenderTexture;
	import starling.textures.Texture;

	import utils.TextJustification;
	
	/**
	 * ...
	 * @author Indra Mahkota
	 */
	public class SimPecBiaskeCamp extends LayoutGroup
	{
		private var lgs:LayoutGroup;
		
		private var inputA:TextInput;
		private var inputB:TextInput;

		private var text1:TextJustification;
		private var text2:TextJustification;
		private var text3:TextJustification;
		
		private var Yposition:Number;
		private var ControlHeigh:Number;
		
		private static var fixed_height:Number;
		private var defaultTexture:Texture;
		
		private var textFormat:TextFormat = new TextFormat("Calibri", 16);
		
		public function SimPecBiaskeCamp()
		{
			super();
		}
		
		override protected function initialize():void
		{
			var layout:VerticalLayout = new VerticalLayout();
			layout.paddingTop = 20;
			layout.paddingLeft = 10;
			layout.paddingRight = 10;
			layout.gap = 10;
			this.layout = layout;

			defaultTexture = inputBackgroundSkin(0xd2d2d2);

			var bgInputA:ImageSkin = new ImageSkin(defaultTexture);
			bgInputA.scale9Grid = new Rectangle(2, 2, 46, 46);

			var image:Image = new Image(Main.roundQuadBackground);
			image.pixelSnapping = true;
			image.scale9Grid = new Rectangle(20, 20, 80, 80);
			this.backgroundSkin = image;

			this.minHeight = stage.stageHeight - 85;
			this.width = stage.stageWidth;
			
			//var judul:Label = createLabel("Masukkan bilangan a dan b dengan syarat a lebih besar dari b dan b ≠ 0 untuk mendapatkan bentuk pecahan campuran.");
			var judul:Label = createLabel("Mendapatkan bentuk pecahan campuran.");
			this.addChild(judul);
			
			lgs = createLayoutGroupWithAnchorLayout();
			this.addChild(lgs);
			
			inputA = new TextInput();
			inputA.prompt = "a";
			inputA.maxChars = 3;
			inputA.restrict = "0-9";
			inputA.fontStyles = textFormat;
			inputA.promptFontStyles = textFormat;
			inputA.width = 100;
			inputA.backgroundSkin = bgInputA;
			inputA.layoutData = new AnchorLayoutData(0, NaN, NaN, NaN, 0, NaN);
			inputA.styleNameList.add("text-input-number");
			lgs.addChild(inputA);
			
			inputA.validate();
			var inputAHeight1:Number = inputA.height;
			
			inputB = new TextInput();
			inputB.prompt = "b";
			inputB.maxChars = 3;
			inputB.restrict = "0-9";
			inputB.fontStyles = textFormat;
			inputB.promptFontStyles = textFormat;
			inputB.width = 100;
			inputB.layoutData = new AnchorLayoutData(inputAHeight1, NaN, NaN, NaN, 0, NaN);
			inputB.styleNameList.add("text-input-number");
			lgs.addChild(inputB);
			
			inputA.addEventListener(Event.CHANGE, selanjutnya);
			inputB.addEventListener(Event.CHANGE, selanjutnya);
			
			Yposition = (2 * inputAHeight1 + 10);
		}
		
		override public function dispose():void
		{
			defaultTexture.dispose();
			super.dispose();
		}

		private function inputBackgroundSkin(color:uint):Texture
		{
			var quad:Quad = new Quad(50, 2, color);
			var matrix:Matrix = new Matrix();
			matrix.translate(0, 48);

			var rendertxture:RenderTexture = new RenderTexture(50, 50);
			rendertxture.draw(quad, matrix);
			return rendertxture;
		}
				
		private function selanjutnya():void
		{
			if(lgs.contains(text1)) text1.removeFromParent(true);
			if(lgs.contains(text2)) text2.removeFromParent(true);
			if(lgs.contains(text3)) text3.removeFromParent(true);
			
			if(inputA.text.length > 0 && inputB.text.length > 0)
			{
				var a:int = int(inputA.text);
				var b:int = int(inputB.text);
				var c:int;
				var d:int;
				var e:int;
				var fpb:int = 1;
				
				for (var i:int = 1; i <= a && i <= b; ++i)
				{
					if(a % i == 0 && b % i == 0)
					{
						fpb = i;
					}
				}
				
				var str1:String;
				var str2:String;
				
				if(a == 0 && b == 0)
				{
					text1 = new TextJustification("a dan b tidak boleh 0.", stage.stageWidth - 60);
					text1.layoutData = new AnchorLayoutData(Yposition, NaN, NaN, NaN, 0, NaN);
					lgs.addChild(text1);
				}
				else if(b == 0)
				{
					text1 = new TextJustification("b tidak boleh 0.", stage.stageWidth - 60);
					text1.layoutData = new AnchorLayoutData(Yposition, NaN, NaN, NaN, 0, NaN);
					lgs.addChild(text1);
				}
				else if(a > b)
				{
					if(a % b == 0)
					{
						str1 = "pec(" + a + "/" + b + ") = " + String(a / b) + ".";
						
						text1 = new TextJustification(str1, stage.stageWidth - 60);
						text1.layoutData = new AnchorLayoutData(Yposition, NaN, NaN, NaN, 0, NaN);
						lgs.addChild(text1);
					}
					else
					{
						if(fpb != 1)
						{
							var p:int = a / fpb;
							var q:int = b / fpb;
							
							c = p % q;
							d = p - c;
							e = d / q;
							
							str1 = "pec(" + a + "/" + b + ") = pec(" + a + "_:_" + fpb + "/" + b + "_:_" + fpb + ") = pec(" + p + "/" + q + ")";
							
							text1 = new TextJustification(str1, stage.stageWidth - 60);
							text1.layoutData = new AnchorLayoutData(Yposition, NaN, NaN, NaN, 0, NaN);
							lgs.addChild(text1);

							text1.validate();
							ControlHeigh = text1.height;

							text2 = new TextJustification(p + " : " + q + " = " + e + " bersisa " + c + ".", stage.stageWidth - 60);
							text2.layoutData = new AnchorLayoutData(Yposition + ControlHeigh + 10, NaN, NaN, NaN, 0, NaN);
							lgs.addChild(text2);

							text2.validate();
							ControlHeigh += text2.height;
							
							str2 = "Hasil: " + e + " pec(" + c + "/" + q + ")";
							
							text3 = new TextJustification(str2, stage.stageWidth - 60);
							text3.layoutData = new AnchorLayoutData(Yposition + (ControlHeigh + 10), NaN, NaN, NaN, 0, NaN);
							lgs.addChild(text3);

							/* str1 = "pec(" + a + "/" + b + ") = pec(" + a + "_:_" + fpb + "/" + b + "_:_" + fpb + ") = pec(" + p + "/" + q + ") = pec(" + d + "/" + q + ") + pec(" + c + "/" + q + ") = " + e + " + pec(" + c + "/" + q + ") = " + e + " pec(" + c + "/" + q + ")";
							
							text1 = new TextJustification(str1, stage.stageWidth - 60);
							text1.layoutData = new AnchorLayoutData(Yposition, NaN, NaN, NaN, 0, NaN);
							text1.width = stage.stageWidth - 60;
							lgs.addChild(text1);
							
							text1.validate();
							ControlHeigh = text1.height;
							
							text2 = new TextJustification("atau", stage.stageWidth - 60);
							text2.layoutData = new AnchorLayoutData(Yposition + ControlHeigh + 4, NaN, NaN, NaN, 0, NaN);
							text2.width = stage.stageWidth - 60;
							lgs.addChild(text2);
							
							text2.validate();
							ControlHeigh += text2.height;
							
							str2 = "pec(" + a + "/" + b + ") = pec(" + a + "_:_" + fpb + "/" + b + "_:_" + fpb + ") = pec(" + p + "/" + q + ") = " + p + " : " + q + " = " + e + " bersisa " + c + " = " + e + " pec(" + c + "/" + q + ")";
							
							text3 = new TextJustification(str2, stage.stageWidth - 60);
							text3.layoutData = new AnchorLayoutData(Yposition + (ControlHeigh + 10), NaN, NaN, NaN, 0, NaN);
							text3.width = stage.stageWidth - 60;
							lgs.addChild(text3); */
						}
						else
						{
							c = a % b;
							d = a - c;
							e = d / b;
							
							str1 = a + " : " + b + " = " + e + " bersisa " + c + ".";
							
							text1 = new TextJustification(str1, stage.stageWidth - 60);
							text1.layoutData = new AnchorLayoutData(Yposition, NaN, NaN, NaN, 0, NaN);
							lgs.addChild(text1);

							text1.validate();
							ControlHeigh = text1.height;
							
							text2 = new TextJustification("Hasil: " + e + " pec(" + c + "/" + b + ")", stage.stageWidth - 60);
							text2.layoutData = new AnchorLayoutData(Yposition + ControlHeigh + 4, NaN, NaN, NaN, 0, NaN);
							lgs.addChild(text2);

							/* str1 = "pec(" + a + "/" + b + ") = pec(" + d + "/" + b + ") + pec(" + c + "/" + b + ") = " + e + " + pec(" + c + "/" + b + ") = " + e + " pec(" + c + "/" + b + ")";
							
							text1 = new TextJustification(str1, stage.stageWidth - 60);
							text1.layoutData = new AnchorLayoutData(Yposition, NaN, NaN, NaN, 0, NaN);
							lgs.addChild(text1);
							
							text1.validate();
							ControlHeigh = text1.height;
							
							text2 = new TextJustification("atau", stage.stageWidth - 60);
							text2.layoutData = new AnchorLayoutData(Yposition + ControlHeigh + 4, NaN, NaN, NaN, 0, NaN);
							lgs.addChild(text2);
							
							text2.validate();
							ControlHeigh += text2.height;
							
							str2 = "pec(" + a + "/" + b + ") = " + a + " : " + b + " = " + e + " bersisa " + c + " = " + e + " pec(" + c + "/" + b + ")";
							
							text3 = new TextJustification(str2, stage.stageWidth - 60);
							text3.layoutData = new AnchorLayoutData(Yposition + (ControlHeigh + 10), NaN, NaN, NaN, 0, NaN);
							lgs.addChild(text3); */
						}
					}
				}
				else
				{
					text1 = new TextJustification("a harus lebih besar dari b.", stage.stageWidth - 60);
					text1.layoutData = new AnchorLayoutData(Yposition, NaN, NaN, NaN, 0, NaN);
					lgs.addChild(text1);
				}
			}
		}
		
		private function createLayoutGroupWithAnchorLayout():LayoutGroup
		{
			var lgs:LayoutGroup = new LayoutGroup();
			lgs.layout = new AnchorLayout();
			lgs.layoutData = new VerticalLayoutData(100, NaN);
			return lgs;
		}
		
		private function createLabel(text:String):Label
		{
			var label:Label = new Label();
			label.text = text;
			
			label.paddingLeft = label.paddingRight = 20;
			label.textRendererProperties.wordWrap = true;
			label.layoutData = new VerticalLayoutData(100, NaN);
			return label;
		}
	}
}