<?php
	
	require_once('db.php');

    mysqli_query($conn, "DROP TABLE IF EXISTS pengguna");

    mysqli_query($conn, "CREATE TABLE IF NOT EXISTS pengguna (id int(11) NOT NULL auto_increment, nama varchar(255) NOT NULL, nilai int(11) NOT NULL DEFAULT 0, visual int(11) NOT NULL DEFAULT 0, auditorial int(11) NOT NULL DEFAULT 0, kinestetik int(11) NOT NULL DEFAULT 0, gaya_belajar varchar(255) NOT NULL DEFAULT 'belum tersedia', avatar varchar(255) NOT NULL DEFAULT 'avatar_1s', PRIMARY KEY(id)) ENGINE=MyISAM DEFAULT CHARSET=utf8");

    mysqli_close($conn);
?>