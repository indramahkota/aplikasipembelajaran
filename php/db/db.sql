-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 28, 2018 at 05:46 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `indy1339_pecahan`
--

-- --------------------------------------------------------

--
-- Table structure for table `gayabelajar`
--

CREATE TABLE `gayabelajar` (
  `nPGayaBelajar` varchar(50) NOT NULL,
  `visual` varchar(8) NOT NULL DEFAULT 'kosong',
  `auditorial` varchar(8) NOT NULL DEFAULT 'kosong',
  `kinestetik` varchar(8) NOT NULL DEFAULT 'kosong',
  `dominan` varchar(25) NOT NULL DEFAULT 'kosong',
  `waktu` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gayabelajar`
--

INSERT INTO `gayabelajar` (`nPGayaBelajar`, `visual`, `auditorial`, `kinestetik`, `dominan`, `waktu`) VALUES
('indramahkota', '42.86', '56.67', '40.74', 'auditorial', '2018-04-21 03:37:49'),
('indramahkota', '38.10', '36.67', '55.56', 'kinestetik', '2018-04-22 09:45:39'),
('indramahkota', '95.24', '63.33', '66.67', 'visual', '2018-05-10 16:07:56'),
('indramahkota', '100.00', '66.67', '62.96', 'visual', '2018-05-12 04:42:13'),
('indramahkota', '52.38', '60.00', '55.56', 'auditorial', '2018-09-07 23:59:26'),
('begeg', '61.90', '56.67', '44.44', 'visual', '2018-09-18 23:31:52'),
('kajananstudios', '80.95', '46.67', '62.96', 'visual', '2018-10-27 09:35:10');

-- --------------------------------------------------------

--
-- Table structure for table `nilai`
--

CREATE TABLE `nilai` (
  `nPNilai` varchar(50) NOT NULL,
  `nilai` varchar(25) NOT NULL DEFAULT 'kosong',
  `waktu` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nilai`
--

INSERT INTO `nilai` (`nPNilai`, `nilai`, `waktu`) VALUES
('indramahkota', '60', '2018-04-21 03:35:01'),
('indramahkota', '30', '2018-04-21 04:00:38'),
('indramahkota', '40', '2018-04-21 04:18:58'),
('indramahkota', '20', '2018-04-21 05:09:31'),
('indramahkota', '20', '2018-04-21 05:25:52'),
('indramahkota', '40', '2018-04-21 05:29:52'),
('indramahkota', '0', '2018-04-21 05:30:56'),
('indramahkota', '30', '2018-04-21 05:33:39'),
('indramahkota', '20', '2018-04-21 05:43:15'),
('indramahkota', '20', '2018-04-21 05:43:46'),
('indramahkota', '20', '2018-04-21 05:47:25'),
('indramahkota', '20', '2018-04-21 06:24:49'),
('indramahkota', '40', '2018-04-21 06:28:06'),
('indramahkota', '40', '2018-04-21 06:55:07'),
('indramahkota', '20', '2018-04-21 07:12:17'),
('indramahkota', '30', '2018-04-21 07:18:45'),
('indramahkota', '20', '2018-04-21 07:41:54'),
('indramahkota', '10', '2018-04-21 07:45:15'),
('indramahkota', '10', '2018-04-21 07:46:12'),
('indramahkota', '20', '2018-04-21 08:32:37'),
('indramahkota', '50', '2018-04-21 08:32:59'),
('indramahkota', '30', '2018-04-21 09:22:35'),
('indramahkota', '20', '2018-04-22 02:18:06'),
('indramahkota', '20', '2018-04-22 02:18:39'),
('indramahkota', '20', '2018-04-22 02:18:55'),
('indramahkota', '20', '2018-04-22 02:26:21'),
('indramahkota', '0', '2018-04-22 02:27:13'),
('indramahkota', '30', '2018-04-22 02:27:51'),
('indramahkota', '30', '2018-04-22 02:28:21'),
('indramahkota', '20', '2018-04-22 02:52:02'),
('indramahkota', '20', '2018-04-22 03:07:23'),
('indramahkota', '30', '2018-04-22 03:08:02'),
('indramahkota', '20', '2018-04-22 03:08:49'),
('indramahkota', '0', '2018-04-22 03:09:33'),
('indramahkota', '20', '2018-04-22 03:10:35'),
('indramahkota', '20', '2018-04-22 03:15:08'),
('indramahkota', '50', '2018-04-22 03:15:38'),
('indramahkota', '20', '2018-04-22 03:21:43'),
('indramahkota', '30', '2018-04-22 03:29:46'),
('indramahkota', '10', '2018-04-22 03:30:11'),
('indramahkota', '40', '2018-04-22 03:30:30'),
('indramahkota', '30', '2018-04-22 03:31:26'),
('indramahkota', '30', '2018-04-22 03:32:15'),
('indramahkota', '30', '2018-04-22 03:32:42'),
('indramahkota', '0', '2018-04-22 03:33:16'),
('indramahkota', '60', '2018-04-22 04:17:27'),
('indramahkota', '50', '2018-04-22 09:46:25'),
('indramahkota', '30', '2018-04-22 14:36:57'),
('indramahkota', '40', '2018-04-22 14:37:46'),
('indramahkota', '50', '2018-04-22 14:38:01'),
('indramahkota', '50', '2018-04-22 14:38:30'),
('indramahkota', '30', '2018-04-22 15:07:58'),
('indramahkota', '30', '2018-04-22 15:08:30'),
('indramahkota', '50', '2018-04-23 03:22:40'),
('indramahkota', '20', '2018-04-24 09:31:10'),
('indramahkota', '30', '2018-04-24 09:51:03'),
('indramahkota', '0', '2018-04-24 10:16:00'),
('indramahkota', '20', '2018-04-24 11:16:23'),
('indramahkota', '20', '2018-04-25 01:21:13'),
('indramahkota', '20', '2018-04-25 01:25:29'),
('indramahkota', '20', '2018-04-25 01:35:09'),
('indramahkota', '20', '2018-04-25 01:35:35'),
('indramahkota', '30', '2018-04-25 03:39:19'),
('indramahkota', '30', '2018-04-25 09:28:00'),
('indramahkota', '0', '2018-04-25 10:53:48'),
('indramahkota', '30', '2018-04-25 12:10:01'),
('indramahkota', '30', '2018-04-25 12:18:33'),
('indramahkota', '50', '2018-04-25 12:24:20'),
('indramahkota', '30', '2018-04-25 12:25:34'),
('indramahkota', '50', '2018-04-25 12:27:08'),
('indramahkota', '30', '2018-04-25 12:47:04'),
('indramahkota', '30', '2018-04-25 23:00:39'),
('indramahkota', '20', '2018-04-28 05:53:45'),
('indramahkota', '50', '2018-05-05 15:59:11'),
('indramahkota', '30', '2018-05-06 07:38:50'),
('indramahkota', '30', '2018-05-06 07:39:11'),
('indramahkota', '30', '2018-05-06 07:39:46'),
('indramahkota', '20', '2018-05-08 07:17:14'),
('indramahkota', '30', '2018-05-08 07:17:32'),
('indramahkota', '20', '2018-05-08 07:21:29'),
('indramahkota', '30', '2018-05-08 07:23:04'),
('indramahkota', '30', '2018-05-08 07:24:24'),
('indramahkota', '20', '2018-05-08 07:53:30'),
('indramahkota', '30', '2018-05-08 08:53:09'),
('indramahkota', '30', '2018-05-08 09:11:26'),
('indramahkota', '10', '2018-05-08 09:11:48'),
('indramahkota', '20', '2018-05-08 09:12:16'),
('indramahkota', '20', '2018-05-08 09:12:42'),
('indramahkota', '30', '2018-05-08 09:13:12'),
('indramahkota', '30', '2018-05-08 09:13:27'),
('indramahkota', '20', '2018-05-08 09:13:56'),
('indramahkota', '30', '2018-05-08 09:14:33'),
('indramahkota', '30', '2018-05-08 09:14:59'),
('indramahkota', '30', '2018-05-08 09:15:16'),
('indramahkota', '20', '2018-05-08 09:17:45'),
('indramahkota', '20', '2018-05-08 09:18:03'),
('indramahkota', '20', '2018-05-08 09:21:47'),
('indramahkota', '30', '2018-05-08 09:22:10'),
('indramahkota', '30', '2018-05-08 09:22:25'),
('indramahkota', '10', '2018-05-08 09:23:39'),
('indramahkota', '30', '2018-05-08 22:42:34'),
('indramahkota', '30', '2018-05-08 22:49:09'),
('indramahkota', '40', '2018-05-08 22:56:18'),
('indramahkota', '30', '2018-05-08 22:57:24'),
('indramahkota', '20', '2018-05-08 22:57:42'),
('indramahkota', '50', '2018-05-08 22:58:15'),
('indramahkota', '30', '2018-05-08 23:01:41'),
('indramahkota', '30', '2018-05-08 23:10:44'),
('indramahkota', '30', '2018-05-08 23:11:11'),
('indramahkota', '30', '2018-05-08 23:11:43'),
('indramahkota', '30', '2018-05-08 23:12:45'),
('indramahkota', '30', '2018-05-08 23:13:44'),
('indramahkota', '40', '2018-05-08 23:13:58'),
('indramahkota', '20', '2018-05-08 23:14:25'),
('indramahkota', '30', '2018-05-08 23:15:16'),
('indramahkota', '30', '2018-05-08 23:15:40'),
('indramahkota', '30', '2018-05-08 23:16:40'),
('indramahkota', '50', '2018-05-08 23:27:43'),
('indramahkota', '30', '2018-05-08 23:28:00'),
('indramahkota', '30', '2018-05-09 02:32:32'),
('indramahkota', '30', '2018-05-09 02:32:49'),
('indramahkota', '30', '2018-05-09 02:33:11'),
('indramahkota', '40', '2018-05-09 02:33:50'),
('indramahkota', '20', '2018-05-09 03:47:09'),
('indramahkota', '30', '2018-05-09 05:33:57'),
('indramahkota', '30', '2018-05-09 05:34:27'),
('indramahkota', '30', '2018-05-09 05:35:46'),
('indramahkota', '30', '2018-05-09 05:41:01'),
('indramahkota', '50', '2018-05-09 05:51:06'),
('indramahkota', '50', '2018-05-09 05:51:45'),
('indramahkota', '30', '2018-05-09 05:53:53'),
('indramahkota', '30', '2018-05-10 06:06:00'),
('indramahkota', '20', '2018-05-10 06:10:28'),
('indramahkota', '30', '2018-05-10 06:12:11'),
('indramahkota', '30', '2018-05-10 07:14:09'),
('indramahkota', '30', '2018-05-10 07:14:45'),
('indramahkota', '30', '2018-05-10 07:16:11'),
('indramahkota', '30', '2018-05-10 07:18:09'),
('indramahkota', '30', '2018-05-10 07:20:41'),
('indramahkota', '30', '2018-05-10 07:35:51'),
('indramahkota', '30', '2018-05-10 07:54:29'),
('indramahkota', '30', '2018-05-10 08:01:27'),
('indramahkota', '20', '2018-05-10 08:44:48'),
('indramahkota', '30', '2018-05-10 08:45:10'),
('indramahkota', '30', '2018-05-10 09:08:03'),
('indramahkota', '30', '2018-05-10 09:08:32'),
('indramahkota', '30', '2018-05-10 10:09:03'),
('indramahkota', '30', '2018-05-10 10:09:55'),
('indramahkota', '30', '2018-05-10 10:10:46'),
('indramahkota', '30', '2018-05-10 10:11:42'),
('indramahkota', '30', '2018-05-10 10:12:39'),
('indramahkota', '30', '2018-05-10 10:13:44'),
('indramahkota', '30', '2018-05-10 10:20:11'),
('indramahkota', '30', '2018-05-10 15:17:52'),
('indramahkota', '30', '2018-05-11 09:59:04'),
('indramahkota', '20', '2018-05-12 05:10:41'),
('indramahkota', '50', '2018-05-14 04:35:14'),
('indramahkota', '30', '2018-05-14 06:09:51'),
('indramahkota', '30', '2018-05-15 06:41:01'),
('indramahkota', '30', '2018-05-21 03:59:27'),
('indramahkota', '40', '2018-05-21 03:59:41'),
('indramahkota', '30', '2018-05-21 23:26:31'),
('indramahkota', '20', '2018-05-22 14:40:36'),
('indramahkota', '50', '2018-05-24 06:12:40'),
('indramahkota', '20', '2018-05-25 03:43:47'),
('indramahkota', '30', '2018-05-26 17:48:07'),
('indramahkota', '40', '2018-05-26 18:14:20'),
('indramahkota', '30', '2018-06-04 07:06:36'),
('indramahkota', '41.67', '2018-06-05 05:34:51'),
('indramahkota', '25.00', '2018-06-05 05:44:39'),
('indramahkota', '60.00', '2018-06-05 08:38:16'),
('indramahkota', '60.00', '2018-06-05 08:40:07'),
('indramahkota', '60.00', '2018-06-05 08:40:23'),
('indramahkota', '42.86', '2018-06-05 13:08:34'),
('indramahkota', '71.43', '2018-06-22 10:52:33'),
('indramahkota', '33.33', '2018-06-29 04:31:48'),
('indramahkota', '66.67', '2018-06-29 04:32:21'),
('indramahkota', '50.00', '2018-06-29 04:32:34'),
('indramahkota', '33.33', '2018-06-29 04:32:42'),
('indramahkota', '66.67', '2018-06-29 04:32:50'),
('indramahkota', '33.33', '2018-06-29 04:32:57'),
('indramahkota', '0.00', '2018-06-29 04:33:04'),
('indramahkota', '33.33', '2018-06-29 04:33:12'),
('indramahkota', '16.67', '2018-06-29 04:33:20'),
('indramahkota', '16.67', '2018-06-29 04:33:26'),
('indramahkota', '50.00', '2018-06-29 08:15:44'),
('indramahkota', '16.67', '2018-06-29 08:15:48'),
('indramahkota', '16.67', '2018-06-29 10:15:21'),
('indramahkota', '33.33', '2018-06-29 10:15:35'),
('indramahkota', '16.67', '2018-06-29 11:17:04'),
('indramahkota', '33.33', '2018-06-30 09:14:24'),
('indramahkota', '33.33', '2018-06-30 09:14:31'),
('indramahkota', '20.00', '2018-07-04 08:47:32'),
('indramahkota', '33.33', '2018-07-05 08:30:42'),
('indramahkota', '26.67', '2018-07-05 08:33:07'),
('indramahkota', '33.33', '2018-07-05 14:58:14'),
('indramahkota', '13.33', '2018-07-14 12:14:25'),
('indramahkota', '33.33', '2018-07-16 03:55:06'),
('indramahkota', '40.00', '2018-07-17 12:32:23'),
('indramahkota', '13.33', '2018-07-17 12:32:34'),
('indramahkota', '33.33', '2018-07-20 12:24:50'),
('indramahkota', '46.67', '2018-07-21 12:34:05'),
('indramahkota', '0.00', '2018-07-21 12:34:09'),
('indramahkota', '0.00', '2018-07-21 12:34:17'),
('indramahkota', '33.33', '2018-07-21 12:40:06'),
('indramahkota', '33.33', '2018-07-21 12:40:20'),
('indramahkota', '33.33', '2018-07-21 12:40:36'),
('indramahkota', '33.33', '2018-07-21 12:55:33'),
('indramahkota', '40.00', '2018-07-21 13:02:59'),
('indramahkota', '46.67', '2018-07-21 13:03:14'),
('indramahkota', '26.67', '2018-07-24 13:05:19'),
('indramahkota', '20.00', '2018-07-24 13:30:04'),
('indramahkota', '26.67', '2018-07-25 12:45:31'),
('indramahkota', '20.00', '2018-07-25 14:51:22'),
('indramahkota', '33.33', '2018-07-30 11:27:47'),
('indramahkota', '26.67', '2018-07-30 14:02:28'),
('indramahkota', '33.33', '2018-08-01 12:35:42'),
('mahkota', '33.33', '2018-08-02 13:16:45'),
('mahkota', '20.00', '2018-08-02 13:18:26'),
('indramahkota', '33.33', '2018-08-02 13:22:28'),
('mahkota', '13.33', '2018-08-02 13:23:21'),
('mahkota', '46.67', '2018-08-02 13:27:37'),
('indramahkota', '40.00', '2018-08-02 13:45:07'),
('indramahkota', '40.00', '2018-08-02 14:05:22'),
('indramahkota', '53.33', '2018-08-02 14:11:01'),
('indramahkota', '40.00', '2018-08-02 14:15:00'),
('indramahkota', '33.33', '2018-08-02 14:15:30'),
('indramahkota', '40.00', '2018-08-02 14:18:05'),
('indramahkota', '40.00', '2018-08-02 14:21:08'),
('indramahkota', '26.67', '2018-08-02 14:22:24'),
('indramahkota', '33.33', '2018-08-02 14:25:02'),
('indramahkota', '40.00', '2018-08-02 14:31:24'),
('indramahkota', '40.00', '2018-08-02 14:35:13'),
('indramahkota', '40.00', '2018-08-02 14:44:43'),
('indramahkota', '33.33', '2018-08-02 14:48:13'),
('indramahkota', '40.00', '2018-08-02 14:51:33'),
('indramahkota', '40.00', '2018-08-02 14:52:38'),
('indramahkota', '40.00', '2018-08-02 14:55:36'),
('indramahkota', '40.00', '2018-08-02 14:59:41'),
('indramahkota', '40.00', '2018-08-02 15:02:53'),
('indramahkota', '40.00', '2018-08-02 15:04:53'),
('indramahkota', '40.00', '2018-08-02 15:07:49'),
('indramahkota', '40.00', '2018-08-02 15:13:53'),
('indramahkota', '40.00', '2018-08-02 15:21:29'),
('mahkota', '20.00', '2018-08-02 15:28:03'),
('mahkota', '40.00', '2018-08-02 15:28:25'),
('indramahkota', '40.00', '2018-08-02 15:31:30'),
('mahkota', '40.00', '2018-08-02 15:34:34'),
('indramahkota', '40.00', '2018-08-02 15:35:43'),
('indramahkota', '40.00', '2018-08-02 15:37:44'),
('indramahkota', '33.33', '2018-08-02 15:39:16'),
('indramahkota', '40.00', '2018-08-02 15:40:20'),
('mahkota', '20.00', '2018-08-02 15:49:20'),
('mahkota', '13.33', '2018-08-02 15:50:44'),
('mahkota', '20.00', '2018-08-02 15:51:37'),
('mahkota', '33.33', '2018-08-02 15:52:11'),
('mahkota', '26.67', '2018-08-02 15:52:30'),
('mahkota', '40.00', '2018-08-02 15:53:02'),
('mahkota', '40.00', '2018-08-02 15:53:17'),
('mahkota', '40.00', '2018-08-02 15:54:13'),
('mahkota', '33.33', '2018-08-02 15:54:54'),
('mahkota', '26.67', '2018-08-02 15:55:37'),
('mahkota', '46.67', '2018-08-02 15:56:01'),
('mahkota', '6.67', '2018-08-02 16:09:44'),
('mahkota', '26.67', '2018-08-02 16:10:38'),
('mahkota', '40.00', '2018-08-03 12:48:53'),
('mahkota', '26.67', '2018-08-03 12:49:17'),
('mahkota', '20.00', '2018-08-04 06:24:29'),
('mahkota', '33.33', '2018-08-04 06:24:51'),
('mahkota', '20.00', '2018-08-04 06:33:27'),
('mahkota', '33.33', '2018-08-04 06:33:49'),
('mahkota', '33.33', '2018-08-04 06:34:06'),
('mahkota', '33.33', '2018-08-04 06:34:25'),
('mahkota', '33.33', '2018-08-04 06:34:45'),
('mahkota', '26.67', '2018-08-04 06:42:44'),
('mahkota', '20.00', '2018-08-04 06:43:02'),
('mahkota', '33.33', '2018-08-04 06:48:47'),
('mahkota', '26.67', '2018-08-04 13:27:31'),
('mahkota', '33.33', '2018-08-04 13:28:27'),
('mahkota', '6.67', '2018-08-04 13:33:18'),
('mahkota', '33.33', '2018-08-04 13:33:34'),
('mahkota', '13.33', '2018-08-04 13:33:46'),
('mahkota', '20.00', '2018-08-04 13:34:39'),
('mahkota', '40.00', '2018-08-04 13:49:32'),
('mahkota', '20.00', '2018-08-04 13:49:54'),
('mahkota', '13.33', '2018-08-04 13:50:10'),
('mahkota', '33.33', '2018-08-04 13:50:42'),
('mahkota', '33.33', '2018-08-04 13:55:27'),
('mahkota', '40.00', '2018-08-04 13:55:48'),
('mahkota', '26.67', '2018-08-04 13:56:10'),
('mahkota', '26.67', '2018-08-04 13:58:12'),
('mahkota', '33.33', '2018-08-04 14:18:45'),
('mahkota', '40.00', '2018-08-04 14:21:03'),
('mahkota', '20.00', '2018-08-04 14:25:17'),
('mahkota', '40.00', '2018-08-04 14:25:41'),
('mahkota', '26.67', '2018-08-04 14:26:54'),
('mahkota', '20.00', '2018-08-04 14:29:04'),
('mahkota', '33.33', '2018-08-04 14:29:20'),
('mahkota', '40.00', '2018-08-04 14:34:59'),
('mahkota', '26.67', '2018-08-04 14:35:34'),
('mahkota', '26.67', '2018-08-04 14:45:02'),
('mahkota', '46.67', '2018-08-04 14:50:34'),
('mahkota', '40.00', '2018-08-04 14:53:12'),
('mahkota', '13.33', '2018-08-04 14:53:28'),
('mahkota', '20.00', '2018-08-04 14:53:51'),
('mahkota', '13.33', '2018-08-04 14:54:10'),
('mahkota', '13.33', '2018-08-04 14:54:28'),
('mahkota', '13.33', '2018-08-04 14:54:50'),
('mahkota', '13.33', '2018-08-04 14:55:07'),
('mahkota', '13.33', '2018-08-04 14:55:31'),
('mahkota', '13.33', '2018-08-04 14:55:47'),
('mahkota', '33.33', '2018-08-04 14:56:02'),
('mahkota', '33.33', '2018-08-04 14:56:16'),
('mahkota', '53.33', '2018-08-04 14:59:33'),
('mahkota', '13.33', '2018-08-04 14:59:48'),
('mahkota', '40.00', '2018-08-04 15:00:41'),
('indramahkota', '40.00', '2018-08-04 15:04:47'),
('indramahkota', '26.67', '2018-08-04 15:05:12'),
('indramahkota', '20.00', '2018-08-04 15:06:13'),
('indramahkota', '40.00', '2018-08-04 15:27:35'),
('indramahkota', '33.33', '2018-08-04 15:28:00'),
('mahkota', '40.00', '2018-08-04 15:46:25'),
('mahkota', '13.33', '2018-08-04 16:07:57'),
('mahkota', '40.00', '2018-08-04 16:09:29'),
('mahkota', '40.00', '2018-08-04 16:09:45'),
('mahkota', '40.00', '2018-08-04 16:28:04'),
('mahkota', '40.00', '2018-08-04 16:29:03'),
('mahkota', '13.33', '2018-08-04 16:29:23'),
('mahkota', '33.33', '2018-08-04 16:29:36'),
('mahkota', '26.67', '2018-08-04 21:55:58'),
('mahkota', '40.00', '2018-08-04 21:57:03'),
('indramahkota', '13.33', '2018-08-04 22:44:12'),
('indramahkota', '46.67', '2018-08-04 22:44:34'),
('indramahkota', '26.67', '2018-08-04 22:44:51'),
('indramahkota', '33.33', '2018-08-04 23:05:59'),
('indramahkota', '40.00', '2018-08-04 23:06:28'),
('indramahkota', '33.33', '2018-08-04 23:06:53'),
('indramahkota', '40.00', '2018-08-04 23:07:17'),
('indramahkota', '33.33', '2018-08-04 23:07:40'),
('indramahkota', '33.33', '2018-08-04 23:08:31'),
('mahkota', '46.67', '2018-08-05 11:21:39'),
('indramahkota', '40.00', '2018-08-05 12:55:57'),
('indramahkota', '100.00', '2018-08-08 10:27:33'),
('mahkota', '40.00', '2018-08-08 10:30:41'),
('mahkota', '40.00', '2018-08-08 10:33:26'),
('mahkota', '40.00', '2018-08-08 10:35:32'),
('mahkota', '46.67', '2018-08-11 12:00:14'),
('indramahkota', '53,33', '2018-08-15 11:35:36'),
('indramahkota', '20', '2018-08-25 13:02:03'),
('indramahkota', '40', '2018-08-28 08:17:48'),
('indramahkota', '46,67', '2018-08-28 08:18:31'),
('indramahkota', '20', '2018-08-31 08:33:43'),
('indramahkota', '40', '2018-08-31 11:53:28'),
('indramahkota', '33,33', '2018-08-31 11:55:11'),
('indramahkota', '33,33', '2018-08-31 11:56:06'),
('indramahkota', '33,33', '2018-08-31 11:56:32'),
('indramahkota', '33,33', '2018-09-08 00:01:03'),
('indramahkota', '33,33', '2018-09-09 01:01:37'),
('mahkota', '20', '2018-09-09 01:39:51'),
('indramahkota', '33,33', '2018-09-09 07:57:04'),
('indramahkota', '40', '2018-09-10 02:53:10'),
('indramahkota', '13,33', '2018-09-10 02:53:29'),
('indramahkota', '40', '2018-09-10 02:54:23'),
('indramahkota', '13,33', '2018-09-12 05:56:26'),
('indramahkota', '33,33', '2018-09-12 23:54:44'),
('indramahkota', '33,33', '2018-09-13 01:01:24'),
('indramahkota', '13,33', '2018-09-13 01:20:58'),
('indramahkota', '40', '2018-09-13 01:27:39'),
('indramahkota', '40', '2018-09-13 01:31:25'),
('indramahkota', '33,33', '2018-09-13 01:33:55'),
('indramahkota', '20', '2018-09-13 01:47:23'),
('indramahkota', '40', '2018-09-13 03:10:32'),
('indramahkota', '40', '2018-09-13 03:28:16'),
('indramahkota', '46,67', '2018-09-13 03:45:57'),
('acokiller', '26,67', '2018-09-14 01:28:48'),
('indramahkota', '33,33', '2018-09-14 02:35:06'),
('mahkota', '73,33', '2018-09-14 02:47:20'),
('sidulgajzjsj', '40', '2018-09-14 10:46:40'),
('indramahkota', '66,67', '2018-09-14 11:47:25'),
('bejoku', '40', '2018-09-14 23:59:24'),
('indramahkota', '46,67', '2018-09-15 00:54:52'),
('indramahkota', '33,33', '2018-09-16 09:57:34'),
('indramahkota', '33,33', '2018-09-22 01:54:12'),
('indramahkota', '86,67', '2018-09-23 23:50:12'),
('indramahkota', '40', '2018-09-24 01:11:15'),
('indramahkota', '33,33', '2018-09-24 02:31:43'),
('indramahkota', '40', '2018-09-24 02:42:03'),
('indramahkota', '40', '2018-09-24 05:53:42'),
('indramahkota', '40', '2018-09-24 05:54:07'),
('indramahkota', '6,67', '2018-10-11 04:29:55'),
('indramahkota', '20', '2018-10-13 15:50:00');

-- --------------------------------------------------------

--
-- Table structure for table `pengguna`
--

CREATE TABLE `pengguna` (
  `namaPengguna` varchar(50) NOT NULL,
  `kataSandi` varchar(255) NOT NULL,
  `namaLengkap` varchar(50) NOT NULL,
  `visual` varchar(8) NOT NULL DEFAULT 'kosong',
  `auditorial` varchar(8) NOT NULL DEFAULT 'kosong',
  `kinestetik` varchar(8) NOT NULL DEFAULT 'kosong',
  `dominan` varchar(25) NOT NULL DEFAULT 'kosong',
  `nilai` varchar(25) NOT NULL DEFAULT 'kosong',
  `avatarPengguna` varchar(25) NOT NULL DEFAULT 'avatar_1s',
  `indeksAvatar` varchar(8) NOT NULL DEFAULT '0',
  `waktu` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(10) NOT NULL DEFAULT 'aktif'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengguna`
--

INSERT INTO `pengguna` (`namaPengguna`, `kataSandi`, `namaLengkap`, `visual`, `auditorial`, `kinestetik`, `dominan`, `nilai`, `avatarPengguna`, `indeksAvatar`, `waktu`, `status`) VALUES
('acokiller', '$2y$10$dli3dXhYZLC99O4Rao13De2/XjreOxEaDi96E9I/K8C4yNHQ/CVuS', 'aco', 'kosong', 'kosong', 'kosong', 'kosong', '26,67', 'avatar_1s', '0', '2018-09-14 01:27:31', 'aktif'),
('adisheza', '$2y$10$eSO8aMX/MvUfcD4F1xHRCOQCth/Q1AHr35wQO1yYazRC2rD.UQbvS', 'adi sheza', 'kosong', 'kosong', 'kosong', 'kosong', 'kosong', 'avatar_1s', '0', '2018-09-13 14:34:05', 'aktif'),
('asas', '$2y$10$ZvV6rbxj0Cy3yRszE8qnlu0zIbx98W7nLFsefaNaB0j3sLKJ1wjzO', 'asas', 'kosong', 'kosong', 'kosong', 'kosong', 'kosong', 'avatar_2s', '1', '2018-11-25 09:48:51', 'aktif'),
('begeg', '$2y$10$IC/wSAtt80Zmi5wu9VJcNu56gPaUIHN.SWdvk5sA7ktLZY.PSSIUa', 'Begeg', '61.90', '56.67', '44.44', 'visual', 'kosong', 'avatar_4s', '3', '2018-09-18 23:28:05', 'aktif'),
('bejoku', '$2y$10$SIw4PYp.Yb16oRjFE4w8fuVwp4sd2hfjhdoyVIpGFu6rL2k/YAhF6', 'bejo', 'kosong', 'kosong', 'kosong', 'kosong', '40', 'avatar_1s', '0', '2018-09-14 23:57:06', 'aktif'),
('dedy', '$2y$10$NTSUi4kWqOXYlZ3MPnDqxujCzBI7fiBcoPMkuX.SNJccmuRCQ.vhm', 'dedy darmanto', 'kosong', 'kosong', 'kosong', 'kosong', 'kosong', 'avatar_1s', '0', '2018-09-14 05:22:10', 'aktif'),
('free', '$2y$10$mn4rrjEOyqLl8WhYPIedA.g7YB9qPPvqVw410oMD5wSd3oou8awra', 'free', 'kosong', 'kosong', 'kosong', 'kosong', 'kosong', 'avatar_2s', '1', '2018-11-22 12:06:05', 'aktif'),
('ilyas', '$2y$10$rt8y485wN.6BNarCwtfrWulg2KnCxQyX56PYkDVMsldVZtMDKLkXG', 'ilyas', 'kosong', 'kosong', 'kosong', 'kosong', 'kosong', 'avatar_2s', '1', '2018-11-08 09:55:00', 'aktif'),
('indramahkota', '$2y$10$2PUB/mb3JeEKJpcEmSb5Iu69MMuwzJYiuDWZaU6Rni3p/7FASlWIy', 'Indra Mahkota', '52.38', '60.00', '55.56', 'auditorial', '20', 'avatar_1s', '0', '2018-04-21 03:34:11', 'non-aktif'),
('jddj', '$2y$10$fuGVpu1fKHz.Y/r55LHK6.LaF9xGoDQHXQ2SBTA5AwpSZoNIBuWfW', 'jsjssj', 'kosong', 'kosong', 'kosong', 'kosong', 'kosong', 'avatar_2s', '1', '2018-11-11 12:35:15', 'aktif'),
('kadit', '$2y$10$G9NiYoAjP.mUqC0swLsYG.Ts04DTEhz2GRXq5hyXrQalHNmT82zNi', 'kadit', 'kosong', 'kosong', 'kosong', 'kosong', 'kosong', 'avatar_2s', '1', '2018-11-05 11:14:25', 'aktif'),
('kajananstudios', '$2y$10$nSak5FEwNtIQTJl0LNFQneGTNt38Th6Zd9obQ14NLf4x.zkvhDozu', 'idit aki', '80.95', '46.67', '62.96', 'visual', 'kosong', 'avatar_7s', '6', '2018-10-27 09:28:11', 'aktif'),
('kimi', '$2y$10$rMQl1upqxqz6.HrNdHtHPucNd7pqviJ9oP83wyOkZkCTcQYnhLLP.', 'kimi', 'kosong', 'kosong', 'kosong', 'kosong', 'kosong', 'avatar_7s', '6', '2018-11-25 10:44:52', 'aktif'),
('kokom', '$2y$10$EWF672MCXbKQLPGHBHgnBe4Wf8pNnkwFL1G1flTXvX8WRUUPNBuke', 'Kokom Malay', 'kosong', 'kosong', 'kosong', 'kosong', 'kosong', 'avatar_2s', '1', '2018-11-08 10:41:24', 'aktif'),
('kui', '$2y$10$zY/ODK7B9EKQtcFxW62XzufMu/I9XD80hsgJZ5cJLyK7OcCRTwCRq', 'kui', 'kosong', 'kosong', 'kosong', 'kosong', 'kosong', 'avatar_2s', '1', '2018-11-27 10:32:08', 'aktif'),
('mahkota', '$2y$10$UL7ywDOaq8KToOQTakXHpOOUX.gro.TDjNoTfCesUdbDPZEcv.RQK', 'Mahkota', 'kosong', 'kosong', 'kosong', 'kosong', '73,33', 'avatar_4s', '3', '2018-06-05 14:06:32', 'non-aktif'),
('nebula', '$2y$10$VaoWdcbmvEdJayThBU033uNpmBhUP2KQNsapis.LZxESJjUAfK7OK', 'nebula', 'kosong', 'kosong', 'kosong', 'kosong', 'kosong', 'avatar_2s', '1', '2018-11-21 05:29:37', 'aktif'),
('sidulgajzjsj', '$2y$10$uYemhxXQdBjffPvYaLkigu30TvyiH66CWSueCfBR7QjulYLdvO2p.', 'bejo sidul', 'kosong', 'kosong', 'kosong', 'kosong', '40', 'avatar_1s', '0', '2018-09-14 10:45:46', 'aktif'),
('tik', '$2y$10$WyKakuK8UH4JHqsGE6sq3.iMtCiGlIH.D63FMFkMTJkE8YbphAvP.', 'tik', 'kosong', 'kosong', 'kosong', 'kosong', 'kosong', 'avatar_2s', '1', '2018-11-27 10:33:59', 'aktif');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `gayabelajar`
--
ALTER TABLE `gayabelajar`
  ADD KEY `FK_npgayabelajar` (`nPGayaBelajar`);

--
-- Indexes for table `nilai`
--
ALTER TABLE `nilai`
  ADD KEY `FK_npnilai` (`nPNilai`);

--
-- Indexes for table `pengguna`
--
ALTER TABLE `pengguna`
  ADD PRIMARY KEY (`namaPengguna`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `gayabelajar`
--
ALTER TABLE `gayabelajar`
  ADD CONSTRAINT `FK_npgayabelajar` FOREIGN KEY (`nPGayaBelajar`) REFERENCES `pengguna` (`namaPengguna`);

--
-- Constraints for table `nilai`
--
ALTER TABLE `nilai`
  ADD CONSTRAINT `FK_npnilai` FOREIGN KEY (`nPNilai`) REFERENCES `pengguna` (`namaPengguna`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
