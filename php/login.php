<?php
	
	require_once('db.php');

    $namaPengguna = isset($_POST['nama_pengguna']) ? $_POST['nama_pengguna'] : '';
    $kataSandi = isset($_POST['kata_sandi']) ? $_POST['kata_sandi'] : '';
	
	$query = mysqli_query($conn, "SELECT * FROM pengguna WHERE namaPengguna = '$namaPengguna'");
	$user = mysqli_fetch_array ($query);
	
	if ($user)
	{
		if (password_verify($kataSandi, $user["kataSandi"]))
		{
			$result = array();
			
			array_push($result, array(
					'namaPengguna'     => $user['namaPengguna'],
					'kataSandi'        => $user['kataSandi'],
					'namaLengkap'      => ucwords($user['namaLengkap']),
					'visual'           => $user['visual'],
					'auditorial'       => $user['auditorial'],
					'kinestetik'       => $user['kinestetik'],
					'gaya_belajar'     => $user['dominan'],
					'nilai'        	   => $user['nilai'],
					'avatar'       	   => $user['avatarPengguna'],
					'index_avatar'     => $user['indeksAvatar']
				));
			
			echo json_encode(array('result' => $result));
			
		}
		else
		{
			echo "error";
		}
	}
	else
	{
		echo "error";
	}
	
    mysqli_close($conn);
    
?>