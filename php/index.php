<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Pecahan</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  </head>
    
    <body>
        <section id="isiTabel">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-sm-12">
                        <h2 class="text-center m-5">ISI DATABASE</h2>
                    </div>
                </div>

                <div class="row justify-content-center">
                    <div class="col-sm-12">
                        <table class="table">
                            <thead class="thead-light">
                                <tr>
                                    <th>No</th>
                                    <th>Nama Lengkap</th>
                                    <th>Nama Pengguna</th>
                                    <th>Avatar</th>
                                    <th>Nilai</th>
                                    <th>Visual</th>
                                    <th>Auditorial</th>
                                    <th>Kinestetik</th>
                                    <th>Gaya Belajar</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php
                                require_once('db.php');
                                $query = mysqli_query($conn, "SELECT * FROM pengguna ORDER BY nilai DESC");
                                $result = array();
                                $a = 1;
                                while($row = mysqli_fetch_array($query))
                                {
                                    print "<tr>";
                                        print "<td>" . $a . '.' ."</td>";
                                        print "<td>" . ucwords($row['namaLengkap']) . "</td>";
                                        print "<td>" . $row['namaPengguna'] . "</td>";
                                        print "<td> <img src= images/avatar/" . $row['avatarPengguna'] . ".png height='42' width='42'></td>";
                                        print "<td>" . $row['nilai'] . "</td>";
                                        print "<td>" . $row['visual'] . "</td>";
                                        print "<td>" . $row['auditorial'] . "</td>";
                                        print "<td>" . $row['kinestetik'] . "</td>";
                                        print "<td>" . ucwords($row['dominan']) . "</td>";
                                    print "</tr>";
                                    $a++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
  
    </body>
    
</html>