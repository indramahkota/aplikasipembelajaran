<?php 

	require_once('db.php');

	$query = mysqli_query($conn, "SELECT * FROM pengguna ORDER BY namaLengkap ASC");
	
	if ($query)
	{
		$result = array();
		
		while($row = mysqli_fetch_array($query))
		{
			array_push($result, array(
				'nama'          => ucwords($row['namaLengkap']),
				'nilai'         => $row['nilai'],
				'avatar'        => $row['avatarPengguna']
			));
		}
		
		echo json_encode(array('result'=>$result));
	}
	else
	{
		die('Invalid query: ' . mysqli_error($conn));
	}
    
	mysqli_close($conn);
 ?>